package com.apptmyz.splwms;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.LoadUnload;
import com.apptmyz.splwms.data.VehicleNumbersResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;

import java.util.ArrayList;

public class UnloadingScreen extends BaseActivity {

    boolean isQc;
    private Button okBtn;
    private EditText vehNoEt;
    private Context context;
    private DataSource dataSource;
    private ArrayList<String> vehicleNumbers = new ArrayList<>();
    private String warehouseCode;

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_ok:
                    vehicleNum = vehicleNumSp.getText().toString();
                    if (isQc) {

                    } else {
                        if (Utils.isValidString(vehicleNum) && Utils.isValidVehNo(vehicleNum)) {
                            Globals.vehicleNo = vehicleNum;
                            Intent intent = new Intent(context, PartsScanningScreen.class);
                            intent.putExtra(Constants.CLASS_NAME, getClass().getName());
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        } else
                            Utils.showToast(context, getString(R.string.invalidVehNo));
                    }
                    break;

                case R.id.home_iv:
                    goBack();
                    break;

            }
        }

    };
    private ImageView home_iv;
    private AutoCompleteTextView vehicleNumSp;
    private String vehicleNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.unloading_screen, frameLayout);

        context = UnloadingScreen.this;
        dataSource = new DataSource(context);
        titleTv.setText(getString(R.string.unloading_sheet));
        vehicleNumSp = (AutoCompleteTextView) findViewById(R.id.sp_vehicle_no);
        String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);

        vehicleNumSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                vehicleNum = vehicleNumSp.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        vehicleNumSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    vehicleNumSp.showDropDown();
                }
            }
        });

        vehicleNumSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                vehicleNumSp.showDropDown();
                return false;
            }
        });

        okBtn = (Button) findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(onClickListener);

        vehNoEt = (EditText) findViewById(R.id.et_vehicle_no);
        Intent intent = getIntent();
        isQc = intent.getBooleanExtra("QC", false);

        new VehicleNumberTask().execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        vehicleNumSp.setOnFocusChangeListener(null);
        vehicleNumSp.setOnTouchListener(null);
    }

    ProgressDialog vehicleNumsDialog;

    class VehicleNumberTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                vehicleNumsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getVehicleNumbersUrl(warehouseCode);

                Utils.logD("Log 1");
                VehicleNumbersResponse response = (VehicleNumbersResponse) HttpRequest
                        .getInputStreamFromUrl(url, VehicleNumbersResponse.class,
                                context);

                if (response != null) {
                    dataSource.vehicleNumbers.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<String> data = (ArrayList<String>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                            dataSource.vehicleNumbers.saveVehicleNumbers(data, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && vehicleNumsDialog != null && vehicleNumsDialog.isShowing()) {
                vehicleNumsDialog.dismiss();
            }
            if (showErrorDialog()) {
                setData();
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(UnloadingScreen.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void setData() {
        vehicleNumbers = dataSource.vehicleNumbers.getVehicleNumbers(warehouseCode);
        ArrayAdapter<String> vehicleNumAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, vehicleNumbers);
        vehicleNumSp.setAdapter(vehicleNumAdapter);

        Globals.selectedSheet = new LoadUnload();


        Globals.selectedSheet.setSheetNo("WLABOHX0000037");
        Globals.selectedSheet.setCarrier("VEHICLE");
        Globals.selectedSheet.setTripNo("");
        Globals.selectedSheet.setTxnDate("21-Oct-2020 00:00:00");
        Globals.selectedSheet.setSheetType("QC");
        Globals.selectedSheet.setSheetDate("21-Oct-2020 00:00:00");
        Globals.selectedSheet.setBranchCode("BLRH");
        Globals.selectedSheet.setBranchType("HUB");
        Globals.selectedSheet.setPartiallyScanned(0);
        Globals.selectedSheet.setConCount(2);
        Globals.selectedSheet.setPcsCount(3);
        Globals.selectedSheet.setToBranchCode("");
        Globals.selectedSheet.setLtutFlag("LT");
        Globals.selectedSheet.setLockMacId(null);
        Globals.selectedSheet.setIotNo(null);
        Globals.selectedSheet.setIsValidIOT(null);
        Globals.selectedSheet.setLoadPercent(0);

        Utils.logD(Globals.selectedSheet.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPostResume() {
        vehNoEt.setText("");
        super.onPostResume();
    }
}
