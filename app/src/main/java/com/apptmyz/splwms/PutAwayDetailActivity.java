package com.apptmyz.splwms;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.BinMasterModel;
import com.apptmyz.splwms.data.Cons;
import com.apptmyz.splwms.data.CreateGrnInput;
import com.apptmyz.splwms.data.CreateGrnPartJson;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.PutAwayData;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PutAwayDetailActivity extends BaseActivity {
    public static int BOXES_MAX_COUNT = 0;

    TextView titleTv, productNumTv, qtyTv, inventoryTypeTv;
    CounterView counterView;
    TextInputEditText binNumberEt;
    Button addBinButton, confirmBtn;
    ImageView scanBinIv;
    Context context;
    RecyclerView binsRv;
    LinearLayoutManager layoutManager;
    Gson gson = new Gson();
    PutAwayData bin = null;
    BinMasterModel binMasterModel = null;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan, quantity;
    private TimerTask timerScanTask;
    private DataSource dataSource;
    private String vehicleNum, productNumber, warehouseCode;
    private int partId, id;
    private boolean isBinInvalid = true;
    PutAwayData scannedBin = null;
    private List<Integer> inventoryTypeIds;
    private HashMap<String, Integer> partQtyMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_put_away_detail, frameLayout);

        context = PutAwayDetailActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Put Away");

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);
        binsRv.setNestedScrollingEnabled(false);
        binAdapter = new BinAdapter(bins);

        productNumTv = (TextView) findViewById(R.id.tv_product_number);
        qtyTv = (TextView) findViewById(R.id.tv_qty);
        inventoryTypeTv = (TextView) findViewById(R.id.tv_inventory_type);

        counterView = (CounterView) findViewById(R.id.counterView);
        binNumberEt = (TextInputEditText) findViewById(R.id.input_bin_number);
        binNumberEt.addTextChangedListener(productNumWatcher);
        scanBinIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanBinIv.setOnClickListener(listener);

        addBinButton = (Button) findViewById(R.id.btn_add_bin);
        confirmBtn = (Button) findViewById(R.id.btn_confirm);
        addBinButton.setOnClickListener(listener);
        confirmBtn.setOnClickListener(listener);

        if (getIntent().getExtras() != null) {
            vehicleNum = getIntent().getStringExtra("vehicleNum");
            String binStr = getIntent().getStringExtra("bin");
            binMasterModel = gson.fromJson(binStr, BinMasterModel.class);
            binNumber = binMasterModel.getWhBin();
            productNumTv.setText(binMasterModel.getWhBin());
            inventoryTypeIds = binMasterModel.getInventoryTypeIds();
        }
    }

    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumberEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (scannedValue.length() >= 2) {
                                        productNumber = scannedValue;
                                        int matchingCount = dataSource.putAway.matchesData(scannedValue, warehouseCode);
                                        if (matchingCount == 0) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Part doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        } else {
                                            scannedBin = dataSource.putAway.getPutAwayPart(scannedValue, warehouseCode);
                                            if (scannedBin != null) {
                                                if (inventoryTypeIds.contains(scannedBin.getInventoryTypeId())) {
                                                    int remainingQty = scannedBin.getNoOfBoxesToScan();
                                                    if (partQtyMap.containsKey(scannedBin.getPartNo())) {
                                                        remainingQty = scannedBin.getNoOfBoxesToScan() - partQtyMap.get(scannedBin.getPartNo());
                                                    }
                                                    if (remainingQty > 0) {
                                                        id = scannedBin.getId();
                                                        partId = scannedBin.getPartId();
                                                        quantity = scannedBin.getNoOfBoxesToScan();
                                                        counterView.setValue(remainingQty);
                                                        counterView.setmMaxLimit(remainingQty);
                                                        BOXES_MAX_COUNT = remainingQty;
                                                        qtyTv.setText(String.valueOf(remainingQty));
                                                    } else {
                                                        Utils.showSimpleAlert(context, getString(R.string.alert), "You are trying to add more boxes than the total qty of the part");
                                                        timeScan = 0;
                                                        if (timerScan != null)
                                                            timerScan.cancel();
                                                        timerScan = null;
                                                        return;
                                                    }
                                                } else {
                                                    Utils.showSimpleAlert(context, getString(R.string.alert), "The BIN cannot take a Part/Product of Inventory Type: " + scannedBin.getInventoryTypeDesc());
                                                    timeScan = 0;
                                                    if (timerScan != null)
                                                        timerScan.cancel();
                                                    timerScan = null;
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }
                            }
                        });
                    }
                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }
        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()) {
                case R.id.iv_scan_bin:
                    binNumberEt.setText("");
                    binNumberEt.requestFocus();
                    break;
                case R.id.btn_add_bin:
                    //validate, add a new entry to the recyclerview and clear existing fields to enter new ones.
                    String msg = validateBinDetails();
                    if (!Utils.isValidString(msg)) {
                        if (partQtyMap.containsKey(bin.getPartNo())) {
                            int qty = partQtyMap.get(bin.getPartNo()) + bin.getQty();
                            partQtyMap.put(bin.getPartNo(), qty);
                        } else {
                            partQtyMap.put(bin.getPartNo(), bin.getQty());
                        }
                        bins.add(bin);
                        bin = null;
                        binAdapter = new BinAdapter(bins);
                        binsRv.setAdapter(binAdapter);
                        binAdapter.notifyDataSetChanged();
                        binNumberEt.setText("");
                        qtyTv.setText("0");
                        counterView.setValue(0);
                        counterView.setmMaxLimit(Integer.MAX_VALUE);
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), msg);
                    }
                    break;
                case R.id.btn_confirm:
                    if (!Utils.isValidArrayList(bins)) {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Scan the BINs for submission");
                        return;
                    }
                    reason = "";
                    submit();
                    break;
                default:
                    break;
            }
            if (intent != null)
                startActivity(intent);
        }
    };

    private CreateGrnInput generateSubmitJson() {
        CreateGrnInput input = new CreateGrnInput();
        input.setVehicleNo(vehicleNum);
        input.setReason(reason);
        List<CreateGrnPartJson> partList = new ArrayList<>();
        for (PutAwayData data : bins) {
            CreateGrnPartJson model = new CreateGrnPartJson(data.getId(), ((data.getIsProduct() != null && data.getIsProduct() == 1) ? null : data.getPartNo()), data.getPartId(), data.getQty(),
                    data.getBinNumber(), data.getScanTime(), data.getInventoryTypeDesc(), data.getIsProduct(), ((data.getIsProduct() != null && data.getIsProduct() == 1) ? data.getProductId() : null),
                    ((data.getIsProduct() != null && data.getIsProduct() == 1) ? data.getProductName() : null));
            partList.add(model);
        }
        input.setPartList(partList);

        return input;
    }

    private String reason;

    private void showReasonPopUp() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        final Spinner spinner = new Spinner(context);
        alert.setMessage("You have not scanned all of the boxes. Select a Reason: ");
        alert.setTitle("Reason for Shortage of Quantity");
        final String[] s = getResources().getStringArray(R.array.reasons);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(PutAwayDetailActivity.this,
                android.R.layout.simple_dropdown_item_1line, s);
        LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        spinner.setPadding(5, 5, 5, 5);
        spinner.setLayoutParams(lp);
        spinner.setAdapter(adp);
        alert.setView(spinner);
        reason = s[0];

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reason = s[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                reason = s[0];
            }
        });

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                submit();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alert.show();
    }

    private void submit() {
        if (Utils.getConnectivityStatus(context)) {
            CreateGrnInput input = generateSubmitJson();
            new SubmitPutAway(input).execute();
        }
    }

    private String binNumber;
    private int addedBoxesCount;
    BinAdapter binAdapter;
    private ArrayList<PutAwayData> bins = new ArrayList<>();

    private String validateBinDetails() {
        String message = "";
        productNumber = binNumberEt.getText().toString().trim();
        if (Utils.isValidString(productNumber)) {
            if (scannedBin != null) {
                int boxesCount = counterView.getValue();
                if (boxesCount != 0) {
                    if (boxesCount > BOXES_MAX_COUNT) {
                        message = "You are trying to add more boxes than required";
                    } else {
                        addedBoxesCount = boxesCount;
                        bin = new PutAwayData();
                        bin.setId(scannedBin.getId());
                        bin.setIsProduct(scannedBin.getIsProduct());
                        bin.setBinNumber(binNumber);
                        bin.setQty(boxesCount);
                        if (scannedBin.getIsProduct() == 1)
                            bin.setProductId(partId);
                        else
                            bin.setPartId(partId);
                        bin.setInventoryTypeDesc(scannedBin.getInventoryTypeDesc());
                        if (scannedBin.getIsProduct() == 1)
                            bin.setProductName(binNumberEt.getText().toString().trim());
                        else
                            bin.setPartNo(binNumberEt.getText().toString().trim());
                        bin.setScanTime(Utils.getScanTime());
                    }
                } else {
                    message = "Please Scan Atleast one Box";
                }
            } else {
                message = "Please Scan the Part";
            }
        } else {
            message = "Please Scan the Part";
        }
        return message;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, PutAwayActivity.class));
    }

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<PutAwayData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv, inventoryTypeTv;
            ImageView deleteIv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes_count);
                inventoryTypeTv = (TextView) itemView.findViewById(R.id.tv_inventory_type);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.iv_delete) {
                    int position = (int) view.getTag();
                    PutAwayData deleteBin = bins.get(position);
                    int value = counterView.getValue();
                    counterView.setValue(0);
                    addedBoxesCount = 0;
                    bins.remove(position);
                    notifyDataSetChanged();
                }
            }
        }

        public BinAdapter(ArrayList<PutAwayData> myDataset) {
            list = myDataset;
        }

        @Override
        public BinAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_putaway_bin_item, parent, false);
            return new BinAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BinAdapter.DataObjectHolder holder, final int position) {
            PutAwayData model = list.get(position);
            if (model != null) {
                holder.binNumTv.setText(model.getBinNumber());
                holder.productNumTv.setText(model.getIsProduct() == 1? model.getProductName() : model.getPartNo());
                holder.noOfBoxesTv.setText(String.valueOf(model.getQty()));
                holder.inventoryTypeTv.setText(model.getInventoryTypeDesc());

                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.deleteIv.setTag(position);
                holder.inventoryTypeTv.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<PutAwayData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PutAwayData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    ProgressDialog submitDialog;

    class SubmitPutAway extends AsyncTask<String, String, Object> {
        CreateGrnInput input;
        GeneralResponse response;

        SubmitPutAway(CreateGrnInput input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSubmitPutAwayUrl(warehouseCode);
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, CreateGrnInput.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }
            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        if (BOXES_MAX_COUNT - addedBoxesCount == 0)
                            dataSource.putAway.updateAsSubmitted(id, warehouseCode);
                        else
                            dataSource.putAway.updateAsPartialScan(id, BOXES_MAX_COUNT - addedBoxesCount, warehouseCode);
                    }
                    Utils.showToast(context, response.getMessage());
                }
                finish();
            }
            super.onPostExecute(result);
        }

    }

    private void openHomeScreen() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PutAwayDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }
}
