package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.apptmyz.splwms.PartsScanningScreen;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.data.UnloadingGoodPartData;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ScanInProgress extends Fragment {
    public ArrayList<String> recentScannedList = new ArrayList<String>();
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private ListView recentScannedListView;
    private TextView labelTv;
    private Context context;
    private PartsScanningScreen scanningScreen;
    private ListView toBeScannedLv;
    private MyAdapter adapter;
    private DataSource dataSource;

    private Button scanBtn;
    private boolean isRBChanged = false;
    public List<UnloadingGoodPartData> list = new ArrayList<UnloadingGoodPartData>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
        this.scanningScreen = (PartsScanningScreen) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.wms_scan_in_progress, container,
                false);

        dataSource = new DataSource(context);

        toBeScannedLv = (ListView) parentView
                .findViewById(R.id.lv_scan_in_progress);

        labelTv = (TextView) parentView.findViewById(R.id.tv_label);
        labelTv.setText(R.string.scanned);

        adapter = new MyAdapter(context, R.layout.layout_scan_in_progress_item, list);
        toBeScannedLv.setAdapter(adapter);

        return parentView;
    }

    public class MyAdapter extends ArrayAdapter<UnloadingGoodPartData> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<UnloadingGoodPartData> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.partNoTv = (TextView) convertView.findViewById(R.id.tv_con_number);
            holder.boxCountTv = (TextView) convertView.findViewById(R.id.tv_badge);
            holder.inventoryTypeTv = (TextView) convertView.findViewById(R.id.tv_inventory_type);
            holder.grpIv = (ImageView) convertView.findViewById(R.id.iv_group_indicator);

            holder.grpIv.setVisibility(View.GONE);

            UnloadingGoodPartData goodPartData = getItem(position);

            String dest = "";
            if (goodPartData.getIsProduct() == 0)
                dest = goodPartData.getNumber();
            else
                dest = goodPartData.getProductName();

            if (Utils.isValidString(dest))
                holder.partNoTv.setText(dest);
            else
                holder.partNoTv.setText("");

            holder.partNoTv.setTextColor(getResources().getColor(R.color.white));


            String totQty = String.valueOf(goodPartData.getTotalBoxes());
            String scannedQty = String.valueOf(goodPartData.getScannedCount());
            holder.boxCountTv.setText(scannedQty + "/" + totQty);
            holder.inventoryTypeTv.setText(goodPartData.getInventoryTypeDesc());
            return convertView;
        }

    }

    public class ViewHolder {
        public TextView partNoTv, boxCountTv, inventoryTypeTv;
        public ImageView grpIv;


    }


    private void clearList() {
        if (list != null)
            list.clear();
        else
            list = new ArrayList<UnloadingGoodPartData>();
        refresh();
    }

    public void reloadData(List<UnloadingGoodPartData> data) {
        clearList();
        for (UnloadingGoodPartData d : data)
            list.add(d);
        Utils.logD(list.toString());
        refresh();
    }

    private void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }
}
