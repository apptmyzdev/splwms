package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.apptmyz.splwms.LoadingScanningScreen;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewLoadingPartsModel;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class LoadingScanInProgress extends Fragment {
    public ArrayList<String> recentScannedList = new ArrayList<String>();
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private ListView recentScannedListView;
    private TextView labelTv;
    private Context context;
    private LoadingScanningScreen scanningScreen;
    private ListView toBeScannedLv;
    private DataSource dataSource;
    private Button scanBtn;
    private boolean isRBChanged = false;
    public List<NewLoadingPartsModel> list = new ArrayList<NewLoadingPartsModel>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
        this.scanningScreen = (LoadingScanningScreen) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        parentView = inflater.inflate(R.layout.wms_scan_in_progress, container,
                false);

        dataSource = new DataSource(context);
        return parentView;
    }

}
