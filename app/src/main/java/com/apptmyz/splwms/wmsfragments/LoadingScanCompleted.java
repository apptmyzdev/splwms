package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.ScanUtils;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class LoadingScanCompleted extends Fragment {
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private Context context;
    private ExpandableListView completedExpLv;
    private DataSource dataSource;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater
                .inflate(R.layout.scan_completed, container, false);

        dataSource = new DataSource(context);
        return parentView;
    }
}
