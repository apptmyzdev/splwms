package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.apptmyz.splwms.PartsScanningScreen;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ToBeScanned extends Fragment { // implements  AdapterView.OnItemLongClickListener {
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private MyAdapter adapter;
    private Context mContext;
    private PartsScanningScreen scanningScreen;
    private ListView toBeScannedLv;
    private TextView labelTv, packetsTotalTv;
    private DataSource dataSource;
    private String warehouseCode;
    public List<NewProductModel> list = new ArrayList<NewProductModel>();

    ListView.OnItemClickListener onItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            NewProductModel productModel = (NewProductModel) adapterView.getAdapter().getItem(i);
            scanningScreen.showExceptionPopup(productModel);
            Globals.partNo = productModel.getPartNo();
        }
    };

    ListView.OnItemLongClickListener onItemLongClickListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            NewProductModel productModel = (NewProductModel) adapterView.getAdapter().getItem(i);
            Utils.logD(productModel.getIsScanned());
            return false;
        }
    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mContext = activity.getApplicationContext();
        this.scanningScreen = (PartsScanningScreen) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.wms_to_be_scanned, container, false);

        dataSource = new DataSource(mContext);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        toBeScannedLv = (ListView) parentView
                .findViewById(R.id.lv_to_be_scanned);
        toBeScannedLv.setOnItemClickListener(onItemClickListener);
        toBeScannedLv.setOnItemLongClickListener(onItemLongClickListener);
        labelTv = (TextView) parentView.findViewById(R.id.tv_label);
        labelTv.setText(getString(R.string.to_be_scanned));

        packetsTotalTv = (TextView) parentView
                .findViewById(R.id.tv_packets_total);

        adapter = new MyAdapter(mContext, R.layout.con_view, list);
        toBeScannedLv.setAdapter(adapter);

        return parentView;
    }

    public class MyAdapter extends ArrayAdapter<NewProductModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<NewProductModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.partNoTv = (TextView) convertView.findViewById(R.id.tv_con_number);
            holder.boxCountTv = (TextView) convertView.findViewById(R.id.tv_badge);
            holder.grpIv = (ImageView) convertView.findViewById(R.id.iv_group_indicator);
            holder.grpIv.setVisibility(View.GONE);

            NewProductModel newProductModel = getItem(position);


            String dest = newProductModel.getIsProduct() == 1 ? newProductModel.getProductName() : newProductModel.getPartNo();

            if (Utils.isValidString(dest))
                holder.partNoTv.setText(dest);
            else
                holder.partNoTv.setText("");

            holder.partNoTv.setTextColor(getResources().getColor(R.color.white));

            String totQty = String.valueOf(newProductModel.getTotalBoxes());
            String scannedQty = String.valueOf(newProductModel.getScannedCount());

            int count = dataSource.exceptions.getExceptionCount(newProductModel.getIsProduct(), newProductModel.getProductId(), newProductModel.getPartId(), newProductModel.getDataType(), newProductModel.getVehicleNo(), warehouseCode);
            int extrasCount = dataSource.exceptions.getExtrasCount(newProductModel.getIsProduct(), newProductModel.getProductId(), newProductModel.getPartId(), newProductModel.getDataType(), newProductModel.getVehicleNo(), warehouseCode);
            int scannedCount = newProductModel.getScannedCount();
            holder.boxCountTv.setText((scannedCount + count + extrasCount) + "/" + totQty);

            return convertView;
        }

    }

    public class ViewHolder {
        public TextView partNoTv, boxCountTv;
        public ImageView grpIv;
    }

    private void clearList() {
        if (list != null)
            list.clear();
        else
            list = new ArrayList<NewProductModel>();

        refresh();
    }

    public void reloadData(List<NewProductModel> data) {
        clearList();

        for (NewProductModel d : data)
            list.add(d);

        Utils.logD(list.toString());

        refresh();
    }

    private void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

}
