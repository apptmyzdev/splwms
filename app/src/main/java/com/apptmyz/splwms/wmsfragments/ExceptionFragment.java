package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.data.ExceptionModel;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ExceptionFragment extends Fragment {
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private ListView exceptionLv;
    private Context context;
    private TextView labelTv;
    private DataSource dataSource;
    private MyAdapter adapter;
    private String warehouseCode;
    public List<ExceptionModel> list = new ArrayList<ExceptionModel>();

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.wms_to_be_scanned, container, false);

        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);

        exceptionLv = (ListView) parentView
                .findViewById(R.id.lv_to_be_scanned);
        labelTv = (TextView) parentView.findViewById(R.id.tv_label);
        labelTv.setText(getString(R.string.exception));

        adapter = new MyAdapter(context, R.layout.exception_view, list);
        exceptionLv.setAdapter(adapter);
        return parentView;
    }

    private int getNoOfScannedPkts(int groupPosition) {
        int noOfScannedPackets = 0;
        for (NewPacket scanPacket : childData.get(groupPosition)) {
            if (scanPacket.isScanned() && scanPacket.isExcep())
                noOfScannedPackets++;
        }

        return noOfScannedPackets;
    }

    private boolean shortsRecNotFitting(NewCon con) {
        boolean isRec = false;
        String conNumber = con.getConNo();
        NewCon newCon = con;  //dataSource.consData.getCompleteConData(Globals.selectedSheet.getSheetNo(), conNumber);
        for (NewPacket p : newCon.getPackets()) {
            if (!p.isScanned()) {
                if (Utils.isValidString(p.getExcepType())
                        && p.getExcepType().equalsIgnoreCase(
                        Constants.SHORT_CODE)
                        && Utils.isValidString(p.getExcpDtls()) && p.getExcpDtls().equalsIgnoreCase(Constants.NOT_FITTING_IN_VEHICLE)) {
                    isRec = true;
                } else {
                    isRec = false;
                    return isRec;
                }
            }
        }
        return isRec;
    }

    public class MyAdapter extends ArrayAdapter<ExceptionModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<ExceptionModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.partNoTv = (TextView) convertView.findViewById(R.id.tv_con_number);
            holder.boxCountTv = (TextView) convertView.findViewById(R.id.tv_badge);
            holder.reasonTv = (TextView) convertView.findViewById(R.id.tv_reason);
            holder.exceptionTypeTv = (TextView) convertView.findViewById(R.id.tv_exception_type);

            ExceptionModel newProductModel = getItem(position);

            String dest = newProductModel.getIsProduct() == 1 ? newProductModel.getProductName() : newProductModel.getPartNo();
            if (Utils.isValidString(dest))
                holder.partNoTv.setText(dest);
            else
                holder.partNoTv.setText("");

            holder.partNoTv.setTextColor(getResources().getColor(R.color.white));
            holder.reasonTv.setText(newProductModel.getExcepDesc());
            SkipBinReasonModel exceptionType = dataSource.exceptionTypes.getException(newProductModel.getExcepType(), warehouseCode);
            if (exceptionType != null)
                holder.exceptionTypeTv.setText(exceptionType.getReason());
            else
                holder.exceptionTypeTv.setText("Shortage");

            holder.boxCountTv.setText(String.valueOf(newProductModel.getExceptionCount()));

            return convertView;
        }

    }

    public class ViewHolder {
        public TextView partNoTv, boxCountTv, reasonTv, exceptionTypeTv;
        public ImageView grpIv;
    }

    private void clearList() {
        if (list != null)
            list.clear();
        else
            list = new ArrayList<>();

        refresh();
    }

    public void reloadData(List<ExceptionModel> data) {
        clearList();

        for (ExceptionModel d : data)
            list.add(d);

        Utils.logD(list.toString());

        refresh();
    }

    private void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

}