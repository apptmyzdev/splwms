package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.apptmyz.splwms.LoadingScanningScreen;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewLoadingPartsModel;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class LoadingToBeScanned extends Fragment { // implements  AdapterView.OnItemLongClickListener {
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    public List<NewLoadingPartsModel> list = new ArrayList<NewLoadingPartsModel>();
    private View parentView;
    private Context mContext;
    private LoadingScanningScreen scanningScreen;
    private ListView toBeScannedLv;
    private TextView labelTv, packetsTotalTv;
    private DataSource dataSource;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mContext = activity.getApplicationContext();
        this.scanningScreen = (LoadingScanningScreen) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.wms_to_be_scanned, container, false);

        dataSource = new DataSource(mContext);

        return parentView;
    }
}
