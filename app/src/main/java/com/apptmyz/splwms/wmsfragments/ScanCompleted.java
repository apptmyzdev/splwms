package com.apptmyz.splwms.wmsfragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.NewPacket;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.ScanUtils;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class ScanCompleted extends Fragment {
    public List<NewCon> groupData = new ArrayList<NewCon>();
    public ArrayList<ArrayList<NewPacket>> childData = new ArrayList<ArrayList<NewPacket>>();
    private View parentView;
    private ExpandableListAdapter adapter;
    private Context context;
    private ExpandableListView completedExpLv;
    private DataSource dataSource;
    private View.OnClickListener groupClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            int groupPosition = (Integer) v.getTag();
            if (completedExpLv.isGroupExpanded(groupPosition))
                completedExpLv.collapseGroup(groupPosition);
            else
                completedExpLv.expandGroup(groupPosition);

        }
    };
    private OnChildClickListener onChildClick = new OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                                    int groupPosition, int childPosition, long id) {

            String selectedConNo = groupData.get(groupPosition).getConNo();
            int selectedPacketNo = childData.get(groupPosition)
                    .get(childPosition).getPktNo();
            return false;
        }

    };

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.context = activity.getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater
                .inflate(R.layout.scan_completed, container, false);

        dataSource = new DataSource(context);

        completedExpLv = (ExpandableListView) parentView
                .findViewById(R.id.elv_scan_completed);

        completedExpLv.setOnChildClickListener(onChildClick);

        adapter = new ExpandableListAdapter(context);
        completedExpLv.setAdapter(adapter);

        refresh();

        return parentView;
    }

    private int getNoOfScannedPkts(int groupPosition) {

        int noOfScannedPackets = 0;

        for (NewPacket scanPacket : childData.get(groupPosition)) {
            if (scanPacket.isScanned())
                noOfScannedPackets++;
        }

        return noOfScannedPackets;

    }

    private void clearList() {

        if (groupData != null)
            groupData.clear();
        else
            groupData = new ArrayList<NewCon>();

        if (childData != null)
            childData.clear();
        else
            childData = new ArrayList<ArrayList<NewPacket>>();
    }

    public void resetData(ArrayList<NewCon> cons) {

        clearList();

        if (Utils.isValidArrayList(cons)) {
            for (NewCon con : cons) {
                groupData.add(con);
                ArrayList<NewPacket> packets = new ArrayList<NewPacket>();
                packets.addAll(con.getPackets());
                childData.add(packets);
            }
        }

        refresh();
    }

    public void refresh() {
        if (adapter != null)
            adapter.notifyDataSetChanged();

        if (Constants.isAlwaysExpandable) {
            for (int groupPosition = 0; groupPosition < groupData.size(); groupPosition++) {
                try {
                    completedExpLv.collapseGroup(groupPosition);
                    completedExpLv.expandGroup(groupPosition);
                } catch (Exception e) {
                }
            }
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {
        private LayoutInflater inflater;

        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.packet_view, parent, false);

            NewPacket scanPacket = (NewPacket) getChild(groupPosition, childPosition);
            TextView itemTv = (TextView) v.findViewById(R.id.tv_pkt_no);

            if (itemTv != null) {
                itemTv.setText(String.valueOf(scanPacket.getPktNo()));

                if (scanPacket.isScanned() && !scanPacket.isExcep())
                    itemTv.setBackgroundColor(Color.WHITE);
                else
                    itemTv.setBackgroundColor(Color.parseColor("#aaFF3300"));

            }
            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {

            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.con_view, parent, false);

            v.setTag(groupPosition);

            NewCon docket = (NewCon) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (completedExpLv.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);

            TextView conTv = (TextView) v.findViewById(R.id.tv_con_number);
            TextView badgeTv = (TextView) v.findViewById(R.id.tv_badge);

            ImageView flightIv = (ImageView) v.findViewById(R.id.iv_flight);
            if (Utils.isValidString(docket.getProduct())
                    && docket.getProduct().equalsIgnoreCase("02"))
                flightIv.setVisibility(View.VISIBLE);
            else
                flightIv.setVisibility(View.GONE);

            int noOfScannedPackets = (int) docket.getScannedPktCount(); // getNoOfScannedPkts(groupPosition);
            int noOfPkgs = docket.getNumPkgsSheet();

            String conNumber = docket.getConNo();

            String dlyStn = docket.getDlyStn();

            if (Utils.isValidString(dlyStn)
                    && !dlyStn.trim().equalsIgnoreCase("-"))
                conNumber = conNumber + " - " + dlyStn.toUpperCase();

            conTv.setText(conNumber);

            badgeTv.setText(ScanUtils.getGroupCounterText(noOfScannedPackets,
                    noOfPkgs));

            v.setOnClickListener(groupClickListener);

            String status = docket.getConStatus();
            if (Utils.isValidString(status) && status.equalsIgnoreCase("U"))
                v.setBackgroundResource(R.drawable.new_green_docket_bg);
            else
                v.setBackgroundResource(ScanUtils.conBackgroundRes(docket));

            return v;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }

}
