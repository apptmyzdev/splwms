package com.apptmyz.splwms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.splwms.custom.ADRadioGroup;
import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.BinMasterModel;
import com.apptmyz.splwms.data.CreateGrnInput;
import com.apptmyz.splwms.data.CreateGrnPartJson;
import com.apptmyz.splwms.data.CustomerMasterModel;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InboundTypeModel;
import com.apptmyz.splwms.data.PutAwayData;
import com.apptmyz.splwms.data.PutAwayResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.Constants;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class PutAwayActivity extends BaseActivity {
    private static final int BOXES_MAX_COUNT = 10;
    Context context;
    TextView titleTv;
    Button addPutAwayDataBtn, closeBtn, createGrnBtn;
    TextInputEditText binNumberEt, productNumberEt, vehicleNumEt;
    ImageView scanPutAwayDataIv, scanProductIv, vehicleNumProceedIv, refreshIv;
    CounterView counterView;
    private Spinner inboundTypeSp;
    CompletedPutAwayDataAdapter adapter;
    LinearLayout addPutAwayDataLayout, pendingLayout, completedLayout;
    SparseBooleanArray mChecked = new SparseBooleanArray();
    CheckBox selectAllCb;
    RecyclerView binsRv, completedPutAwayDatasRv;
    BinAdapter binAdapter;
    private ArrayList<PutAwayData> bins = new ArrayList<>();
    LinearLayoutManager layoutManager, completedLayoutManager;
    int addedBoxesCount;
    PutAwayData bin = null;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private TextView noProductsTv;
    private String binNumber, productNumber;
    Gson gson = new Gson();
    ADRadioGroup radioGroup;
    private String vehicleNum, warehouseCode;
    int count;
    private List<PutAwayData> selectedList = new ArrayList<>();
    private ArrayList<PutAwayData> binListUi = new ArrayList<>();
    private DataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_put_away, frameLayout);

        context = PutAwayActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        inboundTypeSp = (Spinner) findViewById(R.id.inboundTypeSp);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Put Away");
        radioGroup = (ADRadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(checkListener);
        noProductsTv = (TextView) findViewById(R.id.tv_no_products);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);

        ArrayList<InboundTypeModel> inboundTypesData = dataSource.inboundTypes.getInboundTypesData();
        ArrayAdapter<InboundTypeModel> inboundTypeModelArrayAdapter = new ArrayAdapter<InboundTypeModel>(PutAwayActivity.this,
                android.R.layout.simple_dropdown_item_1line, inboundTypesData);
        inboundTypeSp.setAdapter(inboundTypeModelArrayAdapter);

        addPutAwayDataBtn = (Button) findViewById(R.id.btn_add_bin);
        closeBtn = (Button) findViewById(R.id.btn_close);
        createGrnBtn = (Button) findViewById(R.id.btn_create_grn);
        createGrnBtn.setOnClickListener(listener);

        selectAllCb = (CheckBox) findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(clickListener);

        pendingLayout = (LinearLayout) findViewById(R.id.ll_pending);
        completedLayout = (LinearLayout) findViewById(R.id.ll_completed);

        addPutAwayDataBtn.setOnClickListener(listener);
        closeBtn.setOnClickListener(listener);

        vehicleNumProceedIv = (ImageView) findViewById(R.id.iv_proceed);
        vehicleNumProceedIv.setOnClickListener(listener);

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        binAdapter = new BinAdapter((ArrayList<PutAwayData>) bins);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);
        binsRv.setAdapter(binAdapter);

        completedPutAwayDatasRv = (RecyclerView) findViewById(R.id.rv_completed_bins);
        adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
        completedLayoutManager = new LinearLayoutManager(this);
        completedPutAwayDatasRv.setLayoutManager(completedLayoutManager);
        completedPutAwayDatasRv.setAdapter(adapter);

        productNumberEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumberEt.addTextChangedListener(watcher);

        scanPutAwayDataIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);

        scanProductIv.setOnClickListener(listener);

        counterView = (CounterView) findViewById(R.id.counterView);

        addPutAwayDataLayout = (LinearLayout) findViewById(R.id.addbin_layout);
        setData();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckBox chk = (CheckBox) v;
            int itemCount = count;
            for (int i = 0; i < itemCount; i++) {
                mChecked.put(i, selectAllCb.isChecked());
            }
            adapter.notifyDataSetChanged();
        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_create_grn:
                    selectedList.clear();
                    selectedList = new ArrayList<>();
                    if (mChecked.size() > 0) {
                        for (int i = 0; i < count; i++) {
                            if (mChecked.get(i)) {
                                selectedList.add(binListUi.get(i));
                            }
                        }
                        if (Utils.isValidArrayList((ArrayList<?>) selectedList))
                            Utils.logD(selectedList.toString());

                        goToCreateGRN();
                    } else {
                        Utils.showToast(context, "Please select atleast one order");
                    }
                    break;
                case R.id.iv_refresh:
                    if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayDataTask().execute();
                        }
                    } else {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayCompletedDataTask().execute();
                        }
                    }
                    break;
                case R.id.iv_proceed:
                    if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
                        bins = dataSource.putAway.getPutAwayData(0, warehouseCode);
                        if (Utils.isValidArrayList(bins)) {
                            binAdapter = new BinAdapter(bins);
                            binsRv.setAdapter(binAdapter);
                            binAdapter.notifyDataSetChanged();
                            pendingLayout.setVisibility(View.VISIBLE);
                            noProductsTv.setVisibility(View.GONE);
                        } else {
                            if (Utils.getConnectivityStatus(context)) {
                                new PutAwayDataTask().execute();
                            }
                        }
                    } else {
                        binListUi = dataSource.putAway.getPutAwayData(1, warehouseCode);
                        if (Utils.isValidArrayList((ArrayList<?>) binListUi)) {
                            adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
                            completedPutAwayDatasRv.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            pendingLayout.setVisibility(View.GONE);
                            completedLayout.setVisibility(View.VISIBLE);
                            noProductsTv.setVisibility(View.GONE);
                        } else {
                            if (Utils.getConnectivityStatus(context)) {
                                new PutAwayCompletedDataTask().execute();
                            }
                        }
                    }
                    break;
                case R.id.btn_add_bin:
                    binNumber = "";
                    productNumber = "";
                    addPutAwayDataLayout.setVisibility(View.VISIBLE);
                    counterView.setValue(BOXES_MAX_COUNT - addedBoxesCount);
                    break;
                case R.id.btn_close:
                    if (!Utils.isValidArrayList(bins)) {
                        if (addedBoxesCount != BOXES_MAX_COUNT) {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Boxes Count did not match");
                        } else {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Done");
                        }
                    }
                    break;
                case R.id.iv_scan_bin:
                    binNumberEt.clearComposingText();
                    binNumberEt.requestFocus();
                    break;
                case R.id.iv_scan_product:
                    productNumberEt.clearComposingText();
                    productNumberEt.requestFocus();
                    break;
                default:
                    break;
            }
        }
    };

    private void goToCreateGRN() {
        if (Utils.isValidArrayList((ArrayList<?>) selectedList)) {
            if (Utils.getConnectivityStatus(context)) {
                CreateGrnInput input = new CreateGrnInput();
                input.setVehicleNo(vehicleNum);
                List<CreateGrnPartJson> partList = new ArrayList<>();
                for (PutAwayData data : selectedList) {
                    CreateGrnPartJson model = new CreateGrnPartJson(data.getId(), ((data.getIsProduct() != null && data.getIsProduct() == 1) ? null : data.getPartNo()), data.getPartId(), data.getQty(),
                            data.getBinNumber(), data.getScanTime(), data.getInventoryTypeDesc(), data.getIsProduct(), ((data.getIsProduct() != null && data.getIsProduct() == 1) ? data.getProductId() : null),
                            ((data.getIsProduct() != null && data.getIsProduct() == 1) ? data.getProductName() : null));
                    partList.add(model);
                }
                input.setPartList(partList);
                new CreateGrnTask(input).execute();
            }
        }
    }

    ProgressDialog createGrnDialog;

    class CreateGrnTask extends AsyncTask<String, String, Object> {
        CreateGrnInput input;
        GeneralResponse response;

        CreateGrnTask(CreateGrnInput input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                createGrnDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getCreateGrnUrl(warehouseCode);
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, CreateGrnInput.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        for (CreateGrnPartJson model : input.getPartList()) {
                            dataSource.putAway.delete(model.getId(), warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && createGrnDialog != null && createGrnDialog.isShowing()) {
                createGrnDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null)
                    Utils.showToast(context, response.getMessage());
                openHomeScreen();
            }
            super.onPostExecute(result);
        }

    }

    private void openHomeScreen() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
    }

    ProgressDialog putAwayDialog, putAwayCompletedDialog;

    class PutAwayDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                putAwayDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getPutAwayUrl(warehouseCode).replace(" ", "%20"); //vehicleNum

                Utils.logD("Log 1");
                PutAwayResponse response = (PutAwayResponse) HttpRequest
                        .getInputStreamFromUrl(url, PutAwayResponse.class,
                                context);

                if (response != null) {
                    dataSource.putAway.deletePendingData(warehouseCode);
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PutAwayData> data = (ArrayList<PutAwayData>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            bins = data;
                            dataSource.putAway.insertPutAwayData(bins, 0, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && putAwayDialog != null && putAwayDialog.isShowing()) {
                putAwayDialog.dismiss();
            }
            showErrorDialog();
            setDataAfterApiCall();
            super.onPostExecute(result);
        }

    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PutAwayActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void setData() {
        if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
            completedLayout.setVisibility(View.GONE);
            bins = dataSource.putAway.getPutAwayData(0, warehouseCode);
            binAdapter = new BinAdapter(bins);
            binsRv.setAdapter(binAdapter);
            binAdapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(bins)) {
                pendingLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                pendingLayout.setVisibility(View.GONE);
                noProductsTv.setVisibility(View.VISIBLE);
                if (Utils.getConnectivityStatus(context)) {
                    new PutAwayDataTask().execute();
                }
            }
        } else {
            pendingLayout.setVisibility(View.GONE);
            binListUi = dataSource.putAway.getPutAwayData(1, warehouseCode);
            adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
            completedPutAwayDatasRv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(binListUi)) {
                completedLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                completedLayout.setVisibility(View.GONE);
                noProductsTv.setVisibility(View.VISIBLE);
                if (Utils.getConnectivityStatus(context)) {
                    new PutAwayCompletedDataTask().execute();
                }
            }
        }
    }

    private void setDataAfterApiCall() {
        if (radioGroup.getCheckedRadioButtonId() == R.id.rb_pending) {
            completedLayout.setVisibility(View.GONE);
            bins = dataSource.putAway.getPutAwayData(0, warehouseCode);
            binAdapter = new BinAdapter(bins);
            binsRv.setAdapter(binAdapter);
            binAdapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(bins)) {
                pendingLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                pendingLayout.setVisibility(View.GONE);
                noProductsTv.setVisibility(View.VISIBLE);
            }
        } else {
            pendingLayout.setVisibility(View.GONE);
            binListUi = dataSource.putAway.getPutAwayData(1, warehouseCode);
            adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
            completedPutAwayDatasRv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            if (Utils.isValidArrayList(binListUi)) {
                completedLayout.setVisibility(View.VISIBLE);
                noProductsTv.setVisibility(View.GONE);
            } else {
                completedLayout.setVisibility(View.GONE);
                noProductsTv.setVisibility(View.VISIBLE);
            }
        }
    }


    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumberEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 2) {
                                        binNumber = scannedValue;
                                        int matchCount = dataSource.bins.matchesData(scannedValue, warehouseCode);
                                        if (matchCount == 0) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        } else {
                                            int existsId = dataSource.bins.exists(scannedValue, warehouseCode);
                                            if (existsId != -1) {
                                                Intent intent = new Intent(context, PutAwayDetailActivity.class);
                                                BinMasterModel model = dataSource.bins.getBin(scannedValue, warehouseCode);
                                                intent.putExtra("bin", gson.toJson(model));
                                                intent.putExtra("vehicleNum", vehicleNum);
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        productNumberEt.setText(null);
    }

    private RadioGroup.OnCheckedChangeListener checkListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            switch (checkedId) {
                case R.id.rb_pending:
                    completedLayout.setVisibility(View.GONE);
                    bins = dataSource.putAway.getPutAwayData(0, warehouseCode);
                    if (Utils.isValidArrayList(bins)) {
                        binAdapter = new BinAdapter(bins);
                        binsRv.setAdapter(binAdapter);
                        binAdapter.notifyDataSetChanged();
                        pendingLayout.setVisibility(View.VISIBLE);
                        noProductsTv.setVisibility(View.GONE);
                    } else {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayDataTask().execute();
                        }
                    }
                    break;
                case R.id.rb_completed:
                    pendingLayout.setVisibility(View.GONE);
                    binListUi = dataSource.putAway.getPutAwayData(1, warehouseCode);
                    if (Utils.isValidArrayList((ArrayList<?>) binListUi)) {
                        adapter = new CompletedPutAwayDataAdapter((ArrayList<PutAwayData>) binListUi);
                        completedPutAwayDatasRv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        completedLayout.setVisibility(View.VISIBLE);
                        noProductsTv.setVisibility(View.GONE);
                    } else {
                        if (Utils.getConnectivityStatus(context)) {
                            new PutAwayCompletedDataTask().execute();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    };

    class PutAwayCompletedDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                putAwayCompletedDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getPutAwayCompletedUrl(warehouseCode);

                Utils.logD("Log 1");
                PutAwayResponse response = (PutAwayResponse) HttpRequest
                        .getInputStreamFromUrl(url, PutAwayResponse.class,
                                context);

                if (response != null) {
                    dataSource.putAway.deleteCompletedData(warehouseCode);
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PutAwayData> data = (ArrayList<PutAwayData>) response.getData();
                        if (Utils.isValidArrayList(data)) {
                            binListUi = data;
                            dataSource.putAway.insertPutAwayData(binListUi, 1, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && putAwayCompletedDialog != null && putAwayCompletedDialog.isShowing()) {
                putAwayCompletedDialog.dismiss();
            }
            showErrorDialog();
            setDataAfterApiCall();
            super.onPostExecute(result);
        }

    }

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<PutAwayData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv, inventoryTypeTv;
            LinearLayout putAwayLayout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes_count);
                inventoryTypeTv = (TextView) itemView.findViewById(R.id.tv_inventory_type);

                putAwayLayout = (LinearLayout) itemView.findViewById(R.id.putaway_layout);
                putAwayLayout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                }
            }
        }

        public BinAdapter(ArrayList<PutAwayData> myDataset) {
            list = myDataset;
        }

        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_putaway_item, parent, false);
            return new DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final DataObjectHolder holder, final int position) {
            PutAwayData model = list.get(position);
            if (model != null) {
                if (model.getBinNumber() != null) {
                    holder.binNumTv.setText(model.getBinNumber());
                } else {
                    holder.binNumTv.setText("--");
                }

                holder.productNumTv.setText(model.getPartNo());

                int scannedBoxes = model.getQty() - model.getNoOfBoxesToScan();
                holder.noOfBoxesTv.setText(scannedBoxes + "/"
                        + model.getQty());
                holder.inventoryTypeTv.setText(model.getInventoryTypeDesc());

                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.inventoryTypeTv.setTag(position);
                holder.putAwayLayout.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<PutAwayData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PutAwayData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    class CompletedPutAwayDataAdapter extends RecyclerView.Adapter<CompletedPutAwayDataAdapter.DataObjectHolder> {
        private ArrayList<PutAwayData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv, inventoryTypeTv;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                inventoryTypeTv = (TextView) itemView.findViewById(R.id.tv_inventory_type);

                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (buttonView.getTag() != null) {
                            int pos = (int) buttonView.getTag();

                            if (isChecked) {
                                mChecked.put(pos, isChecked);
                                if (isAllValuesChecked()) {
                                    selectAllCb.setChecked(isChecked);
                                }
                            } else {
                                mChecked.delete(pos);
                                selectAllCb.setChecked(isChecked);

                            }
                        }
                    }
                });
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                }
            }
        }

        public CompletedPutAwayDataAdapter(ArrayList<PutAwayData> myDataset) {
            list = myDataset;
        }

        @Override
        public CompletedPutAwayDataAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_grn_item, parent, false);
            return new CompletedPutAwayDataAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final CompletedPutAwayDataAdapter.DataObjectHolder holder, final int position) {
            PutAwayData model = list.get(position);
            if (model != null) {
                holder.binNumTv.setText(model.getBinNumber());
                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText(String.valueOf(model.getQty()));
                holder.checkBox.setChecked((mChecked.get(position) == true ? true : false));
                holder.inventoryTypeTv.setText(model.getInventoryTypeDesc());

                holder.inventoryTypeTv.setTag(position);
                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.checkBox.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list.size();
        }

        public void update(ArrayList<PutAwayData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PutAwayData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }
        return true;
    }
}
