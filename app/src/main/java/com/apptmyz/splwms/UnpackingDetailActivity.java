package com.apptmyz.splwms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.custom.NonScrollableListView;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.UnpackingBox;
import com.apptmyz.splwms.data.UnpackingPart;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class UnpackingDetailActivity extends BaseActivity {

    private Gson gson = new Gson();
    private PartsAdapter adapter;
    private MyAdapter listAdapter;
    private Context context;
    private DataSource dataSource;
    private List<UnpackingPart> partList = new ArrayList<>();
    SparseBooleanArray mChecked = new SparseBooleanArray();
    private RecyclerView partsRv;
    private String vehicleNum, inwardNum, masterBoxNum;
    private TextView vehicleNumTv, inwardNumTv, masterBoxNumTv;
    private LinearLayoutManager layoutManager;
    EditText searchView;
    private int count;
    private Button submitBtn;
    private UnpackingBox unpackingBox;
    private TextView noDataTv;
    private CheckBox selectAllCb;
    private LinearLayout rootView;
    private NonScrollableListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_unpacking_detail, frameLayout);

        context = UnpackingDetailActivity.this;
        dataSource = new DataSource(context);

        submitBtn = (Button) findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(listener);
        searchView = (EditText) findViewById(R.id.searchView);
        partsRv = (RecyclerView) findViewById(R.id.rv_unpacking_data);
        partsRv.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this);
        partsRv.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(partsRv.getContext(),
                layoutManager.getOrientation());
        partsRv.addItemDecoration(dividerItemDecoration);
        adapter = new PartsAdapter((ArrayList<UnpackingPart>) partList);
        titleTv.setText("Unpacking Master Carton");
        vehicleNumTv = (TextView) findViewById(R.id.spSubproject);
        inwardNumTv = (TextView) findViewById(R.id.spInvoiceNumber);
        masterBoxNumTv = (TextView) findViewById(R.id.tvShipAddress);
        noDataTv = (TextView) findViewById(R.id.tv_no_data);
        selectAllCb = (CheckBox) findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(clickListener);
        listView = (NonScrollableListView) findViewById(R.id.listview);
        rootView = (LinearLayout) findViewById(R.id.rootView);
        rootView.clearFocus();

        listAdapter = new MyAdapter(context, R.layout.layout_unpacking_detail_item, partList);
        listView.setAdapter(listAdapter);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setFilteredData(editable.toString());
            }
        });

        if (getIntent().getExtras() != null) {
            String boxStr = getIntent().getStringExtra("data");
            unpackingBox = gson.fromJson(boxStr, UnpackingBox.class);

            vehicleNum = unpackingBox.getVehicleNumber();
            inwardNum = unpackingBox.getInwardNumber();
            masterBoxNum = unpackingBox.getMasterBoxNumber();
            vehicleNumTv.setText(vehicleNum);
            inwardNumTv.setText(inwardNum);
            masterBoxNumTv.setText(masterBoxNum);
        }

        setData();
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int itemCount = count;
            for (int i = 0; i < itemCount; i++) {
                mChecked.put(i, selectAllCb.isChecked());
            }
            listAdapter.notifyDataSetChanged();
        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_submit:
                    validateAndSubmit();
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndSubmit() {
        for (UnpackingPart part : partList) {
            if (part.getIsSelected() == 1 && part.getIsActedUpon() == 0) {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Some of the Parts are Selected but not Marked as either Good or Edited");
                break;
            }
        }
    }

    private void setFilteredData(String query) {
        ArrayList<UnpackingPart> tempList = new ArrayList<>();

        for (UnpackingPart model : partList) {
            if (model.getPartNo().toLowerCase().contains(query.toLowerCase())) {
                tempList.add(model);
            }
        }
        if (Utils.isValidArrayList(tempList)) {
            noDataTv.setVisibility(View.GONE);
            listAdapter = new MyAdapter(context, R.layout.layout_unpacking_detail_item, partList);
            listView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        } else {
            listView.setAdapter(null);
            noDataTv.setVisibility(View.VISIBLE);
        }
    }

    private void setData() {
        partList = dataSource.unpackingParts.getUnpackingData(vehicleNum, inwardNum, masterBoxNum);
        if (Utils.isValidArrayList((ArrayList<?>) partList)) {
            noDataTv.setVisibility(View.GONE);
            listAdapter = new MyAdapter(context, R.layout.layout_unpacking_detail_item, partList);
            listView.setAdapter(listAdapter);
            listAdapter.notifyDataSetChanged();
        } else {
            noDataTv.setVisibility(View.VISIBLE);
            listView.setAdapter(null);
        }
    }

    class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.DataObjectHolder> {
        private ArrayList<UnpackingPart> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView productNumTv, noOfBoxesTv, cartonsCountTv, exceptionsTv;
            Button goBtn, editBtn;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);

                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                cartonsCountTv = (TextView) itemView.findViewById(R.id.tv_primary_cartons);
                exceptionsTv = (TextView) itemView.findViewById(R.id.tv_exceptions);
                goBtn = (Button) itemView.findViewById(R.id.btn_go);
                editBtn = (Button) itemView.findViewById(R.id.btn_edit);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);

                goBtn.setOnClickListener(this);
                editBtn.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    if (v.getId() == R.id.btn_go) {
                        goBtn.clearFocus();
                        if (Utils.getConnectivityStatus(context)) {
                            UnpackingPart part = list.get(pos);
                            part.setNoOfExceptions(0);
                            part.setBoxId(unpackingBox.getBoxId());
                            part.setNoOfPrimaryCartonsContained(part.getNoOfPrimaryCartons());
                            new SubmitTask(part).execute();
                        }
                        list.get(pos).setStatus(1);
                        list.get(pos).setIsActedUpon(1);
                        notifyDataSetChanged();
                    } else if (v.getId() == R.id.btn_edit) {
                        editBtn.clearFocus();
                        intent = new Intent(UnpackingDetailActivity.this, UnpackingExceptionActivity.class);
                        intent.putExtra("box", gson.toJson(unpackingBox));
                        intent.putExtra("unpackingData", gson.toJson(list.get(pos)));
                        if (intent != null) {
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }
        }

        public PartsAdapter(ArrayList<UnpackingPart> myDataset) {
            list = myDataset;
        }

        @Override
        public PartsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_unpacking_detail_item, parent, false);
            return new PartsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartsAdapter.DataObjectHolder holder, final int position) {
            UnpackingPart model = list.get(position);

            if (model != null) {
                if (model.getIsSelected() == 1) {
                    mChecked.put(position, true);
                }

                holder.checkBox.setChecked(mChecked.get(position));
                if (mChecked.get(position)) {
                    enableView(holder.goBtn);
                    enableView(holder.editBtn);
                    if (model.getIsActedUpon() == 1) {
                        disableView(holder.checkBox);
                    }
                } else {
                    disableView(holder.goBtn);
                    disableView(holder.editBtn);
                    enableView(holder.checkBox);
                }
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        holder.checkBox.clearFocus();
                        if (isChecked) {
                            mChecked.put(position, isChecked);
                            if (isAllValuesChecked()) {
                                selectAllCb.setChecked(true);
                            }
                        } else {
                            mChecked.delete(position);
                            selectAllCb.setChecked(false);
                        }
                        partsRv.post(new Runnable() {
                            @Override
                            public void run() {
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText("Qty: " + model.getQty());
                holder.exceptionsTv.setText("Exceptions: " + model.getNoOfExceptions());
                holder.cartonsCountTv.setText("Primary Cartons: "
                        + model.getNoOfPrimaryCartonsContained() + "/" + model.getNoOfPrimaryCartons());
            }

            holder.productNumTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.cartonsCountTv.setTag(position);
            holder.exceptionsTv.setTag(position);
            holder.goBtn.setTag(position);
            holder.editBtn.setTag(position);
            holder.checkBox.setTag(position);
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list.size();
        }

        public void update(ArrayList<UnpackingPart> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(UnpackingPart dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    ProgressDialog submitDialog;

    class SubmitTask extends AsyncTask<String, String, Object> {
        UnpackingPart input;
        GeneralResponse response;

        SubmitTask(UnpackingPart input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitUnpackingUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        UnpackingBox box = dataSource.unpackingBoxes.getBox(vehicleNum, inwardNum, masterBoxNum);
                        if (box.getBoxStatus() != -1) {
                            dataSource.unpackingBoxes.updateStatus(vehicleNum, inwardNum, masterBoxNum, 1);
                        }
                        dataSource.unpackingParts.updateStatus(input.getPartId(), input.getVehicleNumber(), input.getInwardNumber(), input.getMasterBoxNum(),
                                0, input.getNoOfPrimaryCartons());
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        finish();
                    }
                    Utils.showToast(context, response.getMessage());
                }
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(UnpackingDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void enableView(View view) {
        view.setAlpha(1.0f);
        view.setClickable(true);
        view.setEnabled(true);
    }

    private void disableView(View view) {
        view.setAlpha(0.5f);
        view.setClickable(false);
        view.setEnabled(false);
    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    public class MyAdapter extends ArrayAdapter<UnpackingPart> {
        private LayoutInflater inflater;
        private int layoutId;
        private List<UnpackingPart> list;

        public MyAdapter(Context context, int layoutId, List<UnpackingPart> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.list = list;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.productNumTv = (TextView) convertView.findViewById(R.id.tv_product_number);
            holder.noOfBoxesTv = (TextView) convertView.findViewById(R.id.tv_quantity);
            holder.cartonsCountTv = (TextView) convertView.findViewById(R.id.tv_primary_cartons);
            holder.exceptionsTv = (TextView) convertView.findViewById(R.id.tv_exceptions);
            holder.goBtn = (Button) convertView.findViewById(R.id.btn_go);
            holder.editBtn = (Button) convertView.findViewById(R.id.btn_edit);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.goBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Utils.getConnectivityStatus(context)) {
                        UnpackingPart part = list.get(position);
                        part.setNoOfExceptions(0);
                        part.setBoxId(unpackingBox.getBoxId());
                        part.setNoOfPrimaryCartonsContained(part.getNoOfPrimaryCartons());
                        new SubmitTask(part).execute();
                    }
                    list.get(position).setStatus(1);
                    list.get(position).setIsActedUpon(1);
                    notifyDataSetChanged();
                }
            });
            holder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(UnpackingDetailActivity.this, UnpackingExceptionActivity.class);
                    intent.putExtra("box", gson.toJson(unpackingBox));
                    intent.putExtra("unpackingData", gson.toJson(list.get(position)));
                    if (intent != null) {
                        startActivity(intent);
                        finish();
                    }
                }
            });

            UnpackingPart model = getItem(position);

            if (model != null) {
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            enableView(holder.goBtn);
                            enableView(holder.editBtn);
                        } else {
                            disableView(holder.goBtn);
                            disableView(holder.editBtn);
                        }
                    }
                });
                if (model.getIsSelected() == 1) {
                    holder.checkBox.setChecked(true);
                }

                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText("Qty: " + model.getQty());
                holder.exceptionsTv.setText("Exceptions: " + model.getNoOfExceptions());
                holder.cartonsCountTv.setText("Primary Cartons: "
                        + model.getNoOfPrimaryCartonsContained() + "/" + model.getNoOfPrimaryCartons());
            }

            holder.productNumTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.cartonsCountTv.setTag(position);
            holder.exceptionsTv.setTag(position);
            holder.goBtn.setTag(position);
            holder.editBtn.setTag(position);
            holder.checkBox.setTag(position);
            return convertView;
        }
    }

    public class ViewHolder {
        TextView productNumTv, noOfBoxesTv, cartonsCountTv, exceptionsTv;
        Button goBtn, editBtn;
        CheckBox checkBox;
    }

}
