package com.apptmyz.splwms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;

public class BaseActivity extends AppCompatActivity {
    protected FrameLayout frameLayout;
    protected ImageView homeIv;
    protected TextView titleTv;
    private Context context;
    protected TextView wareHouseNameTv;
    private DataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        context = BaseActivity.this;
        dataSource = new DataSource(context);

        frameLayout = (FrameLayout) findViewById(R.id.content_frame);
        homeIv = (ImageView) findViewById(R.id.home_iv);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        wareHouseNameTv = (TextView) findViewById(R.id.tv_warehouse_name);
        wareHouseNameTv.setText(dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE) + "-" + dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_NAME));
        homeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
