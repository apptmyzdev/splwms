package com.apptmyz.splwms;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.List;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class SplashScreen extends Activity {

    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    String[] permissionsRequired = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.CAMERA};

    private ImageView splashImageView;
    private Context context;
    private DataSource dataSource;
    private AsyncTask task;
    private boolean isAnimEnded = false;
    private SharedPreferences permissionStatus;
    private boolean sentToSettings = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        context = SplashScreen.this;

        dataSource = new DataSource(context);

        permissionStatus = getSharedPreferences("Mypref", 0);

        setDeviceScreenSize();

        splashImageView = (ImageView) findViewById(R.id.splashscreenImg);

        Animation anim = AnimationUtils.loadAnimation(context,
                R.anim.splash_screen);
        splashImageView.setVisibility(View.VISIBLE);
        splashImageView.startAnimation(anim);

        new AsyncTaskProgress().execute();

        isAnimEnded = true;
        anim.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                isAnimEnded = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimEnded = true;
                if (task == null)
                    goNext();
            }
        });

    }

    private void goNext() {
        if (isAnimEnded) {
            checkPermissions();
        }
    }

    private void goNextScreen()
    {
        String logout = dataSource.sharedPreferences.getValue(Constants.LOGOUT_PREF);
        Intent intent = null;
        if (Utils.isValidString(logout) && logout.equals(Constants.FALSE)) {
            intent = new Intent(context, HomeActivity.class);
        } else {
            intent = new Intent(context, LoginActivity.class);
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    private void setDeviceScreenSize() {
        Display display = getWindowManager().getDefaultDisplay();
        Globals.screenWidth = display.getWidth();
        Globals.screenHeight = display.getHeight();

        Utils.logD("Width : " + Globals.screenWidth);
        Utils.logD("Height : " + Globals.screenHeight);


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        return true;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK)
            System.exit(0);
        return true;
    }

    private void proceedAfterPermission() {
        goNextScreen();
    }

    private List<String> getUngrantedPermissions() {
        List<String> permissions = new ArrayList<>();

        for (String s : permissionsRequired) {
            if (ContextCompat.checkSelfPermission(context, s) != PackageManager.PERMISSION_GRANTED)
                permissions.add(s);
        }

        return permissions;
    }

    private void checkPermissions() {
        List<String> permissions = getUngrantedPermissions();
        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(SplashScreen.this,
                    permissions.toArray(new String[permissions.size()]),
                    PERMISSION_CALLBACK_CONSTANT);

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(permissionsRequired[0], true);
            editor.commit();
        } else {
            proceedAfterPermission();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allgranted = false;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                    break;
                }
            }

            if (allgranted) {
                proceedAfterPermission();
            } else if (Utils.isValidArrayList((ArrayList<?>) getUngrantedPermissions())) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
                builder.setTitle(getString(R.string.need_permissions));
                builder.setMessage("error message"); // give required error message to be shown
                builder.setPositiveButton(getString(R.string.grant), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(SplashScreen.this, permissionsRequired, PERMISSION_CALLBACK_CONSTANT);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel_btn), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                Utils.showSimpleAlert(getBaseContext(), getResources().getString(R.string.alert), getString(R.string.unable_toget_permission));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(SplashScreen.this, permissionsRequired[0]) == PackageManager.PERMISSION_GRANTED) {
                proceedAfterPermission();
            }
        }
    }

    public class AsyncTaskProgress extends AsyncTask<Void, Integer, Void> {
        int progress;

        @Override
        protected void onPostExecute(Void result) {
            goNext();
            task = null;
        }

        @Override
        protected void onPreExecute() {
            progress = 0;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            while (progress < Constants.maxProgressRange) {
                progress = progress + 5;
                publishProgress(progress);
                SystemClock.sleep(100);
            }
            return null;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
    }
}
