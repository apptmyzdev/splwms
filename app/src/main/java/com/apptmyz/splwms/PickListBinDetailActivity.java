package com.apptmyz.splwms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CounterListener;
import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.data.SkipBinRequest;
import com.apptmyz.splwms.data.SubmitPickListInput;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.apptmyz.splwms.util.Utils;

import androidx.appcompat.app.AlertDialog;

public class PickListBinDetailActivity extends BaseActivity {
    TextView titleTv, binNumTv, productNumTv, qtyTv;
    TextInputEditText productNumEt;
    ImageView scanProductIv;
    Button confirmBtn, saveBtn, skipBinBtn;
    TextView toPickStatusTv, scanCompleteStatusTv, pickInstructionTv;
    CounterView counterView;
    Context context;
    WmsPartInvoiceModel bin;
    Gson gson = new Gson();
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan, scannedBoxes, subprojectId, nodeId;
    private TimerTask timerScanTask;
    private String binNumber, productNumber, type, dataStr, warehouseCode;
    private DataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_list_bin_detail, frameLayout);

        context = PickListBinDetailActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Pick List");
        saveBtn = (Button) findViewById(R.id.btn_save);
        saveBtn.setOnClickListener(listener);
        counterView = (CounterView) findViewById(R.id.counterView);
        toPickStatusTv = (TextView) findViewById(R.id.tv_boxes_tobepicked);
        scanCompleteStatusTv = (TextView) findViewById(R.id.tv_boxes_scanned);

        if (getIntent().getExtras() != null) {
            String binStr = getIntent().getStringExtra("bin");
            subprojectId = getIntent().getIntExtra("subprojectId", 0);
            nodeId = getIntent().getIntExtra("nodeId", 0);
            bin = gson.fromJson(binStr, WmsPartInvoiceModel.class);
            type = getIntent().getStringExtra("type");
            dataStr = getIntent().getStringExtra("data");
            counterView.setmMaxLimit(bin.getTotalBoxes());
            toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes()));
        }

        skipBinBtn = (Button) findViewById(R.id.btn_skip_bin);
        skipBinBtn.setOnClickListener(listener);

        scanCompleteStatusTv.setText(String.valueOf(0));

        binNumTv = (TextView) findViewById(R.id.tv_bin_number);
        binNumTv.setText(bin.getBinNo());
        productNumTv = (TextView) findViewById(R.id.tv_product_number);
        productNumTv.setText(bin.getPartNo());
        qtyTv = (TextView) findViewById(R.id.tv_qty);
        qtyTv.setText(String.valueOf(bin.getTotalBoxes()));

        pickInstructionTv = (TextView) findViewById(R.id.tv_pick_instruction);
        pickInstructionTv.setText("Pick " + bin.getTotalBoxes() + " Quantity");

        productNumEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumEt.addTextChangedListener(productNumWatcher);

        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);
        scanProductIv.setOnClickListener(listener);

        confirmBtn = (Button) findViewById(R.id.btn_confirm);
        confirmBtn.setOnClickListener(listener);

        counterView.setCounterListener(new CounterListener() {
            @Override
            public void onIncrementClick(String value) {
                toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
            }

            @Override
            public void onDecrementClick(String value) {
                toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
            }

            @Override
            public void onEditValue(String value) {
                toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
            }
        });

    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_skip_bin:
                    showSkipBinReasonPopUp();
                    break;
                case R.id.btn_save:
                    toPickStatusTv.setText(String.valueOf(bin.getTotalBoxes() - counterView.getValue()));
                    scanCompleteStatusTv.setText(String.valueOf(counterView.getValue()));
                    break;
                case R.id.iv_scan_product:
                    productNumEt.requestFocus();
                    productNumEt.setText("");
                    break;
                case R.id.btn_confirm:
                    scannedBoxes = counterView.getValue();
                    if (scannedBoxes != 0) {
                        reason = "";
                        remarks = "";
                        if (scannedBoxes < bin.getTotalBoxes()) {
                            showReasonPopUp();
                        } else if (scannedBoxes == bin.getTotalBoxes()) {
                            if (Utils.getConnectivityStatus(context)) {
                                submit(getSubmitData());
                            }
                        } else {
                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "You can't scan more than the Total Qty");
                        }
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Scan the Quantity");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void submit(SubmitPickListInput input) {
        List<SubmitPickListInput> list = new ArrayList<>();
        list.add(input);
        new SubmitPickList(list).execute();
    }

    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() > 0) {
                                        productNumber = scannedValue;
                                    }

                                    if (scannedValue.length() >= 2) {
                                        counterView.setValue(bin.getTotalBoxes());
                                    }

                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    String reason = "", remarks = "";

    private void showReasonPopUp() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        alert.setMessage("You have Scanned less than the Actual No. of Boxes. Pick a Reason: ");
        alert.setTitle("Select Reason");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_pick_list_reason, null);
        alert.setView(view);
        Spinner spinner = view.findViewById(R.id.sp_reason);
        EditText remarksEt = view.findViewById(R.id.et_remarks);
        String[] s = getResources().getStringArray(R.array.reasons);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(PickListBinDetailActivity.this,
                android.R.layout.simple_dropdown_item_1line, s);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        spinner.setPadding(5, 5, 5, 5);
        spinner.setLayoutParams(lp);
        spinner.setAdapter(adp);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reason = s[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                reason = s[0];
            }
        });

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                remarks = remarksEt.getText().toString().trim();
                submit(getSubmitData());
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alert.show();
    }

    private SubmitPickListInput getSubmitData() {
        SubmitPickListInput input = new SubmitPickListInput();
        input.setId(bin.getId());
        input.setBinId(bin.getBinId());
        input.setOrderNo(bin.getOrderNo());
        input.setPartId(bin.getPartId());
        input.setPartNo(bin.getPartNo());
        input.setTotalBoxes(counterView.getValue());

        input.setReasonForShortage(reason);
        input.setRemarks(remarks);

        input.setScanTime(Utils.getScanTime());
        return input;
    }

    ProgressDialog submitDialog;

    class SubmitPickList extends AsyncTask<String, String, Object> {
        List<SubmitPickListInput> input;
        GeneralResponse response;

        SubmitPickList(List<SubmitPickListInput> input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSubmitPickListUrl(warehouseCode);
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        dataSource.pickListParts.updateBoxCount(bin.getTotalBoxes() - counterView.getValue(), type, dataStr, bin.getId(), warehouseCode);
                        Intent intent = new Intent(context, PickListActivity.class);
                        intent.putExtra("isRefresh", true);
                        intent.putExtra("subprojectId", subprojectId);
                        intent.putExtra("nodeId", nodeId);
                        intent.putExtra("type", type);
                        startActivity(intent);
                    }
                    Utils.showToast(context, response.getMessage());
                }
                finish();
            }
            super.onPostExecute(result);
        }

    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickListBinDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    int reasonId;

    private void showSkipBinReasonPopUp() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        final Spinner spinner = new Spinner(context);
        alert.setMessage("Select a Reason for Skipping the BIN : ");
        alert.setTitle("Select Reason");
        String[] s = getResources().getStringArray(R.array.reasons);
        List<SkipBinReasonModel> reasons = dataSource.skipBinReasons.getSkipBinReasons(warehouseCode);
        final ArrayAdapter<SkipBinReasonModel> adp = new ArrayAdapter<SkipBinReasonModel>(PickListBinDetailActivity.this,
                android.R.layout.simple_dropdown_item_1line, reasons);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        spinner.setPadding(5, 5, 5, 5);
        spinner.setLayoutParams(lp);
        spinner.setAdapter(adp);
        alert.setView(spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reasonId = reasons.get(i).getReasonId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
                if (Utils.getConnectivityStatus(context)) {
                    SkipBinRequest input = new SkipBinRequest();
                    input.setBinId(bin.getBinId());
                    input.setBinNo(bin.getBinNo());
                    input.setExcepType(reasonId);
                    input.setPartId(bin.getPartId());
                    input.setPartNo(bin.getPartNo());
                    input.setTotalBoxes(bin.getTotalBoxes());
                    input.setScanTime(Utils.getScanTime());
                    new SkipBinTask(input).execute();
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alert.show();
    }

    ProgressDialog skipBinDialog;

    class SkipBinTask extends AsyncTask<String, String, Object> {
        SkipBinRequest input;
        GeneralResponse response;

        SkipBinTask(SkipBinRequest input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                skipBinDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSkipBinUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && skipBinDialog != null && skipBinDialog.isShowing()) {
                skipBinDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        dataSource.pickListParts.updateAsSubmitted(type, dataStr, bin.getPartId(), bin.getBinId(), warehouseCode);
                        Intent intent = new Intent(context, PickListActivity.class);
                        intent.putExtra("subprojectId", subprojectId);
                        intent.putExtra("nodeId", nodeId);
                        intent.putExtra("type", type);
                        startActivity(intent);
                    }
                    Utils.showToast(context, response.getMessage());
                }
                finish();
            }
            super.onPostExecute(result);
        }
    }
}
