package com.apptmyz.splwms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.data.InventoryData;
import com.apptmyz.splwms.data.Part;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Utils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class NormalInventoryDetailActivity extends BaseActivity {
    private static int MAX_QUANTITY = 5;

    Context context;
    TextView titleTv;
    TextInputEditText productNumEt;
    ImageView scanProductIv;
    CounterView counterView;
    Button saveBtn, submitBtn;
    RecyclerView partsRv;
    PartAdapter adapter;
    InventoryData inventoryData;
    LinearLayoutManager layoutManager;
    ArrayList<Part> parts = new ArrayList<>();
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String productNumber;
    Part part = null;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_normal_inventory_detail, frameLayout);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Normal Inventory");
        context = NormalInventoryDetailActivity.this;

        productNumEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumEt.addTextChangedListener(productNumWatcher);

        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);
        scanProductIv.setOnClickListener(listener);

        counterView = (CounterView) findViewById(R.id.counterView);
        saveBtn = (Button) findViewById(R.id.btn_save);
        saveBtn.setOnClickListener(listener);
        submitBtn = (Button) findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(listener);

        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        adapter = new PartAdapter(parts);
        layoutManager = new LinearLayoutManager(context);
        partsRv.setLayoutManager(layoutManager);
        partsRv.setAdapter(adapter);

        if (getIntent().getExtras() != null) {
            String str = getIntent().getStringExtra("inventory");
            inventoryData = gson.fromJson(str, InventoryData.class);
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()) {
                case R.id.iv_scan_product:
                    productNumEt.setText("");
                    productNumEt.requestFocus();
                    break;
                case R.id.btn_save:
                    String msg = validatePartDetails();
                    if (!Utils.isValidString(msg)) {
                        parts.add(part);
                        part = null;
                        adapter = new PartAdapter(parts);
                        partsRv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        productNumEt.setText("");
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), msg);
                    }
                    break;
                case R.id.btn_submit:
                    if (Utils.isValidArrayList(parts)) {
                        Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();

                        intent = new Intent(context, NormalInventoryActivity.class);
                    }
                    break;
                default:
                    break;
            }
            if (intent != null)
                startActivity(intent);
        }
    };

    private String validatePartDetails() {
        String message = "";
        productNumber = (Utils.isValidString(productNumber)) ? productNumber : productNumEt.getText().toString().trim();

        if (Utils.isValidString(productNumber)) {
            int boxesCount = counterView.getValue();
            if (boxesCount != 0) {
                part = new Part();
                part.setQuantity(boxesCount);
                part.setProductNumber(productNumEt.getText().toString().trim());
            } else {
                message = "Please Scan Atleast one Part";
            }
        } else {
            message = "Please Scan the Part";
        }
        return message;
    }

    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (scannedValue.length() >= 2) {
                                        counterView.setValue(MAX_QUANTITY);
                                        productNumber = scannedValue;
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    class PartAdapter extends RecyclerView.Adapter<PartAdapter.DataObjectHolder> {
        private ArrayList<Part> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView productNumTv, noOfBoxesTv, scannedTv, unScannedTv;
            ImageView scannedIv, notScannedIv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                scannedTv = (TextView) itemView.findViewById(R.id.tv_scanned_boxes);
                unScannedTv = (TextView) itemView.findViewById(R.id.tv_unscanned_boxes);

                scannedIv = (ImageView) itemView.findViewById(R.id.iv_scanned);
                notScannedIv = (ImageView) itemView.findViewById(R.id.iv_not_scanned);
            }
        }

        public PartAdapter(ArrayList<Part> myDataset) {
            list = myDataset;
        }

        @Override
        public PartAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_inventory_detail_item, parent, false);
            return new PartAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartAdapter.DataObjectHolder holder, final int position) {
            Part model = list.get(position);
            if (model != null) {
                holder.productNumTv.setText(model.getProductNumber());
                holder.noOfBoxesTv.setText(String.valueOf(MAX_QUANTITY));
                holder.scannedTv.setText(String.valueOf(model.getQuantity()));
                if (model.getQuantity() <= MAX_QUANTITY)
                    holder.unScannedTv.setText(String.valueOf(MAX_QUANTITY - model.getQuantity()));

                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.scannedTv.setTag(position);
                holder.unScannedTv.setTag(position);
                holder.scannedIv.setTag(position);
                holder.notScannedIv.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<Part> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Part dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

}
