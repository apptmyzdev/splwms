package com.apptmyz.splwms;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.GenerateTcModel;
import com.apptmyz.splwms.data.GenerateTcResponse;
import com.apptmyz.splwms.data.TcOrderResponse;
import com.apptmyz.splwms.data.TcOrderResponseModel;
import com.apptmyz.splwms.data.VendorMasterModel;
import com.apptmyz.splwms.data.VendorMasterResponse;
import com.apptmyz.splwms.data.VendorMastersRequestModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsException;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class GenerateTcActivity extends BaseActivity {
    private static final String TAG = "GenerateTcActivity";
    Context context;
    LinearLayout vendorLl;
    TextView vendorTv;
    Button assignVendorBtn;
    String vendor, selectedTime, dateString, conNo, ewayNo, noOfBoxes, ewayBilleDate, eta, weight, freight, vendorCode, orderNo, slaDesc, orderDateTime,
            custCode, orderType, origin, destination;
    int mYear, mMonth, mDay, mHour, mMinute, count, wt, ft;
    VendorMasterResponse vendorMasterResponse;
    List<VendorMasterModel> vendorMasterModelList;
    TcOrderResponse tcOrderResponse;
    List<TcOrderResponseModel> tcOrderResponseModelList = new ArrayList<>();
    List<TcOrderResponseModel> tcList = new ArrayList<>();
    CheckBox selectAllCb;
    TcAdapter adapter;
    RecyclerView generateTcRv;
    GenerateTcResponse generateTcResponse;
    GenerateTcModel generateTcModel;
    SparseBooleanArray mChecked = new SparseBooleanArray();
    TextView billDateTv, etaDateTv;
    boolean dateFlag = false;
    EditText conNoEt, ewayNoEt, weightEt, freightEt, noOfBoxesEt;
    TextView cancelTv, saveTv;
    ImageView wtAddIv, wtSubIv, freightAddIv, freightSubIv;
    LinearLayout billDateLl, etaLl;
    Integer orderId;
    private DataSource dataSource;
    private Spinner orderTypeSp;
    private VendorMastersRequestModel vendorMastersRequestModel;
    private Gson gson = new Gson();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_generatetc, frameLayout);
        context = GenerateTcActivity.this;
        dataSource = new DataSource(context);
        titleTv.setText("Generate TC");
        orderTypeSp = (Spinner) findViewById(R.id.sp_order_type);

        vendorLl = (LinearLayout) findViewById(R.id.vendor_ll);
        vendorLl.setOnClickListener(listener);
        vendorTv = (TextView) findViewById(R.id.vendor_tv);
        assignVendorBtn = (Button) findViewById(R.id.assignVendor_btn);
        assignVendorBtn.setOnClickListener(listener);
        selectAllCb = (CheckBox) findViewById(R.id.selectAll_cb);
        selectAllCb.setOnClickListener(listener);
        generateTcRv = (RecyclerView) findViewById(R.id.generateTc_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(generateTcRv.getContext(),
                layoutManager.getOrientation());
        generateTcRv.addItemDecoration(dividerItemDecoration);
        generateTcRv.setNestedScrollingEnabled(false);
        generateTcRv.setHasFixedSize(false);
        RecyclerView.LayoutManager recycle = new
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        generateTcRv.setLayoutManager(recycle);

        String[] orderTypesArr = getResources().getStringArray(R.array.order_types);
        final ArrayAdapter<String> adp = new ArrayAdapter<String>(GenerateTcActivity.this,
                android.R.layout.simple_dropdown_item_1line, orderTypesArr);
        orderTypeSp.setAdapter(adp);
        orderTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                orderType = orderTypesArr[i];
                if (Utils.isValidArrayList((ArrayList<?>) tcList))
                    tcList.clear();
                new OrderDetailsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.vendor_ll:
                    if (Utils.isValidArrayList((ArrayList<?>) vendorMasterModelList))
                        showVendorDialog();
                    else
                        Toast.makeText(context, "No Data Available", Toast.LENGTH_LONG).show();
                    break;

                case R.id.assignVendor_btn:
                    if (Utils.isValidArrayList((ArrayList<?>) tcList)) {
                        Utils.logD("tcList: " + tcList.toString());
                        String message = validateSelectedOrders();
                        if (!Utils.isValidString(message))
                            new VendorMasterTask().execute();
                        else
                            Utils.showSimpleAlert(context, getString(R.string.alert), message);
                    } else
                        Toast.makeText(context, "Please Select Atleast One Order", Toast.LENGTH_LONG).show();
                    break;

                case R.id.selectAll_cb:
                    int itemCount = count;
                    for (int i = 0; i < itemCount; i++) {
                        mChecked.put(i, selectAllCb.isChecked());
                    }
                    adapter.notifyDataSetChanged();
                    break;
                case R.id.billDate_ll:
                    dateFlag = true;
                    showDatePicker();
                    break;
                case R.id.eta_ll:
                    dateFlag = false;
                    showDateTimePicker();
                    break;
                case R.id.wtAdd_iv:
                    if (Utils.isValidString(weightEt.getText().toString())) {
                        wt = Integer.valueOf(weightEt.getText().toString());
                        wt++;
                        weightEt.setText(String.valueOf(wt), TextView.BufferType.EDITABLE);
                    }
                    break;
                case R.id.wtSubt_iv:
                    if (Utils.isValidString(weightEt.getText().toString())) {
                        wt = Integer.valueOf(weightEt.getText().toString());
                        wt--;
                        weightEt.setText(String.valueOf(wt), TextView.BufferType.EDITABLE);
                    }
                    break;
                case R.id.freightAdd_iv:
                    if (Utils.isValidString(freightEt.getText().toString())) {
                        ft = Integer.valueOf(freightEt.getText().toString());
                        ft++;
                        freightEt.setText(String.valueOf(ft), TextView.BufferType.EDITABLE);
                    }
                    break;
                case R.id.freightSub_iv:
                    if (Utils.isValidString(freightEt.getText().toString())) {
                        ft = Integer.valueOf(freightEt.getText().toString());
                        ft--;
                        freightEt.setText(String.valueOf(ft), TextView.BufferType.EDITABLE);
                    }
                    break;
            }
        }
    };

    Calendar etaDate;

    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        etaDate = Calendar.getInstance();
        new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                etaDate.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        etaDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        etaDate.set(Calendar.MINUTE, minute);
                        etaDate.set(Calendar.SECOND, 0);
                        Log.v(TAG, "The choosen one: " + etaDate.getTime());
                        etaDateTv.setText(Utils.getFormattedDate(etaDate.getTime(), Constants.dateFormat2));
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private String validateSelectedOrders() {
        String message = "";
        String originPin = "", destPin = "", custCode = "", slaDesc = "";
        int sla = 0;
        boolean isValid = true;
        for (TcOrderResponseModel tc : tcList) {
            if (tc.getFromAdress() != null && !Utils.isValidString(originPin))
                originPin = tc.getFromAdress().getPincode();
            else if (tc.getFromAdress() != null && !originPin.equals(tc.getFromAdress().getPincode())) {
                message = "Please Select Orders with Same Origin Pincode";
                isValid = false;
                break;
            }

            if (!Utils.isValidString(slaDesc))
                slaDesc = tc.getSlaNameDescription();

            if (tc.getToAddress() != null && !Utils.isValidString(destPin))
                destPin = tc.getToAddress().getPincode();
            else if (tc.getToAddress() != null && !destPin.equals(tc.getToAddress().getPincode())) {
                message = "Please Select Orders with Same Destination Pincode";
                isValid = false;
                break;
            }

            if (tc.getCustomerCode() != null && !Utils.isValidString(custCode))
                custCode = tc.getCustomerCode();
            else if (tc.getCustomerCode() != null && !custCode.equals(tc.getCustomerCode())) {
                message = "Please Select Orders with Same Customer Code";
                isValid = false;
                break;
            }

            if (sla == 0)
                sla = tc.getSlaName();
            else if (sla != tc.getSlaName()) {
                message = "Please Select Orders with Same SLA";
                isValid = false;
                break;
            }
        }
        if (isValid) {
            vendorMastersRequestModel = new VendorMastersRequestModel();
            vendorMastersRequestModel.setCustomerCode(custCode);
            vendorMastersRequestModel.setOrigin(originPin);
            vendorMastersRequestModel.setDestination(destPin);
            vendorMastersRequestModel.setSlaName(sla);
            vendorMastersRequestModel.setSlaDesc(slaDesc);
        }
        return message;
    }

    @SuppressLint("RestrictedApi")
    public void showVendorDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.vendor_type_radiolist);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        TextView ok = (TextView) dialog.findViewById(R.id.save_tv);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel_tv);
        final RadioGroup vendorRg = (RadioGroup) dialog.findViewById(R.id.vendor_rg);
        final ColorStateList colorStateList = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_enabled}},
                new int[]{getResources().getColor(R.color.green)}
        );
        if (Utils.isValidArrayList((ArrayList<?>) vendorMasterModelList)) {
            for (VendorMasterModel model : vendorMasterModelList) {
                AppCompatRadioButton venorRbtn = new AppCompatRadioButton(context);
                venorRbtn.setHighlightColor(context.getResources().getColor(R.color.green));
                venorRbtn.setId(View.generateViewId());
                Log.e("id", String.valueOf(View.generateViewId()));
                venorRbtn.setText(model.getVendorCode() + "-" + model.getVendorName());
                venorRbtn.setTextSize(14);
                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(context, null);
                params.setMargins(0, 15, 0, 0);
                venorRbtn.setLayoutParams(params);
                venorRbtn.setTextColor(getResources().getColor(R.color.green));
                venorRbtn.setSupportButtonTintList(colorStateList);
                vendorRg.addView(venorRbtn);
            }
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer vendorId = vendorRg.getCheckedRadioButtonId();
                int childCount = vendorRg.getChildCount();
                Log.e("child id", String.valueOf(childCount));
                for (int i = 0; i < childCount; i++) {
                    if (vendorRg.getChildAt(i) instanceof RadioButton) {
                        RadioButton r_btn = (RadioButton) vendorRg.getChildAt(i);
                        if (r_btn.getId() == vendorId) {
                            vendor = r_btn.getText().toString();
                            vendorTv.setText(vendor);
                            vendorCode = vendor.substring(0, 10);
                            if (vendorCode.equalsIgnoreCase("VE00000001")) {
                                new GenerateTcVendorTask().execute();
                            } else {
                                showTcDialog();
                            }
                            break;
                        }
                    }
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public void showTcDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.generatetc_details_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        conNoEt = (EditText) dialog.findViewById(R.id.conNo_et);
        ewayNoEt = (EditText) dialog.findViewById(R.id.eWayNo_et);
        weightEt = (EditText) dialog.findViewById(R.id.weight_et);
        freightEt = (EditText) dialog.findViewById(R.id.freight_et);
        noOfBoxesEt = (EditText) dialog.findViewById(R.id.noOfBoxes_et);
        billDateTv = (TextView) dialog.findViewById(R.id.billDate_tv);
        etaDateTv = (TextView) dialog.findViewById(R.id.etaDate_tv);
        cancelTv = (TextView) dialog.findViewById(R.id.cancel_tv);
        cancelTv.setOnClickListener(listener);
        saveTv = (TextView) dialog.findViewById(R.id.save_tv);
        saveTv.setOnClickListener(listener);
        wtAddIv = (ImageView) dialog.findViewById(R.id.wtAdd_iv);
        wtAddIv.setOnClickListener(listener);
        wtSubIv = (ImageView) dialog.findViewById(R.id.wtSubt_iv);
        wtSubIv.setOnClickListener(listener);
        freightAddIv = (ImageView) dialog.findViewById(R.id.freightAdd_iv);
        freightAddIv.setOnClickListener(listener);
        freightSubIv = (ImageView) dialog.findViewById(R.id.freightSub_iv);
        freightSubIv.setOnClickListener(listener);
        billDateLl = (LinearLayout) dialog.findViewById(R.id.billDate_ll);
        billDateLl.setOnClickListener(listener);
        etaLl = (LinearLayout) dialog.findViewById(R.id.eta_ll);
        etaLl.setOnClickListener(listener);

        saveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndSubmit();
                conNo = conNoEt.getText().toString();
                ewayNo = ewayNoEt.getText().toString();
                weight = weightEt.getText().toString();
                ewayBilleDate = billDateTv.getText().toString();
                eta = etaDateTv.getText().toString();
                freight = freightEt.getText().toString();
                noOfBoxes = noOfBoxesEt.getText().toString();
                if (Utils.isValidString(conNo)) {
                    if (Utils.isValidString(weight)) {
                        if (Utils.isValidString(eta)) {
                            if (Utils.isValidString(noOfBoxes)) {
                                Log.d("con", conNo);
                                Log.d("eway", ewayNo);
                                Log.d("wt", weight);
                                new GenerateTcTask().execute();
                                dialog.cancel();
                            } else
                                Toast.makeText(context, "Please Enter No. Of Boxes", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(context, "Please Enter ETA", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(context, "Please Enter Weight", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(context, "Please Enter Con No.", Toast.LENGTH_LONG).show();

            }
        });
        cancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void validateAndSubmit() {
        tcList.clear();
        tcList = new ArrayList<>();
        if (mChecked.size() > 0) {
            for (int i = 0; i < count; i++) {
                if (mChecked.get(i)) {
                    tcList.add(tcOrderResponseModelList.get(i));
                }
            }
        }
    }


    public String showDatePicker() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                String fm = "" + month;
                String fd = "" + dayOfMonth;
                if (month < 10) {
                    fm = "0" + month;
                }
                if (dayOfMonth < 10) {
                    fd = "0" + dayOfMonth;
                }
                dateString = "" + fd + "-" + fm + "-" + year;
                Log.d("tag", "" + dateString);
                if (dateFlag)
                    billDateTv.setText(dateString);
                else
                    etaDateTv.setText(dateString);
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        return dateString;
    }

    public class VendorMasterTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String url = WmsUtils.getVendorMaster();
            String jsonStr = gson.toJson(vendorMastersRequestModel);
            if (Utils.isValidString(url)) {
                try {
                    vendorMasterResponse = (VendorMasterResponse) HttpRequest.postData(url, jsonStr, VendorMasterResponse.class, context);
                    if (vendorMasterResponse != null && vendorMasterResponse.isStatus()) {
                        vendorMasterModelList = vendorMasterResponse.getData();
                        Log.e("response", String.valueOf(vendorMasterModelList));
                    } else {
                        Globals.lastErrMsg = vendorMasterResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            if (Utils.isValidArrayList((ArrayList<?>) vendorMasterModelList))
                showVendorDialog();
            else
                Toast.makeText(context, "No Data Available", Toast.LENGTH_LONG).show();

        }
    }

    public class OrderDetailsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
            if(Utils.isValidArrayList((ArrayList<?>) tcOrderResponseModelList))
                tcOrderResponseModelList.clear();
            if(adapter != null)
                adapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String url = null;
            if (orderType.equals(getString(R.string.forward))) {
                url = WmsUtils.getForwardOrderDetails();
            } else if (orderType.equals(getString(R.string.transport))) {
                url = WmsUtils.getTransportOrderDetails();
            } else {
                url = WmsUtils.getRmaOrderDetails();
            }

            if (Utils.isValidString(url)) {
                try {
                    tcOrderResponse = (TcOrderResponse) HttpRequest.getInputStreamFromUrl(url, TcOrderResponse.class, context);
                    if (tcOrderResponse != null && tcOrderResponse.isStatus()) {
                        tcOrderResponseModelList = tcOrderResponse.getData();
                        Log.e("response", String.valueOf(tcOrderResponseModelList));
                    } else {
                        Globals.lastErrMsg = tcOrderResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            Log.d("list", tcOrderResponseModelList.toString());
            mChecked.clear();
            adapter = new TcAdapter((ArrayList<TcOrderResponseModel>) tcOrderResponseModelList);
            generateTcRv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public class GenerateTcTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d("eway task", ewayNo);
            Log.d("fre", freight);
            Log.d("con no", conNo);
            Log.d("ewaybilldate", ewayBilleDate);
            Log.d("wt", weight);
            Log.d("boxes", noOfBoxes);
            generateTcModel = new GenerateTcModel(tcList, vendorCode);
            generateTcModel.seteWayBill(ewayNo);
            generateTcModel.setEta(eta);
            generateTcModel.setEwayBillDate(ewayBilleDate);
            generateTcModel.setNoOfBoxes(noOfBoxes);
            generateTcModel.setWeight(weight);
            if (Utils.isValidString(freight))
                generateTcModel.setFreight(Double.parseDouble(freight));
            generateTcModel.setTcNumber(conNo);
            Utils.logD("generateTcModel: " + generateTcModel.toString());
            String url = null;
            if (orderType.equals(getString(R.string.forward))) {
                url = WmsUtils.getForwardGenerateTc().replace(" ", "%20");
            } else if (orderType.equals(getString(R.string.transport))) {
                url = WmsUtils.getTransportGenerateTc().replace(" ", "%20");
            } else {
                url = WmsUtils.getRmaGenerateTc().replace(" ", "%20");
            }

            if (Utils.isValidString(url)) {
                try {
                    Gson gson = new Gson();
                    String data = gson.toJson(generateTcModel);
                    generateTcResponse = (GenerateTcResponse) HttpRequest.postData(url, data, GenerateTcResponse.class, context);
                    if (generateTcResponse != null && generateTcResponse.isStatus()) {
                        Log.e("response", generateTcResponse.toString());
                    } else {
                        Globals.lastErrMsg = generateTcResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            if(Utils.isValidArrayList((ArrayList<?>) tcOrderResponseModelList))
                tcOrderResponseModelList.clear();
            if(adapter != null)
                adapter.notifyDataSetChanged();
            if (showErrorDialog()) {
                if (generateTcResponse.isStatus()) {
                    if (Utils.isValidArrayList((ArrayList<?>) tcList))
                        tcList.clear();
                    new OrderDetailsTask().execute();
                }
            }
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(GenerateTcActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    public class GenerateTcVendorTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String url = null;
            if (orderType.equals(getString(R.string.forward))) {
                url = WmsUtils.getForwardGenerateTcVendor();
            } else if (orderType.equals(getString(R.string.transport))) {
                url = WmsUtils.getTransportGenerateTcVendor();
            } else {
                url = WmsUtils.getRmaGenerateTcVendor();
            }

            if (Utils.isValidString(url)) {
                try {
                    Gson gson = new Gson();
                    String data = gson.toJson(generateTcModel);
                    generateTcResponse = (GenerateTcResponse) HttpRequest.postData(url, data, GenerateTcResponse.class, context);
                    if (generateTcResponse != null && generateTcResponse.isStatus()) {
                        Log.e("response", generateTcResponse.toString());
                    } else {
                        Globals.lastErrMsg = generateTcResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            if(Utils.isValidArrayList((ArrayList<?>) tcOrderResponseModelList))
                tcOrderResponseModelList.clear();
            if(adapter != null)
                adapter.notifyDataSetChanged();
            if (showErrorDialog()) {
                if (generateTcResponse.isStatus()) {
                    if (Utils.isValidArrayList((ArrayList<?>) tcList))
                        tcList.clear();
                    new OrderDetailsTask().execute();
                }
            }
        }
    }

    class TcAdapter extends RecyclerView.Adapter<TcAdapter.DataObjectHolder> {
        ArrayList<TcOrderResponseModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView orderNoTv, slaTypeTv, custCodeTv, orderDateTimeTv, orderTypeTv, originTv, destinationTv;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);
                orderNoTv = (TextView) itemView.findViewById(R.id.orderNo_tv);
                slaTypeTv = (TextView) itemView.findViewById(R.id.slaType_tv);
                orderDateTimeTv = (TextView) itemView.findViewById(R.id.dateAndTime_tv);
                orderTypeTv = (TextView) itemView.findViewById(R.id.orderType_tv);
                custCodeTv = (TextView) itemView.findViewById(R.id.customerCode_tv);
                originTv = (TextView) itemView.findViewById(R.id.origin_tv);
                destinationTv = (TextView) itemView.findViewById(R.id.destination_tv);
                checkBox = (CheckBox) itemView.findViewById(R.id.generateTc_cb);
            }
        }

        public TcAdapter(ArrayList<TcOrderResponseModel> myDataset) {
            list = myDataset;
        }

        @Override
        public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.generatetc_list_items, parent, false);
            return new DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final DataObjectHolder holder, final int position) {
            final TcOrderResponseModel model = list.get(position);

            if (model != null) {
                holder.checkBox.setChecked(mChecked.get(position));
                holder.orderNoTv.setText(model.getOrderNumber());
                holder.slaTypeTv.setText("SLA:" + " " + model.getSlaNameDescription());
                holder.orderDateTimeTv.setText("Date:" + " " + model.getOrderDateTime());
                if (model.getCustomerMasterModel() != null) {
                    holder.custCodeTv.setVisibility(View.VISIBLE);
                    holder.custCodeTv.setText("Customer Code:" + " " + model.getCustomerCode());
                } else {
                    holder.custCodeTv.setVisibility(View.GONE);
                }
                holder.orderTypeTv.setText("Order Type:" + " " + model.getOrderType());
                holder.originTv.setText("Origin:" + " " + model.getOrigin());
                holder.destinationTv.setText("Destination:" + " " + model.getDestination());
            }

            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mChecked.put(position, isChecked);
//                        if (Utils.isValidString(String.valueOf(model.getId())))
//                            orderId = model.getId();
//                        if (Utils.isValidString(model.getOrderNumber()))
//                            orderNo = model.getOrderNumber();
//                        if (Utils.isValidString(model.getSlaType()))
//                            slaDesc = model.getSlaNameDescription();
//                        if (Utils.isValidString(model.getOrderDateTime()))
//                            orderDateTime = model.getOrderDateTime();
//                        if (model.getCustomerMasterModel() != null && Utils.isValidString(model.getCustomerMasterModel().getCustomerCode()))
//                            custCode = model.getCustomerMasterModel().getCustomerCode();
//                        if (Utils.isValidString(model.getOrderType()))
//                            orderType = model.getOrderType();
//                        if (Utils.isValidString(model.getOriginWh()))
//                            origin = model.getOriginWh();
//                        if (Utils.isValidString(model.getReceivingWh()))
//                            destination = model.getReceivingWh();
                        tcList.add(model);
                        generateTcModel = new GenerateTcModel(tcList, vendorCode);
                        Log.d("tc model", generateTcModel.toString());
                        if (isAllValuesChecked()) {
                            selectAllCb.setChecked(true);
                        }
                    } else {
                        mChecked.delete(position);
                        tcList.remove(model);
                        generateTcModel = new GenerateTcModel(tcList, vendorCode);
                        Log.d("tc model", generateTcModel.toString());
                        selectAllCb.setChecked(false);
                    }
                    generateTcRv.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            });

            holder.orderNoTv.setTag(position);
            holder.slaTypeTv.setTag(position);
            holder.orderDateTimeTv.setTag(position);
            holder.custCodeTv.setTag(position);
            holder.originTv.setTag(position);
            holder.destinationTv.setTag(position);
            holder.orderTypeTv.setTag(position);
            holder.checkBox.setTag(position);
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<TcOrderResponseModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(TcOrderResponseModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }
        return true;
    }


}
