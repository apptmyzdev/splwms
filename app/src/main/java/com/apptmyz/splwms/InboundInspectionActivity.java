package com.apptmyz.splwms;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InboundInspectionResponse;
import com.apptmyz.splwms.data.InspectionBarcodeData;
import com.apptmyz.splwms.data.InspectionPart;
import com.apptmyz.splwms.data.InvoiceModel;
import com.apptmyz.splwms.data.InwardNumsResponse;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.data.RmaInfo;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class InboundInspectionActivity extends BaseActivity {

    TextInputLayout vehicleNumEt, inwardNumEt;
    Button goBtn, submitBtn;
    Context context;
    RecyclerView partsRv;
    PartsAdapter adapter;
    List<InspectionPart> partList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    Gson gson = new Gson();
    DataSource dataSource;
    private String vehicleNum, inwardNum;
    private ImageView refreshIv, proceedIv;
    private Spinner inwardNumSp;
    private ArrayList<String> inwardNumsList = new ArrayList<>();
    List<InspectionBarcodeData> barcodesData = new ArrayList<>();
    private AlertDialog barcodesAlert, rmaInfoAlert;
    private BarcodesAdapter barcodesAdapter;
    private RecyclerView barcodesRv;
    ProgressDialog inwardNumsDialog, submitDialog, inspectionDialog, loadingDialog;
    CustomAlertDialog errDlg;
    private AlertDialog addBarcodeAlert;

    public HashMap<String, ArrayList<InspectionPart>> childData = new HashMap<>();
    public List<String> groupData = new ArrayList<>();

    public HashMap<String, ArrayList<InspectionPart>> allChildData = new HashMap<>();
    public List<String> allGroupData = new ArrayList<>();
    private ExpandableListAdapter expandableListAdapter;
    private ExpandableListView inspectionELv;
    private EditText searchEt;
    private String warehouseCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_inbound_inspection, frameLayout);
        context = InboundInspectionActivity.this;
        initializeViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setInitialData();
    }

    private void initializeViews() {
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        vehicleNumEt = (TextInputLayout) findViewById(R.id.layout_vehicle_number);
        vehicleNumEt.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (Utils.isValidArrayList(inwardNumsList)) {
                    inwardNum = "";
                    inwardNumsList.clear();
                    inwardNumSp.setAdapter(null);
                }
            }
        });
        inwardNumEt = (TextInputLayout) findViewById(R.id.layout_inward_number);
        goBtn = (Button) findViewById(R.id.btn_go);
        submitBtn = (Button) findViewById(R.id.btn_submit);
        goBtn.setOnClickListener(listener);
        submitBtn.setOnClickListener(listener);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);
        proceedIv = (ImageView) findViewById(R.id.iv_proceed);
        proceedIv.setOnClickListener(listener);
        searchEt = (EditText) findViewById(R.id.et_search);
        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                search(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        inwardNumSp = (Spinner) findViewById(R.id.sp_inward_number);
        inspectionELv = (ExpandableListView) findViewById(R.id.inspection_elv);
        expandableListAdapter = new ExpandableListAdapter(context);
        inspectionELv.setAdapter(expandableListAdapter);
        inwardNumSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inwardNum = inwardNumsList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        partsRv.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this);
        partsRv.setLayoutManager(layoutManager);
        adapter = new PartsAdapter((ArrayList<InspectionPart>) partList);
        titleTv.setText("Inbound Inspection Summary");
    }

    private void search(String searchText) {
        ArrayList<InspectionPart> filteredParts = dataSource.inboundInspection.getFilteredInboundInspectionData(searchText, warehouseCode);
        Utils.logD(filteredParts.toString());
        fetchGroupedData(filteredParts, false);
    }

    private void validateAndProceed() {
        vehicleNum = vehicleNumEt.getEditText().getText().toString().trim();
        if (Utils.isValidString(vehicleNum) && Utils.isValidVehicleNum(vehicleNum)) {
            if (Utils.getConnectivityStatus(context)) {
                new InwardNumsTask().execute();
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Vehicle Number");
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_proceed:
                    validateAndProceed();
                    break;
                case R.id.iv_refresh:
                    refresh();
                    break;
                case R.id.btn_go:
                    validateAndGetData();
                    break;
                default:
                    break;
            }
        }
    };

    class InwardNumsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inwardNumsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getInwardNumsUrl(vehicleNum, 1).replace(" ", "%20");
                Utils.logD("Log 1");
                InwardNumsResponse response = (InwardNumsResponse) HttpRequest
                        .getInputStreamFromUrl(url, InwardNumsResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        inwardNumsList = (ArrayList<String>) response.getData();
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inwardNumsDialog != null && inwardNumsDialog.isShowing()) {
                inwardNumsDialog.dismiss();
            }
            if (showErrorDialog()) {
                setInwardNumsData();
            }
            super.onPostExecute(result);
        }
    }

    private void setInwardNumsData() {
        if (Utils.isValidArrayList(inwardNumsList)) {
            ArrayAdapter<String> inwardNumAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, inwardNumsList);
            inwardNumSp.setAdapter(inwardNumAdapter);
        }
    }

    class SubmitTask extends AsyncTask<String, String, Object> {
        InspectionPart input;
        GeneralResponse response;

        SubmitTask(InspectionPart input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSubmitInspectionUrl(warehouseCode);
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        refresh();
                    }
                    Utils.showToast(context, response.getMessage());
                }
            }
            super.onPostExecute(result);
        }
    }

    private void refresh() {
        if (Utils.getConnectivityStatus(context)) {
            new InspectionTask().execute();
        }
    }

    private void validateAndGetData() {
        vehicleNum = vehicleNumEt.getEditText().getText().toString().trim();
        if (Utils.isValidString(vehicleNum)) {
            if (Utils.isValidString(inwardNum)) {
                partList = dataSource.inboundInspection.getInboundInspectionData(warehouseCode);
                if (Utils.isValidArrayList((ArrayList<?>) partList)) {
                    setData();
                } else if (Utils.getConnectivityStatus(context)) {
                    new InspectionTask().execute();
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Inward Number");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Vehicle Number");
        }
    }

    class InspectionTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inspectionDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getInspectionUrl(warehouseCode);

                Utils.logD("Log 1");
                InboundInspectionResponse response = (InboundInspectionResponse) HttpRequest
                        .getInputStreamFromUrl(url, InboundInspectionResponse.class,
                                context);

                if (response != null) {
                    dataSource.inboundInspection.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        partList = response.getData();

                        if (Utils.isValidArrayList((ArrayList<?>) partList)) {
                            dataSource.inboundInspection.insertInboundInspectionData(partList, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inspectionDialog != null && inspectionDialog.isShowing()) {
                inspectionDialog.dismiss();
            }
            showErrorDialog();
            setData();
            super.onPostExecute(result);
        }
    }

    private void setInitialData() {
        partList = dataSource.inboundInspection.getInboundInspectionData(warehouseCode);
        groupData.clear();
        childData.clear();
        allGroupData.clear();
        allChildData.clear();
        if (Utils.isValidArrayList((ArrayList<?>) partList)) {
            adapter = new PartsAdapter((ArrayList<InspectionPart>) partList);
            partsRv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            fetchGroupedData((ArrayList<InspectionPart>) partList, true);
            Utils.logD("Child Data: " + childData.toString());
        } else {
            if (Utils.getConnectivityStatus(context)) {
                new InspectionTask().execute();
            }
        }
    }

    private void fetchGroupedData(ArrayList<InspectionPart> partList, boolean isAll) {
        groupData.clear();
        childData.clear();
        if (Utils.isValidArrayList(partList)) {
            for (InspectionPart part : partList) {
                String inventoryType = part.getInventoryTypeDesc();
                if (!childData.containsKey(inventoryType)) {
                    groupData.add(inventoryType);
                    if (isAll)
                        allGroupData.add(inventoryType);
                    ArrayList<InspectionPart> parts = new ArrayList<>();
                    parts.add(part);
                    childData.put(inventoryType, parts);
                    if (isAll)
                        allChildData.put(inventoryType, (ArrayList<InspectionPart>) parts.clone());
                } else {
                    ArrayList<InspectionPart> parts = childData.get(inventoryType);
                    parts.add(part);
                    childData.put(inventoryType, parts);
                    if (isAll)
                        allChildData.put(inventoryType, (ArrayList<InspectionPart>) parts.clone());
                }
            }
        }
        expandableListAdapter.notifyDataSetChanged();
    }

    private void setData() {
        partList = dataSource.inboundInspection.getInboundInspectionData(warehouseCode);
        adapter = new PartsAdapter((ArrayList<InspectionPart>) partList);
        partsRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        searchEt.setText(null);
        fetchGroupedData((ArrayList<InspectionPart>) partList, true);
    }

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(InboundInspectionActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void showBarcodesPopup(InspectionPart part) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Scan Barcodes");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_inspection_barcodes_popup, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        barcodesAlert = alertDialog.show();
        barcodesAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        CheckBox repackedCb = (CheckBox) view.findViewById(R.id.cb_repacked);
        Button submitBtn = (Button) view.findViewById(R.id.btn_submit_barcodes);
        Button cancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        Button addBarcodeBtn = (Button) view.findViewById(R.id.btn_add_barcode);
        addBarcodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddBarcodeDialog();
            }
        });
        barcodesRv = (RecyclerView) view.findViewById(R.id.rv_barcodes);
        barcodesRv.setNestedScrollingEnabled(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setAutoMeasureEnabled(false);
        barcodesRv.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(barcodesRv.getContext(),
                layoutManager.getOrientation());
        barcodesRv.addItemDecoration(dividerItemDecoration);
        barcodesAdapter = new BarcodesAdapter((ArrayList<InspectionBarcodeData>) barcodesData);
        barcodesRv.setAdapter(barcodesAdapter);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (part.getReceiptType().equals("RM")) {
                    showRmaInfoPopup(part, repackedCb.isChecked());
                } else {
                    barcodesAlert.dismiss();
                    submitInspection(part, repackedCb.isChecked());
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodesAlert.dismiss();
                barcodesData.clear();
                barcodesAdapter.notifyDataSetChanged();
            }
        });
    }

    private void submitInspection(InspectionPart part, boolean isRepacked) {
        part.setRepacked(isRepacked);
        part.setBarcodesData(barcodesData);

        if (Utils.getConnectivityStatus(context)) {
            new SubmitTask(part).execute();
        }
    }

    Calendar etaDate;

    public void showDateTimePicker(EditText dateTimeEt) {
        final Calendar currentDate = Calendar.getInstance();
        etaDate = Calendar.getInstance();
        new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                etaDate.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        etaDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        etaDate.set(Calendar.MINUTE, minute);
                        etaDate.set(Calendar.SECOND, 0);
                        Utils.logD("The choosen one: " + etaDate.getTime());
                        String formattedDate = Utils.getFormattedDate(etaDate.getTime(), Constants.dateFormat2);
                        dateTimeEt.setText(formattedDate);
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void showRmaInfoPopup(InspectionPart part, boolean isRepacked) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Enter Service Tag Info");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_rma_info, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        rmaInfoAlert = alertDialog.show();
        rmaInfoAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        EditText serviceTagNumEt, faultDescriptionEt, rmaPartSrNumEt, replacedPartSrNumEt, dateTimeEt;
        Button submitBtn, cancelBtn;
        serviceTagNumEt = (EditText) view.findViewById(R.id.et_service_tag_num);
        faultDescriptionEt = (EditText) view.findViewById(R.id.et_fault_description);
        rmaPartSrNumEt = (EditText) view.findViewById(R.id.et_rma_part_sr_num);
        replacedPartSrNumEt = (EditText) view.findViewById(R.id.et_replaced_part_sr_num);
        dateTimeEt = (EditText) view.findViewById(R.id.et_date_time);
        dateTimeEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateTimePicker(dateTimeEt);
            }
        });
        submitBtn = (Button) view.findViewById(R.id.btn_submit);
        cancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String serviceTagNum = serviceTagNumEt.getText().toString().trim();
                String faultDescription = faultDescriptionEt.getText().toString().trim();
                String rmaPartSrNum = rmaPartSrNumEt.getText().toString().trim();
                String replacedPartSrNum = replacedPartSrNumEt.getText().toString().trim();
                String dateTime = dateTimeEt.getText().toString().trim();
                if (Utils.isValidString(serviceTagNum)) {
                    RmaInfo rmaInfo = new RmaInfo();
                    rmaInfo.setServiceTagNum(serviceTagNum);
                    if (Utils.isValidString(faultDescription)) {
                        rmaInfo.setFaultDescription(faultDescription);
                        if (Utils.isValidString(rmaPartSrNum)) {
                            rmaInfo.setRmaPartSrNum(rmaPartSrNum);
                            if (Utils.isValidString(replacedPartSrNum)) {
                                rmaInfo.setReplacedPartSrNum(replacedPartSrNum);
                                if (Utils.isValidString(dateTime)) {
                                    rmaInfo.setDateTime(dateTime);
                                    rmaInfoAlert.dismiss();
                                    barcodesAlert.dismiss();
                                    part.setRmaInfo(rmaInfo);
                                    submitInspection(part, isRepacked);
                                } else {
                                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select Date Time");
                                }
                            } else {
                                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Serial No. of the Replaced Part");
                            }
                        } else {
                            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Serial No. of the RMA Part");
                        }
                    } else {
                        Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter the Fault Description");
                    }
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter the Service Tag Number");
                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rmaInfoAlert.dismiss();
            }
        });
    }

    private void showAddBarcodeDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Scan Barcode And Serial No.");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_add_barcode, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        addBarcodeAlert = alertDialog.show();
        TextInputEditText barcodeEt, serialNoEt;
        barcodeEt = (TextInputEditText) view.findViewById(R.id.input_barcode);
        serialNoEt = (TextInputEditText) view.findViewById(R.id.input_serial_num);
        addBarcodeAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        Button saveBtn = (Button) view.findViewById(R.id.btn_save_barcode);
        Button cancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        InspectionBarcodeData model = new InspectionBarcodeData();

        barcodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                serialNoEt.setText(editable.toString());
                model.setBarcode(editable.toString());
                model.setSerialNo(editable.toString());
            }
        });
        serialNoEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                model.setSerialNo(editable.toString());
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidString(model.getBarcode())) {
                    if (Utils.isValidString(model.getSerialNo())) {
                        addBarcodeAlert.dismiss();
                        barcodesData.add(model);
                        barcodesAdapter = new BarcodesAdapter((ArrayList<InspectionBarcodeData>) barcodesData);
                        barcodesRv.setAdapter(barcodesAdapter);
                        barcodesAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showSimpleAlert(context, getString(R.string.alert), "Please Scan the Serial Number");
                    }
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Scan the Barcode");
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBarcodeAlert.dismiss();
            }
        });
    }

    class BarcodesAdapter extends RecyclerView.Adapter<BarcodesAdapter.DataObjectHolder> {
        private ArrayList<InspectionBarcodeData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView barcodeTv, serialNoTv;
            ImageView deleteIv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                barcodeTv = (TextView) itemView.findViewById(R.id.tv_barcode);
                serialNoTv = (TextView) itemView.findViewById(R.id.tv_serial_no);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_remove);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.iv_remove) {
                    int pos = (int) view.getTag();
                    barcodesData.remove(pos);
                    barcodesAdapter.notifyDataSetChanged();
                }
            }
        }

        public BarcodesAdapter(ArrayList<InspectionBarcodeData> myDataset) {
            list = myDataset;
        }

        @Override
        public BarcodesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_item_barcode, parent, false);
            return new BarcodesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BarcodesAdapter.DataObjectHolder holder, final int position) {
            InspectionBarcodeData model = list.get(position);

            if (model != null) {
                holder.barcodeTv.setText(model.getBarcode());
                holder.serialNoTv.setText(model.getSerialNo());
            }

            holder.barcodeTv.setTag(position);
            holder.serialNoTv.setTag(position);
            holder.deleteIv.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<InspectionBarcodeData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(InspectionBarcodeData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.DataObjectHolder> {
        private ArrayList<InspectionPart> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView productNumTv, noOfBoxesTv, masterCartonsTv, excepTypeTv, invTypeTv;
            Button goBtn, editBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                excepTypeTv = (TextView) itemView.findViewById(R.id.tv_excep_type);
                invTypeTv = (TextView) itemView.findViewById(R.id.tv_inventory_type);
                goBtn = (Button) itemView.findViewById(R.id.btn_go);
                editBtn = (Button) itemView.findViewById(R.id.btn_edit);
                goBtn.setOnClickListener(this);
                editBtn.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    if (v.getId() == R.id.btn_go) {
                        InspectionPart part = list.get(pos);
                        part.setGoodBoxes(part.getNoOfMasterCartons());
                        showBarcodesPopup(part);
                    } else if (v.getId() == R.id.btn_edit) {
                        intent = new Intent(InboundInspectionActivity.this, InboundInspectionDetailActivity.class);
                        intent.putExtra("data", gson.toJson(list.get(pos)));
                        if (intent != null) {
                            startActivity(intent);
                        }
                    }
                }
            }
        }

        public PartsAdapter(ArrayList<InspectionPart> myDataset) {
            list = myDataset;
        }

        @Override
        public PartsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_inspection_item, parent, false);
            return new PartsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartsAdapter.DataObjectHolder holder, final int position) {
            InspectionPart model = list.get(position);

            if (model != null) {
                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText(String.valueOf(model.getQty()));
                holder.excepTypeTv.setText(model.getExceptTypeDescription());
                holder.invTypeTv.setText(model.getInventoryTypeDesc());
                if (model.getStatus() == 1) {
                    holder.editBtn.setVisibility(View.GONE);
                } else {
                    holder.editBtn.setVisibility(View.VISIBLE);
                }
            }

            holder.productNumTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.excepTypeTv.setTag(position);
            holder.invTypeTv.setTag(position);
            holder.goBtn.setTag(position);
            holder.editBtn.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<InspectionPart> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(InspectionPart dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, HomeActivity.class));
    }

    public class LoadBarcodesTask extends AsyncTask {
        InspectionPart part;

        LoadBarcodesTask(InspectionPart part) {
            this.part = part;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!isFinishing())
                loadingDialog = Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            barcodesData = new ArrayList<>(Collections.nCopies(part.getQty(), new InspectionBarcodeData()));
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (!isFinishing() && loadingDialog != null && loadingDialog.isShowing()) {
                loadingDialog.dismiss();
            }
            showBarcodesPopup(part);
        }
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;
        private View.OnClickListener groupClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int groupPosition = (Integer) v.getTag();
                if (inspectionELv.isGroupExpanded(groupPosition))
                    inspectionELv.collapseGroup(groupPosition);
                else
                    inspectionELv.expandGroup(groupPosition);
            }
        };

        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        public Object getChild(String groupName, int childPosition) {
            return childData.get(groupName).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = inflater.inflate(R.layout.layout_inspection_item, parent, false);
            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);
            String groupName = groupData.get(groupPosition);
            InspectionPart inspectionPart = (InspectionPart) getChild(groupName, childPosition);
            TextView productNumTv, noOfBoxesTv, masterCartonsTv, excepTypeTv, invTypeTv;
            Button goBtn, editBtn;
            productNumTv = (TextView) v.findViewById(R.id.tv_product_number);
            noOfBoxesTv = (TextView) v.findViewById(R.id.tv_quantity);
            excepTypeTv = (TextView) v.findViewById(R.id.tv_excep_type);
            invTypeTv = (TextView) v.findViewById(R.id.tv_inventory_type);
            goBtn = (Button) v.findViewById(R.id.btn_go);
            editBtn = (Button) v.findViewById(R.id.btn_edit);
            goBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    inspectionPart.setGoodBoxes(inspectionPart.getNoOfMasterCartons());
                    showBarcodesPopup(inspectionPart);
                }
            });
            editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(InboundInspectionActivity.this, InboundInspectionDetailActivity.class);
                    intent.putExtra("data", gson.toJson(inspectionPart));
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            });
            if (inspectionPart != null) {
                if (inspectionPart.getIsProduct() == 0)
                    productNumTv.setText(inspectionPart.getPartNo());
                else
                    productNumTv.setText(inspectionPart.getProductName());
                noOfBoxesTv.setText(String.valueOf(inspectionPart.getQty()));
                excepTypeTv.setText(inspectionPart.getExceptTypeDescription());
                invTypeTv.setText(inspectionPart.getInventoryTypeDesc());
                if (inspectionPart.getStatus() == 1) {
                    editBtn.setVisibility(View.GONE);
                } else {
                    editBtn.setVisibility(View.VISIBLE);
                }
            }
            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            String groupName = groupData.get(groupPosition);
            return childData.get(groupName) != null ? childData.get(groupName).size() : 0;
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.group_item_inventory, parent, false);

            v.setTag(groupPosition);

            final String groupName = (String) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (inspectionELv.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);

            TextView partNoTv = (TextView) v.findViewById(R.id.tv_bin_number);
            partNoTv.setText(groupName);
            v.setOnClickListener(groupClickListener);
            return v;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }

}
