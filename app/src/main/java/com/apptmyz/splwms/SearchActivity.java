package com.apptmyz.splwms;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.Bin;
import com.apptmyz.splwms.data.Part;
import com.apptmyz.splwms.data.SearchBinOrPartResponse;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SearchActivity extends BaseActivity {

    TextView titleTv, partResultLabelTv, binResultLabelTv;
    TextInputEditText binNumEt, partNumEt;
    ImageView scanBinIv, scanPartIv, searchBinIv, searchPartIv;
    ExpandableListView binResultsLv, partResultsLv;
    BinResultAdapter binResultsAdapter;
    PartResultAdapter partResultsAdapter;
    LinearLayout binsListLayout, partListLayout;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String binNumber, partNumber, warehouseCode;
    private DataSource dataSource;
    private ArrayList<WmsPartInvoiceModel> searchBinData = new ArrayList<>();
    private ArrayList<WmsPartInvoiceModel> searchPartData = new ArrayList<>();
    private TextView noDataLayout;
    private RadioGroup searchByRg;
    private RelativeLayout searchBinLayout, searchPartLayout;

    ArrayList<Bin> binsData = new ArrayList<>();
    private ArrayList<Bin> groupBinsData = new ArrayList<>();
    private ArrayList<ArrayList<WmsPartInvoiceModel>> childBinsData = new ArrayList<>();

    ArrayList<Part> partsData = new ArrayList<>();
    private ArrayList<Part> groupPartsData = new ArrayList<>();
    private ArrayList<ArrayList<WmsPartInvoiceModel>> childPartsData = new ArrayList<>();

    Context context;
    private boolean isBinSearch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_search, frameLayout);

        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Search BIN/Part");

        searchBinIv = (ImageView) findViewById(R.id.iv_bin_proceed);
        searchPartIv = (ImageView) findViewById(R.id.iv_part_proceed);
        searchBinIv.setOnClickListener(listener);
        searchPartIv.setOnClickListener(listener);

        context = SearchActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);

        noDataLayout = (TextView) findViewById(R.id.tv_no_data);
        searchByRg = (RadioGroup) findViewById(R.id.rg_search_by);
        searchBinLayout = (RelativeLayout) findViewById(R.id.rl_search_bin);
        searchPartLayout = (RelativeLayout) findViewById(R.id.rl_search_part);

        searchByRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radioGroup.getCheckedRadioButtonId() == R.id.rb_bin) {
                    isBinSearch = true;
                    showBinsLayout();
                } else {
                    isBinSearch = false;
                    showPartsLayout();
                }
            }
        });

        binsListLayout = (LinearLayout) findViewById(R.id.ll_bin_list);
        partListLayout = (LinearLayout) findViewById(R.id.ll_part_list);
        binsListLayout.setVisibility(View.GONE);
        partListLayout.setVisibility(View.GONE);

        partResultLabelTv = (TextView) findViewById(R.id.tv_part_result_label);
        binResultLabelTv = (TextView) findViewById(R.id.tv_bin_result_label);

        binNumEt = (TextInputEditText) findViewById(R.id.input_bin_number);
        binNumEt.addTextChangedListener(binWatcher);
        partNumEt = (TextInputEditText) findViewById(R.id.input_part_number);
        partNumEt.addTextChangedListener(partWatcher);

        scanBinIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanPartIv = (ImageView) findViewById(R.id.iv_scan_part);
        scanBinIv.setOnClickListener(listener);
        scanPartIv.setOnClickListener(listener);

        binResultsLv = (ExpandableListView) findViewById(R.id.explv_bins);
        partResultsLv = (ExpandableListView) findViewById(R.id.explv_parts);
        binResultsAdapter = new BinResultAdapter(context);
        binResultsLv.setAdapter(binResultsAdapter);

        partResultsAdapter = new PartResultAdapter(context);
        partResultsLv.setAdapter(partResultsAdapter);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_bin_proceed:
                    binResultLabelTv.setText("BIN Results - " + binNumber);
                    searchBin(binNumber);
                    break;
                case R.id.iv_part_proceed:
                    partResultLabelTv.setText("Part Results - " + partNumber);
                    searchPart(partNumber);
                    break;
                case R.id.iv_scan_bin:
                    binNumEt.requestFocus();
                    partNumEt.setText("");
                    break;
                case R.id.iv_scan_part:
                    partNumEt.requestFocus();
                    binNumEt.setText("");
                    break;
                default:
                    break;
            }
        }
    };

    private TextWatcher binWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 2) {
                                        binNumber = scannedValue;
                                        int dbId = dataSource.bins.exists(binNumber.toUpperCase(), warehouseCode);
                                        if (dbId != -1) {
                                            binResultLabelTv.setText("BIN Results - " + binNumber);
                                            searchBin(binNumber);
                                        } else {
                                            showNoDataLayout();
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        }, 300);

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private void showNoDataLayout() {
        binsListLayout.setVisibility(View.GONE);
        partListLayout.setVisibility(View.GONE);
        noDataLayout.setVisibility(View.VISIBLE);
    }

    private void searchBin(String binNumber) {
        if (Utils.isValidString(binNumber)) {
            if (Utils.getConnectivityStatus(context))
                new SearchBinOrPartTask("B", binNumber).execute();
        }
    }

    private void searchPart(String partNumber) {
        if (Utils.isValidString(partNumber)) {
            if (Utils.getConnectivityStatus(context))
                new SearchBinOrPartTask("P", partNumber).execute();
        }
    }

    ProgressDialog searchDialog;

    class SearchBinOrPartTask extends AsyncTask<String, String, Object> {
        private String data;
        private String type;

        SearchBinOrPartTask(String type, String data) {
            this.type = type;
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                searchDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                if (type.equals("B"))
                    searchBinData.clear();
                else
                    searchPartData.clear();

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSearchBinOrPartUrl(data, type, warehouseCode);

                Utils.logD("Log 1");
                SearchBinOrPartResponse response = (SearchBinOrPartResponse) HttpRequest
                        .getInputStreamFromUrl(url, SearchBinOrPartResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        if (response.getData() != null) {
                            if (type.equals("B")) {
                                searchBinData = (ArrayList<WmsPartInvoiceModel>) response.getData();
                            } else {
                                searchPartData = (ArrayList<WmsPartInvoiceModel>) response.getData();
                            }
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && searchDialog != null && searchDialog.isShowing()) {
                searchDialog.dismiss();
            }
            if (showErrorDialog()) {
                if (type.equals("P")) {
                    setPartData();
                } else if (type.equals("B")) {
                    setBinData();
                }
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(SearchActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void setPartData() {
        if (Utils.isValidArrayList(searchPartData)) {
            groupPartsData.clear();
            groupPartsData.add(new Part(partNumber));
            childPartsData.clear();
            childPartsData.add(searchPartData);
            partResultsAdapter.notifyDataSetChanged();
            showPartsLayout();
        } else {
            showNoDataLayout();
        }
    }

    private void showPartsLayout() {
        showSearchPartInput();
        binResultLabelTv.setVisibility(View.GONE);
        partResultLabelTv.setVisibility(View.VISIBLE);
        binsListLayout.setVisibility(View.GONE);
        partListLayout.setVisibility(View.VISIBLE);
        noDataLayout.setVisibility(View.GONE);
    }

    private void showSearchPartInput() {
        searchBinLayout.setVisibility(View.GONE);
        searchPartLayout.setVisibility(View.VISIBLE);
    }

    private void showBinsLayout() {
        showSearchBinInput();
        binResultLabelTv.setVisibility(View.VISIBLE);
        partResultLabelTv.setVisibility(View.GONE);
        binsListLayout.setVisibility(View.VISIBLE);
        partListLayout.setVisibility(View.GONE);
        noDataLayout.setVisibility(View.GONE);
    }

    private void showSearchBinInput() {
        searchBinLayout.setVisibility(View.VISIBLE);
        searchPartLayout.setVisibility(View.GONE);
    }

    private void setBinData() {
        if (Utils.isValidArrayList(searchBinData)) {
            groupBinsData.clear();
            groupBinsData.add(new Bin(binNumber));
            childBinsData.clear();
            childBinsData.add(searchBinData);
            binResultsAdapter.notifyDataSetChanged();
            showBinsLayout();
        } else {
            showNoDataLayout();
        }
    }


    private TextWatcher partWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = partNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 2) {
                                        partNumber = scannedValue;
                                        int dbId = dataSource.parts.exists(partNumber.toUpperCase(), warehouseCode);
                                        if (dbId != -1) {
                                            partResultLabelTv.setText("Part Results - " + partNumber);
                                            searchPart(partNumber);
                                        } else {
                                            showNoDataLayout();
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        }, 300);

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    public class BinResultAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;

        public BinResultAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public WmsPartInvoiceModel getChild(int groupPosition, int childPosition) {
            return childBinsData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.child_item_bin_search, parent, false);

            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);

            final WmsPartInvoiceModel part = (WmsPartInvoiceModel) getChild(groupPosition, childPosition);
            TextView partNumberTv = (TextView) v.findViewById(R.id.tv_part_number);
            TextView qtyTv = (TextView) v.findViewById(R.id.tv_qty);
            TextView putAwayDateTv = (TextView) v.findViewById(R.id.tv_put_away_date);
            partNumberTv.setText(part.getPartNo());
            qtyTv.setText(String.valueOf(part.getQty()));

            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childBinsData.get(groupPosition).size();
        }

        @Override
        public Bin getGroup(int groupPosition) {
            return groupBinsData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupBinsData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.group_item_search, parent, false);

            v.setTag(groupPosition);
            v.setOnClickListener(groupClickListener);

            final Bin pdcData = (Bin) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (binResultsLv.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);


            final String pdc = pdcData.getBinNumber();
            v.setOnClickListener(groupClickListener);
            TextView conTv = (TextView) v.findViewById(R.id.tv_number);
            TextView qtyTv = (TextView) v.findViewById(R.id.tv_quantity);
            TextView putAwayDateTv = (TextView) v.findViewById(R.id.tv_put_away_date);

            conTv.setText(pdc);
            return v;
        }

        private View.OnClickListener groupClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int groupPosition = (Integer) v.getTag();
                if (binResultsLv.isGroupExpanded(groupPosition))
                    binResultsLv.collapseGroup(groupPosition);
                else
                    binResultsLv.expandGroup(groupPosition);

            }
        };

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }
    }

    public class PartResultAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;

        public PartResultAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public WmsPartInvoiceModel getChild(int groupPosition, int childPosition) {
            return childPartsData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.child_item_part_search, parent, false);

            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);

            final WmsPartInvoiceModel part = (WmsPartInvoiceModel) getChild(groupPosition, childPosition);
            TextView partNumberTv = (TextView) v.findViewById(R.id.tv_bin_number);
            TextView qtyTv = (TextView) v.findViewById(R.id.tv_qty);
            TextView putAwayDateTv = (TextView) v.findViewById(R.id.tv_put_away_date);

            partNumberTv.setText(part.getBinNo());
            qtyTv.setText(String.valueOf(part.getQty()));

            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childPartsData.get(groupPosition).size();
        }

        @Override
        public Part getGroup(int groupPosition) {
            return groupPartsData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupPartsData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.group_item_search, parent, false);

            v.setTag(groupPosition);
            v.setOnClickListener(groupClickListener);

            final Part pdcData = (Part) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (partResultsLv.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);


            final String pdc = pdcData.getProductNumber();
            v.setOnClickListener(groupClickListener);
            TextView conTv = (TextView) v.findViewById(R.id.tv_number);
            TextView qtyTv = (TextView) v.findViewById(R.id.tv_quantity);
            TextView putAwayDateTv = (TextView) v.findViewById(R.id.tv_put_away_date);

            conTv.setText(pdc);
            return v;
        }

        private View.OnClickListener groupClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int groupPosition = (Integer) v.getTag();
                if (partResultsLv.isGroupExpanded(groupPosition))
                    partResultsLv.collapseGroup(groupPosition);
                else
                    partResultsLv.expandGroup(groupPosition);

            }
        };

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }
}