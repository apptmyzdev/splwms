package com.apptmyz.splwms;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.PackingModel;
import com.apptmyz.splwms.data.PackingSubmitJson;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.example.tscdll.TSCActivity;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PackingDetailActivity extends BaseActivity {
    private Context context;
    private DataSource dataSource;
    private TextView subProjectNameTv, orderNumTv;
    private EditText searchView, masterBoxNumEt;
    private Button addMasterCartonBtn, printLabelsBtn;
    private PartsAdapter adapter;
    private RecyclerView partsRv;
    private ArrayList<PackingModel> partsList;
    private List<PackingModel> selectedList = new ArrayList<>();
    private String subProjectName, orderNum, selectedDevice;
    private int subProjectId, count;
    private CheckBox selectAllCb;
    private SparseBooleanArray mChecked = new SparseBooleanArray();
    private Timer timer;
    private TimerTask timerTask;
    public static Intent findDeviceList = null;
    public static TSCActivity TscDll = new TSCActivity();
    private BluetoothAdapter bluetoothAdapter;
    private String labelType, printType = "Single";
    private Handler handler;
    private int time;
    private boolean isPrinting, executed, tscConnected, isAcknowledgement;
    private ProgressDialog progressDialog;
    private PackingSubmitJson input;
    private String shipAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_packing_detail, frameLayout);
        initializeViews();
    }

    private void initializeViews() {
        titleTv.setText("Master Carton Packing");
        context = PackingDetailActivity.this;
        dataSource = new DataSource(context);

        subProjectNameTv = (TextView) findViewById(R.id.tvSubProject);
        orderNumTv = (TextView) findViewById(R.id.tvInvoiceNum);
        searchView = (EditText) findViewById(R.id.searchView);
        masterBoxNumEt = (EditText) findViewById(R.id.etMasterBoxNumber);
        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        partsRv.setLayoutManager(new LinearLayoutManager(context));
        adapter = new PartsAdapter((ArrayList<PackingModel>) partsList);
        partsRv.setAdapter(adapter);
        addMasterCartonBtn = (Button) findViewById(R.id.btn_signature);
        printLabelsBtn = (Button) findViewById(R.id.btn_print_labels);
        addMasterCartonBtn.setOnClickListener(listener);
        printLabelsBtn.setOnClickListener(listener);
        selectAllCb = (CheckBox) findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(clickListener);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setFilteredData(editable.toString());
            }
        });

        if (getIntent().getExtras() != null) {
            subProjectId = getIntent().getIntExtra("subProjectId", 0);
            subProjectName = getIntent().getStringExtra("subProjectName");
            orderNum = getIntent().getStringExtra("orderNum");
            shipAddress = getIntent().getStringExtra("shipAddress");
            orderNumTv.setText(orderNum);
            subProjectNameTv.setText(subProjectName);
            setData();
        }
    }

    ProgressDialog submitDialog;

    class SubmitTask extends AsyncTask<String, String, Object> {
        PackingSubmitJson input;
        GeneralResponse response;

        SubmitTask(PackingSubmitJson input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getPackingSubmitUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, PackingSubmitJson.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (Utils.isValidArrayList((ArrayList<?>) input.getPartList())) {
                    for (PackingModel model : input.getPartList()) {
                        dataSource.packingParts.updatePackingCount(model.getPartId(), subProjectId, orderNum, model.getPrimarCartonsPacked(),
                                model.getRemainingPrimaryCartons() - model.getPrimarCartonsPacked(), model.getNoOfMasterBoxes() + 1);
                    }
                }
                if (response != null)
                    Utils.showToast(context, response.getMessage());
                finish();
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PackingDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void setFilteredData(String query) {
        ArrayList<PackingModel> tempList = new ArrayList<>();

        for (PackingModel model : partsList) {
            if (model.getPartNum().toLowerCase().contains(query.toLowerCase())) {
                tempList.add(model);
            }
        }
        adapter = new PartsAdapter(tempList);
        partsRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_signature:
                    validateAndSubmit();
                    break;
                case R.id.btn_print_labels:
                    validateAndPrint();
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndPrint() {
        String masterBoxNum = masterBoxNumEt.getText().toString().trim();
        if (Utils.isValidString(masterBoxNum)) {
            if (mChecked.size() > 0) {
                printLabels();
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Pack the parts into the Master Box");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter the Master Box Number");
        }
    }

    private void printLabels() {
        PrintDialog dialog = new PrintDialog("Turn on the Printer Before Printing. Is the Printer ready to Print Labels?");
        dialog.setCancelable(false);
        dialog.show();
    }

    class PrintDialog extends Dialog {
        private TextView messageTv;
        private Button yesBtn, noBtn;
        private String msg;

        public PrintDialog(String message) {
            super(context, R.style.CustomAlertDialog);
            msg = message;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert);

            messageTv = (TextView) findViewById(R.id.tv_error_message);
            messageTv.setText(msg);

            yesBtn = (Button) findViewById(R.id.btn_ok);
            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    showPrinterOption();
                }
            });

            noBtn = (Button) findViewById(R.id.btn_cancel);
            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

        }
    }

    public void showPrinterOption() {
        selectedDevice = dataSource.sharedPreferences.getValue(Constants.DEVICE);
        if (Utils.isValidString(selectedDevice)) {
            Utils.logD(selectedDevice);
            checkBTState();
        } else {
            Utils.showToast(context, "Select a printer in Settings Screen");
            finish();
        }
    }

    public boolean isBluetoothEnabled() {
        Utils.logD("isBluetoothEnabled");
        boolean isEnabled = false;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        setSelectedDevice(bluetoothAdapter);
        if (bluetoothAdapter == null) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.bt_not_support),
                    Toast.LENGTH_LONG).show();
        } else if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        } else
            isEnabled = true;
        return isEnabled;
    }

    public boolean isBluetoothDiscovering() {
        boolean isDiscovering = false;
        if (bluetoothAdapter.isDiscovering()) {
            Toast.makeText(context,
                    context.getResources().getString(R.string.bt_enabled),
                    Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(enableBtIntent,
                    Constants.REQUEST_DISCOVERABLE_BT);
            isDiscovering = true;
        }

        return isDiscovering;
    }

    public void setSelectedDevice(BluetoothAdapter mBtAdapter) {
        if (selectedDevice == null) {
            String dev = "Zebra";
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device != null) {
                        if (device.getAddress().equals(dev)) {
                            selectedDevice = device.toString();
                            dataSource.sharedPreferences.set(Constants.DEVICE, selectedDevice);
                        }
                    }
                }
            }
        }
    }

    protected boolean isDeviceConnected() {
        if (!tscConnected) {
            String openport = TscDll.openport(selectedDevice);
            if (openport.equals("-1")) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    public void checkBTState() {
        String state = context.getResources().getString(R.string.connecting_to_printer);
        Globals.BLUETOOTH_STATE = state;
        if (isBluetoothEnabled()) {
            if (!isBluetoothDiscovering()) {
                if (!isFinishing()) {
                    progressDialog = ProgressDialog.show(context, "",
                            getString(R.string.connecting_to_printer));
                }
                tscConnected = false;
                timer = new Timer();
                handler = new Handler() {
                };
                time = Constants.connAttemptsTym;
                timerTask = new TimerTask() {

                    @Override
                    public void run() {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (!Globals.BLUETOOTH_STATE
                                        .equalsIgnoreCase(getString(R.string.connecting_to_printer))) {
                                    boolean deviceConnected = isDeviceConnected();
                                    Utils.logD("" + deviceConnected);
                                    if (deviceConnected) {
                                        if (!tscConnected) {
                                            tscConnected = true;
                                            Globals.selectedPrint = true;
                                            Globals.isPrint = true;
                                            Globals.isPause = true;
                                            if (!isPrinting) {
                                                new PrintInBackground().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                                isPrinting = true;
                                            }
                                            timerTask.cancel();
                                            timer.cancel();
                                            timer.purge();
                                        }
                                        time += Constants.maxConnAttemptsTym;
                                    }
                                    if (time > Constants.maxConnAttemptsTym) {
                                        if (!isFinishing()) {
                                            if (progressDialog != null) {
                                                progressDialog.dismiss();
                                                progressDialog = null;
                                            }
                                        }
                                        timer.cancel();
                                    } else
                                        time = time * 2;
                                }
                            }
                        });
                    }

                };
                timer.schedule(timerTask, Constants.PRINT_SLEEP_TIME, time);
                Globals.BLUETOOTH_STATE = context.getResources()
                        .getString(R.string.connect_devices);
            }
        }
    }

    public void printTSCLabel(String label) {
        Utils.logD("printTSCLabel");
        Utils.logD(label);
        Utils.logD(selectedDevice);
        TscDll.setup(74, 51, 4, 8, 0, 65, 0);
        TscDll.nobackfeed();
        TscDll.clearbuffer();
        String cmd = TscDll.sendcommandUTF8(label);
        Utils.logD(cmd);
    }

    class PrintInBackground extends AsyncTask {

        @Override
        protected void onPreExecute() {
            Utils.logD("Printing Starting");
        }

        @Override
        protected Object doInBackground(Object... arg0) {
            try {
                boolean cancel = false;

                String label = PrinterLabels.getTSCFormattedLabel(input, shipAddress);
                printTSCLabel(label);
                try {
                    Thread.sleep(Constants.PRINT_SLEEP_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (cancel) {
                    Utils.logD("Printing was CANCELLED2");
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.logD("Printing FINISHED");
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            TscDll.closeport();

            if(Utils.getConnectivityStatus(context)) {
                new SubmitTask(input).execute();
            }

            super.onPostExecute(result);
        }
    }

    private void validateAndSubmit() {
        String masterBoxNum = masterBoxNumEt.getText().toString().trim();
        if (Utils.isValidString(masterBoxNum)) {
            selectedList.clear();
            selectedList = new ArrayList<>();
            if (mChecked.size() > 0) {
                for (int i = 0; i < count; i++) {
                    if (mChecked.get(i)) {
                        if (partsList.get(i).getPrimarCartonsPacked() <= partsList.get(i).getRemainingPrimaryCartons()) {
                            selectedList.add(partsList.get(i));
                        } else {
                            Utils.showSimpleAlert(context, getString(R.string.alert), "Can't Pack more than the Remaining Parts");
                            return;
                        }
                    }
                }
            }

            if (Utils.isValidArrayList((ArrayList<?>) selectedList)) {
                Utils.logD(selectedList.toString());
                input = new PackingSubmitJson();
                input.setMasterBoxNo(masterBoxNum);
                input.setOrderNo(orderNum);
                input.setSubproject(subProjectId);
                input.setPartList(selectedList);
                validateAndPrint();
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select Atleast one Part");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Enter Valid Master Box Number");
        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int itemCount = count;
            for (int i = 0; i < itemCount; i++) {
                mChecked.put(i, selectAllCb.isChecked());
            }
            adapter.notifyDataSetChanged();
        }
    };

    private void setData() {
        partsList = dataSource.packingParts.getPackingData(subProjectId, orderNum);
        adapter = new PartsAdapter((ArrayList<PackingModel>) partsList);
        partsRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.DataObjectHolder> {
        private ArrayList<PackingModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView partNumTv, quantityTv, remainingCartonsTv;
            EditText packedCartonsEt;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);

                partNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                quantityTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                packedCartonsEt = (EditText) itemView.findViewById(R.id.et_packed_primary_cartons);
                remainingCartonsTv = (TextView) itemView.findViewById(R.id.tv_remaining_primary_cartons);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                packedCartonsEt.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        int pos = (int) packedCartonsEt.getTag();
                        String str = editable.toString();
                        if (Utils.isValidString(str)) {
                            list.get(pos).setPrimarCartonsPacked(Integer.parseInt(str));
                        }
                    }
                });
            }
        }

        public PartsAdapter(ArrayList<PackingModel> myDataset) {
            list = myDataset;
        }

        @Override
        public PartsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_packing_detail_item, parent, false);
            return new PartsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartsAdapter.DataObjectHolder holder, final int position) {
            PackingModel model = list.get(position);

            if (model != null) {
                holder.partNumTv.setText(String.valueOf(model.getPartNum()));
                holder.quantityTv.setText(String.valueOf(model.getQuantity()));
                holder.remainingCartonsTv.setText(String.valueOf(model.getRemainingPrimaryCartons()));
                holder.checkBox.setChecked(mChecked.get(position));
                if (mChecked.get(position)) {
                    enableView(holder.packedCartonsEt);
                } else {
                    disableView(holder.packedCartonsEt);
                }

                if (model.getPickStatus() == 1) {
                    holder.partNumTv.setBackground(getResources().getDrawable(R.drawable.table_left_green_bg));
                    holder.quantityTv.setBackground(getResources().getDrawable(R.drawable.table_middle_green_bg));
                    holder.packedCartonsEt.setBackground(getResources().getDrawable(android.R.drawable.edit_text));
                    holder.remainingCartonsTv.setBackground(getResources().getDrawable(R.drawable.table_right_green_bg));
                } else if (model.getPickStatus() == 0) {
                    holder.partNumTv.setBackground(getResources().getDrawable(R.drawable.table_left_black));
                    holder.quantityTv.setBackground(getResources().getDrawable(R.drawable.table_middle_black));
                    holder.packedCartonsEt.setBackground(getResources().getDrawable(android.R.drawable.edit_text));
                    holder.remainingCartonsTv.setBackground(getResources().getDrawable(R.drawable.table_right_black));
                } else {
                    holder.partNumTv.setBackground(getResources().getDrawable(R.drawable.table_left_orange_bg));
                    holder.quantityTv.setBackground(getResources().getDrawable(R.drawable.table_middle_orange_bg));
                    holder.packedCartonsEt.setBackground(getResources().getDrawable(android.R.drawable.edit_text));
                    holder.remainingCartonsTv.setBackground(getResources().getDrawable(R.drawable.table_right_black));
                }
            }
            holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mChecked.put(position, isChecked);
                        enableView(holder.packedCartonsEt);
                        if (isAllValuesChecked()) {
                            selectAllCb.setChecked(true);
                        }
                    } else {
                        mChecked.delete(position);
                        disableView(holder.packedCartonsEt);
                        selectAllCb.setChecked(false);
                    }
                    partsRv.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            });

            holder.partNumTv.setTag(position);
            holder.quantityTv.setTag(position);
            holder.packedCartonsEt.setTag(position);
            holder.remainingCartonsTv.setTag(position);
            holder.checkBox.setTag(position);
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<PackingModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(PackingModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void enableView(View view) {
        view.setAlpha(1.0f);
        view.setClickable(true);
        view.setEnabled(true);
    }

    private void disableView(View view) {
        view.setAlpha(0.5f);
        view.setClickable(false);
        view.setEnabled(false);
    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }
}