package com.apptmyz.splwms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;

import com.apptmyz.splwms.custom.BitmapScalingUtil;
import com.apptmyz.splwms.custom.CameraActivity;
import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.LoadingResponse;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.data.TruckImageModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.apptmyz.splwms.wmsfragments.LoadingScanInProgress;
import com.apptmyz.splwms.wmsfragments.LoadingToBeScanned;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class LoadingScanningScreen extends Activity {

    public LoadingToBeScanned toBeScanned;
    public LoadingScanInProgress scanInProgress;
    private Context context;
    private DataSource dataSource;
    private List<NewProductModel> newProductModelList;
    private RadioGroup radioGroup;
    private FragmentManager fm;
    private Button vehicleNoBtn;
    private TextView weightTv, percentTv;
    private String vehNo, vendorId;
    private String truckImageFileName;
    private ImageView home_iv;
    private RelativeLayout boxCountPopup;
    private EditText boxCountEt;
    private TextView productNoTv, scannedCountTv;
    private Button boxSubmitBtn, addBtn, subtractBtn, reconcileBtn;
    private RelativeLayout imageLayout;
    private Button captureBtn, sendBtn;
    private TextView msgTv;
    public Gson gson = new Gson();
    public final int TAKE_PHOTO = 1;
    private ImageView truckIv;
    public final int CROP_IMAGE = 2;
    private CounterView counterView;


    private int maxCount = -1,
            minCount = -1;
//    View.OnClickListener listener = new View.OnClickListener() {
//
//
//        @Override
//        public void onClick(View view) {
//            switch (view.getId()) {
//
//
//                case R.id.substract_btn:
//                    String count1 = boxCountEt.getText().toString();
//                    if (Utils.isValidString(count1)) {
//
//                        int i1 = Integer.parseInt(count1) - 1;
//                        if (i1 >= minCount) {
//                            boxCountEt.setText(Integer.toString(i1));
//                        }
//                    }
//                    break;
//
//                case R.id.add_btn:
//                    String count2 = boxCountEt.getText().toString();
//                    if (Utils.isValidString(count2)) {
//                        int i2 = Integer.parseInt(count2) + 1;
//                        if (maxCount > 0 && i2 <= maxCount) {
//                            boxCountEt.setText(Integer.toString(i2));
//                        }
//                    }
//                    break;
//
//                case R.id.btn_ok:
//
//                    int count3 = counterView.getValue();
//
////                    String count3 = boxCountEt.getText().toString();
//                    if (count3 != 0) {
//
////                    if (Utils.isValidString(count3)) {
////                        int i3 = Integer.parseInt(count3);
////                        int tot = i3 + minCount;
////                        if (tot >= minCount && tot <= maxCount) {
//                        if (count3 >= minCount && count3 <= maxCount) {
//
//                            String partNo = productNoTv.getText().toString();
//                            dataSource.loadingParts.updatePartScan(context, Globals.vehicleNo, (count3), partNo);
//                            boxCountPopup.setVisibility(View.GONE);
//                            Utils.dissmissKeyboard(boxCountEt);
//                        } else {
//                            Utils.showToast(context, getString(R.string.alert_box));
//                        }
//                    } else {
//                        Utils.showToast(context, getString(R.string.alert_box));
//                    }
//                    break;
//
//                case R.id.btn_reconcile:
//
//                    Intent intent = new Intent(context, LoadingReconcileScreen.class);
////                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.putExtra(Constants.VEHICLE_NUMBER, vehNo);
//                    intent.putExtra(Constants.VENDOR_ID, vendorId);
//                    startActivity(intent);
//                    finish();
//
//                    break;
//
//                case R.id.home_iv:
//                    goToHome();
//                    break;
//
//
//                case R.id.btn_capture_image:
//                    truckImageFileName = "";
//                    takePicture(context);
//                    break;
//                case R.id.btn_send_image:
//
//
//                    if (Utils.isValidString(truckImageFileName)) {
//                        Utils.logE("in LT image");
//
//                        String text = "";
//
////                        String time = Globals.selectedSheet.getSheetDate();
////                        if (Utils.isValidString(time))
////                            text = text + "\n" + "Date# : " + time;
//                        String truck = Globals.vehicleNo;
//                        if (Utils.isValidString(truck)) {
//                            text = text + "\n" + "Veh#  : " + truck;
//                        }
//
//                        String s = dataSource.sharedPreferences
//                                .getValue(Constants.IMEI);
//                        if (Utils.isValidString(s))
//                            text = text + "\n" + "T-ID# : " + s;
//
//                        String user = dataSource.sharedPreferences
//                                .getValue(Constants.USERNAME_PREF);
//                        if (Utils.isValidString(user))
//                            text = text + "\n" + "U-ID# : " + user;
//
//                        text = text + "\n" + "I-DT# : "
//                                + Utils.getITime(Long.valueOf(truckImageFileName));
//
//                        imageLayout.setVisibility(View.GONE);
//                        TruckImageModel model = new TruckImageModel();
//
//                        model.setVehTxnDate(Utils.getScanTimeLoading());
//                        model.setLoadUnloadFlag(Constants.LOADING_STARTED);
//
//                        model.setVehicleNo(Globals.vehicleNo);
//
//
//                        ImageLoader loader = new ImageLoader(context, Constants.TRUCK);
//                        try {
//                            String base64 = loader.fileCache.getBase64StringToName(
//                                    truckImageFileName, text, context);
//                            List<String> list = new ArrayList<String>();
//                            if (Utils.isValidString(base64)) {
//                                list.add(base64);
//                                model.setImages(list);
//                                Utils.logD(model.toString());
//                                new SendStackingImage().execute(model);
//                            }
//                        } catch (Throwable e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//                        Utils.logE("in else ");
//
//
//                        Utils.showSimpleAlert(
//                                context,
//                                getString(R.string.alert_dialog_title),
//                                "Please capture stacking image");
//
//                    }
//
//                    break;
//            }
//        }
//    };
    private LinearLayout toBeScannedLayout, scanInProgressLayout,
            scanCompletedLayout, exceptionLayout, topFrags;
//    private RadioGroup.OnCheckedChangeListener changeListener = new RadioGroup.OnCheckedChangeListener() {
//        @Override
//        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
//            resetAllFragmentData();
//            switch (i) {
//                case R.id.rb_tobescanned:
//                    showView(toBeScannedLayout);
//                    hideView(scanInProgressLayout);
//                    hideView(scanCompletedLayout);
//                    hideView(exceptionLayout);
//                    break;
//
//                case R.id.rb_recentscanned:
//                    hideView(toBeScannedLayout);
//                    showView(scanInProgressLayout);
//                    hideView(scanCompletedLayout);
//                    hideView(exceptionLayout);
//                    break;
//
//                case R.id.rb_scancompleted:
//                    hideView(toBeScannedLayout);
//                    hideView(scanInProgressLayout);
//                    showView(scanCompletedLayout);
//                    hideView(exceptionLayout);
//                    break;
//
//                case R.id.rb_exception:
//                    hideView(toBeScannedLayout);
//                    hideView(scanInProgressLayout);
//                    hideView(scanCompletedLayout);
//                    showView(exceptionLayout);
//                    break;
//
//
//                default:
//                    break;
//            }
//        }
//    };

    private void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    private void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.loading_scanning_screen);

//        context = LoadingScanningScreen.this;
//
//        dataSource = new DataSource(context);
//
//        fm = getFragmentManager();
//
//
//        toBeScannedLayout = (LinearLayout) findViewById(R.id.tobe_scanned_layout);
//        scanInProgressLayout = (LinearLayout) findViewById(R.id.sca_inprogress_layout);
//        scanCompletedLayout = (LinearLayout) findViewById(R.id.scan_completed_layout);
//        exceptionLayout = (LinearLayout) findViewById(R.id.exception_layout);
//
//        vehicleNoBtn = (Button) findViewById(R.id.btn_sheet);
//
//
////        if (Utils.isValidString(Globals.vehicleNo))
////            vehicleNoBtn.append(Globals.vehicleNo);
//
//
//        radioGroup = (RadioGroup) findViewById(R.id.rg_display_type);
//        radioGroup.setOnCheckedChangeListener(changeListener);
//
//        weightTv = (TextView) findViewById(R.id.tv_weight);
//        percentTv = (TextView) findViewById(R.id.tv_percentage);
//
//        boxCountPopup = (RelativeLayout) findViewById(R.id.box_count_popup);
//
//        boxCountEt = (EditText) findViewById(R.id.et_box_count);
//        productNoTv = (TextView) findViewById(R.id.tv_product_number);
//        scannedCountTv = (TextView) findViewById(R.id.tv_scanned_count);
//
//        addBtn = (Button) findViewById(R.id.add_btn);
//        addBtn.setOnClickListener(listener);
//
//        subtractBtn = (Button) findViewById(R.id.substract_btn);
//        subtractBtn.setOnClickListener(listener);
//
//        boxSubmitBtn = (Button) findViewById(R.id.btn_ok);
//        boxSubmitBtn.setOnClickListener(listener);
//
//        reconcileBtn = (Button) findViewById(R.id.btn_reconcile);
//        reconcileBtn.setOnClickListener(listener);
//
//
//        home_iv = (ImageView) findViewById(R.id.home_iv);
//        home_iv.setOnClickListener(listener);
//
//
//        imageLayout = (RelativeLayout) findViewById(R.id.layout_image_popup);
//
//        captureBtn = (Button) findViewById(R.id.btn_capture_image);
//        captureBtn.setOnClickListener(listener);
//
//        sendBtn = (Button) findViewById(R.id.btn_send_image);
//        sendBtn.setOnClickListener(listener);
//
//        truckIv = (ImageView) findViewById(R.id.iv_truck);
//        truckIv.setOnClickListener(listener);
//
//
//        msgTv = (TextView) findViewById(R.id.tv_msg);
//        msgTv.setText("Before Starting Loading, Take Picture of shipment stacked inside vehicle, Keeping back doors Open");
//
//
//        counterView = (CounterView) findViewById(R.id.counterView);
//
//
//        toBeScanned = (LoadingToBeScanned) fm
//                .findFragmentById(R.id.tobe_scanned_fragment);
//        scanInProgress = (LoadingScanInProgress) fm
//                .findFragmentById(R.id.scan_inprogress_fragment);
//
//
//        Intent intent = getIntent();
//        vehNo = intent.getStringExtra(Constants.VEHICLE_NUMBER);
//        vendorId = String.valueOf(intent.getIntExtra(Constants.VENDOR_ID, 0));
//        Globals.vendorId = vendorId;
//        if (Utils.isValidString(vehNo)) {
//            Globals.vehicleNo = vehNo;
//            vehicleNoBtn.setText(getString(R.string.vehicle_num) + " : " + vehNo);
//            if (dataSource.loadingParts.getPartsData(vehNo))
//                resetAllFragmentData();
//            else
//                new LoadingTask().execute(vendorId, vehNo);
//
//        }

    }

//    public void resetAllFragmentData() {
//        Utils.dismissProgressDialog();
//        int id = radioGroup.getCheckedRadioButtonId();
//        int tag = -1;
//
//        switch (id) {
//            case R.id.rb_tobescanned:
//                tag = Constants.toBeScannedTag;
//                break;
//
//            case R.id.rb_recentscanned:
//                tag = Constants.scanInProgressTag;
//                break;
//
//            case R.id.rb_scancompleted:
//                tag = Constants.scanCompletedTag;
//                break;
//
//            case R.id.rb_exception:
//                tag = Constants.exceptionTag;
//                break;
//
//            default:
//                break;
//        }
//
//        dataSource.loadingParts.getScanConsList(context, tag, false);
////        if (tag == Constants.scanInProgressTag) {
////            ArrayList<String> l = dataSource.scanList
////                    .getScanList(Globals.selectedSheet.getSheetNo());
////            if (Utils.isValidArrayList(l))
////                scanInProgress.resetRecentScanned(l);
//
////        }
//        dataSource.loadingParts.getScannedQty(Globals.vehicleNo);
////        weightTv.setText("Scanned Wt/Total Wt : " + scannedWt + "/" + totalWt); // + " , Percentage : " + formattedPercent);
//
//    }
//
//    public void showBoxCountPopup(int scannedCount, String productNo, int totPackets) {
//
//        scannedCountTv.setText(Integer.toString(scannedCount));
//        boxCountPopup.setVisibility(View.VISIBLE);
//        productNoTv.setText(productNo);
//        boxCountEt.setText("");
//        maxCount = totPackets;
//        minCount = scannedCount;
//        counterView.setmMaxLimit(totPackets);
//        counterView.setmMinLimit(scannedCount);
//        counterView.setValue(scannedCount);
//
//
//    }
//
//
//    @Override
//    public boolean onKeyUp(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            // if (depsLayout.getVisibility() == View.VISIBLE) {
//            // return false;
//            // } else {
//            // if (topFrags.getVisibility() == View.GONE
//            // || bottomFrags.getVisibility() == View.GONE)
//            // showAllFrags();
//            // else
//            // goBack();
//            // // else
//            // // showConfirmToBack();
//            // return true;
//            // }
//            if (boxCountPopup.getVisibility() == View.VISIBLE) {
//                boxCountPopup.setVisibility(View.GONE);
//                return false;
//            } else
//                goBack();
//        }
//        return true;
//    }
//
//    private void goBack() {
//        finish();
//    }
//
//    private void goToHome() {
//        Intent intent = new Intent(context, HomeActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//    }
////    @Override
////    protected void onResume() {
////
////        resetAllFragmentData();
////
////        super.onResume();
////    }
//
//    ProgressDialog unloadingDialog;
//    AlertDialog errDlg;
//
//
//    class LoadingTask extends AsyncTask<String, Object, Object> {
//        LoadingResponse loadingResponse;
//        String vehNo = "";
//
//        @Override
//        protected void onPreExecute() {
//            unloadingDialog = Utils.getProgressDialog(context);
//            Globals.lastErrMsg = "";
//        }
//
//        @Override
//        protected Object doInBackground(String... params) {
//
//            try {
//                Globals.lastErrMsg = "";
//
//                String vendorId = params[0];
//                vehNo = params[1];
//
//                String url = WmsUtils.getLoadingDataUrlTag(vendorId, vehNo);
//
//                loadingResponse = (LoadingResponse) HttpRequest
//                        .getInputStreamFromUrl(url, LoadingResponse.class, context);
//                if (loadingResponse != null) {
//                    Utils.logD(loadingResponse.toString());
//                    if (loadingResponse.getStatus()) {
//                        if (!Utils.isValidArrayList((ArrayList<?>) loadingResponse.getData()))
//                            Globals.lastErrMsg = Constants.NO_DATA_AVAILABLE;
//                    } else {
//                        Globals.lastErrMsg = loadingResponse.getMessage();
//                    }
//                }
//
//            } catch (Exception e) {
//                if (!Utils.isValidString(Globals.lastErrMsg))
//                    Globals.lastErrMsg = e.toString();
//            }
//            return null;
//
//        }
//
//        @Override
//        protected void onPostExecute(Object result) {
//            if (!isFinishing() && unloadingDialog != null && unloadingDialog.isShowing())
//                unloadingDialog.dismiss();
//            if (showErrorDialog()) {
//                if (Utils.isValidArrayList((ArrayList<?>) loadingResponse.getData())) {
//                    dataSource.loadingParts.saveData(context, loadingResponse, vehNo);
//                }
//            }
//
//            super.onPostExecute(result);
//        }
//
//    }
//
//    private boolean showErrorDialog() {
//        boolean isNotErr = true;
//        boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
//        if (isLogout)
//            dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);
//
//        try {
//            if (!isFinishing()) {
//                if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
//
//                    errDlg = new AlertDialog.Builder(context).create();
//                    errDlg.setTitle(getString(R.string.alert_dialog_title));
//                    errDlg.setCancelable(false);
//                    errDlg.setMessage(Globals.lastErrMsg);
//                    Globals.lastErrMsg = "";
//                    errDlg.setButton(getString(R.string.ok_btn),
//                            new DialogInterface.OnClickListener() {
//
//                                @Override
//                                public void onClick(DialogInterface dialog,
//                                                    int which) {
//
//
//                                    errDlg.dismiss();
//                                    finish();
//                                }
//                            });
//
//
//                    Utils.dismissProgressDialog();
//                    isNotErr = false;
//                    errDlg.show();
//                }
//            }
//        } catch (Exception e) {
//            Utils.logE(e.toString());
//        }
//        return isNotErr;
//    }
//
//
//    class SendStackingImage extends AsyncTask<TruckImageModel, Object, Object> {
//        @Override
//        protected void onPreExecute() {
//            Globals.lastErrMsg = "";
////            showProgressDialog();
//            Utils.getProgressDialog(context);
//        }
//
//        @Override
//        protected Object doInBackground(TruckImageModel... params) {
//
//            try {
//
//                Globals.lastErrMsg = "";
//
//                TruckImageModel model = params[0];
//                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
//                String url = WmsUtils.getImageSubmitUrlTag(warehouseCode);
//
//                String data = gson.toJson(model);
//
//                GeneralResponse response = (GeneralResponse) HttpRequest
//                        .postData(url, data, GeneralResponse.class, context);
//                if (response != null) {
//                    if (response.isStatus()) {
//                        Utils.logD(response.toString());
//
//
//                    } else {
//                        Globals.lastErrMsg = response.getMessage();
//                    }
//                }
//
//            } catch (Exception e) {
//                if (!Utils.isValidString(Globals.lastErrMsg))
//                    Globals.lastErrMsg = e.toString();
//            }
//            return null;
//
//        }
//
//        @Override
//        protected void onPostExecute(Object result) {
//            Utils.dismissProgressDialog();
//            if (showErrorDialog()) { // && pDialog != null && pDialog.isShowing()) {
//
//                if (Globals.unloadingResponse != null && Utils.isValidArrayList((ArrayList<?>) Globals.unloadingResponse.getData().getPartList())) {
//                    dataSource.unloadingParts.saveData(context, Globals.unloadingResponse, vehNo);
//                }
//
//            }
//
//
//            super.onPostExecute(result);
//        }
//
//    }
//
//
//    private void takePicture(Context context) {
//        // Uri imageUri = Uri.fromFile(getTempFile(context));
//        // Intent intent = createIntentForCamera(imageUri);
//        // startActivityForResult(intent, TAKE_PHOTO);
//        Utils.logD("*");
//        Intent intent = new Intent(context, CameraActivity.class);
//        startActivityForResult(intent, TAKE_PHOTO);
//    }
//
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Bitmap bitmap = null;
//        switch (requestCode) {
//
//
//            case TAKE_PHOTO:
//                try {
//                    Utils.logD("**");
//
//                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
//                    String imageUri = uri.toString();
//                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
//                            Uri.parse(imageUri));
//                    if (bitmap != null) {
//                        int w = bitmap.getWidth();
//                        int h = bitmap.getHeight();
//                        Matrix mat = new Matrix();
//                        if (w > h)
//                            mat.postRotate(90);
//
//                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
//                        Globals.bitmap = bitmap;
//
//                        String diff = dataSource.sharedPreferences
//                                .getValue(Constants.TIME_DIFF);
//                        long d = 0;
//                        if (Utils.isValidString(diff))
//                            d = Long.valueOf(diff);
//
//                        addImage(bitmap, Utils.getImageTime(d), false);
//                    }
//                } catch (Exception e) {
//
//                }
//
//                break;
//        }
//
//    }
//
//    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
//        Utils.logD("***");
//
//        if (truckIv != null && bitmap != null) {
////        if (imageAdapter != null && imageGallery != null && bitmap != null) {
//            Utils.logD("*****");
//
//
//            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
//            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
//                    imageFileName);
//            if (uri != null) {
//
//
//                if (truckIv.getVisibility() == View.GONE || truckIv.getVisibility() == View.INVISIBLE)
//                    truckIv.setVisibility(View.VISIBLE);
//
//                truckImageFileName = imageFileName;
//                truckIv.setImageBitmap(bitmap);
//            }
//
//        }
//    }


}
