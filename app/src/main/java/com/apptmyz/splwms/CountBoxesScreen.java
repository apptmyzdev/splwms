package com.apptmyz.splwms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.apptmyz.splwms.data.CountBoxesModel;
import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CountBoxesScreen extends Activity {

    private ListView listView;
    private EditText productNoEt, boxCountEt;
    private Button okBtn, addBtn, substractBtn;
    private TextView titleTv;
    private Context context;
    private ImageView scanIv;
    private HashMap<String, CountBoxesModel> map;
    private List<CountBoxesModel> list;
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btn_ok:
                    String product = productNoEt.getText().toString();
                    int boxCOunt = Integer.parseInt(boxCountEt.getText().toString());

                    map.get(product).setScanned(true);
                    map.get(product).setBoxCount(boxCOunt);

                    setData(1);

                    boxCountEt.setText("");
                    productNoEt.setText("");
                    productNoEt.requestFocus();
                    productNoEt.setFocusable(true);
                    productNoEt.setFocusableInTouchMode(true);


                    break;
                case R.id.scanner_iv:
                    productNoEt.requestFocus();
                    productNoEt.setFocusable(true);
                    productNoEt.setFocusableInTouchMode(true);
                    productNoEt.setText("");
                    boxCountEt.setText("");

                    break;

                case R.id.add_btn:
                    String count = boxCountEt.getText().toString();
                    int i = Integer.parseInt(count) + 1;
                    boxCountEt.setText(Integer.toString(i));
                    break;

                case R.id.substract_btn:
                    String count1 = boxCountEt.getText().toString();
                    int i1 = Integer.parseInt(count1) - 1;
                    boxCountEt.setText(Integer.toString(i1));
                    break;

                case R.id.rb_tobescanned:
                    setData(1);
                    break;
                case R.id.rb_recentscanned:
                    setData(2);
                    break;

                case R.id.rb_scancompleted:
                    setData(2);
                    break;

                case R.id.home_iv:
                    goBack();
                    break;
            }

        }
    };
    private List<CountBoxesModel> tobeScannedList;
    private ImageView home_iv;
    private RadioButton tobeScannedRb, recentRb, exceptionRb, scanCompletedRb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.count_boxes_screen);

        context = CountBoxesScreen.this;

        listView = (ListView) findViewById(R.id.listview);
        scanIv = (ImageView) findViewById(R.id.scanner_iv);
        scanIv.setOnClickListener(onClickListener);

        okBtn = (Button) findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(onClickListener);

        addBtn = (Button) findViewById(R.id.add_btn);
        addBtn.setOnClickListener(onClickListener);


        substractBtn = (Button) findViewById(R.id.substract_btn);
        substractBtn.setOnClickListener(onClickListener);

        tobeScannedRb = (RadioButton) findViewById(R.id.rb_tobescanned);
        tobeScannedRb.setOnClickListener(onClickListener);

        recentRb = (RadioButton) findViewById(R.id.rb_recentscanned);
        recentRb.setOnClickListener(onClickListener);

        scanCompletedRb = (RadioButton) findViewById(R.id.rb_scancompleted);
        scanCompletedRb.setOnClickListener(onClickListener);

        exceptionRb = (RadioButton) findViewById(R.id.rb_exception);
        exceptionRb.setOnClickListener(onClickListener);

        home_iv = (ImageView) findViewById(R.id.home_iv);
        home_iv.setOnClickListener(onClickListener);



        titleTv = (TextView) findViewById(R.id.title_tv);
        titleTv.setText("Scan Part");

        productNoEt = (EditText) findViewById(R.id.et_input_packet);
        productNoEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    String productNo = productNoEt.getText().toString();
                    if (productNo.length() == 9) {
                        int boxCount = map.get(productNo).getBoxCount();
                        boxCountEt.setText(Integer.toString(boxCount));
                        Utils.dissmissKeyboard(productNoEt);
                        Utils.dissmissKeyboard(boxCountEt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        boxCountEt = (EditText) findViewById(R.id.et_box_count);

        Utils.dissmissKeyboard(productNoEt);
        Utils.dissmissKeyboard(boxCountEt);

        addData();
    }

    private void addData() {
        map = new HashMap<>();
        list = new ArrayList<>();
        tobeScannedList = new ArrayList<>();
        map.put("567349293", new CountBoxesModel("", 45, "567349293", false, false));
        map.put("567349294", new CountBoxesModel("", 300, "567349294", false, false));
        map.put("567349295", new CountBoxesModel("", 12, "567349295", false, false));
        map.put("567349296", new CountBoxesModel("", 8, "567349296", false, false));
        map.put("567349297", new CountBoxesModel("", 34, "567349297", false, false));
        map.put("567349298", new CountBoxesModel("", 98, "567349298", false, false));
        map.put("567349299", new CountBoxesModel("", 56, "567349299", false, false));
        map.put("567349300", new CountBoxesModel("", 320, "567349300", false, false));
        map.put("567349301", new CountBoxesModel("", 10, "567349301", false, false));

        tobeScannedList.add(new CountBoxesModel("", 45, "567349293", false, false));
        tobeScannedList.add(new CountBoxesModel("", 300, "567349294", false, false));
        tobeScannedList.add(new CountBoxesModel("", 12, "567349295", false, false));
        tobeScannedList.add(new CountBoxesModel("", 8, "567349296", false, false));
        tobeScannedList.add(new CountBoxesModel("", 34, "567349297", false, false));
        tobeScannedList.add(new CountBoxesModel("", 98, "567349298", false, false));
        tobeScannedList.add(new CountBoxesModel("", 56, "567349299", false, false));
        tobeScannedList.add(new CountBoxesModel("", 320, "567349300", false, false));
        tobeScannedList.add(new CountBoxesModel("", 10, "567349301", false, false));

        MyAdapter adapter = new MyAdapter(context, R.layout.box_list_item, tobeScannedList);
        listView.setAdapter(adapter);

    }

    private void setData(int flag) {
        List<CountBoxesModel> modelList = new ArrayList<>();
        if (flag == 1) {

            for (Map.Entry<String, CountBoxesModel> entry : map.entrySet()) {
                if (entry.getValue().isScanned()) {
                    list.add(entry.getValue());
                }
            }

            for (CountBoxesModel model : list) {
                map.remove(model.getProduct());
            }


            for (Map.Entry<String, CountBoxesModel> entry : map.entrySet()) {
                Utils.logD(entry.toString());
                modelList.add(entry.getValue());
            }

            MyAdapter adapter = new MyAdapter(context, R.layout.box_list_item, modelList);
            listView.setAdapter(adapter);

        } else if (flag == 2) {
            Utils.logD("testttt" + String.valueOf(list.size()));
            MyAdapter adapter = new MyAdapter(context, R.layout.box_list_item, list);
            listView.setAdapter(adapter);

        } else if (flag == 3) {
            MyAdapter adapter = new MyAdapter(context, R.layout.box_list_item, list);
            listView.setAdapter(adapter);

        } else if (flag == 4) {

        }
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public class MyAdapter extends ArrayAdapter<CountBoxesModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<CountBoxesModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.productNoTv = (TextView) convertView.findViewById(R.id.product_no_tv);
            holder.boxCountTv = (TextView) convertView.findViewById(R.id.box_no_tv);


            CountBoxesModel countBoxesModel = getItem(position);


            String product = countBoxesModel.getProduct();

            if (Utils.isValidString(product))
                holder.productNoTv.setText(product);
            else
                holder.productNoTv.setText("");

            holder.boxCountTv.setText(Integer.toString(countBoxesModel.getBoxCount()));


            return convertView;
        }

    }

    public class ViewHolder {
        public TextView productNoTv;
        public TextView boxCountTv;
    }
}
