package com.apptmyz.splwms;

import android.Manifest;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.BinMasterModel;
import com.apptmyz.splwms.data.BinMasterResponse;
import com.apptmyz.splwms.data.CustomerMasterModel;
import com.apptmyz.splwms.data.CustomerResponse;
import com.apptmyz.splwms.data.CustomersResponse;
import com.apptmyz.splwms.data.ExceptionTypeResponse;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InboundTypeModel;
import com.apptmyz.splwms.data.InboundTypesResponse;
import com.apptmyz.splwms.data.InventoryTypeModel;
import com.apptmyz.splwms.data.InventoryTypesResponse;
import com.apptmyz.splwms.data.InvoiceModel;
import com.apptmyz.splwms.data.NodeMasterModel;
import com.apptmyz.splwms.data.PartMasterModel;
import com.apptmyz.splwms.data.PartMasterResponse;
import com.apptmyz.splwms.data.PermissionModel;
import com.apptmyz.splwms.data.ProductMasterModel;
import com.apptmyz.splwms.data.ProductMasterResponse;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.data.SkipBinReasonsResponse;
import com.apptmyz.splwms.data.SlaNameComponentModel;
import com.apptmyz.splwms.data.SlaResponse;
import com.apptmyz.splwms.data.TcNumbersResponse;
import com.apptmyz.splwms.data.VehicleNumbersResponse;
import com.apptmyz.splwms.data.VendorMasterResponse;
import com.apptmyz.splwms.data.WarehouseMasterModel;
import com.apptmyz.splwms.data.WarehousesResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

public class HomeActivity extends BaseActivity {
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 100;
    public static final int INBOUND_FEATURE_ID = 60;
    public static final int OUTBOUND_FEATURE_ID = 61;
    public static final int INVENTORY_FEATURE_ID = 62;
    public static final int GATE_IN_FEATURE_ID = 63;
    public static final int GATE_OUT_FEATURE_ID = 64;

    GridView grid;
    List<String> list;
    CustomGridViewAdapter customGridAdapter;
    TextView titleTv;
    Context context;
    DataSource dataSource;
    private boolean isManualSync;
    private android.app.AlertDialog alert = null;
    private String selectedDevice;
    private static final String OTG = "OTG";
    private static final String TSC = "TSC";
    public static Intent findDeviceList = null;
    private BluetoothAdapter bluetoothAdapter;
    private int roleId;
    private List<PermissionModel> permissionModels;
    private Gson gson = new Gson();
    private String permissionsStr;
    private HashMap<Integer, PermissionModel> permissionMap;
    private String warehouseCode;
    private boolean isRefreshMasters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_home, frameLayout);

        context = HomeActivity.this;

        dataSource = new DataSource(context);
        if (getIntent().getExtras() != null) {
            isRefreshMasters = getIntent().getBooleanExtra("refreshMasters", false);
        }
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        wareHouseNameTv.setText(warehouseCode + "-" + dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_NAME));
        permissionsStr = dataSource.sharedPreferences.getValue(Constants.PERMISSIONS);
        Type token = new TypeToken<List<PermissionModel>>() {
        }.getType();
        permissionModels = gson.fromJson(permissionsStr, token);
        permissionMap = new HashMap<>();
        if (Utils.isValidArrayList((ArrayList<?>) permissionModels)) {
            for (PermissionModel model : permissionModels) {
                permissionMap.put(model.getPermissionId(), model);
            }
        }

        String roleIdStr = dataSource.sharedPreferences.getValue(Constants.ROLE_ID);
        if (Utils.isValidString(roleIdStr)) {
            roleId = Integer.parseInt(roleIdStr);
        }
        homeIv.setVisibility(View.GONE);

        if (!Utils.isUpdatesCalledToday(context, Constants.CHECK_MASTERS_PREF)) {
            if (Utils.getConnectivityStatus(context)) {
                new GetTokenTask().execute();
            } else {
                openLoginScreen();
                finish();
            }
        } else {
            if (isRefreshMasters) {
                new BinMastersTask().execute();
            } else {
                setData();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                return;
            }
            default:
                break;
        }
    }

    ProgressDialog warehousesDialog;

    class GetWarehousesTask extends AsyncTask {
        WarehousesResponse response = null;

        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String username = dataSource.sharedPreferences.getValue(Constants.USERNAME_PREF);
                String url = WmsUtils.getWarehousesUrl(username);

                Utils.logD("Log 1");
                response = (WarehousesResponse) HttpRequest
                        .getInputStreamFromUrl(url, WarehousesResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                warehousesDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && warehousesDialog != null && warehousesDialog.isShowing()) {
                warehousesDialog.dismiss();
            }
            Utils.updateTimeStamp(context, Constants.CHECK_TOKEN_PREF);

            if (showErrorDialog()) {
                if (response.isStatus()) {
                    List<WarehouseMasterModel> warehouseMasterModels = response.getData();
                    if (Utils.isValidArrayList((ArrayList<?>) warehouseMasterModels)) {
                        if (warehouseMasterModels.size() == 1) {
                            dataSource.sharedPreferences.set(Constants.WAREHOUSE_CODE, warehouseMasterModels.get(0).getWhCode());
                            Toast.makeText(context, "There is Only one Warehouse: " + warehouseMasterModels.get(0).getWhCode()
                                    + ". Setting it as the Warehouse.", Toast.LENGTH_LONG).show();
                        } else {
                            Intent intent = new Intent(context, WarehouseActivity.class);
                            dataSource.sharedPreferences.set(Constants.WAREHOUSES_JSON, gson.toJson(warehouseMasterModels));
                            startActivity(intent);
                        }
                    } else {
                        Toast.makeText(context, "No Warehouses Assigned", Toast.LENGTH_SHORT).show();
                        logout();
                    }
                }
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog tokenDialog;

    class GetTokenTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getTokenUrl();

                Utils.logD("Log 1");
                GeneralResponse response = (GeneralResponse) HttpRequest
                        .getInputStreamFromUrl(url, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                tokenDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && tokenDialog != null && tokenDialog.isShowing()) {
                tokenDialog.dismiss();
            }
            Utils.updateTimeStamp(context, Constants.CHECK_TOKEN_PREF);

            if (showErrorDialog()) {
                syncMasters();
            } else {
                openLoginScreen();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog binsDialog;

    class BinMastersTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getBinMastersUrl(warehouseCode);

                Utils.logD("Log 1");
                BinMasterResponse response = (BinMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, BinMasterResponse.class,
                                context);

                if (response != null) {
                    dataSource.bins.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<BinMasterModel> bins = (ArrayList<BinMasterModel>) response.getData();
                        if (Utils.isValidArrayList(bins)) {
                            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                            dataSource.bins.saveBins(bins, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                binsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && binsDialog != null && binsDialog.isShowing()) {
                binsDialog.dismiss();
            }

            showErrorDialog();

            if (Utils.getConnectivityStatus(context)) {
                new ProductMastersTask().execute();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog productsDialog;

    class ProductMastersTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getProductMastersUrl();

                Utils.logD("Log 1");
                ProductMasterResponse response = (ProductMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, ProductMasterResponse.class,
                                context);

                if (response != null) {
                    dataSource.products.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<ProductMasterModel> products = (ArrayList<ProductMasterModel>) response.getData();
                        if (Utils.isValidArrayList(products)) {
                            dataSource.products.saveProducts(products);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                productsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && productsDialog != null && productsDialog.isShowing()) {
                productsDialog.dismiss();
            }

            showErrorDialog();

            if (Utils.getConnectivityStatus(context)) {
                new PartMastersTask().execute();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog partsDialog;

    class PartMastersTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getPartMastersUrl(warehouseCode);

                Utils.logD("Log 1");
                PartMasterResponse response = (PartMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, PartMasterResponse.class,
                                context);

                if (response != null) {
                    dataSource.parts.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<PartMasterModel> parts = (ArrayList<PartMasterModel>) response.getData();
                        if (Utils.isValidArrayList(parts)) {
                            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                            dataSource.parts.saveParts(parts, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                partsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && partsDialog != null && partsDialog.isShowing()) {
                partsDialog.dismiss();
            }

            showErrorDialog();

            if (Utils.getConnectivityStatus(context)) {
                new SkipBinReasonsTask().execute();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog skipBinDialog;

    class SkipBinReasonsTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSkipBinReasonsUrl(warehouseCode);

                Utils.logD("Log 1");
                SkipBinReasonsResponse response = (SkipBinReasonsResponse) HttpRequest
                        .getInputStreamFromUrl(url, SkipBinReasonsResponse.class,
                                context);

                if (response != null) {
                    dataSource.skipBinReasons.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SkipBinReasonModel> bins = (ArrayList<SkipBinReasonModel>) response.getData();
                        if (Utils.isValidArrayList(bins))
                            dataSource.skipBinReasons.saveReasons(bins, warehouseCode);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                skipBinDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && skipBinDialog != null && skipBinDialog.isShowing()) {
                skipBinDialog.dismiss();
            }
            showErrorDialog();

            if (Utils.getConnectivityStatus(context))
                new ExceptionTypesTask().execute();
            super.onPostExecute(result);
        }
    }

    ProgressDialog vendorDialog;

    class GetVendorMasterTask extends AsyncTask {
        VendorMasterResponse response = null;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                vendorDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getVendorMasterTag();

                response = (VendorMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, VendorMasterResponse.class,
                                context);

                if (response != null) {
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && vendorDialog != null && vendorDialog.isShowing()) {
                vendorDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (Utils.isValidArrayList((ArrayList<?>) response.getData()))
                    dataSource.vendorMaster.saveVendorMaster(response.getData());
                setData();
            }

            if (Utils.getConnectivityStatus(context))
                new ExceptionTypesTask().execute();

            super.onPostExecute(result);
        }
    }

    ProgressDialog exceptionTypeDialog;

    class ExceptionTypesTask extends AsyncTask {
        @Override
        protected Object doInBackground(Object[] objects) {
            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getExceptionTypesUrl(warehouseCode);

                Utils.logD("Log 1");
                ExceptionTypeResponse response = (ExceptionTypeResponse) HttpRequest
                        .getInputStreamFromUrl(url, ExceptionTypeResponse.class,
                                context);

                if (response != null) {
                    dataSource.exceptionTypes.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SkipBinReasonModel> exceptions = (ArrayList<SkipBinReasonModel>) response.getData();
                        if (Utils.isValidArrayList(exceptions))
                            dataSource.exceptionTypes.saveExceptionTypes(exceptions, warehouseCode);
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                exceptionTypeDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && exceptionTypeDialog != null && exceptionTypeDialog.isShowing()) {
                exceptionTypeDialog.dismiss();
            }
            showErrorDialog();
            setData();
            if (Utils.getConnectivityStatus(context))
                new CustomersTask().execute();
            super.onPostExecute(result);
        }
    }

    ProgressDialog customersDialog;

    class CustomersTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                customersDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getPickListCustomersUrl();

                Utils.logD("Log 1");
                CustomersResponse response = (CustomersResponse) HttpRequest
                        .getInputStreamFromUrl(url, CustomersResponse.class,
                                context);

                if (response != null) {
                    dataSource.nodes.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<CustomerMasterModel> data = (ArrayList<CustomerMasterModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.nodes.insertNodeData(data);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && customersDialog != null && customersDialog.isShowing()) {
                customersDialog.dismiss();
            }
            showErrorDialog();
            if (Utils.getConnectivityStatus(context))
                new SlaTask().execute();
            super.onPostExecute(result);
        }
    }

    ProgressDialog slaDialog;

    class SlaTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                slaDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getSlaUrl();

                Utils.logD("Log 1");
                SlaResponse response = (SlaResponse) HttpRequest
                        .getInputStreamFromUrl(url, SlaResponse.class,
                                context);

                if (response != null) {
                    dataSource.sla.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<SlaNameComponentModel> data = (ArrayList<SlaNameComponentModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.sla.saveSlaData(data);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && slaDialog != null && slaDialog.isShowing()) {
                slaDialog.dismiss();
            }
            showErrorDialog();
            if (Utils.getConnectivityStatus(context))
                new InboundTypesTask().execute();
            super.onPostExecute(result);
        }
    }

    ProgressDialog inboundDialog;

    class InboundTypesTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inboundDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getInboundTypesUrl();

                Utils.logD("Log 1");
                InboundTypesResponse response = (InboundTypesResponse) HttpRequest
                        .getInputStreamFromUrl(url, InboundTypesResponse.class,
                                context);

                if (response != null) {
                    dataSource.inboundTypes.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<InboundTypeModel> data = (ArrayList<InboundTypeModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.inboundTypes.saveInboundTypes(data);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inboundDialog != null && inboundDialog.isShowing()) {
                inboundDialog.dismiss();
            }
            showErrorDialog();
            if (Utils.getConnectivityStatus(context))
                new InventoryTypesTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            super.onPostExecute(result);
        }
    }

    ProgressDialog inventoryTypesDialog;

    class InventoryTypesTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inventoryTypesDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getInventoryTypesUrl();

                Utils.logD("Log 1");
                InventoryTypesResponse response = (InventoryTypesResponse) HttpRequest
                        .getInputStreamFromUrl(url, InventoryTypesResponse.class,
                                context);

                if (response != null) {
                    dataSource.inventoryTypes.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<InventoryTypeModel> data = (ArrayList<InventoryTypeModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.inventoryTypes.saveInventoryTypes(data);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inventoryTypesDialog != null && inventoryTypesDialog.isShowing()) {
                inventoryTypesDialog.dismiss();
            }
            showErrorDialog();
            if (Utils.getConnectivityStatus(context)) {
                new VehicleNumberTask().execute();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog vehicleNumsDialog;

    class VehicleNumberTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                vehicleNumsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getVehicleNumbersUrl(warehouseCode);

                Utils.logD("Log 1");
                VehicleNumbersResponse response = (VehicleNumbersResponse) HttpRequest
                        .getInputStreamFromUrl(url, VehicleNumbersResponse.class,
                                context);

                if (response != null) {
                    dataSource.vehicleNumbers.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<String> data = (ArrayList<String>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                            dataSource.vehicleNumbers.saveVehicleNumbers(data, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && vehicleNumsDialog != null && vehicleNumsDialog.isShowing()) {
                vehicleNumsDialog.dismiss();
            }
            showErrorDialog();
            if (Utils.getConnectivityStatus(context)) {
                new TcNumberTask().execute();
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog tcNumbersDialog;

    class TcNumberTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                tcNumbersDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getTcNumbersUrl(warehouseCode);

                Utils.logD("Log 1");
                TcNumbersResponse response = (TcNumbersResponse) HttpRequest
                        .getInputStreamFromUrl(url, TcNumbersResponse.class,
                                context);

                if (response != null) {
                    Utils.updateTimeStamp(context, Constants.CHECK_MASTERS_PREF);
                    dataSource.tcNumbers.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<String> data = (ArrayList<String>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.tcNumbers.saveTcNumbers(data, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && tcNumbersDialog != null && tcNumbersDialog.isShowing()) {
                tcNumbersDialog.dismiss();
            }
            showErrorDialog();
            super.onPostExecute(result);
        }
    }

    private void openLoginScreen() {
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
    }

    private void setData() {
        list = new ArrayList<String>();
        grid = (GridView) findViewById(R.id.gv_menu);
        titleTv = (TextView) findViewById(R.id.tv_top_title);

        if (permissionMap != null) {
            if (permissionMap.containsKey(GATE_IN_FEATURE_ID) && permissionMap.get(GATE_IN_FEATURE_ID).getAccessType() == 1) {
                list.add(Constants.GATE_IN_STR);
            }

            if (permissionMap.containsKey(INBOUND_FEATURE_ID) && permissionMap.get(INBOUND_FEATURE_ID).getAccessType() == 1) {
                list.add(Constants.UNLOADING);
                list.add(Constants.INBOUND_INSPECTION);
                list.add(Constants.PUTAWAY);
            }
            if (permissionMap.containsKey(OUTBOUND_FEATURE_ID) && permissionMap.get(OUTBOUND_FEATURE_ID).getAccessType() == 1) {
                list.add(Constants.PICKLIST);
                list.add(Constants.GENERATE_TC);
                list.add(Constants.DOCUMENT_HANDOVER);
            }

            if (permissionMap.containsKey(INVENTORY_FEATURE_ID) && permissionMap.get(INVENTORY_FEATURE_ID).getAccessType() == 1) {
                list.add(Constants.NORMAL_INVENTORY);
                list.add(Constants.PERPETUAL_INVENTORY);
            }

            if (permissionMap.containsKey(GATE_OUT_FEATURE_ID) && permissionMap.get(GATE_OUT_FEATURE_ID).getAccessType() == 1) {
                list.add(Constants.GATE_OUT_STR);
            }

            if (permissionMap.containsKey(INVENTORY_FEATURE_ID) && permissionMap.get(INVENTORY_FEATURE_ID).getAccessType() == 1) {
                list.add(Constants.BIN_TO_BIN_TRANSFER);
            }
            if ((permissionMap.containsKey(INBOUND_FEATURE_ID) && permissionMap.get(INBOUND_FEATURE_ID).getAccessType() == 1)
                    || (permissionMap.containsKey(OUTBOUND_FEATURE_ID) && permissionMap.get(OUTBOUND_FEATURE_ID).getAccessType() == 1)
                    || (permissionMap.containsKey(INVENTORY_FEATURE_ID) && permissionMap.get(INVENTORY_FEATURE_ID).getAccessType() == 1)) {
                list.add(Constants.SEARCH_BIN_PART);
                list.add(Constants.SYNC_MASTERS);
                list.add(Constants.CHANGE_WAREHOUSE);
            }
        }
//        list.add("Setup Printer");
        list.add(Constants.LOGOUT);
        list.add(Constants.DATABASE);

        customGridAdapter = new CustomGridViewAdapter(this, R.layout.layout_home_menu_item, (ArrayList<String>) list);
        grid.setAdapter(customGridAdapter);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String action = (String) parent.getItemAtPosition(position);
                openActivity(action);
            }
        });

        String imeiStr = dataSource.sharedPreferences.getValue(Constants.IMEI);
        if (!Utils.isValidString(imeiStr)) {
            String imei = "";
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                imei = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            } else {
                TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
                    }
                    return;
                }
                imei = telephonyManager.getDeviceId();

                if (!Utils.isValidString(imei))
                    imei = Settings.Secure.getString(getContentResolver(),
                            Settings.Secure.ANDROID_ID);
            }
            if (Utils.isValidString(imei)) {
                dataSource.sharedPreferences.set(Constants.IMEI, imei);
            }
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(HomeActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
    }

    private void openActivity(String action) {
        Intent intent = null;
        switch (action) {
            case Constants.GATE_IN_STR:
                intent = new Intent(context, GateInActivity.class);
                break;
            case Constants.UNLOADING:
                intent = new Intent(context, UnloadingScreen.class);
                break;
            case Constants.INBOUND_INSPECTION:
                intent = new Intent(context, InboundInspectionActivity.class);
                break;
            case Constants.PUTAWAY:
                intent = new Intent(context, PutAwayActivity.class);
                break;
            case Constants.PICKLIST:
                intent = new Intent(context, PickingTypeActivity.class);
                break;
            case Constants.DOCUMENT_HANDOVER:
                intent = new Intent(context, DocumentHandoverActivity.class);
                break;
            case Constants.NORMAL_INVENTORY:
                intent = new Intent(context, NormalInventoryActivity.class);
                break;
            case Constants.PERPETUAL_INVENTORY:
                intent = new Intent(context, NormalInventoryActivity.class);
                intent.putExtra("perpetual", true);
                break;
            case Constants.GENERATE_TC:
                intent = new Intent(context, GenerateTcActivity.class);
                break;
            case Constants.GATE_OUT_STR:
                intent = new Intent(context, GateOutActivity.class);
                break;
            case Constants.BIN_TO_BIN_TRANSFER:
                intent = new Intent(context, BinToBinTransferActivity.class);
                break;
            case Constants.SEARCH_BIN_PART:
                intent = new Intent(context, SearchActivity.class);
                break;
            case Constants.SYNC_MASTERS:
                syncMasters();
                break;
            case Constants.LOGOUT:
                showLogoutAlert();
                break;
            case Constants.DATABASE:
                intent = new Intent(context, AndroidDatabaseManager.class);
                break;
            case Constants.CHANGE_WAREHOUSE:
                changeWarehouse();
                break;
            default:
                break;
        }
        if (intent != null)
            startActivity(intent);
    }

    private void changeWarehouse() {
        if (Utils.getConnectivityStatus(context))
            new GetWarehousesTask().execute();
    }

    private void setupPrinter() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(getResources().getString(R.string.whichPrinterTitel));
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.selected_device, null);
        TextView deviceName, deviceMac;
        final Button chooseDevice;

        deviceName = (TextView) view.findViewById(R.id.devicename);
        deviceMac = (TextView) view.findViewById(R.id.devicemac);
        chooseDevice = (Button) view.findViewById(R.id.choosedevice);
        selectedDevice = dataSource.sharedPreferences.getValue(Constants.DEVICE);
        if (selectedDevice != null && selectedDevice.length() != 0) {
            deviceMac.setText(selectedDevice);
        } else if (Boolean.parseBoolean(dataSource.sharedPreferences.getValue(OTG))) {
            String name = "";
            name += TSC;
            deviceName.setText(name + "-" + OTG);
        }
        alertDialog.setView(view);
        chooseDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                startPrinting();
            }
        });
        alert = alertDialog.show();
    }

    public void startPrinting() {
        dataSource.sharedPreferences.set(Constants.isPrinterAvailble, "Y");
        Globals.BLUETOOTH_STATE = "";
        checkBluetoothState();
    }

    public void checkBluetoothState() {
        String state = getResources().getString(R.string.connecting_to_printer);
        Globals.BLUETOOTH_STATE = state;
        if (isBluetoothEnabled()) {
            if (!isBluetoothDiscovering()) {
                startDeviceList();
                Globals.BLUETOOTH_STATE = context.getResources()
                        .getString(R.string.connect_devices);
            }
        }
    }

    public boolean isBluetoothEnabled() {
        Utils.logD("isBluetoothEnabled");
        boolean isEnabled = false;

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        setSelectedDevice(bluetoothAdapter);

        if (bluetoothAdapter == null) {
            Toast.makeText(context,
                    getResources().getString(R.string.bt_not_support),
                    Toast.LENGTH_LONG).show();
        } else if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Constants.REQUEST_ENABLE_BT);
        } else
            isEnabled = true;

        return isEnabled;
    }

    public boolean isBluetoothDiscovering() {
        boolean isDiscovering = false;

        if (bluetoothAdapter.isDiscovering()) {
            Toast.makeText(context,
                    getResources().getString(R.string.bt_enabled),
                    Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivityForResult(enableBtIntent,
                    Constants.REQUEST_DISCOVERABLE_BT);
            isDiscovering = true;
        }

        return isDiscovering;
    }

    public void startDeviceList() {
        findDeviceList = new Intent(context, DeviceList.class);
        startActivityForResult(findDeviceList,
                Constants.REQUEST_PAIRED_DEVICE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case Constants.REQUEST_ENABLE_BT:
                    checkBluetoothState();
                    break;
                case Constants.REQUEST_PAIRED_DEVICE:
                    String device = data.getStringExtra("device");
                    break;
                case Constants.REQUEST_DISCOVERABLE_BT:
                    checkBluetoothState();
                    break;
                default:
                    break;
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void setSelectedDevice(BluetoothAdapter mBtAdapter) {
        if (selectedDevice == null) {
            String dev = "Zebra";
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    if (device != null) {
                        if (device.getAddress().equals(dev)) {
                            selectedDevice = device.toString();
                            dataSource.sharedPreferences.set(Constants.DEVICE, selectedDevice);
                        }
                    }
                }
            }
        }
    }

    private void syncMasters() {
        if (Utils.getConnectivityStatus(context))
            new BinMastersTask().execute();
    }

    private void showLogoutAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(getString(R.string.alert_dialog_title));
        builder.setMessage(getResources().getString(R.string.logout_confirmation));
        builder.setCancelable(false);

        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        logout();
                    }
                });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        if (!isFinishing()) {
            alert.show();
        }
    }

    private void logout() {
        clearLoginPrefs();
        Intent intent = new Intent(context, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void clearLoginPrefs() {
        dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);
    }

    public class CustomGridViewAdapter extends ArrayAdapter<String> {
        Context context;
        int layoutResourceId;
        ArrayList<String> data = new ArrayList<String>();

        public CustomGridViewAdapter(Context context, int layoutResourceId,
                                     ArrayList<String> data) {
            super(context, layoutResourceId, data);
            this.layoutResourceId = layoutResourceId;
            this.context = context;
            this.data = data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            RecordHolder holder = null;

            if (row == null) {
                LayoutInflater inflater = getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);

                holder = new RecordHolder();
                holder.txtTitle = (TextView) row.findViewById(R.id.tv_menu_title);
                row.setTag(holder);
            } else {
                holder = (RecordHolder) row.getTag();
            }

            holder.txtTitle.setText(data.get(position));
            return row;

        }

        class RecordHolder {
            TextView txtTitle;
        }
    }
}
