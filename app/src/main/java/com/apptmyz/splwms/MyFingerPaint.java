package com.apptmyz.splwms;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.apptmyz.splwms.signature.ColorPickerDialog;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@TargetApi(8)
public class MyFingerPaint extends Activity implements
        ColorPickerDialog.OnColorChangedListener {
    final Context context = this;
    RelativeLayout mainLayout;
    private FrameLayout paintView;
    MyView myView;
    private Button done, clear;
    public static Bitmap b1;
    public static String imageBase64Str = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature);

//		Intent intent = getIntent();
//		if (intent != null) {
//			heading = intent.getStringExtra("SCAN");
//		}

        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);

        done = (Button) findViewById(R.id.done);
        done.setEnabled(true);
        clear = (Button) findViewById(R.id.clear);
        paintView = (FrameLayout) findViewById(R.id.paintView);
        setView(null);

        imageBase64Str = "";

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLUE);
        mPaint.setStyle(Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(3);

        mEmboss = new EmbossMaskFilter(new float[]{1, 1, 1}, 0.4f, 6, 3.5f);

        mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);

        clear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                setView(null);
                mPaint = new Paint();
                mPaint.setAntiAlias(true);
                mPaint.setDither(true);
                mPaint.setColor(Color.BLUE);
                mPaint.setStyle(Style.STROKE);
                mPaint.setStrokeJoin(Paint.Join.ROUND);
                mPaint.setStrokeCap(Paint.Cap.ROUND);
                mPaint.setStrokeWidth(3);
                myView.dirty = false;

                mEmboss = new EmbossMaskFilter(new float[]{1, 1, 1}, 0.4f,
                        6, 3.5f);

                mBlur = new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL);
            }
        });

        done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                imageBase64Str = "";
                boolean viewDirty = myView.dirty;
                if (viewDirty == false) {
                    Utils.showSimpleAlert(MyFingerPaint.this, getString(R.string.alert),
                            "Signature is mandatory");
                } else {
                    try {
                        myView.setInfo(null, null);
                        imageBase64Str = getImageBase64(context, myView);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent intent = MyFingerPaint.this.getIntent();
                    intent.putExtra(Constants.SIGNATURE, imageBase64Str);
                    MyFingerPaint.this.setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    private Paint mPaint;
    private MaskFilter mEmboss;
    private MaskFilter mBlur;

    @Override
    public void colorChanged(int color) {
        mPaint.setColor(color);
    }

    public class MyView extends View {

        private static final float MINP = 0.25f;
        private static final float MAXP = 0.75f;

        public boolean dirty = false;
        private Bitmap mBitmap;
        private Canvas mCanvas;
        private Path mPath;
        private Paint mBitmapPaint;

        public Bitmap getmBitmap() {
            return mBitmap;
        }

        public void setmBitmap(Bitmap mBitmap) {
            this.mBitmap = mBitmap;
        }

        public MyView(Context c) {
            super(c);

            mPath = new Path();
            mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }

        @Override
        protected void onDraw(Canvas canvas) {

            canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);

            canvas.drawPath(mPath, mPaint);

        }

        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        private void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        private void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;
            }
        }

        private void touch_up() {
            mPath.lineTo(mX, mY);
            // commit the path to our offscreen
            mCanvas.drawPath(mPath, mPaint);
            // kill this so we don't double draw
            mPath.reset();
            dirty = true;
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }

        public void setInfo(String docketNumber, String deliveredTo) {
            Paint p = new Paint(Paint.ANTI_ALIAS_FLAG);
            p.setTextSize(40);
            p.setColor(Color.BLUE);
            p.setStyle(Style.FILL);
            mCanvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void setView(Drawable drawable) {

        if (myView != null)
            paintView.removeView(myView);

        myView = new MyView(this);

        paintView.addView(myView);

        mPaint = new Paint();

        mPaint.setStyle(Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mainLayout.invalidate();
        mainLayout.refreshDrawableState();

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public String savecolorToSD(Context context, View mycolorView,
                                String imageName) throws IOException {

        mycolorView.setDrawingCacheEnabled(true);
        Bitmap bitmap = mycolorView.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        String saveimageBase64str = Base64.encodeToString(byteArray,
                Base64.NO_WRAP);

        mycolorView.setDrawingCacheEnabled(false);

        return saveimageBase64str;
    }

    public String getImageBase64(Context context, View myCustomImageview)
            throws IOException {

        myCustomImageview.setDrawingCacheEnabled(true);
        Bitmap bitmap = myCustomImageview.getDrawingCache();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 30, stream);

//        showPop(bitmap);

        byte[] byteArray = stream.toByteArray();

        String imageBase64str = Base64
                .encodeToString(byteArray, Base64.NO_WRAP);

        return imageBase64str;

    }

    private void showPop(Bitmap b) {
        ImageView view = new ImageView(context);
        // view.setBackgroundColor(Color.WHITE);
        view.setImageBitmap(b);
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(view);
        alert.setPositiveButton(context.getString(R.string.ok_btn),
                new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alert.show();
    }

}
