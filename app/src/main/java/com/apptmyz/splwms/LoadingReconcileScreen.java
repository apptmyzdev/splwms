package com.apptmyz.splwms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.BitmapScalingUtil;
import com.apptmyz.splwms.custom.CameraActivity;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.LoginResponse;
import com.apptmyz.splwms.data.NewLoadingPartsModel;
import com.apptmyz.splwms.data.PartsListModel;
import com.apptmyz.splwms.data.TruckImageModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class LoadingReconcileScreen extends Activity {

    ProgressDialog submitDialog;
    private Context context;
    private DataSource dataSource;
    private TextView vehicleNoTv, totalPartsTv, totalBoxesTv, partsScannedTv, partsNotScannedTv;
    private Button submitBtn, rescanBtn;
    private List<NewLoadingPartsModel> partsList;
    private AlertDialog errDlg;
    private String vehNo, vendorId;
    private RelativeLayout imageLayout;
    private Button captureBtn,sendBtn;
    private TextView msgTv;
    public Gson gson = new Gson();
    public final int TAKE_PHOTO = 1;
    private ImageView truckIv;
    public final int CROP_IMAGE = 2;
    private String truckImageFileName;
    private ImageView home_iv;
    private List<PartsListModel> partSumbitList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reconcile_screen);
    }
}
