package com.apptmyz.splwms.data;

import java.util.List;

public class InboundTypesResponse {
    private boolean status;
    private String message;
    private List<InboundTypeModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<InboundTypeModel> getData() {
        return data;
    }

    public void setData(List<InboundTypeModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InboundTypesResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
