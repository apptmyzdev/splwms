package com.apptmyz.splwms.data;

import java.util.List;

public class PackingResponse {
    private boolean status;
    private String message;
    private List<PackingModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PackingModel> getData() {
        return data;
    }

    public void setData(List<PackingModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PackingResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
