package com.apptmyz.splwms.data;

import java.util.List;

public class Part {
    private String productNumber;
    private int quantity;
    private int scannedBoxes;
    private int remainingBoxes;
    private boolean isConfirmed;
    private boolean isQtyCorrect;
    private int actualQty;
    private List<Bin> bins;
    private String putAwayDate;

    public Part() {
    }

    public Part(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getScannedBoxes() {
        return scannedBoxes;
    }

    public void setScannedBoxes(int scannedBoxes) {
        this.scannedBoxes = scannedBoxes;
    }

    public int getRemainingBoxes() {
        return remainingBoxes;
    }

    public void setRemainingBoxes(int remainingBoxes) {
        this.remainingBoxes = remainingBoxes;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public boolean isQtyCorrect() {
        return isQtyCorrect;
    }

    public void setQtyCorrect(boolean qtyCorrect) {
        isQtyCorrect = qtyCorrect;
    }

    public int getActualQty() {
        return actualQty;
    }

    public void setActualQty(int actualQty) {
        this.actualQty = actualQty;
    }

    public List<Bin> getBins() {
        return bins;
    }

    public void setBins(List<Bin> bins) {
        this.bins = bins;
    }

    public String getPutAwayDate() {
        return putAwayDate;
    }

    public void setPutAwayDate(String putAwayDate) {
        this.putAwayDate = putAwayDate;
    }

    @Override
    public String toString() {
        return "Part{" +
                "productNumber='" + productNumber + '\'' +
                ", quantity=" + quantity +
                ", scannedBoxes=" + scannedBoxes +
                ", remainingBoxes=" + remainingBoxes +
                ", isConfirmed=" + isConfirmed +
                ", isQtyCorrect=" + isQtyCorrect +
                ", actualQty=" + actualQty +
                ", bins=" + bins +
                ", putAwayDate='" + putAwayDate + '\'' +
                '}';
    }
}
