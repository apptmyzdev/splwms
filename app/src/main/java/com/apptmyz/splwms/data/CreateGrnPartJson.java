package com.apptmyz.splwms.data;

public class CreateGrnPartJson {
    private int id;
    private String partNo;
    private int partId;
    private int qty;
    private String binNo;
    private String scanTime;
    private String inventoryType;
    private Integer isProduct;
    private Integer productId;
    private String productName;

    public CreateGrnPartJson(int id, String partNo, int partId, int totalBoxes, String binNo, String scanTime, String inventoryType,
                             Integer isProduct, Integer productId, String productName) {
        this.id = id;
        this.partNo = partNo;
        this.partId = partId;
        this.qty = totalBoxes;
        this.binNo = binNo;
        this.scanTime = scanTime;
        this.inventoryType = inventoryType;
        this.isProduct = isProduct;
        this.productId = productId;
        this.productName = productName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(String inventoryType) {
        this.inventoryType = inventoryType;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "CreateGrnPartJson{" +
                "id=" + id +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", qty=" + qty +
                ", binNo='" + binNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", inventoryType='" + inventoryType + '\'' +
                ", isProduct=" + isProduct +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                '}';
    }
}
