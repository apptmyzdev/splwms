package com.apptmyz.splwms.data;

import java.util.ArrayList;

public class Images {

	private ArrayList<String> excpPtkImgs;
	private ArrayList<String> resolvPtkImgs;

	public Images() {
		super();
	}

	public Images(ArrayList<String> excpPtkImgs, ArrayList<String> resolvPtkImgs) {
		super();
		this.excpPtkImgs = excpPtkImgs;
		this.resolvPtkImgs = resolvPtkImgs;
	}

	public ArrayList<String> getExcpPtkImgs() {
		return excpPtkImgs;
	}

	public void setExcpPtkImgs(ArrayList<String> excpPtkImgs) {
		this.excpPtkImgs = excpPtkImgs;
	}

	public ArrayList<String> getResolvPtkImgs() {
		return resolvPtkImgs;
	}

	public void setResolvPtkImgs(ArrayList<String> resolvPtkImgs) {
		this.resolvPtkImgs = resolvPtkImgs;
	}

	@Override
	public String toString() {
		return "Images [excpPtkImgs=" + excpPtkImgs + ", resolvPtkImgs="
				+ resolvPtkImgs + "]";
	}

}
