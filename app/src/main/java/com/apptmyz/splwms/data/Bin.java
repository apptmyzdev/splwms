package com.apptmyz.splwms.data;

import java.util.List;

public class Bin {
    private String partNo;
    private int partId;
    private List<Invoice> invoiceList;
    private int totalBoxes;
    private int totalQty;

    private String binNumber;
    private int noOfBoxesToScan;
    private boolean isPicked;
    private int totalNumOfBoxes;
    private List<Part> parts;
    private String putAwayDate;

    public Bin() {
    }

    public Bin(String binNumber) {
        this.binNumber = binNumber;
    }

    public String getBinNumber() {
        return binNumber;
    }

    public void setBinNumber(String binNumber) {
        this.binNumber = binNumber;
    }

    public String getProductNumber() {
        return partNo;
    }

    public void setProductNumber(String productNumber) {
        this.partNo = productNumber;
    }

    public int getNoOfBoxesToScan() {
        return noOfBoxesToScan;
    }

    public void setNoOfBoxesToScan(int noOfBoxesToScan) {
        this.noOfBoxesToScan = noOfBoxesToScan;
    }

    public int getTotalNumOfBoxes() {
        return totalNumOfBoxes;
    }

    public void setTotalNumOfBoxes(int totalNumOfBoxes) {
        this.totalNumOfBoxes = totalNumOfBoxes;
    }

    public boolean isPicked() {
        return isPicked;
    }

    public void setPicked(boolean picked) {
        isPicked = picked;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void setParts(List<Part> parts) {
        this.parts = parts;
    }

    public String getPutAwayDate() {
        return putAwayDate;
    }

    public void setPutAwayDate(String putAwayDate) {
        this.putAwayDate = putAwayDate;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public List<Invoice> getInvoicesList() {
        return invoiceList;
    }

    public void setInvoicesList(List<Invoice> invoicesList) {
        this.invoiceList = invoicesList;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    @Override
    public String toString() {
        return "Bin{" +
                "partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", invoiceList=" + invoiceList +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", binNumber='" + binNumber + '\'' +
                ", noOfBoxesToScan=" + noOfBoxesToScan +
                ", isPicked=" + isPicked +
                ", totalNumOfBoxes=" + totalNumOfBoxes +
                ", parts=" + parts +
                ", putAwayDate='" + putAwayDate + '\'' +
                '}';
    }
}
