package com.apptmyz.splwms.data;

/**
 * Created by swathilolla on 28/08/17.
 */

public class RoutePoint {
    private String scCode;
    private String scType;
    private int sortOrder;

    public RoutePoint() {
        super();
    }

    public RoutePoint(String scCode, String scType, int sortOrder) {
        this.scCode = scCode;
        this.scType = scType;
        this.sortOrder = sortOrder;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getScType() {
        return scType;
    }

    public void setScType(String scType) {
        this.scType = scType;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public String toString() {
        return "RoutePoint{" +
                "scCode='" + scCode + '\'' +
                ", scType='" + scType + '\'' +
                ", sortOrder=" + sortOrder +
                '}';
    }
}
