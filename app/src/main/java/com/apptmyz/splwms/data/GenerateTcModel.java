package com.apptmyz.splwms.data;

import java.util.List;

public class GenerateTcModel {
    private List<TcOrderResponseModel> orderInfoDataModels;
    private String vendorCode;
    private String eWayBill;
    private Double freight ;
    private String tcNumber;
    private String ewayBillDate;
    private String weight;
    private String eta;
    private String noOfBoxes;

    public GenerateTcModel(List<TcOrderResponseModel> orderInfoDataModels, String vendorCode) {
        this.orderInfoDataModels = orderInfoDataModels;
        this.vendorCode = vendorCode;
    }

    public List<TcOrderResponseModel> getOrderInfoDataModels() {
        return orderInfoDataModels;
    }

    public void setOrderInfoDataModels(List<TcOrderResponseModel> orderInfoDataModels) {
        this.orderInfoDataModels = orderInfoDataModels;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String geteWayBill() {
        return eWayBill;
    }

    public void seteWayBill(String eWayBill) {
        this.eWayBill = eWayBill;
    }

    public Double getFreight() {
        return freight;
    }

    public void setFreight(Double freight) {
        this.freight = freight;
    }

    public String getTcNumber() {
        return tcNumber;
    }

    public void setTcNumber(String tcNumber) {
        this.tcNumber = tcNumber;
    }

    public String getEwayBillDate() {
        return ewayBillDate;
    }

    public void setEwayBillDate(String ewayBillDate) {
        this.ewayBillDate = ewayBillDate;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getNoOfBoxes() {
        return noOfBoxes;
    }

    public void setNoOfBoxes(String noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    @Override
    public String toString() {
        return "GenerateTcModel{" +
                "orderInfoDataModels=" + orderInfoDataModels +
                ", vendorCode='" + vendorCode + '\'' +
                ", eWayBill='" + eWayBill + '\'' +
                ", freight=" + freight +
                ", tcNumber='" + tcNumber + '\'' +
                ", ewayBillDate='" + ewayBillDate + '\'' +
                ", weight='" + weight + '\'' +
                ", eta='" + eta + '\'' +
                ", noOfBoxes='" + noOfBoxes + '\'' +
                '}';
    }
}
