package com.apptmyz.splwms.data;

import java.util.List;

/**
 * Created by swathilolla on 17/08/18.
 */

public class QcImages {
    private List<String> qcPktImgs;

    public QcImages() {
        super();
    }

    public QcImages(List<String> qcPktImgs) {
        this.qcPktImgs = qcPktImgs;
    }

    public List<String> getQcPktImgs() {
        return qcPktImgs;
    }

    public void setQcPktImgs(List<String> qcPktImgs) {
        this.qcPktImgs = qcPktImgs;
    }

    @Override
    public String toString() {
        return "QcImages{" +
                "qcPktImgs=" + qcPktImgs +
                '}';
    }
}
