package com.apptmyz.splwms.data;

import java.util.List;

public class GenerateTcResponseModel {
    private List<String> orderIds;
    private String remarks;
    private FromModel from;
    private ToModel to;
    private String conNumber;

    public List<String> getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public FromModel getFrom() {
        return from;
    }

    public void setFrom(FromModel from) {
        this.from = from;
    }

    public ToModel getTo() {
        return to;
    }

    public void setTo(ToModel to) {
        this.to = to;
    }

    public String getConNumber() {
        return conNumber;
    }

    public void setConNumber(String conNumber) {
        this.conNumber = conNumber;
    }

    @Override
    public String toString() {
        return "GenerateTcResponseModel{" +
                "orderIds=" + orderIds +
                ", remarks='" + remarks + '\'' +
                ", from=" + from +
                ", to=" + to +
                ", conNumber='" + conNumber + '\'' +
                '}';
    }
}
