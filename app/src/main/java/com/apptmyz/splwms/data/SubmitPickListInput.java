package com.apptmyz.splwms.data;

public class SubmitPickListInput {
    private Integer id;
    private String orderNo;
    private String partNo;
    private int partId;
    private int totalBoxes;
    private int binId;
    private String scanTime;
    private String reasonForShortage;
    private String remarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getReasonForShortage() {
        return reasonForShortage;
    }

    public void setReasonForShortage(String reasonForShortage) {
        this.reasonForShortage = reasonForShortage;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "SubmitPickListInput{" +
                "id=" + id +
                ", orderNo='" + orderNo + '\'' +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", binId=" + binId +
                ", scanTime='" + scanTime + '\'' +
                ", reasonForShortage='" + reasonForShortage + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
