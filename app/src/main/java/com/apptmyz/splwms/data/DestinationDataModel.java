package com.apptmyz.splwms.data;

public class DestinationDataModel {

    private String destination;
    private String invoiceNo;
    private int quantity;
    private double weight;

    public DestinationDataModel(String destination, String invoiceNo, int quantity, double weight) {
        this.destination = destination;
        this.invoiceNo = invoiceNo;
        this.quantity = quantity;
        this.weight = weight;
    }

    public DestinationDataModel() {
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "DestinationDataModel{" +
                "destination='" + destination + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", quantity=" + quantity +
                ", weight=" + weight +
                '}';
    }
}
