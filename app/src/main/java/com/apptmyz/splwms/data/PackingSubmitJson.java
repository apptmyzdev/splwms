package com.apptmyz.splwms.data;

import java.util.List;

public class PackingSubmitJson {
    private String orderNo;
    private String masterBoxNo;
    private int subproject;
    private List<PackingModel> partList;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getMasterBoxNo() {
        return masterBoxNo;
    }

    public void setMasterBoxNo(String masterBoxNo) {
        this.masterBoxNo = masterBoxNo;
    }

    public int getSubproject() {
        return subproject;
    }

    public void setSubproject(int subproject) {
        this.subproject = subproject;
    }

    public List<PackingModel> getPartList() {
        return partList;
    }

    public void setPartList(List<PackingModel> partList) {
        this.partList = partList;
    }

    @Override
    public String toString() {
        return "PackingSubmitJson{" +
                "orderNo='" + orderNo + '\'' +
                ", masterBoxNo='" + masterBoxNo + '\'' +
                ", subproject=" + subproject +
                ", partList=" + partList +
                '}';
    }
}
