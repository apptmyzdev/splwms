package com.apptmyz.splwms.data;

public class Invoice {
    private String invoiceNo;
    private int noOfBoxes;
    private int totalQty;

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getNoOfBoxes() {
        return noOfBoxes;
    }

    public void setNoOfBoxes(int noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceNo='" + invoiceNo + '\'' +
                ", noOfBoxes=" + noOfBoxes +
                ", totalQty=" + totalQty +
                '}';
    }
}
