package com.apptmyz.splwms.data;

import java.util.Date;
import java.util.List;

public class CustomerMasterModel {
    private Integer id;
    private String customerCode;
    private String customerName;
    private String legalEntityName;
    private String address1;
    private String address2;
    private String address3;
    private String pinCode;
    private String city;
    private String state;
    private long createdOn;
    private long updatedOn;
    private String createdBy;
    private String updatedBy;
    private String gstin;
    private String primaryContactName;
    private String secondaryContactName;
    private String primaryPhoneNumber;
    private String secondaryPhoneNumber;
    private String primaryAltPhoneNumber;
    private String secondaryAltPhoneNumber;
    private String primaryEmailId;
    private String secondaryEmailId;
    private String primaryDept;
    private String secondaryDept;
    private String pan;

    // private List<CustomerContactModel> contactList;

    /**
     * Jan 14, 2021 * @return the id
     */

    public Integer getId() {
        return id;
    }

    /**
     * Jan 14, 2021 * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLegalEntityName() {
        return legalEntityName;
    }

    public void setLegalEntityName(String legalEntityName) {
        this.legalEntityName = legalEntityName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        this.createdOn = createdOn;
    }

    public long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    // public List<CustomerContactModel> getContactList() {
    // return contactList;
    // }
    // public void setContactList(List<CustomerContactModel> contactList) {
    // this.contactList = contactList;
    // }

    /**
     * @return the primaryContactName
     */
    public String getPrimaryContactName() {
        return primaryContactName;
    }

    /**
     * @param primaryContactName the primaryContactName to set
     */
    public void setPrimaryContactName(String primaryContactName) {
        this.primaryContactName = primaryContactName;
    }

    /**
     * @return the secondaryContactName
     */
    public String getSecondaryContactName() {
        return secondaryContactName;
    }

    /**
     * @param secondaryContactName the secondaryContactName to set
     */
    public void setSecondaryContactName(String secondaryContactName) {
        this.secondaryContactName = secondaryContactName;
    }

    /**
     * @return the primaryPhoneNumber
     */
    public String getPrimaryPhoneNumber() {
        return primaryPhoneNumber;
    }

    /**
     * @param primaryPhoneNumber the primaryPhoneNumber to set
     */
    public void setPrimaryPhoneNumber(String primaryPhoneNumber) {
        this.primaryPhoneNumber = primaryPhoneNumber;
    }

    /**
     * @return the secondaryPhoneNumber
     */
    public String getSecondaryPhoneNumber() {
        return secondaryPhoneNumber;
    }

    /**
     * @param secondaryPhoneNumber the secondaryPhoneNumber to set
     */
    public void setSecondaryPhoneNumber(String secondaryPhoneNumber) {
        this.secondaryPhoneNumber = secondaryPhoneNumber;
    }

    /**
     * @return the primaryAltPhoneNumber
     */
    public String getPrimaryAltPhoneNumber() {
        return primaryAltPhoneNumber;
    }

    /**
     * @param primaryAltPhoneNumber the primaryAltPhoneNumber to set
     */
    public void setPrimaryAltPhoneNumber(String primaryAltPhoneNumber) {
        this.primaryAltPhoneNumber = primaryAltPhoneNumber;
    }

    /**
     * @return the secondaryAltPhoneNumber
     */
    public String getSecondaryAltPhoneNumber() {
        return secondaryAltPhoneNumber;
    }

    /**
     * @param secondaryAltPhoneNumber the secondaryAltPhoneNumber to set
     */
    public void setSecondaryAltPhoneNumber(String secondaryAltPhoneNumber) {
        this.secondaryAltPhoneNumber = secondaryAltPhoneNumber;
    }

    /**
     * @return the primaryEmailId
     */
    public String getPrimaryEmailId() {
        return primaryEmailId;
    }

    /**
     * @param primaryEmailId the primaryEmailId to set
     */
    public void setPrimaryEmailId(String primaryEmailId) {
        this.primaryEmailId = primaryEmailId;
    }

    /**
     * @return the secondaryEmaildId
     */
    public String getSecondaryEmailId() {
        return secondaryEmailId;
    }

    /**
     * @param secondaryEmaildId the secondaryEmaildId to set
     */
    public void setSecondaryEmaildId(String secondaryEmailId) {
        this.secondaryEmailId = secondaryEmailId;
    }

    /**
     * @return the primaryDept
     */
    public String getPrimaryDept() {
        return primaryDept;
    }

    /**
     * @param primaryDept the primaryDept to set
     */
    public void setPrimaryDept(String primaryDept) {
        this.primaryDept = primaryDept;
    }

    /**
     * @return the secondaryDept
     */
    public String getSecondaryDept() {
        return secondaryDept;
    }

    /**
     * @param secondaryDept the secondaryDept to set
     */
    public void setSecondaryDept(String secondaryDept) {
        this.secondaryDept = secondaryDept;
    }

    /**
     * @return the gstin
     */
    public String getGstin() {
        return gstin;
    }

    /**
     * @param gstin the gstin to set
     */
    public void setGstin(String gstin) {
        this.gstin = gstin;
    }

    /**
     * @return the pan
     */
    public String getPan() {
        return pan;
    }

    /**
     * @param pan the pan to set
     */
    public void setPan(String pan) {
        this.pan = pan;
    }


    @Override
    public String toString() {
        return customerCode + " " + customerName;
    }
}
