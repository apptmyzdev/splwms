package com.apptmyz.splwms.data;

import java.util.List;

public class ExceptionModel {
    private int excepType;
    private String excepDesc;
    private List<String> imgBase64s;
    private int exceptionCount;
    private String vehicleNum;
    private String partNo;
    private String scanTime;
    private String dataType;

    private String inventoryTypeDesc;
    private int inventoryTypeId;

    private int actualInventoryTypeId;

    private PartMasterModel partMasterModel;

    private String receiptType;

    private Integer partId;
    private String productName;
    private Integer productId;
    private Integer isProduct;

    public String getInventoryTypeDesc() {
        return inventoryTypeDesc;
    }

    public void setInventoryTypeDesc(String inventoryTypeDesc) {
        this.inventoryTypeDesc = inventoryTypeDesc;
    }

    public int getInventoryType() {
        return inventoryTypeId;
    }

    public void setInventoryType(int inventoryType) {
        this.inventoryTypeId = inventoryType;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    public String getExcepDesc() {
        return excepDesc;
    }

    public void setExcepDesc(String excepDesc) {
        this.excepDesc = excepDesc;
    }

    public List<String> getImgBase64s() {
        return imgBase64s;
    }

    public void setImgBase64s(List<String> imgBase64s) {
        this.imgBase64s = imgBase64s;
    }

    public int getExceptionCount() {
        return exceptionCount;
    }

    public void setExceptionCount(int exceptionCount) {
        this.exceptionCount = exceptionCount;
    }

    public String getVehicleNum() {
        return vehicleNum;
    }

    public void setVehicleNum(String vehicleNum) {
        this.vehicleNum = vehicleNum;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public int getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(int inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public int getActualInventoryTypeId() {
        return actualInventoryTypeId;
    }

    public void setActualInventoryTypeId(int actualInventoryTypeId) {
        this.actualInventoryTypeId = actualInventoryTypeId;
    }

    public PartMasterModel getPartMasterModel() {
        return partMasterModel;
    }

    public void setPartMasterModel(PartMasterModel partMasterModel) {
        this.partMasterModel = partMasterModel;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public Integer getPartId() {
        return partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    @Override
    public String toString() {
        return "ExceptionModel{" +
                "excepType=" + excepType +
                ", excepDesc='" + excepDesc + '\'' +
                ", imgBase64s=" + imgBase64s +
                ", exceptionCount=" + exceptionCount +
                ", vehicleNum='" + vehicleNum + '\'' +
                ", partNo='" + partNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", dataType='" + dataType + '\'' +
                ", inventoryTypeDesc='" + inventoryTypeDesc + '\'' +
                ", inventoryTypeId=" + inventoryTypeId +
                ", actualInventoryTypeId=" + actualInventoryTypeId +
                ", partMasterModel=" + partMasterModel +
                ", receiptType='" + receiptType + '\'' +
                ", partId=" + partId +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                '}';
    }
}
