package com.apptmyz.splwms.data;

import java.util.List;

public class ResponseCon {

    private String conNo;
    private int numPkgsBooking;
    private int numPkgsSheet;
    private long dlyDate;
    private String priority;
    private String nextDest;
    private String product;
    private String dlyStn;
    private String mbag;
    private int scannedPktCount;
    private String conStatus;
    private String duration;
    private String conCustCode;
    private String conCustName;
    private List<ResponsePacket> packets;
    private String originalSheetNo;

    public ResponseCon() {
        super();
    }

    public ResponseCon(String conNo, int numPkgsBooking, int numPkgsSheet, long dlyDate, String priority, String nextDest, String product, String dlyStn, String mbag, int scannedPktCount, String conStatus, String duration, String conCustCode, String conCustName, List<ResponsePacket> packets, String originalSheetNo) {
        this.conNo = conNo;
        this.numPkgsBooking = numPkgsBooking;
        this.numPkgsSheet = numPkgsSheet;
        this.dlyDate = dlyDate;
        this.priority = priority;
        this.nextDest = nextDest;
        this.product = product;
        this.dlyStn = dlyStn;
        this.mbag = mbag;
        this.scannedPktCount = scannedPktCount;
        this.conStatus = conStatus;
        this.duration = duration;
        this.conCustCode = conCustCode;
        this.conCustName = conCustName;
        this.packets = packets;
        this.originalSheetNo = originalSheetNo;
    }

    public String getConNo() {
        return conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public int getNumPkgsBooking() {
        return numPkgsBooking;
    }

    public void setNumPkgsBooking(int numPkgsBooking) {
        this.numPkgsBooking = numPkgsBooking;
    }

    public int getNumPkgsSheet() {
        return numPkgsSheet;
    }

    public void setNumPkgsSheet(int numPkgsSheet) {
        this.numPkgsSheet = numPkgsSheet;
    }

    public long getDlyDate() {
        return dlyDate;
    }

    public void setDlyDate(long dlyDate) {
        this.dlyDate = dlyDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getNextDest() {
        return nextDest;
    }

    public void setNextDest(String nextDest) {
        this.nextDest = nextDest;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDlyStn() {
        return dlyStn;
    }

    public void setDlyStn(String dlyStn) {
        this.dlyStn = dlyStn;
    }

    public String getMbag() {
        return mbag;
    }

    public void setMbag(String mbag) {
        this.mbag = mbag;
    }

    public int getScannedPktCount() {
        return scannedPktCount;
    }

    public void setScannedPktCount(int scannedPktCount) {
        this.scannedPktCount = scannedPktCount;
    }

    public String getConStatus() {
        return conStatus;
    }

    public void setConStatus(String conStatus) {
        this.conStatus = conStatus;
    }

    public List<ResponsePacket> getPackets() {
        return packets;
    }

    public void setPackets(List<ResponsePacket> packets) {
        this.packets = packets;
    }


    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getConCustCode() {
        return conCustCode;
    }

    public void setConCustCode(String conCustCode) {
        this.conCustCode = conCustCode;
    }

    public String getConCustName() {
        return conCustName;
    }

    public void setConCustName(String conCustName) {
        this.conCustName = conCustName;
    }

    public String getOriginalSheetNo() {
        return originalSheetNo;
    }

    public void setOriginalSheetNo(String originalSheetNo) {
        this.originalSheetNo = originalSheetNo;
    }

    @Override
    public String toString() {
        return "ResponseCon{" +
                "conNo='" + conNo + '\'' +
                ", numPkgsBooking=" + numPkgsBooking +
                ", numPkgsSheet=" + numPkgsSheet +
                ", dlyDate=" + dlyDate +
                ", priority='" + priority + '\'' +
                ", nextDest='" + nextDest + '\'' +
                ", product='" + product + '\'' +
                ", dlyStn='" + dlyStn + '\'' +
                ", mbag='" + mbag + '\'' +
                ", scannedPktCount=" + scannedPktCount +
                ", conStatus='" + conStatus + '\'' +
                ", duration='" + duration + '\'' +
                ", conCustCode='" + conCustCode + '\'' +
                ", conCustName='" + conCustName + '\'' +
                ", packets=" + packets +
                ", originalSheetNo='" + originalSheetNo + '\'' +
                '}';
    }
}
