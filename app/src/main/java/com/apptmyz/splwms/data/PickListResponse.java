package com.apptmyz.splwms.data;

import java.util.List;

public class PickListResponse {
    private boolean status;
    private String message;
    private List<WmsPartInvoiceModel> data;
    private String token;
    private int statusCode;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WmsPartInvoiceModel> getData() {
        return data;
    }

    public void setData(List<WmsPartInvoiceModel> data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "PickListResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", token='" + token + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }
}
