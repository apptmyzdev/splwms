package com.apptmyz.splwms.data;

import java.util.List;

public class PartMasterResponse {
    private boolean status;
    private String message;
    private List<PartMasterModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PartMasterModel> getData() {
        return data;
    }

    public void setData(List<PartMasterModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PartMasterResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
