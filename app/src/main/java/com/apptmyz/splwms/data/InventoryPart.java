package com.apptmyz.splwms.data;

public class InventoryPart {
    private int partId;
    private int totalBoxes;
    private int actualBoxes;

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getActualBoxes() {
        return actualBoxes;
    }

    public void setActualBoxes(int actualBoxes) {
        this.actualBoxes = actualBoxes;
    }

    @Override
    public String toString() {
        return "InventoryPart{" +
                "partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", actualBoxes=" + actualBoxes +
                '}';
    }
}
