package com.apptmyz.splwms.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NewPacket implements Serializable {

    private int pktNo;
    private String newPktNo;
    private boolean scanned;
    private boolean excep;
    private boolean resolved;
    private boolean manuallyScanned;
    private String scanTime;
    private String excepType;
    private String excpDtls;
    private ArrayList<String> excpPtkImgs;
    private String resolvType;
    private String resolvDtls;
    private ArrayList<String> resolvPtkImgs;
    private String forceImageCapture;
    private double actualWt;
    private int xd;
    private String vehNo;
    private String routeId;
    private List<String> qcPktImgs;

    public NewPacket() {
        super();
    }


    public NewPacket(int pktNo, String newPktNo, boolean scanned, boolean excep, boolean resolved, boolean manuallyScanned, String scanTime, String excepType, String excpDtls, ArrayList<String> excpPtkImgs, String resolvType, String resolvDtls, ArrayList<String> resolvPtkImgs, String forceImageCapture, double actualWt, int xd, String vehNo, String routeId, List<String> qcPktImgs) {
        this.pktNo = pktNo;
        this.newPktNo = newPktNo;
        this.scanned = scanned;
        this.excep = excep;
        this.resolved = resolved;
        this.manuallyScanned = manuallyScanned;
        this.scanTime = scanTime;
        this.excepType = excepType;
        this.excpDtls = excpDtls;
        this.excpPtkImgs = excpPtkImgs;
        this.resolvType = resolvType;
        this.resolvDtls = resolvDtls;
        this.resolvPtkImgs = resolvPtkImgs;
        this.forceImageCapture = forceImageCapture;
        this.actualWt = actualWt;
        this.xd = xd;
        this.vehNo = vehNo;
        this.routeId = routeId;
        this.qcPktImgs = qcPktImgs;
    }

    public int getPktNo() {
        return pktNo;
    }

    public void setPktNo(int pktNo) {
        this.pktNo = pktNo;
    }

    public String getNewPktNo() {
        return newPktNo;
    }

    public void setNewPktNo(String newPktNo) {
        this.newPktNo = newPktNo;
    }

    public boolean isScanned() {
        return scanned;
    }

    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    public boolean isExcep() {
        return excep;
    }

    public void setExcep(boolean excep) {
        this.excep = excep;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public boolean isManuallyScanned() {
        return manuallyScanned;
    }

    public void setManuallyScanned(boolean manuallyScanned) {
        this.manuallyScanned = manuallyScanned;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getExcepType() {
        return excepType;
    }

    public void setExcepType(String excepType) {
        this.excepType = excepType;
    }

    public String getExcpDtls() {
        return excpDtls;
    }

    public void setExcpDtls(String excpDtls) {
        this.excpDtls = excpDtls;
    }

    public ArrayList<String> getExcpPtkImgs() {
        return excpPtkImgs;
    }

    public void setExcpPtkImgs(ArrayList<String> excpPtkImgs) {
        this.excpPtkImgs = excpPtkImgs;
    }

    public String getResolvType() {
        return resolvType;
    }

    public void setResolvType(String resolvType) {
        this.resolvType = resolvType;
    }

    public String getResolvDtls() {
        return resolvDtls;
    }

    public void setResolvDtls(String resolvDtls) {
        this.resolvDtls = resolvDtls;
    }

    public ArrayList<String> getResolvPtkImgs() {
        return resolvPtkImgs;
    }

    public void setResolvPtkImgs(ArrayList<String> resolvPtkImgs) {
        this.resolvPtkImgs = resolvPtkImgs;
    }

    public String getForceImageCapture() {
        return forceImageCapture;
    }

    public void setForceImageCapture(String forceImageCapture) {
        this.forceImageCapture = forceImageCapture;
    }

    public double getActualWt() {
        return actualWt;
    }

    public void setActualWt(double actualWt) {
        this.actualWt = actualWt;
    }

    public int getXd() {
        return xd;
    }

    public void setXd(int xd) {
        this.xd = xd;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public List<String> getQcPktImgs() {
        return qcPktImgs;
    }

    public void setQcPktImgs(List<String> qcPktImgs) {
        this.qcPktImgs = qcPktImgs;
    }

//    @Override
//    public String toString() {
//        return "NewPacket{" +
//                "pktNo=" + pktNo +
//                ", newPktNo='" + newPktNo + '\'' +
//                ", scanned=" + scanned +
//                ", excep=" + excep +
//                ", resolved=" + resolved +
//                ", manuallyScanned=" + manuallyScanned +
//                ", scanTime='" + scanTime + '\'' +
//                ", excepType='" + excepType + '\'' +
//                ", excpDtls='" + excpDtls + '\'' +
//                ", excpPtkImgs=" + excpPtkImgs +
//                ", resolvType='" + resolvType + '\'' +
//                ", resolvDtls='" + resolvDtls + '\'' +
//                ", resolvPtkImgs=" + resolvPtkImgs +
//                ", forceImageCapture='" + forceImageCapture + '\'' +
//                ", actualWt=" + actualWt +
//                ", xd=" + xd +
//                ", vehNo='" + vehNo + '\'' +
//                ", routeId='" + routeId + '\'' +
//                ", qcPktImgs=" + qcPktImgs +
//                '}';
//    }


    @Override
    public String toString() {
        return "NewPacket{" +
                "pktNo=" + pktNo +
                ", newPktNo='" + newPktNo + '\'' +
                ", scanned=" + scanned +
                ", excep=" + excep +
                ", resolved=" + resolved +
                ", manuallyScanned=" + manuallyScanned +
                ", scanTime='" + scanTime + '\'' +
                ", excepType='" + excepType + '\'' +
                ", excpDtls='" + excpDtls + '\'' +
//                ", excpPtkImgs=" + excpPtkImgs +
                ", resolvType='" + resolvType + '\'' +
                ", resolvDtls='" + resolvDtls + '\'' +
                ", resolvPtkImgs=" + resolvPtkImgs +
                ", forceImageCapture='" + forceImageCapture + '\'' +
                ", actualWt=" + actualWt +
                ", xd=" + xd +
                ", vehNo='" + vehNo + '\'' +
                ", routeId='" + routeId + '\'' +
                ", qcPktImgs=" + qcPktImgs +
                '}';
    }



}