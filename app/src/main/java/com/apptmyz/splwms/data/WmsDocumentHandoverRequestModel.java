package com.apptmyz.splwms.data;

import java.util.List;

public class WmsDocumentHandoverRequestModel {
	private String tcNo;
	private String signature;
	private List<WmsDocumentHandoverInvoiceModel> invoiceList;
	private int receivedManifest;
	private int receivedEwayBill;
	public String getTcNo() {
		return tcNo;
	}
	public void setTcNo(String tcNo) {
		this.tcNo = tcNo;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public List<WmsDocumentHandoverInvoiceModel> getInvoiceList() {
		return invoiceList;
	}
	public void setInvoiceList(List<WmsDocumentHandoverInvoiceModel> invoiceList) {
		this.invoiceList = invoiceList;
	}
	public int getReceivedManifest() {
		return receivedManifest;
	}
	public void setReceivedManifest(int receivedManifest) {
		this.receivedManifest = receivedManifest;
	}
	public int getReceivedEwayBill() {
		return receivedEwayBill;
	}
	public void setReceivedEwayBill(int receivedEwayBill) {
		this.receivedEwayBill = receivedEwayBill;
	}

	@Override
	public String toString() {
		return "WmsDocumentHandoverRequestModel{" +
				"tcNo='" + tcNo + '\'' +
				", signature='" + signature + '\'' +
				", invoiceList=" + invoiceList +
				", receivedManifest=" + receivedManifest +
				", receivedEwayBill=" + receivedEwayBill +
				'}';
	}
}
