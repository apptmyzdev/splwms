package com.apptmyz.splwms.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by swathilolla on 26/10/18.
 */

public class StockPacket implements Serializable {
    private String pktNo;
    private String newPktNo;
    private boolean scanned;
    private boolean excep;
    private boolean resolved;
    private boolean manuallyScanned;
    private String scanTime;
    private String excepType;
    private String excpDtls;
    private ArrayList<String> excpPtkImgs;
    private String resolvType;
    private String resolvDtls;
    private ArrayList<String> resolvPtkImgs;
    private double actualWt;
    private String nextDest;
    private String scannedDest;
    private String moveToDest;

    public StockPacket() {
        super();
    }

    public StockPacket(String pktNo, String newPktNo, boolean scanned, boolean excep, boolean resolved, boolean manuallyScanned, String scanTime, String excepType, String excpDtls, ArrayList<String> excpPtkImgs, String resolvType, String resolvDtls, ArrayList<String> resolvPtkImgs, double actualWt, String nextDest, String scannedDest, String moveToDest) {
        this.pktNo = pktNo;
        this.newPktNo = newPktNo;
        this.scanned = scanned;
        this.excep = excep;
        this.resolved = resolved;
        this.manuallyScanned = manuallyScanned;
        this.scanTime = scanTime;
        this.excepType = excepType;
        this.excpDtls = excpDtls;
        this.excpPtkImgs = excpPtkImgs;
        this.resolvType = resolvType;
        this.resolvDtls = resolvDtls;
        this.resolvPtkImgs = resolvPtkImgs;
        this.actualWt = actualWt;
        this.nextDest = nextDest;
        this.scannedDest = scannedDest;
        this.moveToDest = moveToDest;
    }

    public String getPktNo() {
        return pktNo;
    }

    public void setPktNo(String pktNo) {
        this.pktNo = pktNo;
    }

    public String getNewPktNo() {
        return newPktNo;
    }

    public void setNewPktNo(String newPktNo) {
        this.newPktNo = newPktNo;
    }

    public boolean isScanned() {
        return scanned;
    }

    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    public boolean isExcep() {
        return excep;
    }

    public void setExcep(boolean excep) {
        this.excep = excep;
    }

    public boolean isResolved() {
        return resolved;
    }

    public void setResolved(boolean resolved) {
        this.resolved = resolved;
    }

    public boolean isManuallyScanned() {
        return manuallyScanned;
    }

    public void setManuallyScanned(boolean manuallyScanned) {
        this.manuallyScanned = manuallyScanned;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getExcepType() {
        return excepType;
    }

    public void setExcepType(String excepType) {
        this.excepType = excepType;
    }

    public String getExcpDtls() {
        return excpDtls;
    }

    public void setExcpDtls(String excpDtls) {
        this.excpDtls = excpDtls;
    }

    public ArrayList<String> getExcpPtkImgs() {
        return excpPtkImgs;
    }

    public void setExcpPtkImgs(ArrayList<String> excpPtkImgs) {
        this.excpPtkImgs = excpPtkImgs;
    }

    public String getResolvType() {
        return resolvType;
    }

    public void setResolvType(String resolvType) {
        this.resolvType = resolvType;
    }

    public String getResolvDtls() {
        return resolvDtls;
    }

    public void setResolvDtls(String resolvDtls) {
        this.resolvDtls = resolvDtls;
    }

    public ArrayList<String> getResolvPtkImgs() {
        return resolvPtkImgs;
    }

    public void setResolvPtkImgs(ArrayList<String> resolvPtkImgs) {
        this.resolvPtkImgs = resolvPtkImgs;
    }

    public double getActualWt() {
        return actualWt;
    }

    public void setActualWt(double actualWt) {
        this.actualWt = actualWt;
    }

    public String getNextDest() {
        return nextDest;
    }

    public void setNextDest(String nextDest) {
        this.nextDest = nextDest;
    }

    public String getScannedDest() {
        return scannedDest;
    }

    public void setScannedDest(String scannedDest) {
        this.scannedDest = scannedDest;
    }

    public String getMoveToDest() {
        return moveToDest;
    }

    public void setMoveToDest(String moveToDest) {
        this.moveToDest = moveToDest;
    }

    @Override
    public String toString() {
        return "StockPacket{" +
                "pktNo='" + pktNo + '\'' +
                ", newPktNo='" + newPktNo + '\'' +
                ", scanned=" + scanned +
                ", excep=" + excep +
                ", resolved=" + resolved +
                ", manuallyScanned=" + manuallyScanned +
                ", scanTime='" + scanTime + '\'' +
                ", excepType='" + excepType + '\'' +
                ", excpDtls='" + excpDtls + '\'' +
                ", excpPtkImgs=" + excpPtkImgs +
                ", resolvType='" + resolvType + '\'' +
                ", resolvDtls='" + resolvDtls + '\'' +
                ", resolvPtkImgs=" + resolvPtkImgs +
                ", actualWt=" + actualWt +
                ", nextDest='" + nextDest + '\'' +
                ", scannedDest='" + scannedDest + '\'' +
                ", moveToDest='" + moveToDest + '\'' +
                '}';
    }
}
