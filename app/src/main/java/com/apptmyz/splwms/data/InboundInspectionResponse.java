package com.apptmyz.splwms.data;

import java.util.List;

public class InboundInspectionResponse {
    private boolean status;
    private String message;
    private List<InspectionPart> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<InspectionPart> getData() {
        return data;
    }

    public void setData(List<InspectionPart> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InboundInspectionResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
