package com.apptmyz.splwms.data;

public class LoadUnload {
    private String sheetNo;
    private String carrier;
    private String tripNo;
    private String txnDate;
    private String sheetType;
    private String sheetDate;
    private String branchCode;
    private String branchType;
    private int partiallyScanned;
    private int conCount;
    private int pcsCount;
    private String toBranchCode;
    private String ltutFlag;
    private String lockMacId;
    private String iotNo;
    private String isValidIOT;
    private int loadPercent;

    public LoadUnload() {
        super();
    }

    public LoadUnload(String sheetNo, String carrier, String tripNo, String txnDate, String sheetType, String sheetDate, String branchCode, String branchType, int partiallyScanned, int conCount, int pcsCount, String toBranchCode, String ltutFlag, String lockMacId, String iotNo, String isValidIOT) {
        this.sheetNo = sheetNo;
        this.carrier = carrier;
        this.tripNo = tripNo;
        this.txnDate = txnDate;
        this.sheetType = sheetType;
        this.sheetDate = sheetDate;
        this.branchCode = branchCode;
        this.branchType = branchType;
        this.partiallyScanned = partiallyScanned;
        this.conCount = conCount;
        this.pcsCount = pcsCount;
        this.toBranchCode = toBranchCode;
        this.ltutFlag = ltutFlag;
        this.lockMacId = lockMacId;
        this.iotNo = iotNo;
        this.isValidIOT = isValidIOT;
    }

    public String getSheetNo() {
        return sheetNo;
    }

    public void setSheetNo(String sheetNo) {
        this.sheetNo = sheetNo;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getTripNo() {
        return tripNo;
    }

    public void setTripNo(String tripNo) {
        this.tripNo = tripNo;
    }

    public String getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(String txnDate) {
        this.txnDate = txnDate;
    }

    public String getSheetType() {
        return sheetType;
    }

    public void setSheetType(String sheetType) {
        this.sheetType = sheetType;
    }

    public String getSheetDate() {
        return sheetDate;
    }

    public void setSheetDate(String sheetDate) {
        this.sheetDate = sheetDate;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchType() {
        return branchType;
    }

    public void setBranchType(String branchType) {
        this.branchType = branchType;
    }

    public int getPartiallyScanned() {
        return partiallyScanned;
    }

    public void setPartiallyScanned(int partiallyScanned) {
        this.partiallyScanned = partiallyScanned;
    }

    public int getConCount() {
        return conCount;
    }

    public void setConCount(int conCount) {
        this.conCount = conCount;
    }

    public int getPcsCount() {
        return pcsCount;
    }

    public void setPcsCount(int pcsCount) {
        this.pcsCount = pcsCount;
    }

    public String getToBranchCode() {
        return toBranchCode;
    }

    public void setToBranchCode(String toBranchCode) {
        this.toBranchCode = toBranchCode;
    }

    public String getLtutFlag() {
        return ltutFlag;
    }

    public void setLtutFlag(String ltutFlag) {
        this.ltutFlag = ltutFlag;
    }

    public String getLockMacId() {
        return lockMacId;
    }

    public void setLockMacId(String lockMacId) {
        this.lockMacId = lockMacId;
    }

    public String getIotNo() {
        return iotNo;
    }

    public void setIotNo(String iotNo) {
        this.iotNo = iotNo;
    }

    public String getIsValidIOT() {
        return isValidIOT;
    }

    public void setIsValidIOT(String isValidIOT) {
        this.isValidIOT = isValidIOT;
    }

    public int getLoadPercent() { return loadPercent; }

    public void setLoadPercent(int loadPercent) { this.loadPercent = loadPercent; }

    @Override
    public String toString() {
        return "LoadUnload{" +
                "sheetNo='" + sheetNo + '\'' +
                ", carrier='" + carrier + '\'' +
                ", tripNo='" + tripNo + '\'' +
                ", txnDate='" + txnDate + '\'' +
                ", sheetType='" + sheetType + '\'' +
                ", sheetDate='" + sheetDate + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", branchType='" + branchType + '\'' +
                ", partiallyScanned=" + partiallyScanned +
                ", conCount=" + conCount +
                ", pcsCount=" + pcsCount +
                ", toBranchCode='" + toBranchCode + '\'' +
                ", ltutFlag='" + ltutFlag + '\'' +
                ", lockMacId='" + lockMacId + '\'' +
                ", iotNo='" + iotNo + '\'' +
                ", isValidIOT='" + isValidIOT + '\'' +
                ", loadPercent=" + loadPercent +
                '}';

}
}
