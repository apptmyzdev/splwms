package com.apptmyz.splwms.data;

import java.util.List;

public class UnpackingBox {
    private int boxId;
    private String masterBoxNumber;
    private int quantity;
    private int status;
    private int boxStatus;
    private String vehicleNumber;
    private String inwardNumber;
    private List<UnpackingPart> partList;

    public int getBoxId() {
        return boxId;
    }

    public void setBoxId(int boxId) {
        this.boxId = boxId;
    }

    public String getMasterBoxNumber() {
        return masterBoxNumber;
    }

    public void setMasterBoxNumber(String masterBoxNumber) {
        this.masterBoxNumber = masterBoxNumber;
    }

    public int getBoxStatus() {
        return boxStatus;
    }

    public void setBoxStatus(int boxStatus) {
        this.boxStatus = boxStatus;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getInwardNumber() {
        return inwardNumber;
    }

    public void setInwardNumber(String inwardNumber) {
        this.inwardNumber = inwardNumber;
    }

    public List<UnpackingPart> getPartList() {
        return partList;
    }

    public void setPartList(List<UnpackingPart> partList) {
        this.partList = partList;
    }

    @Override
    public String toString() {
        return "UnpackingBox{" +
                "boxId=" + boxId +
                ", masterBoxNumber='" + masterBoxNumber + '\'' +
                ", quantity=" + quantity +
                ", status=" + status +
                ", boxStatus=" + boxStatus +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", inwardNumber='" + inwardNumber + '\'' +
                ", partList=" + partList +
                '}';
    }
}
