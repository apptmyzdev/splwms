package com.apptmyz.splwms.data;

public class ScanConData {
	private String conNumber;
	private int pktNumber;
	private double actualWt;
	private String scanTime;
	private String excepType;

	public ScanConData() {
		super();
	}

	public ScanConData(String conNumber, int pktNumber, double actualWt,
                       String scanTime, String excepType) {
		super();
		this.conNumber = conNumber;
		this.pktNumber = pktNumber;
		this.actualWt = actualWt;
		this.scanTime = scanTime;
		this.excepType = excepType;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public int getPktNumber() {
		return pktNumber;
	}

	public void setPktNumber(int pktNumber) {
		this.pktNumber = pktNumber;
	}

	public double getActualWt() {
		return actualWt;
	}

	public void setActualWt(double actualWt) {
		this.actualWt = actualWt;
	}

	public String getScanTime() {
		return scanTime;
	}

	public void setScanTime(String scanTime) {
		this.scanTime = scanTime;
	}

	public String getExcepType() {
		return excepType;
	}

	public void setExcepType(String excepType) {
		this.excepType = excepType;
	}

	@Override
	public String toString() {
		return "ScanConData [conNumber=" + conNumber + ", pktNumber="
				+ pktNumber + ", actualWt=" + actualWt + ", scanTime="
				+ scanTime + ", excepType=" + excepType + "]";
	}

}
