package com.apptmyz.splwms.data;

public class DBRecord {
    int rowNum;

    String conNumber;
    String conScanTime;
    String conScanCompleted;
    int conNumPkgsSheet;
    String conDlyDate;
    String conDlyStn;
    String conPriority;
    String conNextDest;
    String conProduct;
    String conMBag;
    int conScannedPktCount;

    int conPktNo;
    String conNewPktNo;
    String conPktScanned;
    String conPktExcep;
    String docktPktResolved;
    String conPktScannedTime;
    String conPktExcepType;
    String conPktExcepDtls;
    String conPktResolveTyp;
    String conPktResolveDetails;
    String conPktImages;
    String conPktForceImage;

    String ltUtFlag;

    String xdVehCheck;
    int xd;
    String vehNo;


    public DBRecord() {
        super();
    }

    public DBRecord(int rowNum, String conNumber, String conScanTime,
                    String conScanCompleted, int conNumPkgsSheet, String conDlyDate,
                    String conDlyStn, String conPriority, String conNextDest,
                    String conProduct, String conMBag, int conScannedPktCount,
                    int conPktNo, String conNewPktNo, String conPktScanned,
                    String conPktExcep, String docktPktResolved,
                    String conPktScannedTime, String conPktExcepType,
                    String conPktExcepDtls, String conPktResolveTyp,
                    String conPktResolveDetails, String conPktImages,
                    String conPktForceImage, String ltUtFlag, String xdVehCheck, int xd, String vehNo) {
        super();
        this.rowNum = rowNum;
        this.conNumber = conNumber;
        this.conScanTime = conScanTime;
        this.conScanCompleted = conScanCompleted;
        this.conNumPkgsSheet = conNumPkgsSheet;
        this.conDlyDate = conDlyDate;
        this.conDlyStn = conDlyStn;
        this.conPriority = conPriority;
        this.conNextDest = conNextDest;
        this.conProduct = conProduct;
        this.conMBag = conMBag;
        this.conScannedPktCount = conScannedPktCount;
        this.conPktNo = conPktNo;
        this.conNewPktNo = conNewPktNo;
        this.conPktScanned = conPktScanned;
        this.conPktExcep = conPktExcep;
        this.docktPktResolved = docktPktResolved;
        this.conPktScannedTime = conPktScannedTime;
        this.conPktExcepType = conPktExcepType;
        this.conPktExcepDtls = conPktExcepDtls;
        this.conPktResolveTyp = conPktResolveTyp;
        this.conPktResolveDetails = conPktResolveDetails;
        this.conPktImages = conPktImages;
        this.conPktForceImage = conPktForceImage;
        this.ltUtFlag = ltUtFlag;
        this.xdVehCheck = xdVehCheck;
        this.xd = xd;
        this.vehNo = vehNo;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getConNumber() {
        return conNumber;
    }

    public void setConNumber(String conNumber) {
        this.conNumber = conNumber;
    }

    public String getConScanTime() {
        return conScanTime;
    }

    public void setConScanTime(String conScanTime) {
        this.conScanTime = conScanTime;
    }

    public String getConScanCompleted() {
        return conScanCompleted;
    }

    public void setConScanCompleted(String conScanCompleted) {
        this.conScanCompleted = conScanCompleted;
    }

    public int getConNumPkgsSheet() {
        return conNumPkgsSheet;
    }

    public void setConNumPkgsSheet(int conNumPkgsSheet) {
        this.conNumPkgsSheet = conNumPkgsSheet;
    }

    public String getConDlyDate() {
        return conDlyDate;
    }

    public void setConDlyDate(String conDlyDate) {
        this.conDlyDate = conDlyDate;
    }

    public String getConDlyStn() {
        return conDlyStn;
    }

    public void setConDlyStn(String conDlyStn) {
        this.conDlyStn = conDlyStn;
    }

    public String getConPriority() {
        return conPriority;
    }

    public void setConPriority(String conPriority) {
        this.conPriority = conPriority;
    }

    public String getConNextDest() {
        return conNextDest;
    }

    public void setConNextDest(String conNextDest) {
        this.conNextDest = conNextDest;
    }

    public String getConProduct() {
        return conProduct;
    }

    public void setConProduct(String conProduct) {
        this.conProduct = conProduct;
    }

    public String getConMBag() {
        return conMBag;
    }

    public void setConMBag(String conMBag) {
        this.conMBag = conMBag;
    }

    public int getConScannedPktCount() {
        return conScannedPktCount;
    }

    public void setConScannedPktCount(int conScannedPktCount) {
        this.conScannedPktCount = conScannedPktCount;
    }

    public int getConPktNo() {
        return conPktNo;
    }

    public void setConPktNo(int conPktNo) {
        this.conPktNo = conPktNo;
    }

    public String getConNewPktNo() {
        return conNewPktNo;
    }

    public void setConNewPktNo(String conNewPktNo) {
        this.conNewPktNo = conNewPktNo;
    }

    public String getConPktScanned() {
        return conPktScanned;
    }

    public void setConPktScanned(String conPktScanned) {
        this.conPktScanned = conPktScanned;
    }

    public String getConPktExcep() {
        return conPktExcep;
    }

    public void setConPktExcep(String conPktExcep) {
        this.conPktExcep = conPktExcep;
    }

    public String getDocktPktResolved() {
        return docktPktResolved;
    }

    public void setDocktPktResolved(String docktPktResolved) {
        this.docktPktResolved = docktPktResolved;
    }

    public String getConPktScannedTime() {
        return conPktScannedTime;
    }

    public void setConPktScannedTime(String conPktScannedTime) {
        this.conPktScannedTime = conPktScannedTime;
    }

    public String getConPktExcepType() {
        return conPktExcepType;
    }

    public void setConPktExcepType(String conPktExcepType) {
        this.conPktExcepType = conPktExcepType;
    }

    public String getConPktExcepDtls() {
        return conPktExcepDtls;
    }

    public void setConPktExcepDtls(String conPktExcepDtls) {
        this.conPktExcepDtls = conPktExcepDtls;
    }

    public String getConPktResolveTyp() {
        return conPktResolveTyp;
    }

    public void setConPktResolveTyp(String conPktResolveTyp) {
        this.conPktResolveTyp = conPktResolveTyp;
    }

    public String getConPktResolveDetails() {
        return conPktResolveDetails;
    }

    public void setConPktResolveDetails(String conPktResolveDetails) {
        this.conPktResolveDetails = conPktResolveDetails;
    }

    public String getConPktImages() {
        return conPktImages;
    }

    public void setConPktImages(String conPktImages) {
        this.conPktImages = conPktImages;
    }

    public String getConPktForceImage() {
        return conPktForceImage;
    }

    public void setConPktForceImage(String conPktForceImage) {
        this.conPktForceImage = conPktForceImage;
    }

    public String getLtUtFlag() {
        return ltUtFlag;
    }

    public void setLtUtFlag(String ltUtFlag) {
        this.ltUtFlag = ltUtFlag;
    }


    public String getXdVehCheck() {
        return xdVehCheck;
    }

    public void setXdVehCheck(String xdVehCheck) {
        this.xdVehCheck = xdVehCheck;
    }

    public int getXd() {
        return xd;
    }

    public void setXd(int xd) {
        this.xd = xd;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    @Override
    public String toString() {
        return "DBRecord{" +
                "rowNum=" + rowNum +
                ", conNumber='" + conNumber + '\'' +
                ", conScanTime='" + conScanTime + '\'' +
                ", conScanCompleted='" + conScanCompleted + '\'' +
                ", conNumPkgsSheet=" + conNumPkgsSheet +
                ", conDlyDate='" + conDlyDate + '\'' +
                ", conDlyStn='" + conDlyStn + '\'' +
                ", conPriority='" + conPriority + '\'' +
                ", conNextDest='" + conNextDest + '\'' +
                ", conProduct='" + conProduct + '\'' +
                ", conMBag='" + conMBag + '\'' +
                ", conScannedPktCount=" + conScannedPktCount +
                ", conPktNo=" + conPktNo +
                ", conNewPktNo='" + conNewPktNo + '\'' +
                ", conPktScanned='" + conPktScanned + '\'' +
                ", conPktExcep='" + conPktExcep + '\'' +
                ", docktPktResolved='" + docktPktResolved + '\'' +
                ", conPktScannedTime='" + conPktScannedTime + '\'' +
                ", conPktExcepType='" + conPktExcepType + '\'' +
                ", conPktExcepDtls='" + conPktExcepDtls + '\'' +
                ", conPktResolveTyp='" + conPktResolveTyp + '\'' +
                ", conPktResolveDetails='" + conPktResolveDetails + '\'' +
                ", conPktImages='" + conPktImages + '\'' +
                ", conPktForceImage='" + conPktForceImage + '\'' +
                ", ltUtFlag='" + ltUtFlag + '\'' +
                ", xdVehCheck='" + xdVehCheck + '\'' +
                ", xd=" + xd +
                ", vehNo='" + vehNo + '\'' +
                '}';
    }
}
