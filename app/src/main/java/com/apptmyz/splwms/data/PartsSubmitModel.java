package com.apptmyz.splwms.data;

import java.util.List;

public class PartsSubmitModel {
    private String vehicleNo;
    private boolean finalSubmission;
    private String dataType;
    private List<PartsListModel> partList;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public List<PartsListModel> getPartList() {
        return partList;
    }

    public void setPartList(List<PartsListModel> partList) {
        this.partList = partList;
    }

    public boolean isFinalSubmission() {
        return finalSubmission;
    }

    public void setFinalSubmission(boolean finalSubmission) {
        this.finalSubmission = finalSubmission;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return "PartsSubmitModel{" +
                "vehicleNo='" + vehicleNo + '\'' +
                ", finalSubmission=" + finalSubmission +
                ", dataType='" + dataType + '\'' +
                ", partList=" + partList +
                '}';
    }
}
