package com.apptmyz.splwms.data;

import java.util.List;

public class UnpackingData {
    private List<UnpackingBox> boxList;
    private List<UnpackingPart> partList;

    public List<UnpackingBox> getBoxList() {
        return boxList;
    }

    public void setBoxList(List<UnpackingBox> boxList) {
        this.boxList = boxList;
    }

    public List<UnpackingPart> getPartList() {
        return partList;
    }

    public void setPartList(List<UnpackingPart> partList) {
        this.partList = partList;
    }

    @Override
    public String toString() {
        return "UnpackingData{" +
                "boxList=" + boxList +
                ", partList=" + partList +
                '}';
    }
}
