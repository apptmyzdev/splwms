package com.apptmyz.splwms.data;

import java.util.Date;

public class VendorMasterModel {
    private Integer id;
    private Integer activeFlag;
    private String city;
//    private Date createdTimeStamp;
    private Integer pincode;
    private String stateCode;
//    private Date updatedTimestamp;
    private String vendorAddress1;
    private String vendorAddress2;
    private String vendorAddress3;
    private String vendorCode;
    private String vendorGstin;
    private String vendorMobileNumber;
    private String vendorName;
    private String vendorType;
    private String updatedBy;
    private String createdBy;
    private Integer vendorRating;
    private String movementType;
    private String emailId;
    private String vendorPan;
    private Integer urp;
    private String cinNo;
    private String flag;
    private Integer warehouseId;
//    private List<VendorContactModel> contactList;
    private Integer shipmentType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(Integer activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

//    public Date getCreatedTimeStamp() {
//        return createdTimeStamp;
//    }
//
//    public void setCreatedTimeStamp(Date createdTimeStamp) {
//        this.createdTimeStamp = createdTimeStamp;
//    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
//
//    public Date getUpdatedTimestamp() {
//        return updatedTimestamp;
//    }

//    public void setUpdatedTimestamp(Date updatedTimestamp) {
//        this.updatedTimestamp = updatedTimestamp;
//    }

    public String getVendorAddress1() {
        return vendorAddress1;
    }

    public void setVendorAddress1(String vendorAddress1) {
        this.vendorAddress1 = vendorAddress1;
    }

    public String getVendorAddress2() {
        return vendorAddress2;
    }

    public void setVendorAddress2(String vendorAddress2) {
        this.vendorAddress2 = vendorAddress2;
    }

    public String getVendorAddress3() {
        return vendorAddress3;
    }

    public void setVendorAddress3(String vendorAddress3) {
        this.vendorAddress3 = vendorAddress3;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorGstin() {
        return vendorGstin;
    }

    public void setVendorGstin(String vendorGstin) {
        this.vendorGstin = vendorGstin;
    }

    public String getVendorMobileNumber() {
        return vendorMobileNumber;
    }

    public void setVendorMobileNumber(String vendorMobileNumber) {
        this.vendorMobileNumber = vendorMobileNumber;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVendorRating() {
        return vendorRating;
    }

    public void setVendorRating(Integer vendorRating) {
        this.vendorRating = vendorRating;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getVendorPan() {
        return vendorPan;
    }

    public void setVendorPan(String vendorPan) {
        this.vendorPan = vendorPan;
    }

    public Integer getUrp() {
        return urp;
    }

    public void setUrp(Integer urp) {
        this.urp = urp;
    }

    public String getCinNo() {
        return cinNo;
    }

    public void setCinNo(String cinNo) {
        this.cinNo = cinNo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(Integer shipmentType) {
        this.shipmentType = shipmentType;
    }

    @Override
    public String toString() {
        return "VendorMasterModel{" +
                "id=" + id +
                ", activeFlag=" + activeFlag +
                ", city='" + city + '\'' +
//                ", createdTimeStamp=" + createdTimeStamp +
                ", pincode=" + pincode +
                ", stateCode='" + stateCode + '\'' +
//                ", updatedTimestamp=" + updatedTimestamp +
                ", vendorAddress1='" + vendorAddress1 + '\'' +
                ", vendorAddress2='" + vendorAddress2 + '\'' +
                ", vendorAddress3='" + vendorAddress3 + '\'' +
                ", vendorCode='" + vendorCode + '\'' +
                ", vendorGstin='" + vendorGstin + '\'' +
                ", vendorMobileNumber='" + vendorMobileNumber + '\'' +
                ", vendorName='" + vendorName + '\'' +
                ", vendorType='" + vendorType + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", vendorRating=" + vendorRating +
                ", movementType='" + movementType + '\'' +
                ", emailId='" + emailId + '\'' +
                ", vendorPan='" + vendorPan + '\'' +
                ", urp=" + urp +
                ", cinNo='" + cinNo + '\'' +
                ", flag='" + flag + '\'' +
                ", warehouseId=" + warehouseId +
                ", shipmentType=" + shipmentType +
                '}';
    }
}
