package com.apptmyz.splwms.data;

public class UnScanConResponse {

    private String conNo;
    private String scCode;
    private boolean status;
    private String msg;
    private ResponseCon con;
    private int numPktsImpacted;
    private double scannedWt;
    private double totalallConWt;
    private double scannedallConWt;

    public String getConNo() {
        return conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public ResponseCon getCon() {
        return con;
    }

    public void setCon(ResponseCon con) {
        this.con = con;
    }

    public int getNumPktsImpacted() {
        return numPktsImpacted;
    }

    public void setNumPktsImpacted(int numPktsImpacted) {
        this.numPktsImpacted = numPktsImpacted;
    }

    public double getScannedWt() {
        return scannedWt;
    }

    public void setScannedWt(double scannedWt) {
        this.scannedWt = scannedWt;
    }

    public double getTotalallConWt() {
        return totalallConWt;
    }

    public void setTotalallConWt(double totalallConWt) {
        this.totalallConWt = totalallConWt;
    }

    public double getScannedallConWt() {
        return scannedallConWt;
    }

    public void setScannedallConWt(double scannedallConWt) {
        this.scannedallConWt = scannedallConWt;
    }

    @Override
    public String toString() {
        return "UnScanConResponse{" +
                "conNo='" + conNo + '\'' +
                ", scCode='" + scCode + '\'' +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                ", con=" + con +
                ", numPktsImpacted=" + numPktsImpacted +
                ", scannedWt=" + scannedWt +
                ", totalallConWt=" + totalallConWt +
                ", scannedallConWt=" + scannedallConWt +
                '}';
    }
}
