package com.apptmyz.splwms.data;

public class InvoiceModel {
    private String invoiceNo;
    private int noOfBoxes;
    private int totalQty;
    private String barcode;
    private int scannedBoxes;

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getNoOfBoxes() {
        return noOfBoxes;
    }

    public void setNoOfBoxes(int noOfBoxes) {
        this.noOfBoxes = noOfBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getScannedBoxes() {
        return scannedBoxes;
    }

    public void setScannedBoxes(int scannedBoxes) {
        this.scannedBoxes = scannedBoxes;
    }

    @Override
    public String toString() {
        return "InvoiceModel{" +
                "invoiceNo='" + invoiceNo + '\'' +
                ", noOfBoxes=" + noOfBoxes +
                ", totalQty=" + totalQty +
                ", barcode='" + barcode + '\'' +
                ", scannedBoxes=" + scannedBoxes +
                '}';
    }
}
