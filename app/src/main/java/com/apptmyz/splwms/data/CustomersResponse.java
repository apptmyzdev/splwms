package com.apptmyz.splwms.data;

import java.util.List;

public class CustomersResponse {
    private boolean status;
    private String message;
    private List<CustomerMasterModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CustomerMasterModel> getData() {
        return data;
    }

    public void setData(List<CustomerMasterModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CustomersResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
