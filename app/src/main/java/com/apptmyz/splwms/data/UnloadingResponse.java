package com.apptmyz.splwms.data;

public class UnloadingResponse {
    private Boolean status;
    private String message;
    private UnloadingModel data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UnloadingModel getData() {
        return data;
    }

    public void setData(UnloadingModel data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "UnloadingResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
