package com.apptmyz.splwms.data;

public class LoadingaPartsModel {
    private String orderNo;
    private String partNo;
    private int partId;
    private int totalBoxes;
    private int totalQty;
    private String binNo;
    private String binId;
    private String scanTime;
    private int xdFlag;
    private String nextDest;
    private int excepType;
    private String excepDesc;
    private String excepImage;
    private String invoiceList;


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public int getXdFlag() {
        return xdFlag;
    }

    public void setXdFlag(int xdFlag) {
        this.xdFlag = xdFlag;
    }

    public String getNextDest() {
        return nextDest;
    }

    public void setNextDest(String nextDest) {
        this.nextDest = nextDest;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    public String getExcepDesc() {
        return excepDesc;
    }

    public void setExcepDesc(String excepDesc) {
        this.excepDesc = excepDesc;
    }

    public String getExcepImage() {
        return excepImage;
    }

    public void setExcepImage(String excepImage) {
        this.excepImage = excepImage;
    }

    public String getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(String invoiceList) {
        this.invoiceList = invoiceList;
    }

    @Override
    public String toString() {
        return "LoadingaPartsModel{" +
                "orderNo='" + orderNo + '\'' +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", binNo='" + binNo + '\'' +
                ", binId='" + binId + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", xdFlag=" + xdFlag +
                ", nextDest='" + nextDest + '\'' +
                ", excepType=" + excepType +
                ", excepDesc='" + excepDesc + '\'' +
                ", excepImage='" + excepImage + '\'' +
                ", invoiceList='" + invoiceList + '\'' +
                '}';
    }
}
