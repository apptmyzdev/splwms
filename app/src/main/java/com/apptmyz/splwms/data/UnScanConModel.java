package com.apptmyz.splwms.data;

public class UnScanConModel {

    private String conNo;
    private String scCode;
    private String sheetNo;
    private String route;
    private String rsType;
    private String reason;
    private String imei;
    private int dataVersion;

    public String getConNo() {
        return conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getSheetNo() {
        return sheetNo;
    }

    public void setSheetNo(String sheetNo) {
        this.sheetNo = sheetNo;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getRsType() {
        return rsType;
    }

    public void setRsType(String rsType) {
        this.rsType = rsType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public int getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(int dataVersion) {
        this.dataVersion = dataVersion;
    }

    @Override
    public String toString() {
        return "UnScanConModel{" +
                "conNo='" + conNo + '\'' +
                ", scCode='" + scCode + '\'' +
                ", sheetNo='" + sheetNo + '\'' +
                ", route='" + route + '\'' +
                ", rsType='" + rsType + '\'' +
                ", reason='" + reason + '\'' +
                ", imei='" + imei + '\'' +
                ", dataVersion=" + dataVersion +
                '}';
    }
}
