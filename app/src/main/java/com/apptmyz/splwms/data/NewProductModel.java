package com.apptmyz.splwms.data;

import java.util.List;

public class NewProductModel {
    private int id;
    private String partNo;
    private String daNo;
    private String invoiceNo;
    private String dataType;
    private String barcode;
    private String masterBoxNo;
    private int partId;
    private int totalBoxes;
    private int totalQty;
    private int scannedCount;
    private String isScanned;
    private String vehicleNo;
    private List<InvoiceModel> invoiceList;//not to be used anymore
    private String scanTime; //used in sqlite
    private String exceptionType;
    private String exceptionDesc;
    private String images;
    private String isException;
    private String isScanCompleted;

    private int exceptionCount;
    private String receiptType;

    private String productName;
    private Integer productId;
    private Integer isProduct;
    private String uniqueKey;

    public String getMasterBoxNo() {
        return masterBoxNo;
    }

    public void setMasterBoxNo(String masterBoxNo) {
        this.masterBoxNo = masterBoxNo;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getScannedCount() {
        return scannedCount;
    }

    public void setScannedCount(int scannedCount) {
        this.scannedCount = scannedCount;
    }

    public String isScanned() {
        return isScanned;
    }

    public void setScanned(String scanned) {
        isScanned = scanned;
    }

    public List<InvoiceModel> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public String getIsScanned() {
        return isScanned;
    }

    public void setIsScanned(String isScanned) {
        this.isScanned = isScanned;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getExceptionType() {
        return exceptionType;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionDesc() {
        return exceptionDesc;
    }

    public void setExceptionDesc(String exceptionDesc) {
        this.exceptionDesc = exceptionDesc;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getIsException() {
        return isException;
    }

    public void setIsException(String isException) {
        this.isException = isException;
    }

    public String getIsScanCompleted() {
        return isScanCompleted;
    }

    public void setIsScanCompleted(String isScanCompleted) {
        this.isScanCompleted = isScanCompleted;
    }

    public int getExceptionCount() {
        return exceptionCount;
    }

    public void setExceptionCount(int exceptionCount) {
        this.exceptionCount = exceptionCount;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    @Override
    public String toString() {
        return "NewProductModel{" +
                "id=" + id +
                ", partNo='" + partNo + '\'' +
                ", daNo='" + daNo + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", dataType='" + dataType + '\'' +
                ", barcode='" + barcode + '\'' +
                ", masterBoxNo='" + masterBoxNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", scannedCount=" + scannedCount +
                ", isScanned='" + isScanned + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", invoiceList=" + invoiceList +
                ", scanTime='" + scanTime + '\'' +
                ", exceptionType='" + exceptionType + '\'' +
                ", exceptionDesc='" + exceptionDesc + '\'' +
                ", images='" + images + '\'' +
                ", isException='" + isException + '\'' +
                ", isScanCompleted='" + isScanCompleted + '\'' +
                ", exceptionCount=" + exceptionCount +
                ", receiptType='" + receiptType + '\'' +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                ", unique='" + uniqueKey + '\'' +
                '}';
    }
}
