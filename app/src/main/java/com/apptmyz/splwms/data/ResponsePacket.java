package com.apptmyz.splwms.data;

public class ResponsePacket {

	private int pktNo;
	private double actualWt;
	private String scanStatus;
	private String excepType;
	private String excpDtls;
	private String scanTime;
	private String forceImageCapture;

	public ResponsePacket() {
		super();
	}

	public ResponsePacket(int pktNo, double actualWt, String scanStatus,
                          String excepType, String excpDtls, String scanTime,
                          String forceImageCapture) {
		super();
		this.pktNo = pktNo;
		this.actualWt = actualWt;
		this.scanStatus = scanStatus;
		this.excepType = excepType;
		this.excpDtls = excpDtls;
		this.scanTime = scanTime;
		this.forceImageCapture = forceImageCapture;
	}

	public int getPktNo() {
		return pktNo;
	}

	public void setPktNo(int pktNo) {
		this.pktNo = pktNo;
	}

	public double getActualWt() {
		return actualWt;
	}

	public void setActualWt(double actualWt) {
		this.actualWt = actualWt;
	}

	public String getScanStatus() {
		return scanStatus;
	}

	public void setScanStatus(String scanStatus) {
		this.scanStatus = scanStatus;
	}

	public String getExcepType() {
		return excepType;
	}

	public void setExcepType(String excepType) {
		this.excepType = excepType;
	}

	public String getExcpDtls() {
		return excpDtls;
	}

	public void setExcpDtls(String excpDtls) {
		this.excpDtls = excpDtls;
	}

	public String getScanTime() {
		return scanTime;
	}

	public void setScanTime(String scanTime) {
		this.scanTime = scanTime;
	}

	public String getForceImageCapture() {
		return forceImageCapture;
	}

	public void setForceImageCapture(String forceImageCapture) {
		this.forceImageCapture = forceImageCapture;
	}

	@Override
	public String toString() {
		return "ResponsePacket [pktNo=" + pktNo + ", actualWt=" + actualWt
				+ ", scanStatus=" + scanStatus + ", excepType=" + excepType
				+ ", excpDtls=" + excpDtls + ", scanTime=" + scanTime
				+ ", forceImageCapture=" + forceImageCapture + "]";
	}

}
