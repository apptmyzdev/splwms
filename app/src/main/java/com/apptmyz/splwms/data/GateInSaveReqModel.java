package com.apptmyz.splwms.data;

import android.view.View;

import java.util.List;

public class GateInSaveReqModel {
    private Integer registerType;
    private String transporterPhNo;
    private String transporterRepresentativeName;
    private String noOfBox;
    private String remarks;
    private List<String> addInvoice;


    public GateInSaveReqModel(Integer registerType, String transporterPhNo, String transporterRepresentativeName, String noOfBox, String remarks, List<String> addInvoice) {
        this.registerType = registerType;
        this.transporterPhNo = transporterPhNo;
        this.transporterRepresentativeName = transporterRepresentativeName;
        this.noOfBox = noOfBox;
        this.remarks = remarks;
        this.addInvoice = addInvoice;
    }





    public Integer getRegisterType() {
        return registerType;
    }

    public void setRegisterType(Integer registerType) {
        this.registerType = registerType;
    }

    public String getTransporterPhNo() {
        return transporterPhNo;
    }

    public void setTransporterPhNo(String transporterPhNo) {
        this.transporterPhNo = transporterPhNo;
    }

    public String getTransporterRepresentativeName() {
        return transporterRepresentativeName;
    }

    public void setTransporterRepresentativeName(String transporterRepresentativeName) {
        this.transporterRepresentativeName = transporterRepresentativeName;
    }

    public String getNoOfBox() {
        return noOfBox;
    }

    public void setNoOfBox(String noOfBox) {
        this.noOfBox = noOfBox;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public List<String> getAddInvoice() {
        return addInvoice;
    }

    public void setAddInvoice(List<String> addInvoice) {
        this.addInvoice = addInvoice;
    }

    @Override
    public String toString() {
        return "GateInSaveReqModel{" +
                "registerType='" + registerType + '\'' +
                ", transporterPhNo='" + transporterPhNo + '\'' +
                ", transporterRepresentativeName='" + transporterRepresentativeName + '\'' +
                ", noOfBox='" + noOfBox + '\'' +
                ", remarks='" + remarks + '\'' +
                ", addInvoice=" + addInvoice +
                '}';
    }
}
