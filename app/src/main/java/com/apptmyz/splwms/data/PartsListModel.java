package com.apptmyz.splwms.data;

import java.util.List;

public class PartsListModel {
    private String partNo;
    private String daNo;
    private String invoiceNo;
    private String dataType;
    private String masterBoxNo;
    private int partId;
    private int totalBoxes;
    private String binNo;
    private String scanTime;
    private int exceptionCount;
    private List<ExceptionModel> exceptionsList;
    private int scanComplete;

    private String inventoryTypeDesc;
    private int inventoryTypeId;

    private String receiptType;

    private String productName;
    private Integer productId;
    private Integer isProduct;

    public String getInventoryTypeDesc() {
        return inventoryTypeDesc;
    }

    public void setInventoryTypeDesc(String inventoryTypeDesc) {
        this.inventoryTypeDesc = inventoryTypeDesc;
    }

    public int getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(int inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public String getMasterBoxNo() {
        return masterBoxNo;
    }

    public void setMasterBoxNo(String masterBoxNo) {
        this.masterBoxNo = masterBoxNo;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public List<ExceptionModel> getExceptionsList() {
        return exceptionsList;
    }

    public void setExceptionsList(List<ExceptionModel> exceptionsList) {
        this.exceptionsList = exceptionsList;
    }

    public int getExceptionCount() {
        return exceptionCount;
    }

    public void setExceptionCount(int exceptionCount) {
        this.exceptionCount = exceptionCount;
    }

    public int getScanComplete() {
        return scanComplete;
    }

    public void setScanComplete(int scanComplete) {
        this.scanComplete = scanComplete;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    @Override
    public String toString() {
        return "PartsListModel{" +
                "partNo='" + partNo + '\'' +
                ", daNo='" + daNo + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", dataType='" + dataType + '\'' +
                ", masterBoxNo='" + masterBoxNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", binNo='" + binNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", exceptionCount=" + exceptionCount +
                ", exceptionsList=" + exceptionsList +
                ", scanComplete=" + scanComplete +
                ", inventoryTypeDesc='" + inventoryTypeDesc + '\'' +
                ", inventoryTypeId=" + inventoryTypeId +
                ", receiptType='" + receiptType + '\'' +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                '}';
    }
}
