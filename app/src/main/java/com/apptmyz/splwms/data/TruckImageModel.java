package com.apptmyz.splwms.data;

import java.util.List;

public class TruckImageModel {
    private int warehouseId;
    private String vehicleNo;
    private int loadUnloadFlag;
    private String vehTxnDate;
    private String bay;
    private List<String> images;

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public int getLoadUnloadFlag() {
        return loadUnloadFlag;
    }

    public void setLoadUnloadFlag(int loadUnloadFlag) {
        this.loadUnloadFlag = loadUnloadFlag;
    }

    public String getVehTxnDate() {
        return vehTxnDate;
    }

    public void setVehTxnDate(String vehTxnDate) {
        this.vehTxnDate = vehTxnDate;
    }

    public String getBay() {
        return bay;
    }

    public void setBay(String bay) {
        this.bay = bay;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "TruckImageModel{" +
                "warehouseId=" + warehouseId +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", loadUnloadFlag=" + loadUnloadFlag +
                ", vehTxnDate='" + vehTxnDate + '\'' +
                ", bay='" + bay + '\'' +
                ", images=" + images +
                '}';
    }
}
