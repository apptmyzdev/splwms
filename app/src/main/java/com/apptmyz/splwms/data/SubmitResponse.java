package com.apptmyz.splwms.data;

public class SubmitResponse {
    private boolean result;
    private String errMsg;
    private String userid;
    private String sccode;
    private boolean warnFlag;

    public SubmitResponse() {
        super();
    }

    public SubmitResponse(boolean result, String errMsg, String userid, String sccode, boolean warnFlag) {
        this.result = result;
        this.errMsg = errMsg;
        this.userid = userid;
        this.sccode = sccode;
        this.warnFlag = warnFlag;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSccode() {
        return sccode;
    }

    public void setSccode(String sccode) {
        this.sccode = sccode;
    }

    public boolean isWarnFlag() {
        return warnFlag;
    }

    public void setWarnFlag(boolean warnFlag) {
        this.warnFlag = warnFlag;
    }

    @Override
    public String toString() {
        return "SubmitResponse{" +
                "result=" + result +
                ", errMsg='" + errMsg + '\'' +
                ", userid='" + userid + '\'' +
                ", sccode='" + sccode + '\'' +
                ", warnFlag=" + warnFlag +
                '}';
    }
}
