package com.apptmyz.splwms.data;

import java.util.List;

public class LoadingResponse {

    private Boolean status;
    private String message;
    private List<LoadingaPartsModel> data;
    private int statusCode;
    private String token;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LoadingaPartsModel> getData() {
        return data;
    }

    public void setData(List<LoadingaPartsModel> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoadingResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                ", token='" + token + '\'' +
                '}';
    }
}
