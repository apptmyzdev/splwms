package com.apptmyz.splwms.data;

import java.util.List;

public class CreateGrnInput {
    private String vehicleNo;
    private String reason;
    private List<CreateGrnPartJson> partList;

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public List<CreateGrnPartJson> getPartList() {
        return partList;
    }

    public void setPartList(List<CreateGrnPartJson> partList) {
        this.partList = partList;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "CreateGrnInput{" +
                "vehicleNo='" + vehicleNo + '\'' +
                ", reason='" + reason + '\'' +
                ", partList=" + partList +
                '}';
    }
}
