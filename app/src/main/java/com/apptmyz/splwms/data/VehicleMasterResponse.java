package com.apptmyz.splwms.data;

import java.util.ArrayList;

public class VehicleMasterResponse {
    private Boolean status;
    private String message;
    private ArrayList<VehicleMasterModel> data;
    private int statusCode;
    private String token;


    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<VehicleMasterModel> getData() {
        return data;
    }

    public void setData(ArrayList<VehicleMasterModel> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "VehicleMasterResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                ", token='" + token + '\'' +
                '}';
    }
}
