package com.apptmyz.splwms.data;

import java.util.List;

public class IntDataResponse {
	private boolean result;
	private String sErrMsg;
	private List<NewCon> cons;
	private double totalWt;

	public IntDataResponse() {
		super();
	}

	public IntDataResponse(boolean result, String sErrMsg,
                           List<NewCon> cons, double totalWt) {
		super();
		this.result = result;
		this.sErrMsg = sErrMsg;
		this.cons = cons;
		this.totalWt = totalWt;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public String getsErrMsg() {
		return sErrMsg;
	}

	public void setsErrMsg(String sErrMsg) {
		this.sErrMsg = sErrMsg;
	}

	public List<NewCon> getCons() {
		return cons;
	}

	public void setCons(List<NewCon> cons) {
		this.cons = cons;
	}

	public double getTotalWt() {
		return totalWt;
	}

	public void setTotalWt(double totalWt) {
		this.totalWt = totalWt;
	}

	@Override
	public String toString() {
		return "IntDataResponse [result=" + result
				+ ", sErrMsg=" + sErrMsg + ", cons=" + cons + ", totalWt="
				+ totalWt + "]";
	}

}
