package com.apptmyz.splwms.data;

import java.util.List;

/**
 * Created by swathilolla on 28/08/17.
 */

public class RouteData {
    private String routeType;
    private String routeId;
    private String routeName;
    private String routeDescription;
    private List<RoutePoint> points;
    private String originSC;
    private String destinationSC;

    public RouteData() {
        super();
    }

    public RouteData(String routeType, String routeId, String routeName, String routeDescription, List<RoutePoint> points, String originSC, String destinationSC) {
        this.routeType = routeType;
        this.routeId = routeId;
        this.routeName = routeName;
        this.routeDescription = routeDescription;
        this.points = points;
        this.originSC = originSC;
        this.destinationSC = destinationSC;
    }

    public String getRouteType() {
        return routeType;
    }

    public void setRouteType(String routeType) {
        this.routeType = routeType;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getRouteDescription() {
        return routeDescription;
    }

    public void setRouteDescription(String routeDescription) {
        this.routeDescription = routeDescription;
    }

    public List<RoutePoint> getPoints() {
        return points;
    }

    public void setPoints(List<RoutePoint> points) {
        this.points = points;
    }

    public String getOriginSC() {
        return originSC;
    }

    public void setOriginSC(String originSC) {
        this.originSC = originSC;
    }

    public String getDestinationSC() {
        return destinationSC;
    }

    public void setDestinationSC(String destinationSC) {
        this.destinationSC = destinationSC;
    }

    @Override
    public String toString() {
        return "RouteData{" +
                "routeType='" + routeType + '\'' +
                ", routeId=" + routeId +
                ", routeName='" + routeName + '\'' +
                ", routeDescription='" + routeDescription + '\'' +
                ", points=" + points +
                ", originSC='" + originSC + '\'' +
                ", destinationSC='" + destinationSC + '\'' +
                '}';
    }
}
