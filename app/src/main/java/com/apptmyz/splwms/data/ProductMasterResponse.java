package com.apptmyz.splwms.data;

import java.util.List;

public class ProductMasterResponse {
    private boolean status;
    private String message;
    private List<ProductMasterModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductMasterModel> getData() {
        return data;
    }

    public void setData(List<ProductMasterModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ProductMasterResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
