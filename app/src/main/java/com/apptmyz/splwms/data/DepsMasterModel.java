package com.apptmyz.splwms.data;

/**
 * Created by swathilolla on 22/08/19.
 */

public class DepsMasterModel {
    private String depsId;
    private String desc;

    public DepsMasterModel() {
        super();
    }

    public DepsMasterModel(String depsId, String desc) {
        this.depsId = depsId;
        this.desc = desc;
    }

    public String getDepsId() {
        return depsId;
    }

    public void setDepsId(String depsId) {
        this.depsId = depsId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "DepsMasterModel{" +
                "depsId='" + depsId + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
