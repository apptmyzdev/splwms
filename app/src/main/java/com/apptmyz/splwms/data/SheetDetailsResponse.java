package com.apptmyz.splwms.data;

import java.util.List;

public class SheetDetailsResponse {

    private boolean result;
    private String errMsg;
    private boolean takeTruckImage;
    private List<ResponseCon> cons;
    private String sheet;
    private String sheetType;
    private String ltutFlag;
    private String serverDate;
    private double totalWt;
    private double scannedWt;
    private int loadPercent;
    private int dataVersion;
    private int fullyScannedConCount;
    private int fullyScannedPktCount;

    public SheetDetailsResponse() {
        super();
    }

    public SheetDetailsResponse(boolean result, String errMsg, boolean takeTruckImage, List<ResponseCon> cons, String sheet, String sheetType, String ltutFlag, String serverDate, double totalWt, double scannedWt, int loadPercent, int dataVersion, int fullyScannedConCount, int fullyScannedPktCount) {
        this.result = result;
        this.errMsg = errMsg;
        this.takeTruckImage = takeTruckImage;
        this.cons = cons;
        this.sheet = sheet;
        this.sheetType = sheetType;
        this.ltutFlag = ltutFlag;
        this.serverDate = serverDate;
        this.totalWt = totalWt;
        this.scannedWt = scannedWt;
        this.loadPercent = loadPercent;
        this.dataVersion = dataVersion;
        this.fullyScannedConCount = fullyScannedConCount;
        this.fullyScannedPktCount = fullyScannedPktCount;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public boolean isTakeTruckImage() {
        return takeTruckImage;
    }

    public void setTakeTruckImage(boolean takeTruckImage) {
        this.takeTruckImage = takeTruckImage;
    }

    public List<ResponseCon> getCons() {
        return cons;
    }

    public void setCons(List<ResponseCon> cons) {
        this.cons = cons;
    }

    public String getSheet() {
        return sheet;
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public String getSheetType() {
        return sheetType;
    }

    public void setSheetType(String sheetType) {
        this.sheetType = sheetType;
    }

    public String getLtutFlag() {
        return ltutFlag;
    }

    public void setLtutFlag(String ltutFlag) {
        this.ltutFlag = ltutFlag;
    }

    public String getServerDate() {
        return serverDate;
    }

    public void setServerDate(String serverDate) {
        this.serverDate = serverDate;
    }

    public double getTotalWt() {
        return totalWt;
    }

    public void setTotalWt(double totalWt) {
        this.totalWt = totalWt;
    }

    public double getScannedWt() {
        return scannedWt;
    }

    public void setScannedWt(double scannedWt) {
        this.scannedWt = scannedWt;
    }

    public int getLoadPercent() {
        return loadPercent;
    }

    public void setLoadPercent(int loadPercent) {
        this.loadPercent = loadPercent;
    }

    public int getDataVersion() {
        return dataVersion;
    }

    public void setDataVersion(int dataVersion) {
        this.dataVersion = dataVersion;
    }

    public int getFullyScannedConCount() {
        return fullyScannedConCount;
    }

    public void setFullyScannedConCount(int fullyScannedConCount) {
        this.fullyScannedConCount = fullyScannedConCount;
    }

    public int getFullyScannedPktCount() {
        return fullyScannedPktCount;
    }

    public void setFullyScannedPktCount(int fullyScannedPktCount) {
        this.fullyScannedPktCount = fullyScannedPktCount;
    }

    @Override
    public String toString() {
        return "SheetDetailsResponse{" +
                "result=" + result +
                ", errMsg='" + errMsg + '\'' +
                ", takeTruckImage=" + takeTruckImage +
                ", cons=" + cons +
                ", sheet='" + sheet + '\'' +
                ", sheetType='" + sheetType + '\'' +
                ", ltutFlag='" + ltutFlag + '\'' +
                ", serverDate='" + serverDate + '\'' +
                ", totalWt=" + totalWt +
                ", scannedWt=" + scannedWt +
                ", loadPercent=" + loadPercent +
                ", dataVersion=" + dataVersion +
                ", fullyScannedConCount=" + fullyScannedConCount +
                ", fullyScannedPktCount=" + fullyScannedPktCount +
                '}';
    }
}
