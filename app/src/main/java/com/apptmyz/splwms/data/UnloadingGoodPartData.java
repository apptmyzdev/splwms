package com.apptmyz.splwms.data;

public class UnloadingGoodPartData {
    private String vehicleNumber;
    private String number;
    private int scannedCount;
    private String scannedTime;
    private String dataType;
    private String inventoryTypeDesc;
    private int inventoryTypeId;
    private int totalBoxes;

    private String productName;
    private Integer productId;
    private Integer isProduct;
    private String uniqueKey;
    private int partId;

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getScannedCount() {
        return scannedCount;
    }

    public void setScannedCount(int scannedCount) {
        this.scannedCount = scannedCount;
    }

    public String getScannedTime() {
        return scannedTime;
    }

    public void setScannedTime(String scannedTime) {
        this.scannedTime = scannedTime;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getInventoryTypeDesc() {
        return inventoryTypeDesc;
    }

    public void setInventoryTypeDesc(String inventoryTypeDesc) {
        this.inventoryTypeDesc = inventoryTypeDesc;
    }

    public int getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(int inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    @Override
    public String toString() {
        return "UnloadingGoodPartData{" +
                "vehicleNumber='" + vehicleNumber + '\'' +
                ", number='" + number + '\'' +
                ", scannedCount=" + scannedCount +
                ", scannedTime='" + scannedTime + '\'' +
                ", dataType='" + dataType + '\'' +
                ", inventoryTypeDesc='" + inventoryTypeDesc + '\'' +
                ", inventoryTypeId=" + inventoryTypeId +
                ", totalBoxes=" + totalBoxes +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                ", unique='" + uniqueKey + '\'' +
                ", partId=" + partId +
                '}';
    }
}
