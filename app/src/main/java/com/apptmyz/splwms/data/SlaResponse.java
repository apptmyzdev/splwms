package com.apptmyz.splwms.data;

import java.util.List;

public class SlaResponse {
    private boolean status;
    private String message;
    private List<SlaNameComponentModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SlaNameComponentModel> getData() {
        return data;
    }

    public void setData(List<SlaNameComponentModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SlaResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
