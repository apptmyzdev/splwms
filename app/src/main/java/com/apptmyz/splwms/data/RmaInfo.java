package com.apptmyz.splwms.data;

public class RmaInfo {
    private String serviceTagNum;
    private String faultDescription;
    private String rmaPartSrNum;
    private String replacedPartSrNum;
    private String dateTime;

    public String getServiceTagNum() {
        return serviceTagNum;
    }

    public void setServiceTagNum(String serviceTagNum) {
        this.serviceTagNum = serviceTagNum;
    }

    public String getFaultDescription() {
        return faultDescription;
    }

    public void setFaultDescription(String faultDescription) {
        this.faultDescription = faultDescription;
    }

    public String getRmaPartSrNum() {
        return rmaPartSrNum;
    }

    public void setRmaPartSrNum(String rmaPartSrNum) {
        this.rmaPartSrNum = rmaPartSrNum;
    }

    public String getReplacedPartSrNum() {
        return replacedPartSrNum;
    }

    public void setReplacedPartSrNum(String replacedPartSrNum) {
        this.replacedPartSrNum = replacedPartSrNum;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "RmaInfo{" +
                "serviceTagNum='" + serviceTagNum + '\'' +
                ", faultDescription='" + faultDescription + '\'' +
                ", rmaPartSrNum='" + rmaPartSrNum + '\'' +
                ", replacedPartSrNum='" + replacedPartSrNum + '\'' +
                ", dateTime='" + dateTime + '\'' +
                '}';
    }
}
