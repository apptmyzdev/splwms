package com.apptmyz.splwms.data;

import java.util.List;

public class NewSheetSubmitModel {
    String sheet;
    String sheetType;
    boolean finalSubmit;
    String userid;
    boolean streaming;
    String empId;
    String scCode;
    String imei;
    List<NewCon> cons;
    boolean confirmFlag;
    String bayNumber;
    String bayNoScanned;
    String groupId;


    public NewSheetSubmitModel() {
        super();
    }

    public NewSheetSubmitModel(String sheet, String sheetType, boolean finalSubmit, String userid, boolean streaming, String empId, String scCode, String imei, List<NewCon> cons, boolean confirmFlag, String bayNumber, String bayNoScanned) {
        this.sheet = sheet;
        this.sheetType = sheetType;
        this.finalSubmit = finalSubmit;
        this.userid = userid;
        this.streaming = streaming;
        this.empId = empId;
        this.scCode = scCode;
        this.imei = imei;
        this.cons = cons;
        this.confirmFlag = confirmFlag;
        this.bayNumber = bayNumber;
        this.bayNoScanned = bayNoScanned;
    }

    public String getSheet() {
        return sheet;
    }

    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public String getSheetType() {
        return sheetType;
    }

    public void setSheetType(String sheetType) {
        this.sheetType = sheetType;
    }

    public boolean isFinalSubmit() {
        return finalSubmit;
    }

    public void setFinalSubmit(boolean finalSubmit) {
        this.finalSubmit = finalSubmit;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public boolean isStreaming() {
        return streaming;
    }

    public void setStreaming(boolean streaming) {
        this.streaming = streaming;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getScCode() {
        return scCode;
    }

    public void setScCode(String scCode) {
        this.scCode = scCode;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public List<NewCon> getCons() {
        return cons;
    }

    public void setCons(List<NewCon> cons) {
        this.cons = cons;
    }

    public boolean isConfirmFlag() {
        return confirmFlag;
    }

    public void setConfirmFlag(boolean confirmFlag) {
        this.confirmFlag = confirmFlag;
    }

    public String getBayNumber() {
        return bayNumber;
    }

    public void setBayNumber(String bayNumber) {
        this.bayNumber = bayNumber;
    }

    public String getBayNoScanned() {
        return bayNoScanned;
    }

    public void setBayNoScanned(String bayNoScanned) {
        this.bayNoScanned = bayNoScanned;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "NewSheetSubmitModel{" +
                "sheet='" + sheet + '\'' +
                ", sheetType='" + sheetType + '\'' +
                ", finalSubmit=" + finalSubmit +
                ", userid='" + userid + '\'' +
                ", streaming=" + streaming +
                ", empId='" + empId + '\'' +
                ", scCode='" + scCode + '\'' +
                ", imei='" + imei + '\'' +
                ", cons=" + cons +
                ", confirmFlag=" + confirmFlag +
                ", bayNumber='" + bayNumber + '\'' +
                ", bayNoScanned='" + bayNoScanned + '\'' +
                ", groupId='" + groupId + '\'' +
                '}';
    }
}
