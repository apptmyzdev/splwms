package com.apptmyz.splwms.data;

import java.util.List;

public class GenerateTcResponse {
    private boolean status;
    private String message;
    private List<GenerateTcResponseModel> data;
    private int statusCode;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<GenerateTcResponseModel> getData() {
        return data;
    }

    public void setData(List<GenerateTcResponseModel> data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "GenerateTcResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", statusCode=" + statusCode +
                '}';
    }
}
