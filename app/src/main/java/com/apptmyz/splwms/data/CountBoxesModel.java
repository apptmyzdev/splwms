package com.apptmyz.splwms.data;

public class CountBoxesModel {
    private String invoiceNo;
    private int boxCount;
    private String product;
    private boolean isScanned;
    private boolean isException;

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public int getBoxCount() {
        return boxCount;
    }

    public void setBoxCount(int boxCount) {
        this.boxCount = boxCount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public boolean isScanned() {
        return isScanned;
    }

    public void setScanned(boolean scanned) {
        isScanned = scanned;
    }

    public boolean isException() {
        return isException;
    }

    public void setException(boolean exception) {
        isException = exception;
    }

    public CountBoxesModel(String invoiceNo, int boxCount, String product, boolean isScanned, boolean isException) {
        this.invoiceNo = invoiceNo;
        this.boxCount = boxCount;
        this.product = product;
        this.isScanned = isScanned;
        this.isException = isException;
    }

    @Override
    public String toString() {
        return "CountBoxesModel{" +
                "invoiceNo='" + invoiceNo + '\'' +
                ", boxCount=" + boxCount +
                ", product='" + product + '\'' +
                ", isScanned=" + isScanned +
                ", isException=" + isException +
                '}';
    }
}
