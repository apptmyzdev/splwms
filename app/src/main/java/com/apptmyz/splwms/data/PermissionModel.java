package com.apptmyz.splwms.data;

public class PermissionModel {
    private Integer id;
    private String permissionName;
    private Integer permissionId;
    private Integer accessType;
    private String category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "PermissionModel{" +
                "id=" + id +
                ", permissionName='" + permissionName + '\'' +
                ", permissionId=" + permissionId +
                ", accessType=" + accessType +
                ", category='" + category + '\'' +
                '}';
    }
}
