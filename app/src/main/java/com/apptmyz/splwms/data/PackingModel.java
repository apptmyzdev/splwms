package com.apptmyz.splwms.data;

public class PackingModel {
    private int partId;
    private String partNum;
    private int quantity;
    private int primarCartonsPacked;
    private int remainingPrimaryCartons;
    private int subProjectId;
    private String subProjectName;
    private int invoiceId;
    private String invoiceNum;
    private String shipToAddress;
    private int pickStatus;
    private int noOfMasterBoxes;
    private double weight;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getPartNum() {
        return partNum;
    }

    public void setPartNum(String partNum) {
        this.partNum = partNum;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrimarCartonsPacked() {
        return primarCartonsPacked;
    }

    public void setPrimarCartonsPacked(int primarCartonsPacked) {
        this.primarCartonsPacked = primarCartonsPacked;
    }

    public int getRemainingPrimaryCartons() {
        return remainingPrimaryCartons;
    }

    public void setRemainingPrimaryCartons(int remainingPrimaryCartons) {
        this.remainingPrimaryCartons = remainingPrimaryCartons;
    }

    public int getSubProjectId() {
        return subProjectId;
    }

    public void setSubProjectId(int subProjectId) {
        this.subProjectId = subProjectId;
    }

    public String getSubProjectName() {
        return subProjectName;
    }

    public void setSubProjectName(String subProjectName) {
        this.subProjectName = subProjectName;
    }

    public int getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getShipToAddress() {
        return shipToAddress;
    }

    public void setShipToAddress(String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }

    public int getPickStatus() {
        return pickStatus;
    }

    public void setPickStatus(int pickStatus) {
        this.pickStatus = pickStatus;
    }

    public int getNoOfMasterBoxes() {
        return noOfMasterBoxes;
    }

    public void setNoOfMasterBoxes(int noOfMasterBoxes) {
        this.noOfMasterBoxes = noOfMasterBoxes;
    }

    @Override
    public String toString() {
        return "PackingModel{" +
                "partId=" + partId +
                ", partNum='" + partNum + '\'' +
                ", quantity=" + quantity +
                ", primarCartonsPacked=" + primarCartonsPacked +
                ", remainingPrimaryCartons=" + remainingPrimaryCartons +
                ", subProjectId=" + subProjectId +
                ", subProjectName='" + subProjectName + '\'' +
                ", invoiceId=" + invoiceId +
                ", invoiceNum='" + invoiceNum + '\'' +
                ", shipToAddress='" + shipToAddress + '\'' +
                ", pickStatus=" + pickStatus +
                ", noOfMasterBoxes=" + noOfMasterBoxes +
                ", weight=" + weight +
                '}';
    }
}
