package com.apptmyz.splwms.data;

import java.util.List;

public class PutAwayData {
    private int id;
    private String partNo;
    private int partId;
    private List<InvoiceModel> invoiceList;
    private int totalBoxes;
    private int totalQty;
    private int noOfBoxesToScan;
    private int qty;

    private String binNo;
    private String scanTime;

    private String vehicleNumber;
    private int status;
    private Integer inventoryTypeId;
    private String inventoryTypeDesc;
//    private String receiptType;
    private String productName;
    private Integer productId;
    private Integer isProduct;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public List<InvoiceModel> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getNoOfBoxesToScan() {
        return noOfBoxesToScan;
    }

    public void setNoOfBoxesToScan(int noOfBoxesToScan) {
        this.noOfBoxesToScan = noOfBoxesToScan;
    }

    public String getBinNumber() {
        return binNo;
    }

    public void setBinNumber(String binNumber) {
        this.binNo = binNumber;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getInventoryTypeDesc() {
        return inventoryTypeDesc;
    }

    public void setInventoryTypeDesc(String inventoryTypeDesc) {
        this.inventoryTypeDesc = inventoryTypeDesc;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }
//    public String getReceiptType() {
//        return receiptType;
//    }
//
//    public void setReceiptType(String receiptType) {
//        this.receiptType = receiptType;
//    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    public Integer getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(Integer inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    @Override
    public String toString() {
        return "PutAwayData{" +
                "id=" + id +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", invoiceList=" + invoiceList +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", noOfBoxesToScan=" + noOfBoxesToScan +
                ", qty=" + qty +
                ", binNo='" + binNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", status=" + status +
                ", inventoryTypeId=" + inventoryTypeId +
                ", inventoryTypeDesc='" + inventoryTypeDesc + '\'' +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                '}';
    }
}
