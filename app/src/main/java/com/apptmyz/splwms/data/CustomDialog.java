package com.apptmyz.splwms.data;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.apptmyz.splwms.R;

public class CustomDialog extends Dialog {
    private TextView messageTv;
    private Button yesBtn, noBtn;
    private String msg;
    private DialogListener listener;

    public CustomDialog(Context context, String message, DialogListener listener) {
        super(context, R.style.CustomAlertDialog);
        this.listener = listener;
        msg = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_alert);

        messageTv = (TextView) findViewById(R.id.tv_error_message);
        messageTv.setText(msg);

        yesBtn = (Button) findViewById(R.id.btn_ok);
        noBtn = (Button) findViewById(R.id.btn_cancel);
        yesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                listener.onPositiveButtonClick();
            }
        });
        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                listener.onNegativeButtonClick();
            }
        });
    }
}
