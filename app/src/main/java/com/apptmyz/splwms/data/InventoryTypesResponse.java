package com.apptmyz.splwms.data;

import java.util.List;

public class InventoryTypesResponse {
    private boolean status;
    private String message;
    private List<InventoryTypeModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<InventoryTypeModel> getData() {
        return data;
    }

    public void setData(List<InventoryTypeModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "InventoryTypesResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
