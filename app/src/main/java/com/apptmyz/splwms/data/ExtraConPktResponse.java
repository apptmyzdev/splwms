package com.apptmyz.splwms.data;

import java.util.List;

/**
 * Created by swathilolla on 29/08/18.
 */

public class ExtraConPktResponse {
    private boolean result;
    private String errMsg;
    private int conNo;
    private List<Integer> pcsNo;

    public ExtraConPktResponse() {
        super();
    }

    public ExtraConPktResponse(boolean result, String errMsg, int conNo, List<Integer> pcsNo) {
        this.result = result;
        this.errMsg = errMsg;
        this.conNo = conNo;
        this.pcsNo = pcsNo;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public int getConNo() {
        return conNo;
    }

    public void setConNo(int conNo) {
        this.conNo = conNo;
    }

    public List<Integer> getPcsNo() {
        return pcsNo;
    }

    public void setPcsNo(List<Integer> pcsNo) {
        this.pcsNo = pcsNo;
    }

    @Override
    public String toString() {
        return "ExtraConPktResponse{" +
                "result=" + result +
                ", errMsg='" + errMsg + '\'' +
                ", conNo=" + conNo +
                ", pcsNo=" + pcsNo +
                '}';
    }
}
