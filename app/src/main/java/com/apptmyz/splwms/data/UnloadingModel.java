package com.apptmyz.splwms.data;

import java.util.List;

public class UnloadingModel {
    private boolean imgFlag;
    private String dataType;
    private List<ProductModel> partList;

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public boolean isImgFlag() {
        return imgFlag;
    }

    public void setImgFlag(boolean imgFlag) {
        this.imgFlag = imgFlag;
    }

    public List<ProductModel> getPartList() {
        return partList;
    }

    public void setPartList(List<ProductModel> partList) {
        this.partList = partList;
    }

    @Override
    public String toString() {
        return "UnloadingModel{" +
                "imgFlag=" + imgFlag +
                ", dataType='" + dataType + '\'' +
                ", partList=" + partList +
                '}';
    }
}
