package com.apptmyz.splwms.data;

public class InspectionBarcodeData {
    private String barcode;
    private String serialNo;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    @Override
    public String toString() {
        return "InspectionBarcodeData{" +
                "barcode='" + barcode + '\'' +
                ", serialNo='" + serialNo + '\'' +
                '}';
    }
}
