package com.apptmyz.splwms.data;

import java.util.Date;

public class WarehouseMasterModel {
    private Integer id;
    private String whCode;
    private String whName;
    private String address1;
    private String address2;
    private String address3;
    private String pincode;
    private String city;
    private String state;
    private String createdBy;
    private String updatedBy;
    private String controllingArea;
    private String controllingDepot;
    private String region;
    private String gst;
    private String pan;
    private String stateCode;
    private String landlineNo;
    private String scInchargeName;
    private String scInchargeMobileNo;
    private String scInchargeEmailId;
    private String secondLineEmailId;
    private String aomName;
    private String aomMailId;
    private String aomContactNo;
    private String domName;
    private String domMailId;
    private String domContactNo;
    private String romName;
    private String romContactNo;
    private String warehouseType;
    private String supplyingWarehouse;
    private String cinNo;
    private String parentWarehouse;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getControllingArea() {
        return controllingArea;
    }

    public void setControllingArea(String controllingArea) {
        this.controllingArea = controllingArea;
    }

    public String getControllingDepot() {
        return controllingDepot;
    }

    public void setControllingDepot(String controllingDepot) {
        this.controllingDepot = controllingDepot;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getLandlineNo() {
        return landlineNo;
    }

    public void setLandlineNo(String landlineNo) {
        this.landlineNo = landlineNo;
    }

    public String getScInchargeName() {
        return scInchargeName;
    }

    public void setScInchargeName(String scInchargeName) {
        this.scInchargeName = scInchargeName;
    }

    public String getScInchargeMobileNo() {
        return scInchargeMobileNo;
    }

    public void setScInchargeMobileNo(String scInchargeMobileNo) {
        this.scInchargeMobileNo = scInchargeMobileNo;
    }

    public String getScInchargeEmailId() {
        return scInchargeEmailId;
    }

    public void setScInchargeEmailId(String scInchargeEmailId) {
        this.scInchargeEmailId = scInchargeEmailId;
    }

    public String getSecondLineEmailId() {
        return secondLineEmailId;
    }

    public void setSecondLineEmailId(String secondLineEmailId) {
        this.secondLineEmailId = secondLineEmailId;
    }

    public String getAomName() {
        return aomName;
    }

    public void setAomName(String aomName) {
        this.aomName = aomName;
    }

    public String getAomMailId() {
        return aomMailId;
    }

    public void setAomMailId(String aomMailId) {
        this.aomMailId = aomMailId;
    }

    public String getAomContactNo() {
        return aomContactNo;
    }

    public void setAomContactNo(String aomContactNo) {
        this.aomContactNo = aomContactNo;
    }

    public String getDomName() {
        return domName;
    }

    public void setDomName(String domName) {
        this.domName = domName;
    }

    public String getDomMailId() {
        return domMailId;
    }

    public void setDomMailId(String domMailId) {
        this.domMailId = domMailId;
    }

    public String getDomContactNo() {
        return domContactNo;
    }

    public void setDomContactNo(String domContactNo) {
        this.domContactNo = domContactNo;
    }

    public String getRomName() {
        return romName;
    }

    public void setRomName(String romName) {
        this.romName = romName;
    }

    public String getRomContactNo() {
        return romContactNo;
    }

    public void setRomContactNo(String romContactNo) {
        this.romContactNo = romContactNo;
    }

    public String getWarehouseType() {
        return warehouseType;
    }

    public void setWarehouseType(String warehouseType) {
        this.warehouseType = warehouseType;
    }

    public String getSupplyingWarehouse() {
        return supplyingWarehouse;
    }

    public void setSupplyingWarehouse(String supplyingWarehouse) {
        this.supplyingWarehouse = supplyingWarehouse;
    }

    public String getCinNo() {
        return cinNo;
    }

    public void setCinNo(String cinNo) {
        this.cinNo = cinNo;
    }

    public String getParentWarehouse() {
        return parentWarehouse;
    }

    public void setParentWarehouse(String parentWarehouse) {
        this.parentWarehouse = parentWarehouse;
    }

    @Override
    public String toString() {
        return whCode + "-" + whName;
    }
}
