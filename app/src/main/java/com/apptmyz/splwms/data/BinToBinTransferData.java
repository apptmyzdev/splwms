package com.apptmyz.splwms.data;

import java.util.List;

public class BinToBinTransferData {
    private int sourceBin;
    private int destBin;
    private List<TransferPart> partList;

    public int getSourceBin() {
        return sourceBin;
    }

    public void setSourceBin(int sourceBin) {
        this.sourceBin = sourceBin;
    }

    public int getDestBin() {
        return destBin;
    }

    public void setDestBin(int destBin) {
        this.destBin = destBin;
    }

    public List<TransferPart> getPartList() {
        return partList;
    }

    public void setPartList(List<TransferPart> partList) {
        this.partList = partList;
    }

    @Override
    public String toString() {
        return "BinToBinTransferData{" +
                "srcId=" + sourceBin +
                ", destId=" + destBin +
                ", partList=" + partList +
                '}';
    }
}
