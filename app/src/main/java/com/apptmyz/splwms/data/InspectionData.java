package com.apptmyz.splwms.data;

import java.util.List;

public class InspectionData {
	private String dataType;
	private List<InspectionPart> dataList;
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public List<InspectionPart> getDataList() {
		return dataList;
	}
	public void setDataList(List<InspectionPart> dataList) {
		this.dataList = dataList;
	}

	@Override
	public String toString() {
		return "InspectionData{" +
				"dataType='" + dataType + '\'' +
				", dataList=" + dataList +
				'}';
	}
}
