package com.apptmyz.splwms.data;

import java.util.List;

import androidx.annotation.NonNull;

public class SubprojectResponse {
    private boolean status;
    private String message;
    private List<SubProjectMasterModel> data;
    private String token;
    private int statusCode;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubProjectMasterModel> getData() {
        return data;
    }

    public void setData(List<SubProjectMasterModel> data) {
        this.data = data;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @NonNull
    @Override
    public String toString() {
        return super.toString();
    }
}
