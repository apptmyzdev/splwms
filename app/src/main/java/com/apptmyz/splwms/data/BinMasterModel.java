package com.apptmyz.splwms.data;

import java.util.List;

public class BinMasterModel {
    private Integer id;
    private String whCode;
    private String whAreaCode;
    private String whZoneCode;
    private String whAisleCode;
    private String whRackCode;
    private String whLocationCode;
    private String whBin;
    private String whBinBarCode;
    private String levelNumber;
    private Integer binPosition;
    private Integer activeFlag;
    private List<Integer> inventoryTypeIds;
    private Integer reserveType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWhCode() {
        return whCode;
    }

    public void setWhCode(String whCode) {
        this.whCode = whCode;
    }

    public String getWhAreaCode() {
        return whAreaCode;
    }

    public void setWhAreaCode(String whAreaCode) {
        this.whAreaCode = whAreaCode;
    }

    public String getWhZoneCode() {
        return whZoneCode;
    }

    public void setWhZoneCode(String whZoneCode) {
        this.whZoneCode = whZoneCode;
    }

    public String getWhAisleCode() {
        return whAisleCode;
    }

    public void setWhAisleCode(String whAisleCode) {
        this.whAisleCode = whAisleCode;
    }

    public String getWhRackCode() {
        return whRackCode;
    }

    public void setWhRackCode(String whRackCode) {
        this.whRackCode = whRackCode;
    }

    public String getWhLocationCode() {
        return whLocationCode;
    }

    public void setWhLocationCode(String whLocationCode) {
        this.whLocationCode = whLocationCode;
    }

    public String getWhBin() {
        return whBin;
    }

    public void setWhBin(String whBin) {
        this.whBin = whBin;
    }

    public String getWhBinBarCode() {
        return whBinBarCode;
    }

    public void setWhBinBarCode(String whBinBarCode) {
        this.whBinBarCode = whBinBarCode;
    }

    public String getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(String levelNumber) {
        this.levelNumber = levelNumber;
    }

    public Integer getBinPosition() {
        return binPosition;
    }

    public void setBinPosition(Integer binPosition) {
        this.binPosition = binPosition;
    }

    public Integer getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(Integer activeFlag) {
        this.activeFlag = activeFlag;
    }

    public List<Integer> getInventoryTypeIds() {
        return inventoryTypeIds;
    }

    public void setInventoryTypeIds(List<Integer> inventoryTypeIds) {
        this.inventoryTypeIds = inventoryTypeIds;
    }

    public Integer getReserveType() {
        return reserveType;
    }

    public void setReserveType(Integer reserveType) {
        this.reserveType = reserveType;
    }

    @Override
    public String toString() {
        return "BinMasterModel{" +
                "id=" + id +
                ", whCode='" + whCode + '\'' +
                ", whAreaCode='" + whAreaCode + '\'' +
                ", whZoneCode='" + whZoneCode + '\'' +
                ", whAisleCode='" + whAisleCode + '\'' +
                ", whRackCode='" + whRackCode + '\'' +
                ", whLocationCode='" + whLocationCode + '\'' +
                ", whBin='" + whBin + '\'' +
                ", whBinBarCode='" + whBinBarCode + '\'' +
                ", levelNumber='" + levelNumber + '\'' +
                ", binPosition=" + binPosition +
                ", activeFlag=" + activeFlag +
                ", inventoryTypeIds=" + inventoryTypeIds +
                ", reserveType=" + reserveType +
                '}';
    }
}
