package com.apptmyz.splwms.data;

import java.util.List;

public class ProductModel {
    private String partNo;
    private String daNo;
    private String invoiceNo;
    private String barcode;
    private String masterBoxNo;
    private int partId;
    private int totalBoxes;
    private int totalQty;
    private int scanComplete;
    private int scannedBoxes;

    private List<ExceptionModel> exceptionsList;
    private List<InvoiceModel> invoiceList;//not to be used anymore.

    private String receiptType;

    private String productName;
    private Integer productId;
    private Integer isProduct;
    private String uniqueKey;

    public int getScannedBoxes() {
        return scannedBoxes;
    }

    public void setScannedBoxes(int scannedBoxes) {
        this.scannedBoxes = scannedBoxes;
    }

    public String getMasterBoxNo() {
        return masterBoxNo;
    }

    public void setMasterBoxNo(String masterBoxNo) {
        this.masterBoxNo = masterBoxNo;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public List<InvoiceModel> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public int getScanComplete() {
        return scanComplete;
    }

    public void setScanComplete(int scanComplete) {
        this.scanComplete = scanComplete;
    }

    public List<ExceptionModel> getExceptionsList() {
        return exceptionsList;
    }

    public void setExceptionsList(List<ExceptionModel> exceptionsList) {
        this.exceptionsList = exceptionsList;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    @Override
    public String toString() {
        return "ProductModel{" +
                "partNo='" + partNo + '\'' +
                ", daNo='" + daNo + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", barcode='" + barcode + '\'' +
                ", masterBoxNo='" + masterBoxNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", scanComplete=" + scanComplete +
                ", scannedBoxes=" + scannedBoxes +
                ", exceptionsList=" + exceptionsList +
                ", invoiceList=" + invoiceList +
                ", receiptType='" + receiptType + '\'' +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                ", unique='" + uniqueKey + '\'' +
                '}';
    }
}
