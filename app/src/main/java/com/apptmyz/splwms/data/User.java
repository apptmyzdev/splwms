package com.apptmyz.splwms.data;

import java.util.List;

public class User {
    private String username;
    private String password;
    private String warehouse;
    private int userId;
    private int role;
    private String warehouseName;
    private Integer warehouseId;
    private List<PermissionModel> permissionslist;
    private List<PermissionModel> permissionDataModelList;
    private List<WarehouseMasterModel> warehouseMasterModels;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public List<PermissionModel> getPermissionslist() {
        return permissionslist;
    }

    public void setPermissionslist(List<PermissionModel> permissionslist) {
        this.permissionslist = permissionslist;
    }

    public List<PermissionModel> getPermissionDataModelList() {
        return permissionDataModelList;
    }

    public void setPermissionDataModelList(List<PermissionModel> permissionDataModelList) {
        this.permissionDataModelList = permissionDataModelList;
    }

    public List<WarehouseMasterModel> getWarehouseMasterModels() {
        return warehouseMasterModels;
    }

    public void setWarehouseMasterModels(List<WarehouseMasterModel> warehouseMasterModels) {
        this.warehouseMasterModels = warehouseMasterModels;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", userId=" + userId +
                ", role=" + role +
                ", warehouse='" + warehouse + '\'' +
                ", warehouseName='" + warehouseName + '\'' +
                ", warehouseId=" + warehouseId +
                ", permissionslist=" + permissionslist +
                ", permissionDataModelList=" + permissionDataModelList +
                ", warehouseMasterModels=" + warehouseMasterModels +
                '}';
    }
}
