package com.apptmyz.splwms.data;

import java.util.List;

public class PackingOrderNoResponse {
    private boolean status;
    private String message;
    private List<PackingOrderNoModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PackingOrderNoModel> getData() {
        return data;
    }

    public void setData(List<PackingOrderNoModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "PackingOrderNoResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
