package com.apptmyz.splwms.data;

import android.os.Parcel;
import android.os.Parcelable;

public class DrsUserMaster implements Parcelable {
    private String appUserId;
    private String appUserName;

    protected DrsUserMaster(Parcel in) {
        appUserId = in.readString();
        appUserName = in.readString();
    }

    public static final Creator<DrsUserMaster> CREATOR = new Creator<DrsUserMaster>() {
        @Override
        public DrsUserMaster createFromParcel(Parcel in) {
            return new DrsUserMaster(in);
        }

        @Override
        public DrsUserMaster[] newArray(int size) {
            return new DrsUserMaster[size];
        }
    };

    public String getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(String appUserId) {
        this.appUserId = appUserId;
    }

    public String getAppUserName() {
        return appUserName;
    }

    public void setAppUserName(String appUserName) {
        this.appUserName = appUserName;
    }

    @Override
    public String toString() {
        return "DrsUserMaster{" +
                "appUserId='" + appUserId + '\'' +
                ", appUserName='" + appUserName + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(appUserId);
        parcel.writeString(appUserName);
    }
}
