package com.apptmyz.splwms.data;

public class DrsVendorMasterModel {
    private String vendorCode;
    private String vendorName;

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public DrsVendorMasterModel(String vendorCode, String vendorName) {
        this.vendorCode = vendorCode;
        this.vendorName = vendorName;
    }

    @Override
    public String toString() {
        return "DrsVendorMasterModel{" +
                "vendorCode='" + vendorCode + '\'' +
                ", vendorName='" + vendorName + '\'' +
                '}';
    }
}
