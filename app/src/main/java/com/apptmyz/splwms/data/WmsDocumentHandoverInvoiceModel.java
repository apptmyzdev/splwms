package com.apptmyz.splwms.data;

public class WmsDocumentHandoverInvoiceModel {
	private String invoiceNo;

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	@Override
	public String toString() {
		return "WmsDocumentHandoverInvoiceModel{" +
				"invoiceNo='" + invoiceNo + '\'' +
				'}';
	}
}
