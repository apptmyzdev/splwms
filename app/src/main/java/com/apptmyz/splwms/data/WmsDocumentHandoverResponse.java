package com.apptmyz.splwms.data;

import java.util.List;

public class WmsDocumentHandoverResponse {
    private boolean status;
    private String message;
    private List<WmsDocumentHandoverInvoiceModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WmsDocumentHandoverInvoiceModel> getData() {
        return data;
    }

    public void setData(List<WmsDocumentHandoverInvoiceModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "WmsDocumentHandoverResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
