package com.apptmyz.splwms.data;

import java.util.List;

public class InventorySubmitData {
    private int binId;
    private List<InventoryPart> partList;

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public List<InventoryPart> getPartList() {
        return partList;
    }

    public void setPartList(List<InventoryPart> partList) {
        this.partList = partList;
    }

    @Override
    public String toString() {
        return "InventorySubmitData{" +
                "binId=" + binId +
                ", partList=" + partList +
                '}';
    }
}
