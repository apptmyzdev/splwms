package com.apptmyz.splwms.data;

import java.util.List;

public class InspectionPart {
    private int partId;
    private String partNo;
    private String daNo;
    private String invoiceNo;
    private String masterBoxNo;
    private int noOfMasterCartons;
    private int noOfExceptions;
    private int status;
    private int qty;
    private String vehicleNumber;
    private String inwardNumber;
    private int goodBoxes;
    private List<ExceptionModel> exceptionList;
    private int noInvoice;
    private int noMasterBox;
    private String dataType;

    private boolean isRepacked;
    private List<InspectionBarcodeData> barcodesData;

    private String inventoryTypeDesc;
    private int inventoryTypeId;
    private int exceptType;
    private String exceptTypeDescription;

    private String receiptType;
    private RmaInfo rmaInfo;
    private String uniqueKey;

    private String productName;
    private Integer productId;
    private Integer isProduct;


    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getNoOfMasterCartons() {
        return noOfMasterCartons;
    }

    public void setNoOfMasterCartons(int noOfMasterCartons) {
        this.noOfMasterCartons = noOfMasterCartons;
    }

    public int getNoOfExceptions() {
        return noOfExceptions;
    }

    public void setNoOfExceptions(int noOfExceptions) {
        this.noOfExceptions = noOfExceptions;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getInwardNumber() {
        return inwardNumber;
    }

    public void setInwardNumber(String inwardNumber) {
        this.inwardNumber = inwardNumber;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getGoodBoxes() {
        return goodBoxes;
    }

    public void setGoodBoxes(int goodBoxes) {
        this.goodBoxes = goodBoxes;
    }

    public List<ExceptionModel> getExceptionList() {
        return exceptionList;
    }

    public void setExceptionList(List<ExceptionModel> exceptionList) {
        this.exceptionList = exceptionList;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDaNo() {
        return daNo;
    }

    public void setDaNo(String daNo) {
        this.daNo = daNo;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getMasterBoxNo() {
        return masterBoxNo;
    }

    public void setMasterBoxNo(String masterBoxNo) {
        this.masterBoxNo = masterBoxNo;
    }

    public int getNoInvoice() {
        return noInvoice;
    }

    public void setNoInvoice(int noInvoice) {
        this.noInvoice = noInvoice;
    }

    public int getNoMasterBox() {
        return noMasterBox;
    }

    public void setNoMasterBox(int noMasterBox) {
        this.noMasterBox = noMasterBox;
    }

    public boolean isRepacked() {
        return isRepacked;
    }

    public void setRepacked(boolean repacked) {
        isRepacked = repacked;
    }

    public List<InspectionBarcodeData> getBarcodesData() {
        return barcodesData;
    }

    public void setBarcodesData(List<InspectionBarcodeData> barcodesData) {
        this.barcodesData = barcodesData;
    }

    public String getInventoryTypeDesc() {
        return inventoryTypeDesc;
    }

    public void setInventoryTypeDesc(String inventoryTypeDesc) {
        this.inventoryTypeDesc = inventoryTypeDesc;
    }

    public int getInventoryTypeId() {
        return inventoryTypeId;
    }

    public void setInventoryTypeId(int inventoryTypeId) {
        this.inventoryTypeId = inventoryTypeId;
    }

    public int getExceptType() {
        return exceptType;
    }

    public void setExceptType(int exceptType) {
        this.exceptType = exceptType;
    }

    public String getExceptTypeDescription() {
        return exceptTypeDescription;
    }

    public void setExceptTypeDescription(String exceptTypeDescription) {
        this.exceptTypeDescription = exceptTypeDescription;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public RmaInfo getRmaInfo() {
        return rmaInfo;
    }

    public void setRmaInfo(RmaInfo rmaInfo) {
        this.rmaInfo = rmaInfo;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getIsProduct() {
        return isProduct;
    }

    public void setIsProduct(Integer isProduct) {
        this.isProduct = isProduct;
    }

    @Override
    public String toString() {
        return "InspectionPart{" +
                "partId=" + partId +
                ", partNo='" + partNo + '\'' +
                ", daNo='" + daNo + '\'' +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", masterBoxNo='" + masterBoxNo + '\'' +
                ", noOfMasterCartons=" + noOfMasterCartons +
                ", noOfExceptions=" + noOfExceptions +
                ", status=" + status +
                ", qty=" + qty +
                ", vehicleNumber='" + vehicleNumber + '\'' +
                ", inwardNumber='" + inwardNumber + '\'' +
                ", goodBoxes=" + goodBoxes +
                ", exceptionList=" + exceptionList +
                ", noInvoice=" + noInvoice +
                ", noMasterBox=" + noMasterBox +
                ", dataType='" + dataType + '\'' +
                ", isRepacked=" + isRepacked +
                ", barcodesData=" + barcodesData +
                ", inventoryTypeDesc='" + inventoryTypeDesc + '\'' +
                ", inventoryTypeId=" + inventoryTypeId +
                ", exceptType=" + exceptType +
                ", exceptTypeDescription='" + exceptTypeDescription + '\'' +
                ", receiptType='" + receiptType + '\'' +
                ", rmaInfo=" + rmaInfo +
                ", uniqueKey='" + uniqueKey + '\'' +
                ", productName='" + productName + '\'' +
                ", productId=" + productId +
                ", isProduct=" + isProduct +
                '}';
    }
}
