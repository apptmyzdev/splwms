package com.apptmyz.splwms.data;

public class VendorMastersRequestModel {
    private String customerCode;
    private String destination;
    private String origin;
    private int slaName;
    private String slaDesc;

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public int getSlaName() {
        return slaName;
    }

    public void setSlaName(int slaName) {
        this.slaName = slaName;
    }

    public String getSlaDesc() {
        return slaDesc;
    }

    public void setSlaDesc(String slaDesc) {
        this.slaDesc = slaDesc;
    }

    @Override
    public String toString() {
        return "VendorMastersRequestModel{" +
                "customerCode='" + customerCode + '\'' +
                ", destination='" + destination + '\'' +
                ", origin='" + origin + '\'' +
                ", slaName=" + slaName +
                ", slaDesc='" + slaDesc + '\'' +
                '}';
    }
}
