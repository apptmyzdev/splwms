package com.apptmyz.splwms.data;

public class GateInSaveResponse {
    private boolean status;
    private String message;
    private String data;
    private int statusCode;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return "GateInSaveResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data='" + data + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }
}
