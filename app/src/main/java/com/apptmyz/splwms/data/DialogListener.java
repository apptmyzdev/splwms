package com.apptmyz.splwms.data;

public interface DialogListener {
    void onPositiveButtonClick();
    void onNegativeButtonClick();
}
