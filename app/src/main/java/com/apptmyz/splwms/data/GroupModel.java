package com.apptmyz.splwms.data;

public class GroupModel {
    private String bayNo;
    private String grpId;
    private String empId;

    public GroupModel(String bayNo, String grpId, String empId) {
        this.bayNo = bayNo;
        this.grpId = grpId;
        this.empId = empId;
    }

    public GroupModel() {
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getGrpId() {
        return grpId;
    }

    public void setGrpId(String grpId) {
        this.grpId = grpId;
    }

    public String getBayNo() {
        return bayNo;
    }

    public void setBayNo(String bayNo) {
        this.bayNo = bayNo;
    }

    @Override
    public String toString() {
        return "GroupModel{" +
                "empId='" + empId + '\'' +
                ", grpId='" + grpId + '\'' +
                ", bayNo='" + bayNo + '\'' +
                '}';
    }
}
