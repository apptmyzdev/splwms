package com.apptmyz.splwms.data;

import java.util.List;

public class SearchBinOrPartResponse {
    private boolean status;
    private String message;
    private List<WmsPartInvoiceModel> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WmsPartInvoiceModel> getData() {
        return data;
    }

    public void setData(List<WmsPartInvoiceModel> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SearchBinOrPartResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
