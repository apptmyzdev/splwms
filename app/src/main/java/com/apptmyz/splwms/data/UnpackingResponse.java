package com.apptmyz.splwms.data;

public class UnpackingResponse {
    private boolean status;
    private String message;
    private UnpackingData data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UnpackingData getData() {
        return data;
    }

    public void setData(UnpackingData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "UnpackingResponse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
