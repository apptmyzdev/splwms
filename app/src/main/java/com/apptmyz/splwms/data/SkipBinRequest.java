package com.apptmyz.splwms.data;

public class SkipBinRequest {
    private String binNo;
    private int binId;
    private String partNo;
    private int partId;
    private int totalBoxes;
    private String scanTime;
    private int excepType;

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    @Override
    public String toString() {
        return "SkipBinRequest{" +
                "binNo='" + binNo + '\'' +
                ", binId=" + binId +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", scanTime='" + scanTime + '\'' +
                ", excepType='" + excepType + '\'' +
                '}';
    }
}
