package com.apptmyz.splwms.data;

public class TransferPart {
    private int partId;
    private int boxes;

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getBoxes() {
        return boxes;
    }

    public void setBoxes(int boxes) {
        this.boxes = boxes;
    }

    public TransferPart() {
    }

    public TransferPart(int partId, int boxes) {
        this.partId = partId;
        this.boxes = boxes;
    }

    @Override
    public String toString() {
        return "TransferPart{" +
                "partId=" + partId +
                ", boxes=" + boxes +
                '}';
    }
}
