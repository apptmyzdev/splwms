package com.apptmyz.splwms.data;

public class PartMasterModel {
    private int partId;
    private String partNo;
    private String partName;

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    @Override
    public String toString() {
        return partNo + "-" + partName;
    }
}
