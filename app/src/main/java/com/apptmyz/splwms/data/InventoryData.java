package com.apptmyz.splwms.data;

import java.util.List;

public class InventoryData {
    private int binId;
    private String binNo;
    private List<WmsPartInvoiceModel> partList;

    public String getBinNumber() {
        return binNo;
    }

    public void setBinNumber(String binNumber) {
        this.binNo = binNumber;
    }

    public List<WmsPartInvoiceModel> getPartList() {
        return partList;
    }

    public void setPartList(List<WmsPartInvoiceModel> partList) {
        this.partList = partList;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    @Override
    public String toString() {
        return "InventoryData{" +
                "binId=" + binId +
                ", binNumber='" + binNo + '\'' +
                ", partList=" + partList +
                '}';
    }
}
