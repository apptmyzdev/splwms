package com.apptmyz.splwms.data;

public class GateInResponseModel {
    private Integer id;
    private String poNo;
    private String vehicleNo;
    private String transporter;
    private String awbNo;
    private String awbDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPoNo() {
        return poNo;
    }

    public void setPoNo(String poNo) {
        this.poNo = poNo;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getAwbNo() {
        return awbNo;
    }

    public void setAwbNo(String awbNo) {
        this.awbNo = awbNo;
    }

    public String getAwbDate() {
        return awbDate;
    }

    public void setAwbDate(String awbDate) {
        this.awbDate = awbDate;
    }

    @Override
    public String toString() {
        return "GateInResponseModel{" +
                "id=" + id +
                ", poNo='" + poNo + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", transporter='" + transporter + '\'' +
                ", awbNo='" + awbNo + '\'' +
                ", awbDate='" + awbDate + '\'' +
                '}';
    }
}
