package com.apptmyz.splwms.data;

import java.util.ArrayList;
import java.util.HashMap;

public class UpdateScanInfoResponse {
	int status;
	ArrayList<DBRecord> cons;
	HashMap<String, String> collisionCons;
	String nextDest;
	boolean alreadyScanned;
	String product;
	String forceImageCapture;
	String ltUtFlag;
	String xdVehCheck;
	int xd;
	String vehNo;
	boolean scanCompleted;
	String desc;
	String excepType;
	boolean excep;

	public UpdateScanInfoResponse() {
		super();
	}

	public UpdateScanInfoResponse(int status, ArrayList<DBRecord> cons, HashMap<String, String> collisionCons, String nextDest, boolean alreadyScanned, String product, String forceImageCapture, String ltUtFlag, String xdVehCheck, int xd, String vehNo) {
		this.status = status;
		this.cons = cons;
		this.collisionCons = collisionCons;
		this.nextDest = nextDest;
		this.alreadyScanned = alreadyScanned;
		this.product = product;
		this.forceImageCapture = forceImageCapture;
		this.ltUtFlag = ltUtFlag;
		this.xdVehCheck = xdVehCheck;
		this.xd = xd;
		this.vehNo = vehNo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public ArrayList<DBRecord> getCons() {
		return cons;
	}

	public void setCons(ArrayList<DBRecord> cons) {
		this.cons = cons;
	}

	public HashMap<String, String> getCollisionCons() {
		return collisionCons;
	}

	public void setCollisionCons(HashMap<String, String> collisionCons) {
		this.collisionCons = collisionCons;
	}

	public String getNextDest() {
		return nextDest;
	}

	public void setNextDest(String nextDest) {
		this.nextDest = nextDest;
	}

	public boolean isAlreadyScanned() {
		return alreadyScanned;
	}

	public void setAlreadyScanned(boolean alreadyScanned) {
		this.alreadyScanned = alreadyScanned;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getForceImageCapture() {
		return forceImageCapture;
	}

	public void setForceImageCapture(String forceImageCapture) {
		this.forceImageCapture = forceImageCapture;
	}

	public String getLtUtFlag() {
		return ltUtFlag;
	}

	public void setLtUtFlag(String ltUtFlag) {
		this.ltUtFlag = ltUtFlag;
	}

	public String getXdVehCheck() {
		return xdVehCheck;
	}

	public void setXdVehCheck(String xdVehCheck) {
		this.xdVehCheck = xdVehCheck;
	}

	public int getXd() {
		return xd;
	}

	public void setXd(int xd) {
		this.xd = xd;
	}

	public String getVehNo() {
		return vehNo;
	}

	public void setVehNo(String vehNo) {
		this.vehNo = vehNo;
	}

	public boolean isScanCompleted() {
		return scanCompleted;
	}

	public void setScanCompleted(boolean scanCompleted) {
		this.scanCompleted = scanCompleted;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getExcepType() {
		return excepType;
	}

	public void setExcepType(String excepType) {
		this.excepType = excepType;
	}

	public boolean isExcep() {
		return excep;
	}

	public void setExcep(boolean excep) {
		this.excep = excep;
	}

	@Override
	public String toString() {
		return "UpdateScanInfoResponse{" +
				"status=" + status +
				", cons=" + cons +
				", collisionCons=" + collisionCons +
				", nextDest='" + nextDest + '\'' +
				", alreadyScanned=" + alreadyScanned +
				", product='" + product + '\'' +
				", forceImageCapture='" + forceImageCapture + '\'' +
				", ltUtFlag='" + ltUtFlag + '\'' +
				", xdVehCheck='" + xdVehCheck + '\'' +
				", xd=" + xd +
				", vehNo='" + vehNo + '\'' +
				", scanCompleted=" + scanCompleted +
				", desc='" + desc + '\'' +
				'}';
	}
}
