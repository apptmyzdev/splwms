package com.apptmyz.splwms.data;

import android.os.Parcel;
import android.os.Parcelable;

public class VehicleMasterModel implements Parcelable {

    private int vehicleId;
    private int vendorId;
    private String vendorName;
    private String vehicleNo;
    private int vehicleTypeId;
    private String vehicleType;
    private int subProjectId;
    private String subProject;
    private int routeId;
    private String route;
    private int userId;
    private String createdBy;
    private String createdTimestamp;
    private String updatedBy;
    private String updatedTimestamp;
    private boolean activeFlag;

    protected VehicleMasterModel(Parcel in) {
        vehicleId = in.readInt();
        vendorId = in.readInt();
        vendorName = in.readString();
        vehicleNo = in.readString();
        vehicleTypeId = in.readInt();
        vehicleType = in.readString();
        subProjectId = in.readInt();
        subProject = in.readString();
        routeId = in.readInt();
        route = in.readString();
        userId = in.readInt();
        createdBy = in.readString();
        createdTimestamp = in.readString();
        updatedBy = in.readString();
        updatedTimestamp = in.readString();
        activeFlag = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(vehicleId);
        dest.writeInt(vendorId);
        dest.writeString(vendorName);
        dest.writeString(vehicleNo);
        dest.writeInt(vehicleTypeId);
        dest.writeString(vehicleType);
        dest.writeInt(subProjectId);
        dest.writeString(subProject);
        dest.writeInt(routeId);
        dest.writeString(route);
        dest.writeInt(userId);
        dest.writeString(createdBy);
        dest.writeString(createdTimestamp);
        dest.writeString(updatedBy);
        dest.writeString(updatedTimestamp);
        dest.writeByte((byte) (activeFlag ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VehicleMasterModel> CREATOR = new Creator<VehicleMasterModel>() {
        @Override
        public VehicleMasterModel createFromParcel(Parcel in) {
            return new VehicleMasterModel(in);
        }

        @Override
        public VehicleMasterModel[] newArray(int size) {
            return new VehicleMasterModel[size];
        }
    };

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public int getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(int vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public int getSubProjectId() {
        return subProjectId;
    }

    public void setSubProjectId(int subProjectId) {
        this.subProjectId = subProjectId;
    }

    public String getSubProject() {
        return subProject;
    }

    public void setSubProject(String subProject) {
        this.subProject = subProject;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(String updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public boolean isActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(boolean activeFlag) {
        this.activeFlag = activeFlag;
    }

    @Override
    public String toString() {
        return "VehicleMasterModel{" +
                "vehicleId=" + vehicleId +
                ", vendorId=" + vendorId +
                ", vendorName='" + vendorName + '\'' +
                ", vehicleNo='" + vehicleNo + '\'' +
                ", vehicleTypeId=" + vehicleTypeId +
                ", vehicleType='" + vehicleType + '\'' +
                ", subProjectId=" + subProjectId +
                ", subProject='" + subProject + '\'' +
                ", routeId=" + routeId +
                ", route='" + route + '\'' +
                ", userId=" + userId +
                ", createdBy='" + createdBy + '\'' +
                ", createdTimestamp='" + createdTimestamp + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedTimestamp='" + updatedTimestamp + '\'' +
                ", activeFlag=" + activeFlag +
                '}';
    }
}
