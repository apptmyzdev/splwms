package com.apptmyz.splwms.data;

public class NewLoadingPartsModel {

    private int id;
    private String orderNo;
    private String partNo;
    private int partId;
    private int totalBoxes;
    private int totalQty;
    private String binNo;
    private String binId;
    private String scanTime;
    private String isScanned;
    private int scannedCount;
    private String vehNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public int getTotalBoxes() {
        return totalBoxes;
    }

    public void setTotalBoxes(int totalBoxes) {
        this.totalBoxes = totalBoxes;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getBinId() {
        return binId;
    }

    public void setBinId(String binId) {
        this.binId = binId;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public String getIsScanned() {
        return isScanned;
    }

    public void setIsScanned(String isScanned) {
        this.isScanned = isScanned;
    }

    public int getScannedCount() {
        return scannedCount;
    }

    public void setScannedCount(int scannedCount) {
        this.scannedCount = scannedCount;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    @Override
    public String toString() {
        return "NewLoadingPartsModel{" +
                "id=" + id +
                ", orderNo='" + orderNo + '\'' +
                ", partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", totalBoxes=" + totalBoxes +
                ", totalQty=" + totalQty +
                ", binNo='" + binNo + '\'' +
                ", binId='" + binId + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", isScanned='" + isScanned + '\'' +
                ", scannedCount=" + scannedCount +
                ", vehNo=" + vehNo +
                '}';
    }
}

