package com.apptmyz.splwms.data;

public class RouteMasterModel {
    private int routeId;
    private String routeName;
    private String subProject;
    private String subprojName;
    private String startNode;
    private String startNodeId;
    private String startNodeStartTime;
    private String startNodeEndTime;
    private String endNode;
    private String endNodeId;
    private String endNodeStartTime;
    private String endNodeEndTime;
    private String viaNodes;
    private String sequence;
    private String routeNodeList;
    private int userId;
    private String createdBy;
    private String createdTimestamp;
    private String updatedBy;
    private String updatedTimestamp;
    private String revenueModel;
    private String revenueList;
    private String slaDays;
    private String distance;
    private String routeSchema;
    private String plant;
    private String units;

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getSubProject() {
        return subProject;
    }

    public void setSubProject(String subProject) {
        this.subProject = subProject;
    }

    public String getSubprojName() {
        return subprojName;
    }

    public void setSubprojName(String subprojName) {
        this.subprojName = subprojName;
    }

    public String getStartNode() {
        return startNode;
    }

    public void setStartNode(String startNode) {
        this.startNode = startNode;
    }

    public String getStartNodeId() {
        return startNodeId;
    }

    public void setStartNodeId(String startNodeId) {
        this.startNodeId = startNodeId;
    }

    public String getStartNodeStartTime() {
        return startNodeStartTime;
    }

    public void setStartNodeStartTime(String startNodeStartTime) {
        this.startNodeStartTime = startNodeStartTime;
    }

    public String getStartNodeEndTime() {
        return startNodeEndTime;
    }

    public void setStartNodeEndTime(String startNodeEndTime) {
        this.startNodeEndTime = startNodeEndTime;
    }

    public String getEndNode() {
        return endNode;
    }

    public void setEndNode(String endNode) {
        this.endNode = endNode;
    }

    public String getEndNodeId() {
        return endNodeId;
    }

    public void setEndNodeId(String endNodeId) {
        this.endNodeId = endNodeId;
    }

    public String getEndNodeStartTime() {
        return endNodeStartTime;
    }

    public void setEndNodeStartTime(String endNodeStartTime) {
        this.endNodeStartTime = endNodeStartTime;
    }

    public String getEndNodeEndTime() {
        return endNodeEndTime;
    }

    public void setEndNodeEndTime(String endNodeEndTime) {
        this.endNodeEndTime = endNodeEndTime;
    }

    public String getViaNodes() {
        return viaNodes;
    }

    public void setViaNodes(String viaNodes) {
        this.viaNodes = viaNodes;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getRouteNodeList() {
        return routeNodeList;
    }

    public void setRouteNodeList(String routeNodeList) {
        this.routeNodeList = routeNodeList;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setUpdatedTimestamp(String updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public String getRevenueModel() {
        return revenueModel;
    }

    public void setRevenueModel(String revenueModel) {
        this.revenueModel = revenueModel;
    }

    public String getRevenueList() {
        return revenueList;
    }

    public void setRevenueList(String revenueList) {
        this.revenueList = revenueList;
    }

    public String getSlaDays() {
        return slaDays;
    }

    public void setSlaDays(String slaDays) {
        this.slaDays = slaDays;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRouteSchema() {
        return routeSchema;
    }

    public void setRouteSchema(String routeSchema) {
        this.routeSchema = routeSchema;
    }

    public String getPlant() {
        return plant;
    }

    public void setPlant(String plant) {
        this.plant = plant;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    @Override
    public String toString() {
        return "RouteMasterModel{" +
                "routeId=" + routeId +
                ", routeName='" + routeName + '\'' +
                ", subProject='" + subProject + '\'' +
                ", subprojName='" + subprojName + '\'' +
                ", startNode='" + startNode + '\'' +
                ", startNodeId='" + startNodeId + '\'' +
                ", startNodeStartTime='" + startNodeStartTime + '\'' +
                ", startNodeEndTime='" + startNodeEndTime + '\'' +
                ", endNode='" + endNode + '\'' +
                ", endNodeId='" + endNodeId + '\'' +
                ", endNodeStartTime='" + endNodeStartTime + '\'' +
                ", endNodeEndTime='" + endNodeEndTime + '\'' +
                ", viaNodes='" + viaNodes + '\'' +
                ", sequence='" + sequence + '\'' +
                ", routeNodeList='" + routeNodeList + '\'' +
                ", userId=" + userId +
                ", createdBy='" + createdBy + '\'' +
                ", createdTimestamp='" + createdTimestamp + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", updatedTimestamp='" + updatedTimestamp + '\'' +
                ", revenueModel='" + revenueModel + '\'' +
                ", revenueList='" + revenueList + '\'' +
                ", slaDays='" + slaDays + '\'' +
                ", distance='" + distance + '\'' +
                ", routeSchema='" + routeSchema + '\'' +
                ", plant='" + plant + '\'' +
                ", units='" + units + '\'' +
                '}';
    }
}
