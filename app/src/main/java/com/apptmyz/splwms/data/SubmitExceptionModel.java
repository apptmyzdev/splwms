package com.apptmyz.splwms.data;

import java.util.List;

public class SubmitExceptionModel {
    private String partNo;
    private int partId;
    private String binNo;
    private int binId;
    private String scanTime;
    private int excepType;
    private List<String> imgBase64s;

    public SubmitExceptionModel(String partNo, int partId, String binNo, int binId, String scanTime, int excepType, List<String> imgBase64s) {
        this.partNo = partNo;
        this.partId = partId;
        this.binNo = binNo;
        this.binId = binId;
        this.scanTime = scanTime;
        this.excepType = excepType;
        this.imgBase64s = imgBase64s;
    }

    public String getPartNo() {
        return partNo;
    }

    public void setPartNo(String partNo) {
        this.partNo = partNo;
    }

    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    public String getBinNo() {
        return binNo;
    }

    public void setBinNo(String binNo) {
        this.binNo = binNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public int getExcepType() {
        return excepType;
    }

    public void setExcepType(int excepType) {
        this.excepType = excepType;
    }

    public List<String> getImgBase64s() {
        return imgBase64s;
    }

    public void setImgBase64s(List<String> imgBase64s) {
        this.imgBase64s = imgBase64s;
    }

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    @Override
    public String toString() {
        return "SubmitExceptionModel{" +
                "partNo='" + partNo + '\'' +
                ", partId=" + partId +
                ", binNo='" + binNo + '\'' +
                ", binId=" + binId +
                ", scanTime='" + scanTime + '\'' +
                ", excepType='" + excepType + '\'' +
                ", imgBase64='" + imgBase64s + '\'' +
                '}';
    }
}
