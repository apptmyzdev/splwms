package com.apptmyz.splwms.data;

public class SubProjectMasterModel {
	private Integer subProjectId;
	private String name;
	private Integer projectId;
	private String projectName;
	private Integer projectTypeId;
	private String projectType;
	private String location;
	private Integer revenueType;
	private String revenueDetails;
	private String stateCode;
	private String state;
	private String city;
	private int userId;
	private String createdBy;
	private String createdTimestamp;
	private String updatedBy;
	private String updatedTimestamp;
	private Integer branch;
	private String branchName;
	private String saacCode;
	private String description;
	private double discountPercent;

	private int revenueId;
	private String customerName;
	private String revenueTypeName;
//	private List<CustBillingAddressModel> custbilladdr;
//	private List<TaxMasterModel> taxDetails;
//	private List<CollectionModel> collectionModel;
//	private List<FuelChargeModel> fuelChargeList;
	private int plantId;
	//	private BigDecimal wtRate;
	//	private BigDecimal minWtGuarantee;
	//	
	//	private int additionalFlat;
	//	
	//	private BigDecimal tripRate;
	//	private int vehicleTypeId;
	//	private String vehicleType;
	//	private int route;
	//	private BigDecimal kmRate;
	//	private BigDecimal minKmRate;
	//	private BigDecimal percentSale;
	//	private String percentSaleType;
	//	private BigDecimal minPercentGuarantee;
	//	private int costDataId;
	//	
	//	private int costType;
	//	private BigDecimal wtRateCD;
	//	private BigDecimal minWtGuaranteeCD;
	//	private int additionalFlatCD;
	//	private BigDecimal tripRateCD;
	//	private int vehicleTypeIdCD;
	//	private String vehicleTypeCD;
	//	private int routeCD;
	//	
	//	private BigDecimal kmRateCD;
	//	private BigDecimal minKmRateCD;
	//	
	//	
	//	private BigDecimal percentSaleCD;
	//	private String percentSaleTypeCD;
	//	private BigDecimal minPercentGuaranteeCD;

	
	private Integer custAddr;
	//	private CustBillingAddressModel custBillAddr;
	
//	private List<RouteMasterModel> routeList;

	public int getPlantId() {
		return plantId;
	}
	public void setPlantId(int plantId) {
		this.plantId = plantId;
	}
//	public List<FuelChargeModel> getFuelChargeList() {
//		return fuelChargeList;
//	}
//	public void setFuelChargeList(List<FuelChargeModel> fuelChargeList) {
//		this.fuelChargeList = fuelChargeList;
//	}
//	public List<RouteMasterModel> getRouteList() {
//		return routeList;
//	}
//	public void setRouteList(List<RouteMasterModel> routeList) {
//		this.routeList = routeList;
//	}
//	public List<CustBillingAddressModel> getCustbilladdr() {
//		return custbilladdr;
//	}
//	public List<CollectionModel> getCollectionModel() {
//		return collectionModel;
//	}
//	public void setCollectionModel(List<CollectionModel> collectionModel) {
//		this.collectionModel = collectionModel;
//	}
//	public List<TaxMasterModel> getTaxDetails() {
//		return taxDetails;
//	}
//	public void setTaxDetails(List<TaxMasterModel> taxDetails) {
//		this.taxDetails = taxDetails;
//	}
	//	public List<Integer> getCustAddr() {
	//		return custAddr;
	//	}
	//	public void setCustAddr(List<Integer> custAddr) {
	//		this.custAddr = custAddr;
	//	}
//	public void setCustbilladdr(List<CustBillingAddressModel> custbilladdr) {
//		this.custbilladdr = custbilladdr;
//	}
	public Integer getBranch() {
		return branch;
	}
	public void setBranch(Integer branch) {
		this.branch = branch;
	}
	public Integer getCustAddr() {
		return custAddr;
	}
	public void setCustAddr(Integer custAddr) {
		this.custAddr = custAddr;
	}
	public int getRevenueId() {
		return revenueId;
	}
	public void setRevenueId(int revenueId) {
		this.revenueId = revenueId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	//	public String getSubProject() {
	//		return subProject;
	//	}
	//	public void setSubProject(String subProject) {
	//		this.subProject = subProject;
	//	}
	public String getRevenueTypeName() {
		return revenueTypeName;
	}
	public void setRevenueTypeName(String revenueTypeName) {
		this.revenueTypeName = revenueTypeName;
	}
	//	public BigDecimal getWtRate() {
	//		return wtRate;
	//	}
	//	public void setWtRate(BigDecimal wtRate) {
	//		this.wtRate = wtRate;
	//	}
	//	public BigDecimal getMinWtGuarantee() {
	//		return minWtGuarantee;
	//	}
	//	public void setMinWtGuarantee(BigDecimal minWtGuarantee) {
	//		this.minWtGuarantee = minWtGuarantee;
	//	}
	//	public int getAdditionalFlat() {
	//		return additionalFlat;
	//	}
	//	public void setAdditionalFlat(int additionalFlat) {
	//		this.additionalFlat = additionalFlat;
	//	}
	//	public BigDecimal getTripRate() {
	//		return tripRate;
	//	}
	//	public void setTripRate(BigDecimal tripRate) {
	//		this.tripRate = tripRate;
	//	}
	//	public int getVehicleTypeId() {
	//		return vehicleTypeId;
	//	}
	//	public void setVehicleTypeId(int vehicleTypeId) {
	//		this.vehicleTypeId = vehicleTypeId;
	//	}
	//	public String getVehicleType() {
	//		return vehicleType;
	//	}
	//	public void setVehicleType(String vehicleType) {
	//		this.vehicleType = vehicleType;
	//	}
	//	public int getRoute() {
	//		return route;
	//	}
	//	public void setRoute(int route) {
	//		this.route = route;
	//	}
	//	public BigDecimal getKmRate() {
	//		return kmRate;
	//	}
	//	public void setKmRate(BigDecimal kmRate) {
	//		this.kmRate = kmRate;
	//	}
	//	public BigDecimal getMinKmRate() {
	//		return minKmRate;
	//	}
	//	public void setMinKmRate(BigDecimal minKmRate) {
	//		this.minKmRate = minKmRate;
	//	}
	//	public BigDecimal getPercentSale() {
	//		return percentSale;
	//	}
	//	public void setPercentSale(BigDecimal percentSale) {
	//		this.percentSale = percentSale;
	//	}
	//	public String getPercentSaleType() {
	//		return percentSaleType;
	//	}
	//	public void setPercentSaleType(String percentSaleType) {
	//		this.percentSaleType = percentSaleType;
	//	}
	//	public BigDecimal getMinPercentGuarantee() {
	//		return minPercentGuarantee;
	//	}
	//	public void setMinPercentGuarantee(BigDecimal minPercentGuarantee) {
	//		this.minPercentGuarantee = minPercentGuarantee;
	//	}
	//	public int getCostDataId() {
	//		return costDataId;
	//	}
	//	public void setCostDataId(int costDataId) {
	//		this.costDataId = costDataId;
	//	}
	//	
	//	public int getCostType() {
	//		return costType;
	//	}
	//	public void setCostType(int costType) {
	//		this.costType = costType;
	//	}
	//	public BigDecimal getWtRateCD() {
	//		return wtRateCD;
	//	}
	//	public void setWtRateCD(BigDecimal wtRateCD) {
	//		this.wtRateCD = wtRateCD;
	//	}
	//	public BigDecimal getMinWtGuaranteeCD() {
	//		return minWtGuaranteeCD;
	//	}
	//	public void setMinWtGuaranteeCD(BigDecimal minWtGuaranteeCD) {
	//		this.minWtGuaranteeCD = minWtGuaranteeCD;
	//	}
	//	public int getAdditionalFlatCD() {
	//		return additionalFlatCD;
	//	}
	//	public void setAdditionalFlatCD(int additionalFlatCD) {
	//		this.additionalFlatCD = additionalFlatCD;
	//	}
	//	public BigDecimal getTripRateCD() {
	//		return tripRateCD;
	//	}
	//	public void setTripRateCD(BigDecimal tripRateCD) {
	//		this.tripRateCD = tripRateCD;
	//	}
	//	public int getVehicleTypeIdCD() {
	//		return vehicleTypeIdCD;
	//	}
	//	public void setVehicleTypeIdCD(int vehicleTypeIdCD) {
	//		this.vehicleTypeIdCD = vehicleTypeIdCD;
	//	}
	//	public String getVehicleTypeCD() {
	//		return vehicleTypeCD;
	//	}
	//	public void setVehicleTypeCD(String vehicleTypeCD) {
	//		this.vehicleTypeCD = vehicleTypeCD;
	//	}
	//	public int getRouteCD() {
	//		return routeCD;
	//	}
	//	public void setRouteCD(int routeCD) {
	//		this.routeCD = routeCD;
	//	}
	//	public BigDecimal getKmRateCD() {
	//		return kmRateCD;
	//	}
	//	public void setKmRateCD(BigDecimal kmRateCD) {
	//		this.kmRateCD = kmRateCD;
	//	}
	//	public BigDecimal getMinKmRateCD() {
	//		return minKmRateCD;
	//	}
	//	public void setMinKmRateCD(BigDecimal minKmRateCD) {
	//		this.minKmRateCD = minKmRateCD;
	//	}
	//	public BigDecimal getPercentSaleCD() {
	//		return percentSaleCD;
	//	}
	//	public void setPercentSaleCD(BigDecimal percentSaleCD) {
	//		this.percentSaleCD = percentSaleCD;
	//	}
	//	public String getPercentSaleTypeCD() {
	//		return percentSaleTypeCD;
	//	}
	//	public void setPercentSaleTypeCD(String percentSaleTypeCD) {
	//		this.percentSaleTypeCD = percentSaleTypeCD;
	//	}
	//	public BigDecimal getMinPercentGuaranteeCD() {
	//		return minPercentGuaranteeCD;
	//	}
	//	public void setMinPercentGuaranteeCD(BigDecimal minPercentGuaranteeCD) {
	//		this.minPercentGuaranteeCD = minPercentGuaranteeCD;
	//	}
	public String getCreatedTimestamp() {
		return createdTimestamp;
	}
	public void setCreatedTimestamp(String createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getUpdatedTimestamp() {
		return updatedTimestamp;
	}
	public void setUpdatedTimestamp(String updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}
	public Integer getSubProjectId() {
		return subProjectId;
	}
	public void setSubProjectId(Integer subProjectId) {
		this.subProjectId = subProjectId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public Integer getProjectTypeId() {
		return projectTypeId;
	}
	public void setProjectTypeId(Integer projectTypeId) {
		this.projectTypeId = projectTypeId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Integer getRevenueType() {
		return revenueType;
	}
	public void setRevenueType(Integer revenueType) {
		this.revenueType = revenueType;
	}
	public String getRevenueDetails() {
		return revenueDetails;
	}
	public void setRevenueDetails(String revenueDetails) {
		this.revenueDetails = revenueDetails;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	//	public CustBillingAddressModel getCustBillAddr() {
	//		return custBillAddr;
	//	}
	//	public void setCustBillAddr(CustBillingAddressModel custBillAddr) {
	//		this.custBillAddr = custBillAddr;
	//	}
	public String getSaacCode() {
		return saacCode;
	}
	public void setSaacCode(String saacCode) {
		this.saacCode = saacCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getDiscountPercent() {
		return discountPercent;
	}
	public void setDiscountPercent(double discountPercent) {
		this.discountPercent = discountPercent;
	}
}
