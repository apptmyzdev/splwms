package com.apptmyz.splwms.data;

import java.util.List;

public class Cons {

    private String conNo;
    private String scanTime;
    private boolean scanCompleted;
    private int numPkgsSheet;
    private long dlyDate;
    private String priority;
    private String nextDest;
    private String product;
    private String dlyStn;
    private String mbag;
    private int scannedPktCount;
    private String conStatus;
    private List<Packet> packets;

    public Cons() {
        super();
    }

    public Cons(String conNo, String scanTime, boolean scanCompleted, int numPkgsSheet, long dlyDate, String priority, String nextDest, String product, String dlyStn, String mbag, int scannedPktCount, String conStatus, List<Packet> packets) {
        this.conNo = conNo;
        this.scanTime = scanTime;
        this.scanCompleted = scanCompleted;
        this.numPkgsSheet = numPkgsSheet;
        this.dlyDate = dlyDate;
        this.priority = priority;
        this.nextDest = nextDest;
        this.product = product;
        this.dlyStn = dlyStn;
        this.mbag = mbag;
        this.scannedPktCount = scannedPktCount;
        this.conStatus = conStatus;
        this.packets = packets;
    }

    public String getConNo() {
        return conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public String getScanTime() {
        return scanTime;
    }

    public void setScanTime(String scanTime) {
        this.scanTime = scanTime;
    }

    public boolean isScanCompleted() {
        return scanCompleted;
    }

    public void setScanCompleted(boolean scanCompleted) {
        this.scanCompleted = scanCompleted;
    }

    public int getNumPkgsSheet() {
        return numPkgsSheet;
    }

    public void setNumPkgsSheet(int numPkgsSheet) {
        this.numPkgsSheet = numPkgsSheet;
    }

    public long getDlyDate() {
        return dlyDate;
    }

    public void setDlyDate(long dlyDate) {
        this.dlyDate = dlyDate;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getNextDest() {
        return nextDest;
    }

    public void setNextDest(String nextDest) {
        this.nextDest = nextDest;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDlyStn() {
        return dlyStn;
    }

    public void setDlyStn(String dlyStn) {
        this.dlyStn = dlyStn;
    }

    public String getMbag() {
        return mbag;
    }

    public void setMbag(String mbag) {
        this.mbag = mbag;
    }

    public int getScannedPktCount() {
        return scannedPktCount;
    }

    public void setScannedPktCount(int scannedPktCount) {
        this.scannedPktCount = scannedPktCount;
    }

    public String getConStatus() {
        return conStatus;
    }

    public void setConStatus(String conStatus) {
        this.conStatus = conStatus;
    }

    public List<Packet> getPackets() {
        return packets;
    }

    public void setPackets(List<Packet> packets) {
        this.packets = packets;
    }

    @Override
    public String toString() {
        return "Cons{" +
                "conNo='" + conNo + '\'' +
                ", scanTime='" + scanTime + '\'' +
                ", scanCompleted=" + scanCompleted +
                ", numPkgsSheet=" + numPkgsSheet +
                ", dlyDate=" + dlyDate +
                ", priority='" + priority + '\'' +
                ", nextDest='" + nextDest + '\'' +
                ", product='" + product + '\'' +
                ", dlyStn='" + dlyStn + '\'' +
                ", mbag='" + mbag + '\'' +
                ", scannedPktCount=" + scannedPktCount +
                ", conStatus='" + conStatus + '\'' +
                ", packets=" + packets +
                '}';
    }
}
