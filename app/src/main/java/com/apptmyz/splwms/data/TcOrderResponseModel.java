package com.apptmyz.splwms.data;

public class TcOrderResponseModel {
    private String customerCode;
    private Integer slaName;
    private String slaNameDescription; //use it for display at SLA
    private Address fromAddress;
    private Address toAddress;

    private Integer id;
    private String orderNumber;
    private String slaType;
    private String orderDateTime;
    private CustomerMasterModel customerMasterModel;
    private String orderType;
    private String origin;
    private String destination;
    private String originWh;
    private String receivingWh;

    public TcOrderResponseModel(Integer id, String orderNumber, String slaType, String orderDateTime, String orderType, String originWh, String receivingWh) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.slaType = slaType;
        this.orderDateTime = orderDateTime;
        this.orderType = orderType;
        this.originWh = originWh;
        this.receivingWh = receivingWh;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getSlaType() {
        return slaType;
    }

    public void setSlaType(String slaType) {
        this.slaType = slaType;
    }

    public String getOrderDateTime() {
        return orderDateTime;
    }

    public void setOrderDateTime(String orderDateTime) {
        this.orderDateTime = orderDateTime;
    }

    public CustomerMasterModel getCustomerMasterModel() {
        return customerMasterModel;
    }

    public void setCustomerMasterModel(CustomerMasterModel customerMasterModel) {
        this.customerMasterModel = customerMasterModel;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOriginWh() {
        return originWh;
    }

    public void setOriginWh(String originWh) {
        this.originWh = originWh;
    }

    public String getReceivingWh() {
        return receivingWh;
    }

    public void setReceivingWh(String receivingWh) {
        this.receivingWh = receivingWh;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public Integer getSlaName() {
        return slaName;
    }

    public void setSlaName(Integer slaName) {
        this.slaName = slaName;
    }

    public String getSlaNameDescription() {
        return slaNameDescription;
    }

    public void setSlaNameDescription(String slaNameDescription) {
        this.slaNameDescription = slaNameDescription;
    }

    public Address getFromAdress() {
        return fromAddress;
    }

    public void setFromAdress(Address fromAdress) {
        this.fromAddress = fromAdress;
    }

    public Address getToAddress() {
        return toAddress;
    }

    public void setToAddress(Address toAddress) {
        this.toAddress = toAddress;
    }

    @Override
    public String toString() {
        return "TcOrderResponseModel{" +
                "customerCode='" + customerCode + '\'' +
                ", slaName=" + slaName +
                ", slaNameDescription='" + slaNameDescription + '\'' +
                ", fromAdress=" + fromAddress +
                ", toAddress=" + toAddress +
                ", id=" + id +
                ", orderNumber='" + orderNumber + '\'' +
                ", slaType='" + slaType + '\'' +
                ", orderDateTime='" + orderDateTime + '\'' +
                ", customerMasterModel=" + customerMasterModel +
                ", orderType='" + orderType + '\'' +
                ", origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                ", originWh='" + originWh + '\'' +
                ", receivingWh='" + receivingWh + '\'' +
                '}';
    }
}
