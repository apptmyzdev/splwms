package com.apptmyz.splwms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InspectionPart;
import com.apptmyz.splwms.data.InwardNumsResponse;
import com.apptmyz.splwms.data.UnpackingBox;
import com.apptmyz.splwms.data.UnpackingData;
import com.apptmyz.splwms.data.UnpackingResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class UnpackingActivity extends BaseActivity {
    TextInputLayout vehicleNumEt, inwardNumEt;
    Button goBtn, submitBtn;
    RecyclerView partsRv;
    PartsAdapter adapter;
    List<UnpackingBox> boxList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    Gson gson = new Gson();
    DataSource dataSource;
    private String vehicleNum, inwardNum;
    private ImageView refreshIv, proceedIv;
    private Spinner inwardNumSp;
    private ArrayList<String> inwardNumsList = new ArrayList<>();

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_unpacking, frameLayout);
        context = UnpackingActivity.this;
        initializeViews();
    }

    private void initializeViews() {
        titleTv.setText("Unpacking");
        dataSource = new DataSource(context);
        vehicleNumEt = (TextInputLayout) findViewById(R.id.layout_vehicle_number);
        vehicleNumEt.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (Utils.isValidArrayList(inwardNumsList)) {
                    inwardNum = "";
                    inwardNumsList.clear();
                    inwardNumSp.setAdapter(null);
                }
            }
        });

        inwardNumEt = (TextInputLayout) findViewById(R.id.layout_inward_number);
        goBtn = (Button) findViewById(R.id.btn_go);
        submitBtn = (Button) findViewById(R.id.btn_submit);
        goBtn.setOnClickListener(listener);
        submitBtn.setOnClickListener(listener);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);
        proceedIv = (ImageView) findViewById(R.id.iv_proceed);
        proceedIv.setOnClickListener(listener);
        inwardNumSp = (Spinner) findViewById(R.id.sp_inward_number);
        inwardNumSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                inwardNum = inwardNumsList.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        partsRv.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this);
        partsRv.setLayoutManager(layoutManager);
        adapter = new PartsAdapter((ArrayList<UnpackingBox>) boxList);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isValidString(vehicleNum) && Utils.isValidString(inwardNum)) {
            setData();
        }
    }

    private void validateAndProceed() {
        vehicleNum = vehicleNumEt.getEditText().getText().toString().trim();
        if (Utils.isValidString(vehicleNum) && Utils.isValidVehicleNum(vehicleNum)) {
            if (Utils.getConnectivityStatus(context)) {
                new InwardNumsTask().execute();
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Vehicle Number");
        }
    }

    ProgressDialog inwardNumsDialog;

    class InwardNumsTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inwardNumsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getInwardNumsUrl(vehicleNum, 1).replace(" ", "%20");
                Utils.logD("Log 1");
                InwardNumsResponse response = (InwardNumsResponse) HttpRequest
                        .getInputStreamFromUrl(url, InwardNumsResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        inwardNumsList = (ArrayList<String>) response.getData();
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inwardNumsDialog != null && inwardNumsDialog.isShowing()) {
                inwardNumsDialog.dismiss();
            }
            if (showErrorDialog()) {
                setInwardNumsData();
            }
            super.onPostExecute(result);
        }
    }

    private void setInwardNumsData() {
        if (Utils.isValidArrayList(inwardNumsList)) {
            ArrayAdapter<String> inwardNumAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, inwardNumsList);
            inwardNumSp.setAdapter(inwardNumAdapter);
        }
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_proceed:
                    validateAndProceed();
                    break;
                case R.id.iv_refresh:
                    refresh();
                    break;
                case R.id.btn_go:
                    validateAndGetData();
                    break;
                case R.id.btn_submit:
                    validateAndSubmit();
                    break;
                default:
                    break;
            }
        }
    };

    ProgressDialog submitDialog;

    class SubmitTask extends AsyncTask<String, String, Object> {
        InspectionPart input;
        GeneralResponse response;

        SubmitTask(InspectionPart input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitUnpackingUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        refresh();
                    }
                    Utils.showToast(context, response.getMessage());
                }
            }
            super.onPostExecute(result);
        }
    }

    private void refresh() {
        vehicleNum = vehicleNumEt.getEditText().getText().toString().trim();
        if (Utils.isValidString(vehicleNum)) {
            if (Utils.isValidString(inwardNum)) {
                if (Utils.getConnectivityStatus(context)) {
                    new UnpackingTask(vehicleNum, inwardNum).execute();
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Inward Number");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Vehicle Number");
        }
    }

    private void validateAndGetData() {
        vehicleNum = vehicleNumEt.getEditText().getText().toString().trim();
        if (Utils.isValidString(vehicleNum)) {
            if (Utils.isValidString(inwardNum)) {
                boxList = dataSource.unpackingBoxes.getUnpackingBoxData(vehicleNum, inwardNum);
                if (Utils.isValidArrayList((ArrayList<?>) boxList)) {
                    setData();
                } else if (Utils.getConnectivityStatus(context)) {
                    new UnpackingTask(vehicleNum, inwardNum).execute();
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Inward Number");
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Valid Vehicle Number");
        }
    }

    ProgressDialog inspectionDialog;

    class UnpackingTask extends AsyncTask<String, String, Object> {
        private String vehicleNum, inwardNum;

        UnpackingTask(String vehicleNum, String inwardNum) {
            this.vehicleNum = vehicleNum;
            this.inwardNum = inwardNum;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inspectionDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String url = WmsUtils.getUnpackingDataUrl(vehicleNum, inwardNum);

                Utils.logD("Log 1");
                UnpackingResponse response = (UnpackingResponse) HttpRequest
                        .getInputStreamFromUrl(url, UnpackingResponse.class,
                                context);

                if (response != null) {
                    dataSource.unpackingBoxes.deleteUnpackingBoxData(vehicleNum, inwardNum);
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        UnpackingData data = response.getData();
                        if (data != null) {
                            boxList = data.getBoxList();
                            if (Utils.isValidArrayList((ArrayList<?>) boxList)) {
                                dataSource.unpackingBoxes.insertUnpackingBoxData(boxList, vehicleNum, inwardNum);
                                dataSource.unpackingParts.deleteUnpackingData(vehicleNum, inwardNum);
                                for (UnpackingBox box : boxList) {
                                    if (Utils.isValidArrayList((ArrayList<?>) box.getPartList())) {
                                        dataSource.unpackingParts.insertUnpackingData(box.getPartList(), vehicleNum, inwardNum, box.getMasterBoxNumber());
                                    } else if (Utils.isValidArrayList((ArrayList<?>) data.getPartList())) {
                                        dataSource.unpackingParts.insertUnpackingData(data.getPartList(), vehicleNum, inwardNum, box.getMasterBoxNumber());
                                    }
                                }
                            }
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inspectionDialog != null && inspectionDialog.isShowing()) {
                inspectionDialog.dismiss();
            }
            showErrorDialog();
            setData();
            super.onPostExecute(result);
        }
    }

    private void setData() {
        boxList = dataSource.unpackingBoxes.getUnpackingBoxData(vehicleNum, inwardNum);
        adapter = new PartsAdapter((ArrayList<UnpackingBox>) boxList);
        partsRv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(UnpackingActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void validateAndSubmit() {
        // TODO: 24/02/21 call submit API here
    }

    class PartsAdapter extends RecyclerView.Adapter<PartsAdapter.DataObjectHolder> {
        private ArrayList<UnpackingBox> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView masterBoxNumTv, quantityTv;
            Button editBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                masterBoxNumTv = (TextView) itemView.findViewById(R.id.tv_master_box_number);
                quantityTv = (TextView) itemView.findViewById(R.id.tv_status);
                editBtn = (Button) itemView.findViewById(R.id.btn_open);
                editBtn.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    if (v.getId() == R.id.btn_open) {
                        intent = new Intent(UnpackingActivity.this, UnpackingDetailActivity.class);
                        intent.putExtra("data", gson.toJson(list.get(pos)));
                        if (intent != null) {
                            startActivity(intent);
                        }
                    }
                }
            }
        }

        public PartsAdapter(ArrayList<UnpackingBox> myDataset) {
            list = myDataset;
        }

        @Override
        public PartsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_unpacking_item, parent, false);
            return new PartsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartsAdapter.DataObjectHolder holder, final int position) {
            UnpackingBox model = list.get(position);

            if (model != null) {
                holder.masterBoxNumTv.setText(model.getMasterBoxNumber());
                if (model.getBoxStatus() == -1)
                    holder.quantityTv.setText("Exception");
                else if (model.getBoxStatus() == 1)
                    holder.quantityTv.setText("Good");
                if (model.getStatus() == 1) {
                    holder.editBtn.setText("Edit");
                } else {
                    holder.editBtn.setText("Open");
                }
            }

            holder.masterBoxNumTv.setTag(position);
            holder.quantityTv.setTag(position);
            holder.editBtn.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<UnpackingBox> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(UnpackingBox dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }
}
