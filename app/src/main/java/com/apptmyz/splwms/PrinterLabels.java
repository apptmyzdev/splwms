package com.apptmyz.splwms;

import com.apptmyz.splwms.data.PackingModel;
import com.apptmyz.splwms.data.PackingSubmitJson;
import com.apptmyz.splwms.util.Utils;

import java.util.List;

public class PrinterLabels {
    public static String getTSCFormattedLabel(PackingSubmitJson input, String shipAddress) {
        String addressLine1 = "", addressLine2 = "", addressLine3 = "";

        if (Utils.isValidString(shipAddress)) {
            if (shipAddress.length() > 115) {
                shipAddress = shipAddress.substring(0, 115);
            }
            if (shipAddress.length() > 25) {
                addressLine1 = shipAddress.substring(0, 25);
                if (shipAddress.length() > 70) {
                    addressLine2 = shipAddress.substring(25, 70);
                    if (shipAddress.length() > 115) {
                        addressLine3 = shipAddress.substring(70);
                    } else {
                        addressLine3 = shipAddress.substring(70);
                    }
                } else {
                    addressLine2 = shipAddress.substring(25);
                }
            } else {
                addressLine1 = shipAddress;
            }
        }

        double weight = 0;
        int primaryBoxes = 0;
        for(PackingModel model: input.getPartList()) {
            weight += model.getWeight();
            primaryBoxes += model.getPrimarCartonsPacked();
        }

        String label = "SIZE 70 mm, 65 mm\n" +
                "DIRECTION 0\n" +
                "SPEED 10\n" +
                "BOX 20,30,525,75,4\n" +
                "BAR 20,75,3,155\n" + //vertical line

                "BAR 160,75,3,50\n" +
                "BAR 350,75,3,50\n" +

                "BAR 525,75,3,200\n" +

                "BAR 20,125,505,3\n" +

                "BOX 20,230,525,280,2\n" +

                "TEXT 25,45,\"ROMAN.TTF\",0,10,10,0,\"RTS    SPOTON              1800 200 1414\"\n" +
                "TEXT 25,80,\"ROMAN.TTF\",0,8,8,0,\"MC: " + input.getMasterBoxNo() + "\"\n" +
                "TEXT 210,80,\"ROMAN.TTF\",0,8,8,0,\"TOTAL PB: " + primaryBoxes + "\"\n" +
                "TEXT 390,80,\"ROMAN.TTF\",0,8,8,0,\"WEIGHT: " + weight + "\"\n" +

                "TEXT 25,130,\"ROMAN.TTF\",0,8,8,0,\"" + "CONSIGNEE ADDRESS: " + addressLine1 + "\"\n" +
                "TEXT 25,170,\"ROMAN.TTF\",0,8,8,0,\"" + addressLine2 + "\"\n" +
                "TEXT 25,210,\"ROMAN.TTF\",0,8,8,0,\"" + addressLine3 + "\"\n" +

                "TEXT 25,240,\"ROMAN.TTF\",0,10,10,0,\"" + "SL" + "\"\n" +
                "TEXT 130,240,\"ROMAN.TTF\",0,10,10,0,\"" + "PART No." + "\"\n" +
                "TEXT 360,240,\"ROMAN.TTF\",0,10,10,0,\"" + "Qty" + "\"\n" +
                "TEXT 410,240,\"ROMAN.TTF\",0,10,10,0,\"" + "PB" + "\"\n" +
                "TEXT 480,240,\"ROMAN.TTF\",0,10,10,0,\"" + "Wt" + "\"\n" +
                "BAR 75,230,3,50\n" +
                "BAR 350,230,3,50\n" +
                "BAR 400,230,3,50\n" +
                "BAR 460,230,3,50\n" +
                getPartsPrint(input.getPartList()) +
                "PRINT 1\n";

        return label;
    }

    public static String getPartsPrint(List<PackingModel> packingModels) {
        String str = "";
        for(int i = 0; i < packingModels.size(); i++) {
            str += getPartPrint(packingModels.get(i), i+1);
        }
        return str;
    }

    public static String getPartPrint(PackingModel packingModel, int srNo) {
        String label = "";
        int rowHeight = 230 + (srNo * 50);
        label = "BOX 20," + rowHeight +",525,"+ (280 + (srNo*50)) + ",2\n" +
                "TEXT 25," + (rowHeight + 5) + ",\"ROMAN.TTF\",0,10,10,0,\"" + srNo + "\"\n" +
                "TEXT 80," + (rowHeight + 5) + ",\"ROMAN.TTF\",0,10,10,0,\"" + packingModel.getPartNum() + "\"\n" +
                "TEXT 360," + (rowHeight + 5) + ",\"ROMAN.TTF\",0,10,10,0,\"" + packingModel.getQuantity() + "\"\n" +
                "TEXT 410," + (rowHeight + 5) + ",\"ROMAN.TTF\",0,10,10,0,\"" + packingModel.getPrimarCartonsPacked() + "\"\n" +
                "TEXT 480," + (rowHeight + 5) + ",\"ROMAN.TTF\",0,10,10,0,\"" + (int)packingModel.getWeight() + "\"\n" +
                "BAR 75," + rowHeight + ",3,50\n" +
                "BAR 350," + rowHeight + ",3,50\n" +
                "BAR 400," + rowHeight + ",3,50\n" +
                "BAR 460," + rowHeight + ",3,50\n";
        return label;
    }
}
