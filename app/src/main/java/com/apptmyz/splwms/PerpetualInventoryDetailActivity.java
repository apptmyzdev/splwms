package com.apptmyz.splwms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InventoryData;
import com.apptmyz.splwms.data.InventoryPart;
import com.apptmyz.splwms.data.InventorySubmitData;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PerpetualInventoryDetailActivity extends BaseActivity {
    private static int MAX_QUANTITY = 5;

    Context context;
    TextView titleTv, binNoTv;
    TextInputEditText productNumEt, binProductEt;
    ImageView scanProductIv, scanBinProductIv;
    CounterView counterView, binCounterView;
    Button saveBtn, submitBtn, addPartBtn, saveBinBtn, cancelBtn;
    RecyclerView partsRv;
    PartAdapter adapter;
    InventoryData inventoryData;
    LinearLayoutManager layoutManager;
    ArrayList<WmsPartInvoiceModel> parts = new ArrayList<>();
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String productNumber;
    WmsPartInvoiceModel part = null;
    Gson gson = new Gson();
    RadioGroup radioGroup;
    LinearLayout addBinLayout;
    private boolean isPerpetual;
    private int binId;
    private DataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_perpetual_inventory_detail, frameLayout);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        if (getIntent().getExtras() != null)
            isPerpetual = getIntent().getBooleanExtra("perpetual", false);
        if (isPerpetual)
            titleTv.setText("Perpetual Inventory");
        else
            titleTv.setText("Normal Inventory");
        context = PerpetualInventoryDetailActivity.this;
        dataSource = new DataSource(context);

        binNoTv = (TextView) findViewById(R.id.tv_bin_number);

        addBinLayout = (LinearLayout) findViewById(R.id.addbin_layout);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(checkListener);

        productNumEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumEt.addTextChangedListener(productNumWatcher);

        binProductEt = (TextInputEditText) findViewById(R.id.input_bin_product_number);
        binProductEt.addTextChangedListener(productNumWatcher);

        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);
        scanProductIv.setOnClickListener(listener);

        scanBinProductIv = (ImageView) findViewById(R.id.iv_scan_bin_product);
        scanBinProductIv.setOnClickListener(listener);

        saveBinBtn = (Button) findViewById(R.id.btn_save_bin);
        cancelBtn = (Button) findViewById(R.id.btn_cancel);
        saveBinBtn.setOnClickListener(listener);
        cancelBtn.setOnClickListener(listener);

        counterView = (CounterView) findViewById(R.id.counterView);
        binCounterView = (CounterView) findViewById(R.id.binCounterView);
        saveBtn = (Button) findViewById(R.id.btn_save);
        saveBtn.setOnClickListener(listener);
        submitBtn = (Button) findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(listener);

        addPartBtn = (Button) findViewById(R.id.btn_add_part);
        addPartBtn.setOnClickListener(listener);

        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        adapter = new PartAdapter(parts);
        layoutManager = new LinearLayoutManager(context);
        partsRv.setLayoutManager(layoutManager);
        partsRv.setAdapter(adapter);

        if (getIntent().getExtras() != null) {
            String str = getIntent().getStringExtra("inventory");
            inventoryData = gson.fromJson(str, InventoryData.class);
            binNoTv.setText(inventoryData.getBinNumber());
            binId = inventoryData.getBinId();
            parts = (ArrayList<WmsPartInvoiceModel>) inventoryData.getPartList();
            adapter = new PartAdapter(parts);
            partsRv.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    private RadioGroup.OnCheckedChangeListener checkListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            switch (checkedId) {
                case R.id.rb_yes:
//                    part.setConfirmed(true);
                    break;
                case R.id.rb_no:
//                    part.setConfirmed(false);
                    break;
                default:
                    break;
            }
        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()) {
                case R.id.btn_cancel:
                    addBinLayout.setVisibility(View.GONE);
                    break;
                case R.id.btn_add_part:
                    addBinLayout.setVisibility(View.VISIBLE);
                    break;

                case R.id.iv_scan_product:
                    productNumEt.setText("");
                    productNumEt.requestFocus();
                    break;
                case R.id.iv_scan_bin_product:
                    binProductEt.setText("");
                    binProductEt.requestFocus();
                    break;
                case R.id.btn_save_bin:
                    String msg = validatePartDetails();
                    if (!Utils.isValidString(msg)) {
                        parts.add(part);
                        part = null;
                        adapter = new PartAdapter(parts);
                        partsRv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        productNumEt.setText("");
                        addBinLayout.setVisibility(View.GONE);
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), msg);
                    }
                    break;
                case R.id.btn_submit:
                    if (Utils.isValidArrayList(parts)) {
                        boolean isQtyConfirmed = true;
                        for (WmsPartInvoiceModel part : parts) {
                            if (!part.isConfirmed()) {
                                isQtyConfirmed = false;
                                Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Confirm the Quantity for All parts");
                                break;
                            }
                        }
                        if (isQtyConfirmed) {
                            //call the submit api here
                            if (Utils.getConnectivityStatus(context)) {
                                InventorySubmitData input = new InventorySubmitData();
                                input.setBinId(binId);
                                List<InventoryPart> partList = new ArrayList<>();
                                for (WmsPartInvoiceModel part : parts) {
                                    InventoryPart p = new InventoryPart();
                                    p.setPartId(part.getPartId());
                                    if (part.getActualQty() != 0)
                                        p.setActualBoxes(part.getActualQty());
                                    else
                                        p.setActualBoxes(part.getTotalBoxes());
                                    p.setTotalBoxes(part.getTotalBoxes());
                                    partList.add(p);
                                }
                                input.setPartList(partList);
                                new SubmitTask(input).execute();
                            }
                        }
                        Toast.makeText(context, "Done", Toast.LENGTH_SHORT).show();
                        intent = new Intent(context, NormalInventoryActivity.class);
                        intent.putExtra("isPerpetual", true);
                    }
                    break;
                default:
                    break;
            }
            if (intent != null)
                startActivity(intent);
        }
    };

    ProgressDialog submitDialog;

    class SubmitTask extends AsyncTask<String, String, Object> {
        private InventorySubmitData data;

        SubmitTask(InventorySubmitData data) {
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                Gson gson = new Gson();
                String url = WmsUtils.getSubmitInventoryUrl();

                Utils.logD("Log 1");
                GeneralResponse response = (GeneralResponse) HttpRequest
                        .postData(url, gson.toJson(data, InventorySubmitData.class), GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }
            if (showErrorDialog()) {
                Utils.showToast(context, "Done");
                startActivity(new Intent(context, HomeActivity.class));
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PerpetualInventoryDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private String validatePartDetails() {
        String message = "";
        productNumber = (Utils.isValidString(productNumber)) ? productNumber : binProductEt.getText().toString().trim();

        if (Utils.isValidString(productNumber)) {
            part = new WmsPartInvoiceModel();
            part.setConfirmed(true);
            part.setQtyCorrect(true);
            part.setTotalBoxes(binCounterView.getValue());
            part.setPartNo(binProductEt.getText().toString().trim());
        } else {
            message = "Please Scan the Part";
        }
        return message;
    }

    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (scannedValue.length() >= 2) {
                                        productNumber = scannedValue;
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };
    WmsPartInvoiceModel editingPart = null;

    class PartAdapter extends RecyclerView.Adapter<PartAdapter.DataObjectHolder> {
        private ArrayList<WmsPartInvoiceModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView productNumTv, noOfBoxesTv, correctQtyTv, extraQtyTv;
            Button okBtn, editBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                okBtn = (Button) itemView.findViewById(R.id.btn_ok);
                editBtn = (Button) itemView.findViewById(R.id.btn_edit);
                correctQtyTv = (TextView) itemView.findViewById(R.id.tv_correct_qty);
                extraQtyTv = (TextView) itemView.findViewById(R.id.tv_extra_qty);

                okBtn.setOnClickListener(this);
                editBtn.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                if (v.getId() == okBtn.getId()) {
                    list.get(pos).setConfirmed(true);
                    list.get(pos).setQtyCorrect(true);
                    adapter = new PartAdapter(list);
                    partsRv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else if (v.getId() == editBtn.getId()) {
                    editingPart = list.get(pos);
                    showEditDialog();
                }
            }
        }

        public PartAdapter(ArrayList<WmsPartInvoiceModel> myDataset) {
            list = myDataset;
        }

        @Override
        public PartAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_perpetual_inventory_detail_item, parent, false);
            return new PartAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final PartAdapter.DataObjectHolder holder, final int position) {
            WmsPartInvoiceModel model = list.get(position);
            if (model != null) {
                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText(String.valueOf(model.getQty()));
                if (!model.isConfirmed()) {
                    holder.okBtn.setVisibility(View.VISIBLE);
                    holder.editBtn.setVisibility(View.VISIBLE);

                    holder.correctQtyTv.setVisibility(View.GONE);
                    holder.extraQtyTv.setVisibility(View.GONE);
                } else {
                    holder.okBtn.setVisibility(View.GONE);
                    holder.editBtn.setVisibility(View.GONE);

                    holder.correctQtyTv.setVisibility(View.VISIBLE);
                    holder.extraQtyTv.setVisibility(View.VISIBLE);

                    if (model.isQtyCorrect()) {
                        holder.correctQtyTv.setText(String.valueOf(model.getTotalBoxes()));
                        holder.extraQtyTv.setText("--");
                    } else {
                        holder.correctQtyTv.setText(String.valueOf(model.getActualQty()));
                        int difference = model.getActualQty() - model.getTotalBoxes();
                        holder.extraQtyTv.setText(String.valueOf(difference));
                        if (difference < 0) {
                            holder.extraQtyTv.setTextColor(getResources().getColor(R.color.red));
                        } else {
                            holder.extraQtyTv.setTextColor(getResources().getColor(R.color.green));
                        }
                    }
                }

                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.correctQtyTv.setTag(position);
                holder.extraQtyTv.setTag(position);
                holder.okBtn.setTag(position);
                holder.editBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<WmsPartInvoiceModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(WmsPartInvoiceModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void showEditDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCancelable(false);
        final EditText edittext = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        edittext.setLayoutParams(lp);
        edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setMessage("Enter the Actual Quantity");
        alert.setTitle("Edit Quantity");
        alert.setView(edittext);

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = edittext.getText().toString();
                editingPart.setConfirmed(true);
                editingPart.setQtyCorrect(false);
                editingPart.setActualQty(Integer.parseInt(value));
                dialog.dismiss();
                adapter = new PartAdapter(parts);
                partsRv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });

        alert.show();
    }
}
