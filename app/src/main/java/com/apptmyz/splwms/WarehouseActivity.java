package com.apptmyz.splwms;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.apptmyz.splwms.data.InvoiceModel;
import com.apptmyz.splwms.data.WarehouseMasterModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class WarehouseActivity extends BaseActivity {

    private List<WarehouseMasterModel> warehouseMasterModelList;
    private Gson gson = new Gson();
    private AutoCompleteTextView warehousesSp;
    private Button goBtn;
    private String warehouseCode, warehouseName;
    private Context context;
    private DataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_warehouse, frameLayout);
        context = WarehouseActivity.this;
        dataSource = new DataSource(context);

        warehousesSp = (AutoCompleteTextView) findViewById(R.id.sp_warehouse);
        goBtn = (Button) findViewById(R.id.btn_go);
        goBtn.setOnClickListener(listener);

        String warehousesStr = dataSource.sharedPreferences.getValue(Constants.WAREHOUSES_JSON);
        Type token = new TypeToken<List<WarehouseMasterModel>>() {
        }.getType();
        warehouseMasterModelList = gson.fromJson(warehousesStr, token);
        ArrayAdapter<WarehouseMasterModel> warehousesAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, warehouseMasterModelList);
        warehousesSp.setAdapter(warehousesAdapter);

        warehousesSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                WarehouseMasterModel model = ((WarehouseMasterModel) adapterView.getItemAtPosition(i));
                warehouseCode = model.getWhCode();
                warehouseName = model.getWhName();
            }
        });

        warehousesSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    warehousesSp.showDropDown();
                }
            }
        });

        warehousesSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                warehousesSp.showDropDown();
                return false;
            }
        });
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_go:
                    validateAndProceed();
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndProceed() {
        if (Utils.isValidString(warehouseCode)) {
            dataSource.sharedPreferences.set(Constants.WAREHOUSE_CODE, warehouseCode);
            Intent intent = new Intent(context, HomeActivity.class);
            dataSource.sharedPreferences.set(Constants.WAREHOUSE_NAME, warehouseName);
            intent.putExtra("refreshMasters", true);
            startActivity(intent);
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select a Warehouse");
        }
    }
}
