package com.apptmyz.splwms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.apptmyz.splwms.data.Bin;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class GrnActivity extends BaseActivity {
    Gson gson = new Gson();
    SparseBooleanArray mChecked = new SparseBooleanArray();
    BinAdapter adapter;
    CheckBox selectAllCb;
    RecyclerView binsRv;
    Button createGrnBtn;
    private List<Bin> selectedList = new ArrayList<>();
    private List<Bin> selectedBinList = new ArrayList<>();
    private List<Bin> binListUi = new ArrayList<>();
    int count;
    boolean isNotAdded;
    Context context;
    TextView titleTv;
    LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_grn, frameLayout);
        context = GrnActivity.this;

        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Create GRN");
        binListUi = Globals.bins;

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        adapter = new BinAdapter((ArrayList<Bin>) binListUi);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);
        binsRv.setAdapter(adapter);

        selectAllCb = (CheckBox) findViewById(R.id.cb_select_all);
        selectAllCb.setOnClickListener(clickListener);

        createGrnBtn = (Button) findViewById(R.id.btn_create_grn);
        createGrnBtn.setOnClickListener(listener);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_create_grn:
                    selectedList.clear();
                    selectedList = new ArrayList<>();
                    if (mChecked.size() > 0) {
                        for (int i = 0; i < count; i++) {
                            if (mChecked.get(i)) {
                                selectedList.add(binListUi.get(i));
                            }
                        }
                        if (Utils.isValidArrayList((ArrayList<?>) selectedList))
                            Utils.logD(selectedList.toString());

                        selectedBinList = (ArrayList<Bin>) selectedList;
                        goToCreateGRN();
                    } else {
                        Utils.showToast(context, "Please select atleast one order");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void goToCreateGRN() {
        Utils.showToast(context, "Done");
        Intent intent = new Intent(context, PutAwayActivity.class);
        startActivity(intent);
    }

    protected boolean isAllValuesChecked() {
        for (int i = 0; i < count; i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckBox chk = (CheckBox) v;
            int itemCount = count;
            for (int i = 0; i < itemCount; i++) {
                mChecked.put(i, selectAllCb.isChecked());
            }
            adapter.notifyDataSetChanged();
            isNotAdded = false;
        }
    };


    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<Bin> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv;
            CheckBox checkBox;

            public DataObjectHolder(View itemView) {
                super(itemView);

                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (buttonView.getTag() != null) {
                            int pos = (int) buttonView.getTag();

                            if (isChecked) {
                                mChecked.put(pos, isChecked);
                                if (isAllValuesChecked()) {
                                    selectAllCb.setChecked(isChecked);
                                }
                            } else {
                                mChecked.delete(pos);
                                selectAllCb.setChecked(isChecked);

                            }
                        }
                    }
                });
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(GrnActivity.this, PutAwayDetailActivity.class);
                    String binStr = gson.toJson(list.get(pos));
                    intent.putExtra("bin", binStr);

                    if (intent != null) {
                        startActivity(intent);
                        finish();
                    }
                }
            }
        }

        public BinAdapter(ArrayList<Bin> myDataset) {
            list = myDataset;
        }

        @Override
        public BinAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_grn_item, parent, false);
            return new BinAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BinAdapter.DataObjectHolder holder, final int position) {
            Bin model = list.get(position);
            if (model != null) {
                holder.binNumTv.setText(model.getBinNumber());
                holder.productNumTv.setText(model.getProductNumber());
                holder.noOfBoxesTv.setText(String.valueOf(model.getNoOfBoxesToScan()));
                holder.checkBox.setChecked((mChecked.get(position) == true ? true : false));

                holder.binNumTv.setTag(position);
                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
                holder.checkBox.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            count = list.size();
            return list.size();
        }

        public void update(ArrayList<Bin> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Bin dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

}
