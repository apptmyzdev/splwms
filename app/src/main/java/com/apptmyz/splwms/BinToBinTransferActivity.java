package com.apptmyz.splwms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.Bin;
import com.apptmyz.splwms.data.BinMasterModel;
import com.apptmyz.splwms.data.BinToBinTransferData;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.SearchBinOrPartResponse;
import com.apptmyz.splwms.data.TransferPart;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class BinToBinTransferActivity extends BaseActivity {

    TextInputEditText sourceBinNumEt, destBinNumEt, productNumEt;
    ImageView scanSourceBinIv, scanDestBinIv, scanProductIv;
    CounterView counterView;
    Context context;
    TextView titleTv, totalBoxesTv, srcBinTv;
    RecyclerView partsRv;
    Button transferBtn, saveBtn;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String srcBinNumber, destbinNumber, productNumber, warehouseCode;
    private ArrayList<WmsPartInvoiceModel> searchBinData = new ArrayList<>();
    private List<TransferPart> partList = new ArrayList<>();
    ArrayList<Bin> bins = new ArrayList<>();
    Bin bin = null;
    BinAdapter binAdapter;
    LinearLayoutManager layoutManager;
    private DataSource dataSource;
    private boolean isSaved, isValidPart;
    private int srcId, destId, partId, totalBoxes, boxesCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_bin_to_bin_transfer, frameLayout);

        context = BinToBinTransferActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("BIN to BIN Transfer");

        totalBoxesTv = (TextView) findViewById(R.id.tv_total_boxes);
        srcBinTv = (TextView) findViewById(R.id.src_bin_tv);

        sourceBinNumEt = (TextInputEditText) findViewById(R.id.input_source_bin_number);
        sourceBinNumEt.addTextChangedListener(sourceBinWatcher);
        destBinNumEt = (TextInputEditText) findViewById(R.id.input_dest_bin_number);
        destBinNumEt.addTextChangedListener(destBinWatcher);
        productNumEt = (TextInputEditText) findViewById(R.id.input_product_number);
        productNumEt.addTextChangedListener(productNumWatcher);
        scanSourceBinIv = (ImageView) findViewById(R.id.iv_scan_source_bin);
        scanDestBinIv = (ImageView) findViewById(R.id.iv_scan_dest_bin);
        scanProductIv = (ImageView) findViewById(R.id.iv_scan_product);

        saveBtn = (Button) findViewById(R.id.btn_save);
        saveBtn.setOnClickListener(listener);

        scanSourceBinIv.setOnClickListener(listener);
        scanDestBinIv.setOnClickListener(listener);
        scanProductIv.setOnClickListener(listener);

        counterView = (CounterView) findViewById(R.id.counterView);

        partsRv = (RecyclerView) findViewById(R.id.rv_parts);
        layoutManager = new LinearLayoutManager(this);
        partsRv.setLayoutManager(layoutManager);
        binAdapter = new BinAdapter(bins);
        partsRv.setAdapter(binAdapter);

        transferBtn = (Button) findViewById(R.id.btn_transfer);
        transferBtn.setOnClickListener(listener);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = null;
            switch (view.getId()) {
                case R.id.iv_scan_source_bin:
                    sourceBinNumEt.setText("");
                    sourceBinNumEt.requestFocus();
                    break;
                case R.id.iv_scan_dest_bin:
                    destBinNumEt.setText("");
                    destBinNumEt.requestFocus();
                    break;
                case R.id.iv_scan_product:
                    productNumEt.setText("");
                    productNumEt.requestFocus();
                    break;
                case R.id.btn_transfer:
                    validateAndTransfer();
                    break;
                case R.id.btn_save:
                    String msg = validateBinDetails();
                    if (!Utils.isValidString(msg)) {
                        bins.add(bin);
                        bin = null;
                        isSaved = true;
                        destBinNumEt.setEnabled(false);
                        destBinNumEt.setClickable(false);
                        destBinNumEt.setFocusableInTouchMode(false);
                        destBinNumEt.setFocusable(false);
                        scanDestBinIv.setEnabled(false);
                        scanDestBinIv.setClickable(false);

                        sourceBinNumEt.setEnabled(false);
                        sourceBinNumEt.setClickable(false);
                        sourceBinNumEt.setFocusableInTouchMode(false);
                        sourceBinNumEt.setFocusable(false);
                        scanSourceBinIv.setEnabled(false);
                        scanSourceBinIv.setClickable(false);

                        TransferPart part = new TransferPart(partId, counterView.getValue());
                        partList.add(part);

                        binAdapter = new BinAdapter(bins);
                        partsRv.setAdapter(binAdapter);
                        totalBoxesTv.setText(String.valueOf(addedBoxesCount));
                        binAdapter.notifyDataSetChanged();
                        productNumEt.setText("");
                    } else {
                        Utils.showSimpleAlert(context, getResources().getString(R.string.alert), msg);
                    }
                    break;
                default:
                    break;
            }
            if (intent != null) {
                startActivity(intent);
            }
        }
    };

    private void validateAndTransfer() {
        if (Utils.isValidString(srcBinNumber)) {
            if (Utils.isValidString(destbinNumber)) {
                if (Utils.isValidString(productNumber)) {
                    if (Utils.getConnectivityStatus(context)) {
                        BinToBinTransferData data = new BinToBinTransferData();
                        data.setDestBin(destId);
                        data.setSourceBin(srcId);
                        data.setPartList(partList);

                        new TransferTask(data).execute();
                    }
                } else {
                    Utils.showSimpleAlert(context, "Alert", "Please Scan the Part");
                }
            } else {
                Utils.showSimpleAlert(context, "Alert", "Please Scan the Destination BIN");
            }
        } else {
            Utils.showSimpleAlert(context, "Alert", "Please Scan the Source BIN");
        }
    }

    private int addedBoxesCount;

    private String validateBinDetails() {
        String message = "";
        if(!isValidPart) {
            return "The Source BIN doesn't have the scanned Part";
        }
        productNumber = productNumEt.getText().toString().trim();
        if (Utils.isValidString(srcBinNumber)) {
            if (Utils.isValidString(destbinNumber)) {
                if (Utils.isValidString(productNumber)) {
                    int boxesCount = counterView.getValue();
                    if (boxesCount != 0) {
                        addedBoxesCount += boxesCount;
                        bin = new Bin();
                        bin.setNoOfBoxesToScan(boxesCount);
                        bin.setProductNumber(productNumber);
                    } else {
                        message = "Please Scan Atleast one Part";
                    }
                } else {
                    message = "Please Scan the Part";
                }
            } else {
                message = "Please Scan the Destination BIN";
            }
        } else {
            message = "Please Scan the Source BIN";
        }
        return message;
    }

    private TextWatcher sourceBinWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = sourceBinNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 2) {
                                        srcBinNumber = scannedValue;
                                        srcId = 0;
                                        int matchCount = dataSource.bins.matchesData(scannedValue, warehouseCode);
                                        if (matchCount == 0) {
                                            srcBinNumber = "";
                                            searchBinData = new ArrayList<>();
                                            Utils.showSimpleAlert(context, "Alert", "BIN Number doesn't exist");
                                        } else {
                                            if (dataSource.bins.exists(srcBinNumber, warehouseCode) != -1) {
                                                srcBinTv.setText(srcBinNumber);
                                                BinMasterModel model = dataSource.bins.getBin(srcBinNumber, warehouseCode);
                                                srcId = model.getId();
                                                searchBin(srcBinNumber);
                                            }
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private void searchBin(String binNumber) {
        if (Utils.isValidString(binNumber)) {
            if (Utils.getConnectivityStatus(context))
                new SearchBinOrPartTask("B", binNumber).execute();
        }
    }

    ProgressDialog searchDialog;

    class SearchBinOrPartTask extends AsyncTask<String, String, Object> {
        private String data;
        private String type;

        SearchBinOrPartTask(String type, String data) {
            this.type = type;
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                searchDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                searchBinData.clear();
                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSearchBinOrPartUrl(data, type, warehouseCode);

                Utils.logD("Log 1");
                SearchBinOrPartResponse response = (SearchBinOrPartResponse) HttpRequest
                        .getInputStreamFromUrl(url, SearchBinOrPartResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        if (response.getData() != null) {
                            searchBinData = (ArrayList<WmsPartInvoiceModel>) response.getData();
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && searchDialog != null && searchDialog.isShowing()) {
                searchDialog.dismiss();
            }
            if (showErrorDialog()) {
                // TODO: 15/01/21
            }
            super.onPostExecute(result);
        }
    }

    ProgressDialog transferDialog;

    class TransferTask extends AsyncTask<String, String, Object> {
        private BinToBinTransferData data;

        TransferTask(BinToBinTransferData data) {
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                transferDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {
            try {
                Globals.lastErrMsg = "";
                Gson gson = new Gson();
                String url = WmsUtils.getBinTransferUrl();

                Utils.logD("Log 1");
                GeneralResponse response = (GeneralResponse) HttpRequest
                        .postData(url, gson.toJson(data, BinToBinTransferData.class), GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && transferDialog != null && transferDialog.isShowing()) {
                transferDialog.dismiss();
            }
            if (showErrorDialog()) {
                Utils.showToast(context, "Done");
                startActivity(new Intent(context, HomeActivity.class));
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(BinToBinTransferActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private TextWatcher destBinWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = destBinNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (scannedValue.length() >= 2) {
                                        destbinNumber = scannedValue;
                                        destId = 0;

                                        int matchCount = dataSource.bins.matchesData(scannedValue, warehouseCode);
                                        if (matchCount == 0) {
                                            destbinNumber = "";
                                            Utils.showSimpleAlert(context, "Alert", "BIN Number doesn't exist");
                                        } else {
                                            if (dataSource.bins.exists(destbinNumber, warehouseCode) != -1) {
                                                BinMasterModel model = dataSource.bins.getBin(destbinNumber, warehouseCode);
                                                destId = model.getId();
                                            }
                                        }
                                    }

                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };


    private TextWatcher productNumWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);
                                    if (scannedValue.length() >= 3) {
                                        totalBoxes = 0;
                                        partId = 0;
                                        productNumber = scannedValue;
                                        if (Utils.isValidArrayList(searchBinData)) {
                                            boolean isContains = false;
                                            for (WmsPartInvoiceModel model : searchBinData) {
                                                if (model.getPartNo().equals(productNumber.toUpperCase())) {
                                                    isContains = true;
                                                    partId = model.getPartId();
                                                    totalBoxes = model.getQty();
                                                    counterView.setmMinLimit(1);
                                                    counterView.setValue(totalBoxes);
                                                    counterView.setmMaxLimit(totalBoxes);
                                                    break;
                                                } else if (model.getPartNo().contains(productNumber.toUpperCase())) {
                                                    isContains = true;
                                                }
                                            }
                                            isValidPart = true;
                                            if (!isContains) {
                                                isValidPart = false;
                                                Utils.showSimpleAlert(context, "Alert", "The Source BIN doesn't have the Scanned Part");
                                            }
                                        } else {
                                            Utils.showSimpleAlert(context, "Alert", "Please scan a Source BIN which has Parts in it");
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<Bin> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder {
            TextView productNumTv, noOfBoxesTv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                productNumTv = (TextView) itemView.findViewById(R.id.tv_part_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes);
            }
        }

        public BinAdapter(ArrayList<Bin> myDataset) {
            list = myDataset;
        }

        @Override
        public BinAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_bin_transfer_part_item, parent, false);
            return new BinAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BinAdapter.DataObjectHolder holder, final int position) {
            Bin model = list.get(position);
            if (model != null) {
                holder.productNumTv.setText(model.getProductNumber());
                holder.noOfBoxesTv.setText(String.valueOf(model.getNoOfBoxesToScan()));

                holder.productNumTv.setTag(position);
                holder.noOfBoxesTv.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<Bin> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Bin dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

}
