package com.apptmyz.splwms;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.custom.ImageProcessor;
import com.apptmyz.splwms.data.ExceptionModel;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InspectionBarcodeData;
import com.apptmyz.splwms.data.InspectionPart;
import com.apptmyz.splwms.data.InventoryTypeModel;
import com.apptmyz.splwms.data.PartMasterModel;
import com.apptmyz.splwms.data.ProductMasterModel;
import com.apptmyz.splwms.data.RmaInfo;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.CameraUtils;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.ScanUtils;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class InboundInspectionDetailActivity extends BaseActivity {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3, MY_PERMISSIONS_CAMERA = 4;
    private TextView partNumTv, vehicleNumTv, inventoryTypeTv, masterCartonsTv, headingExceptionTv;
    private EditText goodBoxesEt, exceptionTypeEt, reasonEt, partNumEt;
    private Button exceptionBtn, summaryBtn, captureImageBtn, sendBtn, cancelExceptionBtn, captureNewBtn, cancelImgsBtn;
    private RecyclerView imagesRv, exceptionsRv;
    private ImagesAdapter imagesAdapter;
    private CounterView counterView;
    private Context context;
    private RelativeLayout exceptionLayout;
    private LinearLayout inventoryTypesLayout, partNumLayout;
    private DataSource dataSource;
    private ExceptionsAdapter exceptionsAdapter;
    private String imageStoragePath, capBase64Filename = "", capBase64 = "", warehouseCode;
    private static final int CAPTURE_IMG = 1;
    private static final int CROP_IMG = 2;
    private ArrayList<String> imgBase64s;
    private ArrayList<String> imgFileNames;
    private ConstraintLayout imagesView;
    private int imagePosition = -1, exceptionCount, noOfMasterCartons;
    private ImageLoader imageLoader;
    private InspectionPart part;
    private List<ExceptionModel> exceptionModels = new ArrayList<>();
    private Gson gson = new Gson();
    private boolean isHideViewBtn = false, isInventoryTypeMismatch;
    List<InspectionBarcodeData> barcodesData = new ArrayList<>();

    private AlertDialog barcodesAlert, rmaInfoAlert;
    private BarcodesAdapter barcodesAdapter;
    private RecyclerView barcodesRv;
    private AlertDialog addBarcodeAlert;
    private Spinner inventoryTypesSp;
    private int actualInventoryTypeId;
    private String actualInventoryTypeDesc;
    private RmaInfo rmaInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_inbound_inspection_detail, frameLayout);
        context = InboundInspectionDetailActivity.this;
        initializeViews();
    }

    private void initializeViews() {
        titleTv.setText("Inbound Inspection Summary");
        dataSource = new DataSource(this);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        partNumTv = (TextView) findViewById(R.id.tvPartNumber);
        vehicleNumTv = (TextView) findViewById(R.id.spSubproject);
        inventoryTypeTv = (TextView) findViewById(R.id.tvInventoryType);
        masterCartonsTv = (TextView) findViewById(R.id.tvShipAddress);
        goodBoxesEt = (EditText) findViewById(R.id.et_good_boxes);
        exceptionBtn = (Button) findViewById(R.id.btn_exception);
        summaryBtn = (Button) findViewById(R.id.btn_submit);
        summaryBtn.setOnClickListener(listener);
        exceptionBtn.setOnClickListener(listener);

        exceptionLayout = (RelativeLayout) findViewById(R.id.layout_exception_inbound);
        exceptionTypeEt = (EditText) findViewById(R.id.et_exception_type);
        reasonEt = (EditText) findViewById(R.id.reasons_et);
        captureImageBtn = (Button) findViewById(R.id.btn_captureimage);
        counterView = (CounterView) findViewById(R.id.exceptionCv);
        sendBtn = (Button) findViewById(R.id.btn_send);
        cancelExceptionBtn = (Button) findViewById(R.id.btn_cancel);
        imagesView = (ConstraintLayout) findViewById(R.id.layout_image_capture);
        imagesRv = (RecyclerView) findViewById(R.id.rv_exception_images);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));

        exceptionsRv = (RecyclerView) findViewById(R.id.rv_exceptions);
        exceptionsRv.setLayoutManager(new LinearLayoutManager(context));
        exceptionsAdapter = new ExceptionsAdapter((ArrayList<ExceptionModel>) exceptionModels);
        exceptionsRv.setAdapter(exceptionsAdapter);

        captureNewBtn = (Button) findViewById(R.id.btn_captue_new);
        cancelImgsBtn = (Button) findViewById(R.id.btn_cancel_imgs);
        cancelImgsBtn.setOnClickListener(listener);
        captureNewBtn.setOnClickListener(listener);
        imageLoader = Utils.getImageLoader(context, "IMAGES");
        if (Utils.isValidArrayList(imgBase64s)) {
            imagesAdapter = new ImagesAdapter(imgBase64s);
            imagesRv.setAdapter(imagesAdapter);
        }

        if (getIntent().getExtras() != null) {
            String dataStr = getIntent().getStringExtra("data");
            Gson gson = new Gson();
            part = gson.fromJson(dataStr, InspectionPart.class);
            vehicleNumTv.setText(part.getVehicleNumber());
            inventoryTypeTv.setText(part.getInventoryTypeDesc());
            if (part.getIsProduct() == 0)
                partNumTv.setText(part.getPartNo());
            else
                partNumTv.setText(part.getProductName());
            masterCartonsTv.setText(String.valueOf(part.getNoOfMasterCartons()));
            noOfMasterCartons = part.getNoOfMasterCartons();
        }
    }

    private void captureImage(int requestCode, String imageFileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE, imageFileName);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            fileUri = Uri.fromFile(file);
        } else {
            fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, requestCode);
    }

    private AlertDialog alert, imagesAlert;

    private void showExceptionDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_inbound_exception, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        alert = alertDialog.show();
        inventoryTypesLayout = (LinearLayout) view.findViewById(R.id.inventory_types_ll);
        headingExceptionTv = (TextView) view.findViewById(R.id.tv_exception_heading);
        headingExceptionTv.setText("Inbound Exception");
        exceptionTypeEt = (EditText) view.findViewById(R.id.et_exception_type);
        inventoryTypesSp = (Spinner) view.findViewById(R.id.sp_inventory_types);
        ArrayList<InventoryTypeModel> inventoryTypes = dataSource.inventoryTypes.getInventoryTypes();
        ArrayAdapter<InventoryTypeModel> inventoryTypeModelArrayAdapter = new ArrayAdapter<>(InboundInspectionDetailActivity.this,
                android.R.layout.simple_spinner_dropdown_item, inventoryTypes);
        inventoryTypesSp.setAdapter(inventoryTypeModelArrayAdapter);

        inventoryTypesSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                InventoryTypeModel model = (InventoryTypeModel) inventoryTypesSp.getSelectedItem();
                actualInventoryTypeId = model.getId();
                actualInventoryTypeDesc = model.getType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        partNumLayout = (LinearLayout) view.findViewById(R.id.partNumLayout);
        partNumEt = (EditText) view.findViewById(R.id.et_part_number);
        reasonEt = (EditText) view.findViewById(R.id.reasons_et);
        captureImageBtn = (Button) view.findViewById(R.id.btn_captureimage);
        counterView = (CounterView) view.findViewById(R.id.exceptionCv);
        sendBtn = (Button) view.findViewById(R.id.btn_send);
        cancelExceptionBtn = (Button) view.findViewById(R.id.btn_cancel);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndCreateException();
            }
        });
        exceptionTypeEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> depsList = new ArrayList<>();
                List<String> exceptionsList = dataSource.exceptionTypes.getExceptionList(warehouseCode, "IN");
                for (String exception : exceptionsList) {
                    if (!exception.equalsIgnoreCase("Extra")) {
                        depsList.add(exception);
                    }
                }
                String[] a = new String[depsList.size()];
                a = depsList.toArray(a);
                showExceptionType(exceptionTypeEt, a);
            }
        });
        cancelExceptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                resetExceptionView();
            }
        });
        captureImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isHideViewBtn = false;
                capBase64Filename = "IMAGE_Exception";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        return;
                    }
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_CAMERA);

                        return;
                    }
                }
                if (!Utils.isValidArrayList(imgBase64s)) {
                    imagePosition = -1;
                    captureImage(CAPTURE_IMG, capBase64Filename + "_1"); //opens Camera Activity
                } else {
                    showImagesDialog(imgBase64s);
                }
            }
        });
    }

    private void showImagesDialog(ArrayList<String> imageBase64s) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_capture_image, null, false);
        view.setVisibility(View.VISIBLE);
        imagesRv = (RecyclerView) view.findViewById(R.id.rv_exception_images);
        imagesRv.setNestedScrollingEnabled(false);
        captureNewBtn = (Button) view.findViewById(R.id.btn_captue_new);
        cancelImgsBtn = (Button) view.findViewById(R.id.btn_cancel_imgs);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));
        if (Utils.isValidArrayList(imageBase64s)) {
            imagesAdapter = new ImagesAdapter(imageBase64s);
            imagesRv.setAdapter(imagesAdapter);
            imagesAdapter.notifyDataSetChanged();
        }
        cancelImgsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesAlert.dismiss();
                imagesView.setVisibility(View.GONE);
            }
        });
        captureNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidArrayList(imageBase64s) && imageBase64s.size() == 3) {
                    Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                } else {
                    imagePosition = -1;
                    captureImage(CAPTURE_IMG, capBase64Filename + "_" + imagePosition);
                }
            }
        });
        alertDialog.setView(view);
        imagesAlert = alertDialog.show();
    }

    private void validateAndCreateException() {
        if (imagesAlert != null && imagesAlert.isShowing())
            imagesAlert.dismiss();
        imagesView.setVisibility(View.GONE);
        String reason = reasonEt.getText().toString();
        String excepDesc = exceptionTypeEt.getText().toString();
        String exceptionType = "";
        String partNum = partNumEt.getText().toString().trim();
        PartMasterModel partMasterModel = dataSource.parts.getPartData(partNum, warehouseCode);
        ProductMasterModel productModel = dataSource.products.getProductData(partNum);
        exceptionType = dataSource.exceptionTypes.getExceptionId(excepDesc, warehouseCode);
        if (Utils.isValidString(reason)) {
            if (Utils.isValidString(exceptionType)) {
                if (!isInventoryTypeMismatch || (isInventoryTypeMismatch && actualInventoryTypeId != 0)) {
                    int excepTypeVal = Integer.parseInt(exceptionType);
                    int currentCount = counterView.getValue();
                    int goodBoxesCount = 0;
                    if (Utils.isValidString(goodBoxesEt.getText().toString().trim()))
                        goodBoxesCount = Integer.parseInt(goodBoxesEt.getText().toString().trim());
                    int maxLimit = part.getNoOfMasterCartons() - (goodBoxesCount + exceptionCount);
                    if (!exceptionType.equals(Constants.EXTRA) || partMasterModel != null || productModel != null) {
                        if (exceptionType.equals(Constants.EXTRA) || Utils.isValidArrayList(imgFileNames)) {
                            if (currentCount != 0 && (exceptionType.equals(Constants.EXTRA) || currentCount <= maxLimit)) {
                                alert.dismiss();
                                ExceptionModel exceptionModel = new ExceptionModel();
                                exceptionModel.setScanTime(Utils.getScanTime());
                                exceptionModel.setExcepType(excepTypeVal);
                                exceptionModel.setExcepDesc(reason);
                                exceptionModel.setPartMasterModel(partMasterModel);
                                if (Utils.isValidArrayList(imgBase64s))
                                    exceptionModel.setImgBase64s((List<String>) imgBase64s.clone());
                                exceptionModel.setExceptionCount(currentCount);
                                exceptionModel.setActualInventoryTypeId(actualInventoryTypeId);
                                exceptionModel.setReceiptType(part.getReceiptType());
                                exceptionModel.setIsProduct(part.getIsProduct());
                                exceptionModel.setProductId(part.getProductId());
                                exceptionModel.setProductName(part.getProductName());

                                exceptionModels.add(exceptionModel);
//                        dataSource.exceptions.insertException(context, exceptionModel, Globals.partNo, Globals.vehicleNo);
                                setExceptionsData();
                                if (Utils.isValidArrayList(imgBase64s))
                                    imgBase64s.clear();
                                resetExceptionView();
                            } else {
                                Utils.showToast(context, "Please Enter Valid Exception Count");
                            }
                        } else {
                            Utils.showToast(context, "Please capture image");
                        }
                    } else {
                        Utils.showToast(context, "Please Enter Valid Part/Product Number");
                    }
                } else {
                    Utils.showToast(context, "Please Select the Actual Inventory Type");
                }
            } else {
                Utils.showToast(context, "Please select exception type");
            }
        } else {
            Utils.showToast(context, "Please enter valid reason");
        }
    }

    private void setExceptionsData() {
        exceptionCount = 0;
        for (ExceptionModel model : exceptionModels) {
            exceptionCount += model.getExceptionCount();
        }
        exceptionsAdapter = new ExceptionsAdapter((ArrayList<ExceptionModel>) exceptionModels);
        exceptionsRv.setAdapter(exceptionsAdapter);
        exceptionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void resetExceptionView() {
        exceptionTypeEt.setText(null);
        reasonEt.setText(null);
        counterView.setValue(1);
        imagesRv.setAdapter(null);
        captureImageBtn.setBackgroundResource(R.drawable.btn_bg_orange);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_cancel:
                    alert.dismiss();
                    resetExceptionView();
                    break;
                case R.id.btn_send:
                    validateAndCreateException();
                    break;
                case R.id.btn_captureimage:
                    isHideViewBtn = false;
                    capBase64Filename = "IMAGE_Exception";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                            return;
                        }
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_CAMERA);

                            return;
                        }
                    }
                    if (!Utils.isValidArrayList(imgBase64s)) {
                        imagePosition = -1;
                        captureImage(CAPTURE_IMG, capBase64Filename + "_1"); //opens Camera Activity
                    } else {
                        showImagesDialog(imgBase64s);
                    }
                    break;
                case R.id.btn_submit:
                    showBarcodesPopup(part);
                    break;
                case R.id.btn_exception:
                    String goodBoxesStr = goodBoxesEt.getText().toString().trim();
                    int goodBoxesCount = 0;
                    if (Utils.isValidString(goodBoxesStr))
                        goodBoxesCount = Integer.parseInt(goodBoxesStr);
                    if (noOfMasterCartons - (goodBoxesCount + exceptionCount) != 0) {
                        counterView.setmMinLimit(1);
                        counterView.setmMaxLimit(noOfMasterCartons - (goodBoxesCount + exceptionCount));
                        isHideViewBtn = false;
                        captureNewBtn.setVisibility(View.VISIBLE);
                        showExceptionDialog();
                    } else {
                        Utils.showSimpleAlert(context, getString(R.string.alert), "All the Boxes are marked as Either Good or Exception. You Can't add More Exceptions.");
                    }
                    break;
                case R.id.et_exception_type:
                    List<String> depsList = new ArrayList<>();
                    depsList.addAll(dataSource.exceptionTypes.getExceptionList(warehouseCode, "IN"));
                    String[] a = new String[depsList.size()];
                    a = depsList.toArray(a);
                    showExceptionType(exceptionTypeEt, a);
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndSubmit() {
        if (!Utils.isValidArrayList((ArrayList<?>) exceptionModels) || validateExceptionCount()) {
            String goodBoxesStr = goodBoxesEt.getText().toString().trim();
            int goodBoxesCount = 0;
            if (Utils.isValidString(goodBoxesStr)) {
                goodBoxesCount = Integer.parseInt(goodBoxesStr);
            }
            if (noOfMasterCartons == goodBoxesCount + exceptionCount) {
                if (Utils.getConnectivityStatus(context)) {
                    new SubmitTask(getSubmitData()).execute();
                }
            } else {
                Utils.showSimpleAlert(context, getString(R.string.alert), "Some Boxes are Not marked as either Good or Exception");
            }
        }
    }

    private boolean validateExceptionCount() {
        boolean isValid = true;
        String goodBoxesStr = goodBoxesEt.getText().toString().trim();
        int goodBoxesCount = 0;
        if (Utils.isValidString(goodBoxesStr))
            goodBoxesCount = Integer.parseInt(goodBoxesStr);
        int excepCount = 0;
        for (ExceptionModel model : exceptionModels) {
            if (model.getExcepType() != 10)
                excepCount += model.getExceptionCount();
        }
        if (noOfMasterCartons == goodBoxesCount + excepCount) {
            isValid = true;
        } else {
            isValid = false;
        }
        return isValid;
    }

    private InspectionPart getSubmitData() {
        InspectionPart inspectionPart = new InspectionPart();
        int goodBoxesCount = 0;
        if (Utils.isValidString(goodBoxesEt.getText().toString().trim()))
            goodBoxesCount = Integer.parseInt(goodBoxesEt.getText().toString().trim());

        inspectionPart.setMasterBoxNo(part.getMasterBoxNo());
        inspectionPart.setDaNo(part.getDaNo());
        inspectionPart.setInvoiceNo(part.getInvoiceNo());
        inspectionPart.setGoodBoxes(goodBoxesCount);
        inspectionPart.setNoOfMasterCartons(part.getNoOfMasterCartons());
        inspectionPart.setPartId(part.getPartId());
        inspectionPart.setPartNo(part.getPartNo());
        inspectionPart.setNoOfExceptions(part.getNoOfExceptions());
        inspectionPart.setInwardNumber(part.getInwardNumber());
        inspectionPart.setVehicleNumber(part.getVehicleNumber());
        inspectionPart.setQty(part.getQty());
        inspectionPart.setReceiptType(part.getReceiptType());
        inspectionPart.setExceptionList(exceptionModels);
        inspectionPart.setDataType(part.getDataType());
        inspectionPart.setRepacked(part.isRepacked());
        inspectionPart.setBarcodesData(barcodesData);
        inspectionPart.setExceptType(part.getExceptType());
        inspectionPart.setExceptTypeDescription(part.getExceptTypeDescription());
        inspectionPart.setInventoryTypeId(part.getInventoryTypeId());
        inspectionPart.setInventoryTypeDesc(part.getInventoryTypeDesc());
        inspectionPart.setRmaInfo(rmaInfo);
        inspectionPart.setIsProduct(part.getIsProduct());
        inspectionPart.setProductId(part.getProductId());
        inspectionPart.setProductName(part.getProductName());
        return inspectionPart;
    }

    ProgressDialog submitDialog;

    class SubmitTask extends AsyncTask<String, String, Object> {
        InspectionPart input;
        GeneralResponse response;

        SubmitTask(InspectionPart input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getSubmitInspectionUrl(warehouseCode);

                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                        dataSource.inboundInspection.deleteInspectionData(input.getUniqueKey());
                    }
                    Utils.showToast(context, response.getMessage());
                }
                Intent intent = new Intent(context, InboundInspectionActivity.class);
                startActivity(intent);
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(InboundInspectionDetailActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void showExceptionType(final EditText exceptionTv,
                                   final String arr[]) {
        int selectedPos = -1;
        String text = URLDecoder
                .decode(exceptionTv.getText().toString().trim());
        if (Utils.isValidString(text)) {
            for (int i = 0; i < arr.length; i++) {
                if (text.equalsIgnoreCase(arr[i].toString())) {
                    selectedPos = i;
                    break;
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setSingleChoiceItems(arr, selectedPos,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        dialog.dismiss();
                        exceptionTv.setText(arr[pos]);
                        exceptionTypeEt.setText(arr[pos]);
                        if (arr[pos].equals(getString(R.string.mismatch_inventory))) {
                            isInventoryTypeMismatch = true;
                            inventoryTypesLayout.setVisibility(View.VISIBLE);
                        } else {
                            isInventoryTypeMismatch = false;
                            inventoryTypesLayout.setVisibility(View.GONE);
                        }
                        if (arr[pos].equals(getString(R.string.extra))) {
                            captureImageBtn.setVisibility(View.GONE);
                            partNumLayout.setVisibility(View.VISIBLE);
                        } else {
                            captureImageBtn.setVisibility(View.VISIBLE);
                            partNumLayout.setVisibility(View.GONE);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_btn),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).show();
    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<String> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        showImagePreview(getBitmap(imgBase64s.get(pos)), pos);
                        break;
                    case R.id.iv_delete:
                        imgBase64s.remove((int) v.getTag());
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<String> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            String base64 = bitmaps.get(position);
            if (base64 != null) {
                holder.imageView.setImageBitmap(getBitmap(base64));
                if (isHideViewBtn) {
                    holder.viewBtn.setVisibility(View.GONE);
                    holder.deleteIv.setVisibility(View.GONE);
                } else {
                    holder.viewBtn.setVisibility(View.VISIBLE);
                    holder.deleteIv.setVisibility(View.VISIBLE);
                }

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<String> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(String dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imagePosition = position;
                captureImage(CAPTURE_IMG, capBase64Filename + "_" + position);
            }
        });
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }

    private Bitmap getFullBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            int IMAGE_MAX_SIZE = 0;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                IMAGE_MAX_SIZE = 500000; //0.5MP
            } else {
                IMAGE_MAX_SIZE = 1200000; // 1.2MP
            }
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);

            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            Matrix mat = new Matrix();
            mat.postRotate(angle);

            b = Bitmap.createBitmap(b, 0, 0,
                    b.getWidth(), b.getHeight(), mat, true);

            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_IMG:
                    try {
                        bitmap = getFullBitmap(imageStoragePath);
                        if (bitmap != null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] imageBytes = baos.toByteArray();
                            capBase64 = capBase64Filename;
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);

                            if (Utils.isValidString(capBase64))
                                captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            Date date = new Date();
                            long d = date.getTime();
                            Bitmap resizedBitmap = null;
                            if (bitmap != null) {
                                int w = bitmap.getWidth();
                                int h = bitmap.getHeight();
                                Matrix mat = new Matrix();
                                if (w > h)
                                    mat.postRotate(90);

                                resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                                ByteArrayOutputStream baosResized = new ByteArrayOutputStream();
                                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baosResized);
                                byte[] resizedImageBytes = baosResized.toByteArray();
                            }
                        }
                        Date date = new Date();
                        long d = date.getTime();
                        capBase64Filename = Utils.getImageTime(d);

                        if (!Utils.isValidArrayList(imgBase64s)) {
                            imgBase64s = new ArrayList<>();
                            imgFileNames = new ArrayList<>();
                        }

                        if (imagePosition == -1) {
                            imgBase64s.add(getBase64(bitmap));
                            imgFileNames.add(capBase64Filename);
                        } else {
                            imgBase64s.set(imagePosition, getBase64(bitmap));
                            imgFileNames.set(imagePosition, capBase64Filename);
                        }

                        isHideViewBtn = false;
                        imagesAdapter = new ImagesAdapter(imgBase64s);
                        imagesRv.setAdapter(imagesAdapter);
                        imagesAdapter.notifyDataSetChanged();

                        if (imagesAlert == null || !imagesAlert.isShowing())
                            showImagesDialog(imgBase64s);

                        addImage(bitmap, capBase64Filename, false, CROP_IMG);//////
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.logE("CAPTURE ERROR : " + e.toString());
                    }
                    break;

                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64))
                                captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        } else {
            switch (requestCode) {
                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64))
                                captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop, int cropRequestCode) {
        if (bitmap != null) {
            ImageLoader imageLoader = new ImageLoader(context, "POD");
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (!isCrop) {
                } else {
                    doCrop(bitmap, imageFileName, cropRequestCode);
                }
            }
        }
    }

    private void doCrop(Bitmap bitmap, String imageFileName, int cropRequestCode) {
        ImageProcessor.getInstance().setData(context, bitmap, imageFileName);
        Intent intent = new Intent(context, ADCropActivity.class);
        ((Activity) context).startActivityForResult(intent, cropRequestCode);
    }

    class ExceptionsAdapter extends RecyclerView.Adapter<ExceptionsAdapter.DataObjectHolder> {
        private ArrayList<ExceptionModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView exceptionTypeTv, noOfBoxesTv;
            Button imgBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                exceptionTypeTv = (TextView) itemView.findViewById(R.id.tv_exception_type);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                imgBtn = (Button) itemView.findViewById(R.id.btn_image);
                imgBtn.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    if (v.getId() == R.id.btn_image) {
                        ExceptionModel model = list.get(pos);
                        if (Utils.isValidArrayList((ArrayList<?>) model.getImgBase64s())) {
                            isHideViewBtn = true;
                            imagesAdapter = new ImagesAdapter((ArrayList<String>) model.getImgBase64s());
                            imagesRv.setAdapter(imagesAdapter);
                            imagesAdapter.notifyDataSetChanged();
                            showImagesDialog((ArrayList<String>) model.getImgBase64s());
                            captureNewBtn.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }

        public ExceptionsAdapter(ArrayList<ExceptionModel> myDataset) {
            list = myDataset;
        }

        @Override
        public ExceptionsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_inspection_exception_item, parent, false);
            return new ExceptionsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ExceptionsAdapter.DataObjectHolder holder, final int position) {
            ExceptionModel model = list.get(position);

            if (model != null) {
                SkipBinReasonModel exception = dataSource.exceptionTypes.getException(model.getExcepType(), warehouseCode);
                if (exception != null) {
                    holder.exceptionTypeTv.setText(exception.getReason());
                    if (exception.getReasonId() == 10) {
                        holder.imgBtn.setAlpha(0.6f);
                        holder.imgBtn.setEnabled(false);
                        holder.imgBtn.setClickable(false);
                        holder.imgBtn.setFocusableInTouchMode(false);
                    }
                }
                holder.noOfBoxesTv.setText(String.valueOf(model.getExceptionCount()));
            }

            holder.exceptionTypeTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.imgBtn.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<ExceptionModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(ExceptionModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }

    private String getBase64Img(String imageName, String text) {
        ImageLoader imageLoader = ScanUtils.getImageLoader(context);

        String base64Img = "";
        try {
            base64Img = imageLoader.fileCache.getBase64StringToName(imageName, "text", context);
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return base64Img;
    }

    private Bitmap getBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    private void showRmaInfoPopup(boolean isRepacked) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Enter Service Tag Info");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_rma_info, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        rmaInfoAlert = alertDialog.show();
        rmaInfoAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        EditText serviceTagNumEt, faultDescriptionEt, rmaPartSrNumEt, replacedPartSrNumEt, dateTimeEt;
        Button submitBtn, cancelBtn;
        serviceTagNumEt = (EditText) view.findViewById(R.id.et_service_tag_num);
        faultDescriptionEt = (EditText) view.findViewById(R.id.et_fault_description);
        rmaPartSrNumEt = (EditText) view.findViewById(R.id.et_rma_part_sr_num);
        replacedPartSrNumEt = (EditText) view.findViewById(R.id.et_replaced_part_sr_num);
        dateTimeEt = (EditText) view.findViewById(R.id.et_date_time);
        dateTimeEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateTimePicker(dateTimeEt);
            }
        });
        submitBtn = (Button) view.findViewById(R.id.btn_submit);
        cancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String serviceTagNum = serviceTagNumEt.getText().toString().trim();
                String faultDescription = faultDescriptionEt.getText().toString().trim();
                String rmaPartSrNum = rmaPartSrNumEt.getText().toString().trim();
                String replacedPartSrNum = replacedPartSrNumEt.getText().toString().trim();
                String dateTime = dateTimeEt.getText().toString().trim();
                if(Utils.isValidString(serviceTagNum)) {
                    rmaInfo = new RmaInfo();
                    rmaInfo.setServiceTagNum(serviceTagNum);
                    if(Utils.isValidString(faultDescription)) {
                        rmaInfo.setFaultDescription(faultDescription);
                        if(Utils.isValidString(rmaPartSrNum)) {
                            rmaInfo.setRmaPartSrNum(rmaPartSrNum);
                            if(Utils.isValidString(replacedPartSrNum)) {
                                rmaInfo.setReplacedPartSrNum(replacedPartSrNum);
                                if(Utils.isValidString(dateTime)) {
                                    rmaInfo.setDateTime(dateTime);
                                    rmaInfoAlert.dismiss();
                                    barcodesAlert.dismiss();
                                    part.setRmaInfo(rmaInfo);
                                    submitInspection(isRepacked);
                                } else {
                                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Select Date Time");
                                }
                            } else {
                                Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Serial No. of the Replaced Part");
                            }
                        } else {
                            Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter Serial No. of the RMA Part");
                        }
                    } else {
                        Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter the Fault Description");
                    }
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter the Service Tag Number");
                }
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rmaInfoAlert.dismiss();
            }
        });
    }

    Calendar etaDate;

    public void showDateTimePicker(EditText dateTimeEt) {
        final Calendar currentDate = Calendar.getInstance();
        etaDate = Calendar.getInstance();
        new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                etaDate.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        etaDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        etaDate.set(Calendar.MINUTE, minute);
                        etaDate.set(Calendar.SECOND, 0);
                        Utils.logD("The choosen one: " + etaDate.getTime());
                        String formattedDate = Utils.getFormattedDate(etaDate.getTime(), Constants.dateFormat2);
                        dateTimeEt.setText(formattedDate);
                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    private void showBarcodesPopup(InspectionPart part) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Scan Barcodes");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_inspection_barcodes_popup, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        barcodesAlert = alertDialog.show();
        barcodesAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        CheckBox repackedCb = (CheckBox) view.findViewById(R.id.cb_repacked);
        Button submitBtn = (Button) view.findViewById(R.id.btn_submit_barcodes);
        Button cancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        Button addBarcodeBtn = (Button) view.findViewById(R.id.btn_add_barcode);
        addBarcodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddBarcodeDialog();
            }
        });
        barcodesRv = (RecyclerView) view.findViewById(R.id.rv_barcodes);
        barcodesRv.setNestedScrollingEnabled(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setAutoMeasureEnabled(false);
        barcodesRv.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(barcodesRv.getContext(),
                layoutManager.getOrientation());
        barcodesRv.addItemDecoration(dividerItemDecoration);
        barcodesAdapter = new BarcodesAdapter((ArrayList<InspectionBarcodeData>) barcodesData);
        barcodesRv.setAdapter(barcodesAdapter);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(part.getReceiptType().equals("RM")) {
                    showRmaInfoPopup(repackedCb.isChecked());
                } else {
                    barcodesAlert.dismiss();
                    submitInspection(repackedCb.isChecked());
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodesAlert.dismiss();
                barcodesData.clear();
                barcodesAdapter.notifyDataSetChanged();
            }
        });
    }

    private void submitInspection(boolean isRepacked) {
        InspectionPart submitPart = getSubmitData();
        submitPart.setRepacked(isRepacked);
        submitPart.setBarcodesData(barcodesData);
        submitPart.setExceptionList(exceptionModels);

        if (Utils.getConnectivityStatus(context)) {
            new SubmitTask(submitPart).execute();
        }
    }

    private void showAddBarcodeDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        alertDialog.setTitle("Scan Barcode And Serial No.");
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_add_barcode, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        addBarcodeAlert = alertDialog.show();
        TextInputEditText barcodeEt, serialNoEt;
        barcodeEt = (TextInputEditText) view.findViewById(R.id.input_barcode);
        serialNoEt = (TextInputEditText) view.findViewById(R.id.input_serial_num);
        addBarcodeAlert.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        Button saveBtn = (Button) view.findViewById(R.id.btn_save_barcode);
        Button cancelBtn = (Button) view.findViewById(R.id.btn_cancel);
        InspectionBarcodeData model = new InspectionBarcodeData();

        barcodeEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                serialNoEt.setText(editable.toString());
                model.setBarcode(editable.toString());
                model.setSerialNo(editable.toString());
            }
        });
        serialNoEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                model.setSerialNo(editable.toString());
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidString(model.getBarcode())) {
                    if (Utils.isValidString(model.getSerialNo())) {
                        addBarcodeAlert.dismiss();
                        barcodesData.add(model);
                        barcodesAdapter = new BarcodesAdapter((ArrayList<InspectionBarcodeData>) barcodesData);
                        barcodesRv.setAdapter(barcodesAdapter);
                        barcodesAdapter.notifyDataSetChanged();
                    } else {
                        Utils.showSimpleAlert(context, getString(R.string.alert), "Please Scan the Serial Number");
                    }
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Scan the Barcode");
                }
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBarcodeAlert.dismiss();
            }
        });
    }

    class BarcodesAdapter extends RecyclerView.Adapter<BarcodesAdapter.DataObjectHolder> {
        private ArrayList<InspectionBarcodeData> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView barcodeTv, serialNoTv;
            ImageView deleteIv;

            public DataObjectHolder(View itemView) {
                super(itemView);

                barcodeTv = (TextView) itemView.findViewById(R.id.tv_barcode);
                serialNoTv = (TextView) itemView.findViewById(R.id.tv_serial_no);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_remove);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.iv_remove) {
                    int pos = (int) view.getTag();
                    barcodesData.remove(pos);
                    barcodesAdapter.notifyDataSetChanged();
                }
            }
        }

        public BarcodesAdapter(ArrayList<InspectionBarcodeData> myDataset) {
            list = myDataset;
        }

        @Override
        public BarcodesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_item_barcode, parent, false);
            return new BarcodesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BarcodesAdapter.DataObjectHolder holder, final int position) {
            InspectionBarcodeData model = list.get(position);

            if (model != null) {
                holder.barcodeTv.setText(model.getBarcode());
                holder.serialNoTv.setText(model.getSerialNo());
            }

            holder.barcodeTv.setTag(position);
            holder.serialNoTv.setTag(position);
            holder.deleteIv.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<InspectionBarcodeData> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(InspectionBarcodeData dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }
    }
}