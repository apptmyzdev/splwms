package com.apptmyz.splwms;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.custom.NonScrollList;
import com.apptmyz.splwms.custom.NonScrollableListView;
import com.apptmyz.splwms.data.GateInResponse;
import com.apptmyz.splwms.data.GateInResponseModel;
import com.apptmyz.splwms.data.GateInSaveReqModel;
import com.apptmyz.splwms.data.GateInSaveResponse;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.VehicleNumbersResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsException;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import org.kobjects.util.Util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GateInActivity extends BaseActivity {
    Context context;
    LinearLayout asnLl;
    EditText tarnsporterNoEt, transporterRepNameEt, noOfBoxEt, remarksEt;
    Button addAsnBtn, saveBtn, saveInputBtn, cancelBtn;
    NonScrollableListView asnLv;
    TextView asnTv;
    String transporterNo, transporterRepName, noOfBoxes, remarks, asnNo;
    GateInResponse gateInResponse;
    List<GateInResponseModel> gateInResponseModelList;
    List<GateInResponseModel> addedAsnList = new ArrayList<>();
    GateInSaveReqModel gateInSaveReqModel;
    GateInSaveResponse gateInSaveResponse;
    List<String> addInvoiceList = new ArrayList<>();
    private DataSource dataSource;
    private AsnPartAdapter adapter;
    private String warehouseCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_gate_in, frameLayout);

        context = GateInActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        titleTv.setText("Gate In");
        asnLl = (LinearLayout) findViewById(R.id.asn_ll);
        asnLl.setOnClickListener(listener);
        asnTv = (TextView) findViewById(R.id.asn_tv);
        addAsnBtn = (Button) findViewById(R.id.addAsn_btn);
        addAsnBtn.setOnClickListener(listener);
        saveBtn = (Button) findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(listener);
        asnLv = (NonScrollableListView) findViewById(R.id.asn_lv);

        new GateInTask().execute();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.addAsn_btn:
                    addAsn();
                    break;

                case R.id.asn_ll:
                    if (Utils.isValidArrayList((ArrayList<?>) gateInResponseModelList))
                        showAsnDialog();
                    else
                        Toast.makeText(context, "No Data Available", Toast.LENGTH_LONG).show();
                    break;

                case R.id.save_btn:
                    String[] itemsArray = new String[0];
                    asnNo = asnTv.getText().toString();
                    if (Utils.isValidString(asnNo)) {
                        showInputDialog();
                    } else
                        Toast.makeText(context, "Please Select PO/ASN No.", Toast.LENGTH_LONG).show();

                    break;

            }
        }
    };

    private void addAsn() {
        if (Utils.isValidArrayList((ArrayList<?>) gateInResponseModelList)) {
            String asnNumber = asnTv.getText().toString();
            for (GateInResponseModel model : gateInResponseModelList) {
                if (model.getPoNo().equalsIgnoreCase(asnNumber)) {
                    Log.d("asn", asnNumber);
                    Log.d("po", model.getPoNo());
                    if (!containsPo(addedAsnList, asnNumber)) {
                        addedAsnList.add(model);
                        adapter = new AsnPartAdapter(context, R.layout.asn_list_items, addedAsnList);
                        asnLv.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        break;
                    } else
                        Toast.makeText(context, "PO/ASN No. Already Exists, Please Choose Another ASN No.", Toast.LENGTH_LONG).show();
                }

            }
        } else
            Toast.makeText(context, "Please Select PO/ASN No. To Add", Toast.LENGTH_LONG).show();
    }

    public static boolean containsPo(Collection<GateInResponseModel> c, String poNo) {
        for (GateInResponseModel g : c) {
            if (g != null && g.getPoNo().equals(poNo)) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint("RestrictedApi")
    public void showAsnDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.gatein_type_radiolist);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        TextView ok = (TextView) dialog.findViewById(R.id.ok_tv);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel_tv);
        final RadioGroup asnRg = (RadioGroup) dialog.findViewById(R.id.order_rg);
        final ColorStateList colorStateList = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_enabled}},
                new int[]{getResources().getColor(R.color.green)}
        );
        if (Utils.isValidArrayList((ArrayList<?>) gateInResponseModelList)) {
            for (GateInResponseModel model : gateInResponseModelList) {
                AppCompatRadioButton asnRbtn = new AppCompatRadioButton(context);
                asnRbtn.setHighlightColor(context.getResources().getColor(R.color.green));
                asnRbtn.setId(View.generateViewId());
                Log.e("id", String.valueOf(View.generateViewId()));
                asnRbtn.setText(model.getPoNo());
                asnRbtn.setTextSize(14);
                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(context, null);
                params.setMargins(0, 15, 0, 0);
                asnRbtn.setLayoutParams(params);
                asnRbtn.setTextColor(getResources().getColor(R.color.green));
                asnRbtn.setSupportButtonTintList(colorStateList);
                asnRg.addView(asnRbtn);
            }
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer slaId = asnRg.getCheckedRadioButtonId();
                int childCount = asnRg.getChildCount();
                Log.e("child id", String.valueOf(childCount));
                for (int i = 0; i < childCount; i++) {
                    if (asnRg.getChildAt(i) instanceof RadioButton) {
                        RadioButton r_btn = (RadioButton) asnRg.getChildAt(i);
                        if (r_btn.getId() == slaId) {
                            Log.e("btn id", String.valueOf(r_btn.getId()));
                            Log.e("sla id", String.valueOf(slaId));
                            asnNo = r_btn.getText().toString();
                            asnTv.setText(asnNo);
                            addAsn();
                            break;
                        }
                    }
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }

    public class EditVehicleTask extends AsyncTask<Void, Void, Void> {
        int id;
        String vehicleNo;

        EditVehicleTask(int id, String vehicleNo) {
            this.id = id;
            this.vehicleNo = vehicleNo;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
            String url = WmsUtils.getEditVehicleUrl(id, vehicleNo);
            if (Utils.isValidString(url)) {
                InputStream is = null;
                try {
                    GeneralResponse response = (GeneralResponse) HttpRequest.getInputStreamFromUrl(url, GeneralResponse.class, context);
                    if (response != null && response.isStatus()) {
                        Log.e("response", String.valueOf(gateInResponseModelList));
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            if(showErrorDialog()) {
                new GateInTask().execute();
            }
        }
    }


    public class GateInTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
            String url = WmsUtils.getGateIn(warehouseCode);
            if (Utils.isValidString(url)) {
                InputStream is = null;
                try {
                    gateInResponse = (GateInResponse) HttpRequest.postData(url, "", GateInResponse.class, context);
                    if (gateInResponse != null && gateInResponse.isStatus()) {
                        gateInResponseModelList = gateInResponse.getData();
                        Log.e("response", String.valueOf(gateInResponseModelList));
                    } else {
                        Globals.lastErrMsg = gateInResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
        }
    }

    public class GateInSaveTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
            String url = WmsUtils.getGateInSave(warehouseCode);
            if (Utils.isValidString(url)) {
                try {
                    Gson gson = new Gson();
                    String data = gson.toJson(gateInSaveReqModel);
                    gateInSaveResponse = (GateInSaveResponse) HttpRequest.postData(url, data, GateInSaveResponse.class, context);
                    if (gateInSaveResponse != null && gateInSaveResponse.isStatus()) {
                        Log.e("getein", gateInSaveResponse.toString());
                    } else {
                        Globals.lastErrMsg = gateInSaveResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            if (showErrorDialog()) {
                Toast.makeText(context, gateInSaveResponse.getMessage(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, HomeActivity.class));
            }
        }
    }

    ProgressDialog vehicleNumsDialog;

    class VehicleNumberTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                vehicleNumsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getVehicleNumbersUrl(warehouseCode);

                Utils.logD("Log 1");
                VehicleNumbersResponse response = (VehicleNumbersResponse) HttpRequest
                        .getInputStreamFromUrl(url, VehicleNumbersResponse.class,
                                context);

                if (response != null) {
                    dataSource.vehicleNumbers.clearAll();
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<String> data = (ArrayList<String>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                            dataSource.vehicleNumbers.saveVehicleNumbers(data, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && vehicleNumsDialog != null && vehicleNumsDialog.isShowing()) {
                vehicleNumsDialog.dismiss();
            }
            if (showErrorDialog()) {
                startActivity(new Intent(context, HomeActivity.class));
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(GateInActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    public class AsnPartAdapter extends ArrayAdapter<GateInResponseModel> {
        Context context;
        int resource;
        List<GateInResponseModel> gateInResponseModelList;
        Integer qty;

        public AsnPartAdapter(@NonNull Context context, int resource, List<GateInResponseModel> gateInResponseModelList) {
            super(context, 0, gateInResponseModelList);
            this.context = context;
            this.resource = resource;
            this.gateInResponseModelList = gateInResponseModelList;
        }

        @Override
        public int getCount() {
            return gateInResponseModelList.size();
        }

        @SuppressLint("ResourceAsColor")
        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(resource, parent, false);
            TextView asnNoTv = (TextView) convertView.findViewById(R.id.order_tv);
            TextView vehicleNoTv = (TextView) convertView.findViewById(R.id.vehicleNo_tv);
            TextView transporterNameTv = (TextView) convertView.findViewById(R.id.transporterName_tv);
            TextView awbNoTv = (TextView) convertView.findViewById(R.id.awbNo_tv);
            TextView awbDateTv = (TextView) convertView.findViewById(R.id.awbDate_tv);
            ImageView deleteIv = (ImageView) convertView.findViewById(R.id.delete_iv);
            ImageView editIv = (ImageView) convertView.findViewById(R.id.edit_iv);
            GateInResponseModel model = gateInResponseModelList.get(position);
            if (Utils.isValidString(model.getPoNo()))
                asnNoTv.setText(model.getPoNo());
            if (Utils.isValidString(model.getVehicleNo()))
                vehicleNoTv.setText(model.getVehicleNo());
            if (Utils.isValidString(model.getTransporter()))
                transporterNameTv.setText("Transporter No:" + " " + model.getTransporter());
            if (Utils.isValidString(model.getAwbNo()))
                awbNoTv.setText("AWB:" + " " + model.getAwbNo());
            if (Utils.isValidString(model.getAwbDate()))
                awbDateTv.setText("AWB Date:" + " " + model.getAwbDate());
            editIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showEditDialog(model);
                }
            });
            deleteIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gateInResponseModelList.remove(position);
                    notifyDataSetChanged();

                }
            });
            return convertView;
        }
    }

    private void showEditDialog(GateInResponseModel model) {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(this);
        alert.setCancelable(false);
        final EditText edittext = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(16, 16, 16, 16);
        edittext.setLayoutParams(lp);
        edittext.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
        alert.setMessage("Enter the Vehicle Number");
        alert.setTitle("Edit Vehicle Number");
        alert.setView(edittext);

        alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = edittext.getText().toString();
                if(Utils.isValidVehicleNo(value)) {
                    model.setVehicleNo(value);
                    dialog.dismiss();
                    adapter = new AsnPartAdapter(context, R.layout.asn_list_items, addedAsnList);
                    asnLv.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    new EditVehicleTask(model.getId(), value).execute();
                } else {
                    Utils.showSimpleAlert(context, getString(R.string.alert), "Please Enter a Valid Vehicle Number");
                }
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    private AlertDialog alert;

    private void showInputDialog() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_gate_in_input, null, false);
        view.setVisibility(View.VISIBLE);
        tarnsporterNoEt = (EditText) view.findViewById(R.id.transporterNo_et);
        transporterRepNameEt = (EditText) view.findViewById(R.id.transporterRepName_et);
        noOfBoxEt = (EditText) view.findViewById(R.id.noOfBox_et);
        remarksEt = (EditText) view.findViewById(R.id.remarks_et);
        saveInputBtn = (Button) view.findViewById(R.id.save_input_btn);
        cancelBtn = (Button) view.findViewById(R.id.btn_cancel);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        saveInputBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] itemsArray = new String[0];
                asnNo = asnTv.getText().toString();
                transporterNo = tarnsporterNoEt.getText().toString();
                transporterRepName = transporterRepNameEt.getText().toString();
                noOfBoxes = noOfBoxEt.getText().toString();
                remarks = remarksEt.getText().toString();
                if (Utils.isValidString(transporterNo)) {
                    if (Utils.isValidString(transporterRepName)) {
                        if (Utils.isValidString(noOfBoxes)) {
                            if (Utils.isValidArrayList((ArrayList<?>) addedAsnList)) {
                                for (GateInResponseModel model : addedAsnList) {
                                    addInvoiceList.add(model.getPoNo());
                                }
                                Log.d("list", String.valueOf(addInvoiceList));
                                gateInSaveReqModel = new GateInSaveReqModel(Constants.GATE_IN, transporterNo, transporterRepName, noOfBoxes, remarks, addInvoiceList);
                                Log.d("model", gateInSaveReqModel.toString());
                                new GateInSaveTask().execute();
                            } else
                                Toast.makeText(context, "Please Add ASN No.", Toast.LENGTH_LONG).show();

                        } else
                            Toast.makeText(context, "Please Enter No. Of Boxes", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(context, "Please Enter Transporter Representative Name", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(context, "Please Enter Transporter Phone Number", Toast.LENGTH_LONG).show();
            }
        });
        alertDialog.setView(view);
        alert = alertDialog.show();
    }
}
