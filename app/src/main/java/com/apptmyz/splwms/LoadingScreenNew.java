package com.apptmyz.splwms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.data.DrsAlRoutesResponse;
import com.apptmyz.splwms.data.RouteMasterModel;
import com.apptmyz.splwms.data.VehicleMasterModel;
import com.apptmyz.splwms.data.VehicleMasterResponse;
import com.apptmyz.splwms.data.VendorMasterModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;

import java.util.ArrayList;
import java.util.List;

public class LoadingScreenNew extends Activity {

    private Context context;
    private DataSource dataSource;
    private TextView titleTv, vendorTv, vehicleTv, routesTv;
    private RelativeLayout drs_routes_popup_rl, drs_vendor_popup_rl, drs_vehicles_popup_rl;
    private ListView drsRouteLv, drsVendorLv, drsVehiclesLv;
    private List<VendorMasterModel> vendorList, vendorSortList;
    private List<RouteMasterModel> routeList, routeSortList;
    private ArrayList<VehicleMasterModel> vehicleList, vehicleSortList;
    private DrsVehicleAdapter drsVehicleAdapter;
    private VendorAdapter vendorAdapter;
    private DrsRouteAdapter drsRouteAdapter;
    ProgressDialog tokenDialog;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_drssubmit:
                    if(Utils.isValidString(vehicleNo))
                    goNext(vehicleNo,vendorCode);
                    else
                        Utils.showToast(context,"Please select vehicle");
                    break;
                case R.id.vendor_ll:
                    showVendorsDialog();
                    break;

                case R.id.drs_routes_ll:
                    if (vendorCode != -1) {

                        showRoutesDialog();
                    } else {
                        Utils.showToast(context, "Please select a vendor");
                    }
                    break;
                case R.id.vehicle_ll:
                    if (routeId != -1) {
                        showVehiclesDialog();
                    } else {
                        Utils.showToast(context, "Please select a route");
                    }
                    break;

                case R.id.home_iv:
                    goBack();
                    break;
            }
        }
    };
    private String vehicleNo;
    private int vendorCode = -1;
    private int vehicleId = 1, routeId = -1;
    private ImageView home_iv;
    private LinearLayout vendorLl, vehicleLl, drsrouteLl;
    private AlertDialog errDlg;
    private EditText routesSearchEt, vendorSearchEt, vehicleSearchEt;
    private Integer textLength;
    private String searchString;
    private Button drsSumbitBtn;
    private ListView.OnItemClickListener onItemClickListener = new ListView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> view, View arg1, int position,
                                long arg3) {
            switch (view.getId()) {

                case R.id.drs_routes_lv:
                    RouteMasterModel routeMasterModel = (RouteMasterModel) view.getAdapter().getItem(position);
                    String routeName = routeMasterModel.getRouteName();
                    if (Utils.isValidString(routeName))
                        routesTv.setText(routeName);
                    Utils.dissmissKeyboard(routesSearchEt);
                    routeId = routeMasterModel.getRouteId();
                    drs_routes_popup_rl.setVisibility(View.GONE);
                    new GetVehicles().execute();

                    break;


                case R.id.vendor_lv:
                    VendorMasterModel vendorMasterModel = (VendorMasterModel
                            ) view.getAdapter().getItem(position);
                    String name = vendorMasterModel.getVendorName();
                    int code = vendorMasterModel.getId();
                    if (Utils.isValidString(name)) {
                        vendorTv.setText(name);
                    }
                    vendorCode = code;
                    Utils.dissmissKeyboard(vendorSearchEt);
                    drs_vendor_popup_rl.setVisibility(View.GONE);

                    new GetRoutes().execute();

                    break;

                case R.id.drs_vehicle_lv:
                    VehicleMasterModel vehicleMasterModel = (VehicleMasterModel) view.getAdapter().getItem(position);
                    vehicleNo = vehicleMasterModel.getVehicleNo();
                    vehicleId = vehicleMasterModel.getVehicleId();
                    if (Utils.isValidString(vehicleNo)) {
                        vehicleTv.setText(vehicleNo);
                        Globals.vehicleNo = vehicleNo;
                    }
                    Utils.dissmissKeyboard(vehicleSearchEt);

                    drs_vehicles_popup_rl.setVisibility(View.GONE);

                    break;




            }
        }

    };
    private boolean isQc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_screen_new);

        context = LoadingScreenNew.this;

        dataSource = new DataSource(context);

        titleTv = (TextView) findViewById(R.id.title_tv);
        titleTv.setText("Loading Screen");


        drs_routes_popup_rl = (RelativeLayout) findViewById(R.id.drs_popup_ll);
        drs_vendor_popup_rl = (RelativeLayout) findViewById(R.id.drs_vendor_popup_ll);
        drs_vehicles_popup_rl = (RelativeLayout) findViewById(R.id.drs_vehicle_popup_ll);

        vendorTv = (TextView) findViewById(R.id.vendor_tv);
        routesTv = (TextView) findViewById(R.id.routes_tv);
        vehicleTv = (TextView) findViewById(R.id.vehicle_tv);


        drsSumbitBtn = (Button) findViewById(R.id.btn_drssubmit);
        drsSumbitBtn.setOnClickListener(listener);

        vendorLl = (LinearLayout) findViewById(R.id.vendor_ll);
        vendorLl.setOnClickListener(listener);

        drsrouteLl = (LinearLayout) findViewById(R.id.drs_routes_ll);
        drsrouteLl.setOnClickListener(listener);

        vehicleLl = (LinearLayout) findViewById(R.id.vehicle_ll);
        vehicleLl.setOnClickListener(listener);

        Intent intent = getIntent();
        isQc = intent.getBooleanExtra("QC", false);


        routesSearchEt = (EditText) findViewById(R.id.routes_search_et);
        routesSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textLength = routesSearchEt.getText().length();
                searchString = routesSearchEt.getText().toString();

                routeSortList.clear();
                if (routeList != null) {
                    for (int i = 0; i < routeList.size(); i++) {
                        if (routeList.get(i).getRouteName().toLowerCase().trim().contains(
                                searchString.toLowerCase().trim())) {
                            routeSortList.add(routeList.get(i));
                        }
                    }
                }
                drsRouteAdapter = new DrsRouteAdapter(context, R.layout.routes_row, routeSortList);
                drsRouteLv.setAdapter(drsRouteAdapter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        vendorSearchEt = (EditText) findViewById(R.id.vendor_search_et);
        vendorSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textLength = vendorSearchEt.getText().length();
                searchString = vendorSearchEt.getText().toString();

                vendorSortList.clear();
                if (vendorList != null) {
                    for (int i = 0; i < vendorList.size(); i++) {
                        if ((vendorList.get(i).getVendorName().toLowerCase().trim().contains(
                                searchString.toLowerCase().trim()))) {
                            vendorSortList.add(vendorList.get(i));
                            Utils.logD(s.toString());
                        }
                    }

                }
                vendorAdapter = new VendorAdapter(context, R.layout.routes_row, vendorSortList);
                drsVendorLv.setAdapter(vendorAdapter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        vehicleSearchEt = (EditText) findViewById(R.id.vehicle_search_et);
        vehicleSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textLength = vehicleSearchEt.getText().length();
                searchString = vehicleSearchEt.getText().toString();

                vehicleSortList.clear();
                if (vehicleList != null) {
                    for (int i = 0; i < vehicleList.size(); i++) {
                        if ((vehicleList.get(i).getVehicleNo().toLowerCase().trim().contains(
                                searchString.toLowerCase().trim()))) {
                            vehicleSortList.add(vehicleList.get(i));


                        }
                    }

                }
                drsVehicleAdapter = new DrsVehicleAdapter(context, R.layout.routes_row, vehicleSortList);
                drsVehiclesLv.setAdapter(drsVehicleAdapter);

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        drsRouteLv = (ListView) findViewById(R.id.drs_routes_lv);
        drsRouteLv.setOnItemClickListener(onItemClickListener);

        drsVendorLv = (ListView) findViewById(R.id.vendor_lv);
        drsVendorLv.setOnItemClickListener(onItemClickListener);

        drsVehiclesLv = (ListView) findViewById(R.id.drs_vehicle_lv);
        drsVehiclesLv.setOnItemClickListener(onItemClickListener);

        home_iv = (ImageView) findViewById(R.id.home_iv);
        home_iv.setOnClickListener(listener);


        vehicleList = new ArrayList<>();
        vendorList = new ArrayList<>();
        vehicleSortList = new ArrayList<>();
        vendorSortList = new ArrayList<>();
        routeSortList = new ArrayList<>();
        routeList = new ArrayList<>();

        if (isQc) {
            drsrouteLl.setVisibility(View.GONE);
            vehicleLl.setVisibility(View.GONE);
        }


    }

    private void showRoutesDialog() {
        drs_routes_popup_rl.setVisibility(View.VISIBLE);
        if (Utils.isValidArrayList((ArrayList<?>) routeList)) {
            drsRouteAdapter = new DrsRouteAdapter(context, R.layout.routes_row, routeList);
            drsRouteLv.setAdapter(drsRouteAdapter);
        }
    }

    private void showVendorsDialog() {
        drs_vendor_popup_rl.setVisibility(View.VISIBLE);
        vendorList = dataSource.vendorMaster.getAll();
        vendorAdapter = new VendorAdapter(context, R.layout.routes_row, vendorList);
        drsVendorLv.setAdapter(vendorAdapter);

    }

    private void showVehiclesDialog() {
        if (Utils.isValidArrayList(vehicleList)) {
            drsVehicleAdapter = new DrsVehicleAdapter(context, R.layout.routes_row, vehicleList);
            drsVehiclesLv.setAdapter(drsVehicleAdapter);
        }
        drs_vehicles_popup_rl.setVisibility(View.VISIBLE);


    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (drs_routes_popup_rl.getVisibility() == View.VISIBLE)
                drs_routes_popup_rl.setVisibility(View.GONE);
            else if (drs_vendor_popup_rl.getVisibility() == View.VISIBLE)
                drs_vendor_popup_rl.setVisibility(View.GONE);
            else if (drs_vehicles_popup_rl.getVisibility() == View.VISIBLE)
                drs_vehicles_popup_rl.setVisibility(View.GONE);

            else
                finish();
        }
        return true;
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public class Holder {
        public TextView nameTv;
    }

    public class DrsRouteAdapter extends ArrayAdapter<RouteMasterModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public DrsRouteAdapter(Context context, int layoutId, List<RouteMasterModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Holder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new Holder();
            holder.nameTv = (TextView) convertView
                    .findViewById(R.id.tv_route_code);


            RouteMasterModel model = getItem(position);

            String code = model.getRouteName();
            if (Utils.isValidString(code)) {
                holder.nameTv.setText(code);
                holder.nameTv.setTextColor(context.getResources().getColor(R.color.orange));
            } else
                holder.nameTv.setText("");
            return convertView;
        }
    }

    public class VendorHolder {
        public TextView nameTv;
    }

    public class VendorAdapter extends ArrayAdapter<VendorMasterModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public VendorAdapter(Context context, int layoutId, List<VendorMasterModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            VendorHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new VendorHolder();
            holder.nameTv = (TextView) convertView
                    .findViewById(R.id.tv_route_code);


            VendorMasterModel model = getItem(position);

            String code = model.getVendorName();
            if (Utils.isValidString(code)) {
                holder.nameTv.setText(code);
                holder.nameTv.setTextColor(context.getResources().getColor(R.color.orange));
            } else
                holder.nameTv.setText("");

            return convertView;
        }
    }

    public class DrsVehicleAdapter extends ArrayAdapter<VehicleMasterModel> {
        private LayoutInflater inflater;
        private int layoutId;

        public DrsVehicleAdapter(Context context, int layoutId, List<VehicleMasterModel> list) {
            super(context, 0, list);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            VendorHolder holder;

            convertView = inflater.inflate(layoutId, parent, false);
            holder = new VendorHolder();
            holder.nameTv = (TextView) convertView
                    .findViewById(R.id.tv_route_code);


            VehicleMasterModel model = getItem(position);

            String code = model.getVehicleNo();
            if (Utils.isValidString(code)) {
                holder.nameTv.setText(code);
                holder.nameTv.setTextColor(context.getResources().getColor(R.color.orange));
            } else
                holder.nameTv.setText("");

            return convertView;
        }
    }

    class GetVehicles extends AsyncTask<String, Object, VehicleMasterResponse> {
        ArrayList<VehicleMasterModel> list = null;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                tokenDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected VehicleMasterResponse doInBackground(String... params) {
            VehicleMasterResponse response = null;
            try {
                String url;

                url = WmsUtils.getVehicleMasterUrlTag(String.valueOf(routeId));
                response = (VehicleMasterResponse) HttpRequest
                        .getInputStreamFromUrl(url, VehicleMasterResponse.class,
                                context);

                if (response != null) {
                    Utils.logD(response.toString());
                    if (response.getStatus()) {
                        list = response.getData();
                    }
                } else {
                    Globals.lastErrMsg = "Response is null";
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }

            return response;
        }

        @Override
        protected void onPostExecute(VehicleMasterResponse response) {

            if (!isFinishing() && tokenDialog != null && tokenDialog.isShowing()) {
                tokenDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (Utils.isValidArrayList(list)) {
                    vehicleList = list;
                }

            }
            super.onPostExecute(response);
        }

        @SuppressWarnings("deprecation")
        private boolean showErrorDialog() {
            boolean isNotErr = true;
            try {
                if (!isFinishing()) {
                    if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

                        errDlg = new AlertDialog.Builder(context).create();
                        errDlg.setTitle(getString(R.string.alert_dialog_title));
                        errDlg.setCancelable(false);
                        errDlg.setMessage(Globals.lastErrMsg);
                        Globals.lastErrMsg = "";
                        errDlg.setButton(getString(R.string.ok_btn),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        errDlg.dismiss();
                                    }
                                });
                        Utils.dismissProgressDialog();
                        isNotErr = false;
                        errDlg.show();
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            }
            return isNotErr;
        }

    }

    class GetRoutes extends AsyncTask<String, Object, DrsAlRoutesResponse> {
        ArrayList<RouteMasterModel> list = null;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                tokenDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected DrsAlRoutesResponse doInBackground(String... params) {
            DrsAlRoutesResponse response = null;
            try {
                String url;

                url = WmsUtils.getRouteMasterUrlTag(String.valueOf(vendorCode));
                response = (DrsAlRoutesResponse) HttpRequest
                        .getInputStreamFromUrl(url, DrsAlRoutesResponse.class,
                                context);

                if (response != null) {
                    Utils.logD(response.toString());
                    if (response.getStatus()) {
                        list = response.getData();
                    }
                } else {
                    Globals.lastErrMsg = "Response is null";
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }

            return response;
        }

        @Override
        protected void onPostExecute(DrsAlRoutesResponse response) {

            if (!isFinishing() && tokenDialog != null && tokenDialog.isShowing()) {
                tokenDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (Utils.isValidArrayList(list)) {
                    routeList = list;

                }
            }
            super.onPostExecute(response);
        }

        @SuppressWarnings("deprecation")
        private boolean showErrorDialog() {
            boolean isNotErr = true;
            try {
                if (!isFinishing()) {
                    if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

                        errDlg = new AlertDialog.Builder(context).create();
                        errDlg.setTitle(getString(R.string.alert_dialog_title));
                        errDlg.setCancelable(false);
                        errDlg.setMessage(Globals.lastErrMsg);
                        Globals.lastErrMsg = "";
                        errDlg.setButton(getString(R.string.ok_btn),
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {


                                        errDlg.dismiss();
                                    }
                                });


                        Utils.dismissProgressDialog();
                        isNotErr = false;
                        errDlg.show();
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            }
            return isNotErr;
        }

    }

    private void goNext(String vehNo,int vendorCode) {
        Globals.vehicleNo = vehNo;
        Intent intent = new Intent(context, LoadingScanningScreen.class);
        intent.putExtra(Constants.CLASS_NAME, getClass().getName());
        intent.putExtra(Constants.VEHICLE_NUMBER, vehNo);
        intent.putExtra(Constants.VENDOR_ID, vendorCode);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}