package com.apptmyz.splwms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.BitmapScalingUtil;
import com.apptmyz.splwms.custom.CameraActivity;
import com.apptmyz.splwms.data.Cons;
import com.apptmyz.splwms.data.ExceptionModel;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.LoginResponse;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.data.PartsListModel;
import com.apptmyz.splwms.data.PartsSubmitModel;
import com.apptmyz.splwms.data.TruckImageModel;
import com.apptmyz.splwms.data.UnloadingGoodPartData;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.ScanUtils;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class PartsReconcileScreen extends Activity {
    public final int TAKE_PHOTO = 1;
    public Gson gson = new Gson();
    ProgressDialog submitDialog;
    private Context context;
    private DataSource dataSource;
    private TextView vehicleNoTv, totalPartsTv, totalBoxesTv, partsScannedTv,
            partsNotScannedTv, exceptionBoxesTv, extraBoxesTv, productBoxesTv, damagedBoxesTv;
    private Button submitBtn, rescanBtn;
    private List<NewProductModel> partsList;
    private AlertDialog errDlg;
    private Boolean isScanCompleted = false;
    private List<PartsListModel> partSumbitList;
    private RelativeLayout imageLayout;
    private Button captureBtn, sendBtn;
    private TextView msgTv;
    private ImageView truckIv;
    private String truckImageFileName, warehouseCode;
    private Boolean isImageSubmitted = false;
    private CheckBox finalSubmitCb;
    private ImageView homeIv;

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_submit:
                    isScanCompleted = true;
                    for (NewProductModel model : partsList) {
                        String number = Utils.getNumber(model);
                        String dataType = model.getDataType();
                        int count = dataSource.exceptions.getExceptionCount(model.getIsProduct(), model.getProductId(), model.getPartId(), dataType, model.getVehicleNo(), warehouseCode);
                        int extrasCount = dataSource.exceptions.getExtrasCount(model.getIsProduct(), model.getProductId(), model.getPartId(), dataType, model.getVehicleNo(), warehouseCode);
                        if (model.getTotalBoxes() > model.getScannedCount() + count + extrasCount) {
                            isScanCompleted = false;
                            break;
                        }
                    }

                    if (finalSubmitCb.isChecked()) {
                        doFinalSubmit();
                    } else {
                        if (isScanCompleted) {
                            showFinalSubmitAlert();
                        } else {
                            new Submit().execute(Constants.FALSE);
                        }
                    }
                    break;

                case R.id.btn_cancel_submit:
                    goBack();
                    break;

                case R.id.btn_capture_image:
                    truckImageFileName = "";
                    takePicture(context);
                    break;
                case R.id.btn_send_image:
                    if (Utils.isValidString(truckImageFileName)) {
                        Utils.logE("in LT image");

                        String text = "";

                        String truck = Globals.vehicleNo;
                        if (Utils.isValidString(truck)) {
                            text = text + "\n" + "Veh#  : " + truck;
                        }

                        String s = dataSource.sharedPreferences
                                .getValue(Constants.IMEI);
                        if (Utils.isValidString(s))
                            text = text + "\n" + "T-ID# : " + s;

                        String user = dataSource.sharedPreferences
                                .getValue(Constants.USERNAME_PREF);
                        if (Utils.isValidString(user))
                            text = text + "\n" + "U-ID# : " + user;

                        text = text + "\n" + "I-DT# : "
                                + Utils.getITime(Long.valueOf(truckImageFileName));

                        imageLayout.setVisibility(View.GONE);
                        TruckImageModel model = new TruckImageModel();

                        model.setVehTxnDate(Utils.getScanTimeLoading());
                        model.setLoadUnloadFlag(Constants.UNLOADING_COMPLETED);

                        model.setVehicleNo(Globals.vehicleNo);


                        ImageLoader loader = new ImageLoader(context, Constants.TRUCK);
                        try {
                            String base64 = loader.fileCache.getBase64StringToName(
                                    truckImageFileName, text, context);
                            List<String> list = new ArrayList<String>();
                            if (Utils.isValidString(base64)) {
                                list.add(base64);
                                model.setImages(list);
                                Utils.logD(model.toString());
                                new SendStackingImage().execute(model);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.logE("in else ");
                        Utils.showSimpleAlert(
                                context,
                                getString(R.string.alert_dialog_title),
                                "Please capture stacking image");

                    }

                    break;
            }

        }
    };

    private void showFinalSubmitAlert() {
        String message = "You have Scanned All the Boxes. Do you want to do the Final Submit?";
        forceFinalSubmitDialog = new ForceFinalSubmitDialog(message);
        forceFinalSubmitDialog.setCancelable(false);
        forceFinalSubmitDialog.show();
    }

    ForceFinalSubmitDialog forceFinalSubmitDialog = null;

    public class ForceFinalSubmitDialog extends Dialog {
        private TextView messageTv;
        private Button yesBtn, noBtn;
        private String msg;

        public ForceFinalSubmitDialog(String message) {
            super(PartsReconcileScreen.this, R.style.CustomAlertDialog);
            msg = message;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.custom_alert_dialog_2);

            messageTv = (TextView) findViewById(R.id.tv_error_message);
            messageTv.setText(msg);

            yesBtn = (Button) findViewById(R.id.btn_ok);
            noBtn = (Button) findViewById(R.id.btn_cancel);
            yesBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    forceFinalSubmitDialog.dismiss();
                    doFinalSubmit();
                }
            });
            noBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Submit().execute(Constants.FALSE);
                }
            });
        }
    }

    private void doFinalSubmit() {
        if (isScanCompleted) {
            if (isImageSubmitted) {
                new Submit().execute(Constants.TRUE);
            } else {
                imageLayout.setVisibility(View.VISIBLE);
            }
        } else {
            Utils.showSimpleAlert(context, "Alert", "Please record reasons for unscanned packets");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reconcile_screen);

        context = PartsReconcileScreen.this;

        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        vehicleNoTv = (TextView) findViewById(R.id.tv_summary_tally_number);
        totalPartsTv = (TextView) findViewById(R.id.tv_total_dockets);
        totalBoxesTv = (TextView) findViewById(R.id.tv_total_packets);
        partsScannedTv = (TextView) findViewById(R.id.tv_packets_not_exception);
        partsNotScannedTv = (TextView) findViewById(R.id.tv_not_scanned_packets);
        exceptionBoxesTv = (TextView) findViewById(R.id.tv_exception_boxes);
        extraBoxesTv = (TextView) findViewById(R.id.tv_extra_boxes);
        productBoxesTv = (TextView) findViewById(R.id.tv_product_boxes);
        damagedBoxesTv = (TextView) findViewById(R.id.tv_damaged_boxes);

        homeIv = (ImageView) findViewById(R.id.home_iv);
        homeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToHome();
            }
        });

        submitBtn = (Button) findViewById(R.id.btn_submit);
        submitBtn.setOnClickListener(onClickListener);

        rescanBtn = (Button) findViewById(R.id.btn_cancel_submit);
        rescanBtn.setOnClickListener(onClickListener);

        imageLayout = (RelativeLayout) findViewById(R.id.layout_image_popup);

        captureBtn = (Button) findViewById(R.id.btn_capture_image);
        captureBtn.setOnClickListener(onClickListener);

        msgTv = (TextView) findViewById(R.id.tv_msg);
        msgTv.setText("After Unloading the last package and before Final Submission of Sheet, Take Picture with Doors in open position and then Submit the Sheet");

        sendBtn = (Button) findViewById(R.id.btn_send_image);
        sendBtn.setOnClickListener(onClickListener);

        finalSubmitCb = (CheckBox) findViewById(R.id.cb_day_end);

        finalSubmitCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {

                }
            }
        });

        truckIv = (ImageView) findViewById(R.id.iv_truck);


        setData();
    }

    private void setData() {
        partSumbitList = new ArrayList<>();

        int totScannedCount = 0;
        int totUnscannedCount = 0;
        int totCount = 0;
        int totBox = 0;
        int totParts = 0;
        int exceptionBoxes = 0;
        int shortageBoxes = 0;
        int extrasCount = 0, productExceptionCount = 0, damagedCount = 0;

        if (Utils.isValidString(Globals.vehicleNo)) {
            vehicleNoTv.setText(Globals.vehicleNo);
        }

        partsList = dataSource.unloadingParts.getScannedParts(warehouseCode);
        if (Utils.isValidArrayList((ArrayList<?>) partsList)) {
            for (NewProductModel productModel : partsList) {
                if (productModel.getIsScanCompleted().equalsIgnoreCase(Constants.FALSE)) {
                    isScanCompleted = false;
                }
                if (productModel != null) {
                    totScannedCount = totScannedCount + productModel.getScannedCount();
                    totCount = totCount + productModel.getTotalBoxes();
                    totParts = totParts + productModel.getTotalQty();
                    totBox = totBox + productModel.getTotalBoxes();
                    String number = Utils.getNumber(productModel);
                    String dataType = productModel.getDataType();
                    shortageBoxes += dataSource.exceptions.getShortageCount(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(),  dataType, productModel.getVehicleNo(), warehouseCode);
                    int count = dataSource.exceptions.getExceptionCount(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(),  dataType, productModel.getVehicleNo(), warehouseCode);
                    int extras = dataSource.exceptions.getExtrasCount(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(),  dataType, productModel.getVehicleNo(), warehouseCode);
                    productExceptionCount += dataSource.exceptions.getProductExceptionCount(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(), dataType, productModel.getVehicleNo(), warehouseCode);
                    damagedCount += dataSource.exceptions.getDamagedExceptionCount(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(), dataType, productModel.getVehicleNo(), warehouseCode);

                    exceptionBoxes = exceptionBoxes + count;
                    extrasCount = extrasCount + extras;
                    List<UnloadingGoodPartData> goodPartDataList = dataSource.unloadingGoodParts.getGoodParts(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(), productModel.getVehicleNo(), warehouseCode);
                    if (Utils.isValidArrayList((ArrayList<?>) goodPartDataList)) {
                        for (UnloadingGoodPartData goodPartData : goodPartDataList) {
                            ArrayList<ExceptionModel> models = dataSource.exceptions.getExceptions(productModel.getIsProduct(), productModel.getProductId(), productModel.getPartId(),  goodPartData.getInventoryTypeId(), productModel.getVehicleNo(), warehouseCode);
                            PartsListModel partsListModel = new PartsListModel();
                            partsListModel.setPartId(productModel.getPartId());
                            partsListModel.setDataType(dataType);
                            partsListModel.setMasterBoxNo(productModel.getMasterBoxNo());
                            if (dataType.equals("P")) {
                                partsListModel.setPartNo(number);
                            } else if (dataType.equals("DA")) {
                                partsListModel.setDaNo(number);
                            } else if (dataType.equals("IN")) {
                                partsListModel.setInvoiceNo(number);
                            } else if (dataType.equals("MB")) {
                                partsListModel.setMasterBoxNo(number);
                            }
                            partsListModel.setInventoryTypeId(goodPartData.getInventoryTypeId());
                            partsListModel.setInventoryTypeDesc(goodPartData.getInventoryTypeDesc());
                            partsListModel.setScanTime(goodPartData.getScannedTime());
                            partsListModel.setTotalBoxes(goodPartData.getScannedCount());
                            partsListModel.setIsProduct(goodPartData.getIsProduct());
                            partsListModel.setProductId(goodPartData.getProductId());
                            partsListModel.setProductName(goodPartData.getProductName());

                            if (productModel.getTotalBoxes() <= extras + count + goodPartData.getScannedCount()) {
                                partsListModel.setScanComplete(1);
                            } else
                                partsListModel.setScanComplete(0);

                            String text = "";
                            text = "Vehicle#   : " + productModel.getVehicleNo();
                            text = text + productModel.getDataType() + " Number#   : " + Utils.getNumber(productModel);

                            int exceptionCount = 0;
                            for (ExceptionModel model : models) {
                                exceptionCount += model.getExceptionCount();
                                if (Utils.isValidArrayList((ArrayList<?>) model.getImgBase64s())) {
                                    List<String> base64s = new ArrayList<>();
                                    for (String fileName : model.getImgBase64s()) {
                                        base64s.add(getBase64Img(fileName, text));
                                    }
                                    model.setImgBase64s(base64s);
                                }
                            }

                            partsListModel.setExceptionCount(exceptionCount);
                            partsListModel.setReceiptType(productModel.getReceiptType());
                            partsListModel.setExceptionsList(models);
                            partSumbitList.add(partsListModel);
                        }
                    }
                }
            }

            totUnscannedCount = totCount - totScannedCount;
            totalPartsTv.setText(String.valueOf(partsList.size()));
            totalBoxesTv.setText(Integer.toString(totBox));
            partsScannedTv.setText(Integer.toString(totScannedCount));
            partsNotScannedTv.setText(Integer.toString(shortageBoxes)); //totUnscannedCount - exceptionBoxes - extrasCount
        }
        extraBoxesTv.setText(Integer.toString(extrasCount));
        exceptionBoxesTv.setText(Integer.toString(exceptionBoxes));
        productBoxesTv.setText(Integer.toString(productExceptionCount));
        damagedBoxesTv.setText(Integer.toString(damagedCount));
    }

    private void goBack() {
        Intent intent = new Intent(context, PartsScanningScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            goBack();
            return true;
        }
        return true;
    }

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (!isFinishing()) {
            if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
                errDlg = new AlertDialog.Builder(PartsReconcileScreen.this).create();
                errDlg.setTitle(getString(R.string.alert_dialog_title));
                errDlg.setCancelable(false);
                errDlg.setMessage(Globals.lastErrMsg);

                Globals.lastErrMsg = "";
                errDlg.setButton(getString(R.string.ok_btn),
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                errDlg.dismiss();
                            }
                        });

                Utils.dismissProgressDialog();
                isNotErr = false;
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void goToHome() {
        Intent intent = new Intent(context, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void takePicture(Context context) {
        Utils.logD("*");
        Intent intent = new Intent(context, CameraActivity.class);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        switch (requestCode) {
            case TAKE_PHOTO:
                try {
                    Utils.logD("**");

                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (Utils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addImage(bitmap, Utils.getImageTime(d), false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        Utils.logD("***");

        if (truckIv != null && bitmap != null) {
            Utils.logD("*****");
            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (truckIv.getVisibility() == View.GONE || truckIv.getVisibility() == View.INVISIBLE)
                    truckIv.setVisibility(View.VISIBLE);
                Utils.enableView(sendBtn);
                truckImageFileName = imageFileName;
                truckIv.setImageBitmap(bitmap);
            }
        }
    }

    private String getBase64Img(String imageName, String text) {
        ImageLoader imageLoader = ScanUtils.getImageLoader(context);

        String base64Img = "";
        try {
            base64Img = imageLoader.fileCache.getBase64StringToName(imageName, text, context);
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return base64Img;
    }

    class Submit extends AsyncTask<String, Object, Object> {
        boolean isFinalSubmit;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            submitDialog = Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(String... params) {
            try {

                String finalSubmitStr = params[0];

                PartsSubmitModel partsSubmitModel = new PartsSubmitModel();
                isFinalSubmit = Boolean.parseBoolean(finalSubmitStr);
                partsSubmitModel.setFinalSubmission(isFinalSubmit);
                partsSubmitModel.setVehicleNo(Globals.vehicleNo);
                partsSubmitModel.setDataType(dataSource.sharedPreferences.getValue(Constants.DATA_TYPE));

                if (Utils.isValidArrayList((ArrayList<?>) partSumbitList))
                    partsSubmitModel.setPartList(partSumbitList);


                Gson gson = new Gson();


                String data = gson.toJson(partsSubmitModel);
                Utils.logD("Data:" + data);

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getUnloadingSubmitUrlTag(warehouseCode);

                if (Utils.isValidString(data)) {
                    LoginResponse response = (LoginResponse) HttpRequest
                            .postData(url, data, LoginResponse.class,
                                    context);
                    if (response != null) {
                        Utils.logD(response.toString());
                        if (!response.isStatus()) {
                            Globals.lastErrMsg = response.getMessage();
                        } else {
                            dataSource.unloadingGoodParts.deleteGoodPartsData(Globals.vehicleNo, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = "Server Response is null";
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                Utils.logE(e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            Globals.isDataSubmitted = false;
            if (!isFinishing() && submitDialog != null && !submitDialog.isShowing())
                submitDialog.dismiss();
            if (showErrorDialog()) {
                String vehNo = Globals.vehicleNo;
                if (Utils.isValidString(vehNo)) {
                    dataSource.unloadingParts.deleteUnloadingdata(vehNo, warehouseCode);
                    dataSource.exceptions.deleteExceptionsdata(vehNo, warehouseCode);
                }
                Utils.showToast(context, "Successful");
                if (isFinalSubmit)
                    goToHome();
                else
                    goBack();
            }
            super.onPostExecute(result);
        }
    }

    class SendStackingImage extends AsyncTask<TruckImageModel, Object, Object> {
        GeneralResponse response;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(TruckImageModel... params) {
            try {
                Globals.lastErrMsg = "";

                TruckImageModel model = params[0];
                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getImageSubmitUrlTag(warehouseCode);

                String data = gson.toJson(model);

                response = (GeneralResponse) HttpRequest
                        .postData(url, data, GeneralResponse.class, context);
                if (response != null) {
                    if (response.isStatus()) {
                        Utils.logD(response.toString());


                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.dismissProgressDialog();
            if (showErrorDialog()) { // && pDialog != null && pDialog.isShowing()) {
                if (response != null && response.isStatus())
                    isImageSubmitted = true;
                new Submit().execute(Constants.TRUE);
            }
            super.onPostExecute(result);
        }
    }
}
