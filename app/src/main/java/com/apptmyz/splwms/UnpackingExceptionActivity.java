package com.apptmyz.splwms;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CounterView;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.custom.ImageProcessor;
import com.apptmyz.splwms.data.CustomDialog;
import com.apptmyz.splwms.data.DialogListener;
import com.apptmyz.splwms.data.ExceptionModel;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.data.UnpackingBox;
import com.apptmyz.splwms.data.UnpackingPart;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.CameraUtils;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.ScanUtils;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class UnpackingExceptionActivity extends BaseActivity {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3, MY_PERMISSIONS_CAMERA = 4;
    private TextView partNumTv, vehicleNumTv, masterCartonsTv, headingExceptionTv;
    private EditText goodBoxesEt, exceptionTypeEt, reasonEt;
    private Button exceptionBtn, summaryBtn, captureImageBtn, sendBtn, cancelExceptionBtn, captureNewBtn, cancelImgsBtn;
    private RecyclerView imagesRv, exceptionsRv;
    private ImagesAdapter imagesAdapter;
    private CounterView counterView;
    private Context context;
    private RelativeLayout exceptionLayout;
    private DataSource dataSource;
    private ExceptionsAdapter exceptionsAdapter;
    private String imageStoragePath, capBase64Filename = "", capBase64 = "";
    private static final int CAPTURE_IMG = 1;
    private static final int CROP_IMG = 2;
    private ArrayList<String> imgBase64s;
    private ArrayList<String> imgFileNames;
    private ConstraintLayout imagesView;
    private int imagePosition = -1, exceptionCount, noOfMasterCartons;
    private ImageLoader imageLoader;
    private UnpackingPart unpackingPart;
    private UnpackingBox unpackingBox;
    private List<ExceptionModel> exceptionModels = new ArrayList<>();
    private Gson gson = new Gson();
    private boolean isHideViewBtn = false;
    private String warehouseCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_unpacking_exception, frameLayout);
        context = UnpackingExceptionActivity.this;
        initializeViews();
    }

    private void initializeViews() {
        titleTv.setText("Unpacking Exception");
        dataSource = new DataSource(this);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        partNumTv = (TextView) findViewById(R.id.tvPartNumber);
        vehicleNumTv = (TextView) findViewById(R.id.spSubproject);
        masterCartonsTv = (TextView) findViewById(R.id.tvShipAddress);
        goodBoxesEt = (EditText) findViewById(R.id.et_good_boxes);
        exceptionBtn = (Button) findViewById(R.id.btn_exception);
        summaryBtn = (Button) findViewById(R.id.btn_submit);
        summaryBtn.setOnClickListener(listener);
        exceptionBtn.setOnClickListener(listener);
        exceptionLayout = (RelativeLayout) findViewById(R.id.layout_exception_inbound);
        exceptionTypeEt = (EditText) findViewById(R.id.et_exception_type);
        exceptionTypeEt.setOnClickListener(listener);
        reasonEt = (EditText) findViewById(R.id.reasons_et);
        captureImageBtn = (Button) findViewById(R.id.btn_captureimage);
        counterView = (CounterView) findViewById(R.id.exceptionCv);
        sendBtn = (Button) findViewById(R.id.btn_send);
        sendBtn.setOnClickListener(listener);

        cancelExceptionBtn = (Button) findViewById(R.id.btn_cancel);
        cancelExceptionBtn.setOnClickListener(listener);
        captureImageBtn.setOnClickListener(listener);

        imagesView = (ConstraintLayout) findViewById(R.id.layout_image_capture);
        imagesRv = (RecyclerView) findViewById(R.id.rv_exception_images);
        imagesRv.setNestedScrollingEnabled(false);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));

        exceptionsRv = (RecyclerView) findViewById(R.id.rv_exceptions);
        exceptionsRv.setLayoutManager(new LinearLayoutManager(context));
        exceptionsAdapter = new ExceptionsAdapter((ArrayList<ExceptionModel>) exceptionModels);
        exceptionsRv.setAdapter(exceptionsAdapter);

        imagesView = (ConstraintLayout) findViewById(R.id.layout_image_capture);
        captureNewBtn = (Button) findViewById(R.id.btn_captue_new);
        cancelImgsBtn = (Button) findViewById(R.id.btn_cancel_imgs);
        cancelImgsBtn.setOnClickListener(listener);
        captureNewBtn.setOnClickListener(listener);
        imageLoader = Utils.getImageLoader(context, "IMAGES");
        if (Utils.isValidArrayList(imgBase64s)) {
            imagesAdapter = new ImagesAdapter(imgBase64s);
            imagesRv.setAdapter(imagesAdapter);
        }

        if (getIntent().getExtras() != null) {
            Gson gson = new Gson();

            String unpackingDataStr = getIntent().getStringExtra("unpackingData");
            unpackingPart = gson.fromJson(unpackingDataStr, UnpackingPart.class);
            String unpackingBoxStr = getIntent().getStringExtra("box");
            unpackingBox = gson.fromJson(unpackingBoxStr, UnpackingBox.class);
            vehicleNumTv.setText(unpackingPart.getVehicleNumber());
            partNumTv.setText(unpackingPart.getPartNo());
            masterCartonsTv.setText(String.valueOf(unpackingPart.getNoOfPrimaryCartons()));
            noOfMasterCartons = unpackingPart.getNoOfPrimaryCartons();
        }
    }

    private void captureImage(int requestCode, String imageFileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE, imageFileName);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }
        Uri fileUri = null;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            fileUri = Uri.fromFile(file);
        } else {
            fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, requestCode);
    }

    private AlertDialog alert, imagesAlert;

    private void showExceptionDialog() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_inbound_exception, null);
        view.setVisibility(View.VISIBLE);
        alertDialog.setView(view);
        alert = alertDialog.show();
        headingExceptionTv = (TextView) view.findViewById(R.id.tv_exception_heading);
        headingExceptionTv.setText("Unpacking Exception");
        exceptionTypeEt = (EditText) view.findViewById(R.id.et_exception_type);
        reasonEt = (EditText) view.findViewById(R.id.reasons_et);
        captureImageBtn = (Button) view.findViewById(R.id.btn_captureimage);
        counterView = (CounterView) view.findViewById(R.id.exceptionCv);
        sendBtn = (Button) view.findViewById(R.id.btn_send);
        cancelExceptionBtn = (Button) view.findViewById(R.id.btn_cancel);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAndCreateException();
            }
        });
        exceptionTypeEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> depsList = new ArrayList<>();
                depsList.addAll(dataSource.exceptionTypes.getExceptionList(warehouseCode, "UP"));
                String[] a = new String[depsList.size()];
                a = depsList.toArray(a);
                showExceptionType(exceptionTypeEt, a);
            }
        });
        cancelExceptionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                resetExceptionView();
            }
        });
        captureImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isHideViewBtn = false;
                capBase64Filename = "IMAGE_Exception";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        return;
                    }
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_CAMERA);

                        return;
                    }
                }
                if (!Utils.isValidArrayList(imgBase64s)) {
                    imagePosition = -1;
                    captureImage(CAPTURE_IMG, capBase64Filename + "_1"); //opens Camera Activity
                } else {
                    showImagesDialog(imgBase64s);
                }
            }
        });
    }

    private void showImagesDialog(ArrayList<String> imageBase64s) {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_capture_image, null, false);
        view.setVisibility(View.VISIBLE);
        imagesRv = (RecyclerView) view.findViewById(R.id.rv_exception_images);
        imagesRv.setNestedScrollingEnabled(false);
        captureNewBtn = (Button) view.findViewById(R.id.btn_captue_new);
        cancelImgsBtn = (Button) view.findViewById(R.id.btn_cancel_imgs);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));
        if (Utils.isValidArrayList(imageBase64s)) {
            imagesAdapter = new ImagesAdapter(imageBase64s);
            imagesRv.setAdapter(imagesAdapter);
        }
        cancelImgsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagesAlert.dismiss();
                imagesView.setVisibility(View.GONE);
            }
        });
        captureNewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.isValidArrayList(imageBase64s) && imageBase64s.size() == 3) {
                    Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                } else {
                    imagePosition = -1;
                    captureImage(CAPTURE_IMG, capBase64Filename + "_" + imagePosition);
                }
            }
        });
        alertDialog.setView(view);
        imagesAlert = alertDialog.show();
    }

    private void validateAndCreateException() {
        if (imagesAlert != null && imagesAlert.isShowing())
            imagesAlert.dismiss();
        imagesView.setVisibility(View.GONE);
        String reason = reasonEt.getText().toString();
        String excepDesc = exceptionTypeEt.getText().toString();
        String exceptionType = "";
        exceptionType = dataSource.exceptionTypes.getExceptionId(excepDesc, warehouseCode);
        if (Utils.isValidString(reason)) {
            if (Utils.isValidString(exceptionType)) {
                int excepTypeVal = Integer.parseInt(exceptionType);
                int currentCount = counterView.getValue();
                int goodBoxesCount = 0;
                if (Utils.isValidString(goodBoxesEt.getText().toString().trim()))
                    goodBoxesCount = Integer.parseInt(goodBoxesEt.getText().toString().trim());
                int maxLimit = unpackingPart.getNoOfPrimaryCartons() - (goodBoxesCount + exceptionCount);
                if (exceptionType.equals(Constants.EXTRA) || Utils.isValidArrayList(imgFileNames)) {
                    if (currentCount != 0 && (exceptionType.equals(Constants.EXTRA) || currentCount <= maxLimit)) {
                        alert.dismiss();
                        ExceptionModel exceptionModel = new ExceptionModel();
                        exceptionModel.setScanTime(Utils.getScanTime());
                        exceptionModel.setExcepType(excepTypeVal);
                        exceptionModel.setExcepDesc(reason);
                        if (Utils.isValidArrayList(imgBase64s))
                            exceptionModel.setImgBase64s((List<String>) imgBase64s.clone());
                        exceptionModel.setExceptionCount(currentCount);

                        exceptionModels.add(exceptionModel);
                        setExceptionsData();
                        if (Utils.isValidArrayList(imgBase64s))
                            imgBase64s.clear();
                        resetExceptionView();
                    } else {
                        Utils.showToast(context, "Please Enter Valid Exception Count");
                    }
                } else {
                    Utils.showToast(context, "Please capture image");
                }
            } else {
                Utils.showToast(context, "Please select exception type");
            }
        } else {
            Utils.showToast(context, "Please enter valid reason");
        }
    }

    private void setExceptionsData() {
        exceptionCount = 0;
        for (ExceptionModel model : exceptionModels) {
            exceptionCount += model.getExceptionCount();
        }
        exceptionsAdapter = new ExceptionsAdapter((ArrayList<ExceptionModel>) exceptionModels);
        exceptionsRv.setAdapter(exceptionsAdapter);
        exceptionsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(UnpackingExceptionActivity.this, UnpackingDetailActivity.class);
        intent.putExtra("data", gson.toJson(unpackingBox));
        startActivity(intent);
    }

    private void resetExceptionView() {
        exceptionTypeEt.setText(null);
        reasonEt.setText(null);
        counterView.setValue(1);
        imagesRv.setAdapter(null);
        captureImageBtn.setBackgroundResource(R.drawable.btn_bg_orange);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_cancel:
                    alert.dismiss();
                    resetExceptionView();
                    break;
                case R.id.btn_send:
                    validateAndCreateException();
                    break;
                case R.id.btn_captureimage:
                    isHideViewBtn = false;
                    capBase64Filename = "IMAGE_Exception";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                            return;
                        }
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_CAMERA);

                            return;
                        }
                    }
                    if (!Utils.isValidArrayList(imgBase64s)) {
                        imagePosition = -1;
                        captureImage(CAPTURE_IMG, capBase64Filename + "_1"); //opens Camera Activity
                    } else {
                        showImagesDialog(imgBase64s);
                    }
                    break;
                case R.id.btn_submit:
                    validateAndSubmit();
                    break;
                case R.id.btn_exception:
                    String goodBoxesStr = goodBoxesEt.getText().toString().trim();
                    int goodBoxesCount = 0;
                    if (Utils.isValidString(goodBoxesStr))
                        goodBoxesCount = Integer.parseInt(goodBoxesStr);
                    counterView.setmMinLimit(1);
                    counterView.setmMaxLimit(noOfMasterCartons - (goodBoxesCount + exceptionCount));
                    isHideViewBtn = false;
                    captureNewBtn.setVisibility(View.VISIBLE);
                    showExceptionDialog();
                    break;
                case R.id.et_exception_type:
                    List<String> depsList = new ArrayList<>();
                    depsList.addAll(dataSource.exceptionTypes.getExceptionList(warehouseCode, "UP"));
                    String[] a = new String[depsList.size()];
                    a = depsList.toArray(a);
                    showExceptionType(exceptionTypeEt, a);
                    break;
                default:
                    break;
            }
        }
    };

    private void validateAndSubmit() {
        String goodBoxesStr = goodBoxesEt.getText().toString().trim();
        int goodBoxesCount = 0;
        if (Utils.isValidString(goodBoxesStr))
            goodBoxesCount = Integer.parseInt(goodBoxesStr);
        if (noOfMasterCartons <= goodBoxesCount + exceptionCount) {
            submit();
        } else {
            if (goodBoxesCount + exceptionCount == 0) {
                Utils.showSimpleAlert(context, getString(R.string.alert), "No Data is added about the Cartons");
                return;
            } else {
                showAlert("There are more primary cartons to unpack. Are you sure you want to submit?");
            }
        }
    }

    private void showAlert(String message) {
        CustomDialog dialog = new CustomDialog(UnpackingExceptionActivity.this, message, new DialogListener() {
            @Override
            public void onPositiveButtonClick() {
                submit();
            }

            @Override
            public void onNegativeButtonClick() {

            }
        });
        if (!isFinishing()) {
            dialog.show();
        }
    }

    private void submit() {
        if (!Utils.isValidArrayList((ArrayList<?>) exceptionModels) || validateExceptionCount()) {
            String goodBoxesStr = goodBoxesEt.getText().toString().trim();
            int goodBoxesCount = 0;
            if (Utils.isValidString(goodBoxesStr))
                goodBoxesCount = Integer.parseInt(goodBoxesStr);
            if (Utils.getConnectivityStatus(context)) {
                unpackingPart.setNoOfPrimaryCartonsContained(goodBoxesCount);
                unpackingPart.setNoOfExceptions(exceptionCount);
                unpackingPart.setBoxId(unpackingBox.getBoxId());
                if (Utils.isValidArrayList((ArrayList<?>) exceptionModels))
                    unpackingPart.setExceptionList(exceptionModels);
                new SubmitTask(unpackingPart).execute();
            }
        } else {
            Utils.showSimpleAlert(context, getString(R.string.alert), "Some Cartons are not Marked as Either Good or Exception.");
        }
    }

    private boolean validateExceptionCount() {
        boolean isValid = true;
        String goodBoxesStr = goodBoxesEt.getText().toString().trim();
        int goodBoxesCount = 0;
        if (Utils.isValidString(goodBoxesStr))
            goodBoxesCount = Integer.parseInt(goodBoxesStr);
        int excepCount = 0;
        for (ExceptionModel model : exceptionModels) {
            if (model.getExcepType() != 10)
                excepCount += model.getExceptionCount();
        }
        if (noOfMasterCartons == goodBoxesCount + excepCount) {
            isValid = true;
        } else {
            isValid = false;
        }
        return isValid;
    }

    ProgressDialog submitDialog;

    class SubmitTask extends AsyncTask<String, String, Object> {
        UnpackingPart input;
        GeneralResponse response;
        int goodBoxesCount;

        SubmitTask(UnpackingPart input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitDialog = Utils.getProgressDialog(context);
                goodBoxesCount = 0;
                String goodBoxesStr = goodBoxesEt.getText().toString().trim();
                if (Utils.isValidString(goodBoxesStr))
                    goodBoxesCount = Integer.parseInt(goodBoxesStr);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitUnpackingUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        UnpackingBox box = dataSource.unpackingBoxes.getBox(input.getVehicleNumber(), input.getInwardNumber(), input.getMasterBoxNum());
                        if (box.getBoxStatus() != -1) {
                            dataSource.unpackingBoxes.updateStatus(input.getVehicleNumber(), input.getInwardNumber(), input.getMasterBoxNum(), exceptionCount == 0 ? 1 : -1);
                        }
                        dataSource.unpackingParts.updateStatus(input.getPartId(), input.getVehicleNumber(), input.getInwardNumber(), input.getMasterBoxNum(),
                                unpackingPart.getNoOfExceptions(), unpackingPart.getNoOfPrimaryCartonsContained());
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitDialog != null && submitDialog.isShowing()) {
                submitDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null) {
                    if (response.isStatus()) {
                       onBackPressed();
                    }
                    Utils.showToast(context, response.getMessage());
                }
                finish();
            }
            super.onPostExecute(result);
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(UnpackingExceptionActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void showExceptionType(final EditText exceptionTv,
                                   final String arr[]) {
        int selectedPos = -1;
        String text = URLDecoder
                .decode(exceptionTv.getText().toString().trim());
        if (Utils.isValidString(text)) {
            for (int i = 0; i < arr.length; i++) {
                if (text.equalsIgnoreCase(arr[i].toString())) {
                    selectedPos = i;
                    break;
                }
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setSingleChoiceItems(arr, selectedPos,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int pos) {
                        dialog.dismiss();
                        exceptionTv.setText(arr[pos]);
                        exceptionTypeEt.setText(arr[pos]);
                        if (arr[pos].equals("Extra")) {
                            captureImageBtn.setVisibility(View.GONE);
                        } else {
                            captureImageBtn.setVisibility(View.VISIBLE);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.cancel_btn),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        }).show();
    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<String> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        showImagePreview(getBitmap(imgBase64s.get(pos)), pos);
                        break;
                    case R.id.iv_delete:
                        imgBase64s.remove((int) v.getTag());
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<String> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            String base64 = bitmaps.get(position);
            if (base64 != null) {
                holder.imageView.setImageBitmap(getBitmap(base64));
                if (isHideViewBtn) {
                    holder.viewBtn.setVisibility(View.GONE);
                    holder.deleteIv.setVisibility(View.GONE);
                } else {
                    holder.viewBtn.setVisibility(View.VISIBLE);
                    holder.deleteIv.setVisibility(View.VISIBLE);
                }

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<String> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(String dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }

    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imagePosition = position;
                captureImage(CAPTURE_IMG, capBase64Filename + "_" + position);
            }
        });
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }

    private Bitmap getFullBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            int IMAGE_MAX_SIZE = 0;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                IMAGE_MAX_SIZE = 500000; //0.5MP
            } else {
                IMAGE_MAX_SIZE = 1200000; // 1.2MP
            }
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);

            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            Matrix mat = new Matrix();
            mat.postRotate(angle);

            b = Bitmap.createBitmap(b, 0, 0,
                    b.getWidth(), b.getHeight(), mat, true);

            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_IMG:
                    try {
                        bitmap = getFullBitmap(imageStoragePath);
                        if (bitmap != null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] imageBytes = baos.toByteArray();
                            capBase64 = capBase64Filename;
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);

                            if (Utils.isValidString(capBase64))
                                captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            Date date = new Date();
                            long d = date.getTime();
                            Bitmap resizedBitmap = null;
                            if (bitmap != null) {
                                int w = bitmap.getWidth();
                                int h = bitmap.getHeight();
                                Matrix mat = new Matrix();
                                if (w > h)
                                    mat.postRotate(90);

                                resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                                ByteArrayOutputStream baosResized = new ByteArrayOutputStream();
                                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baosResized);
                                byte[] resizedImageBytes = baosResized.toByteArray();
                            }
                        }
                        Date date = new Date();
                        long d = date.getTime();
                        capBase64Filename = Utils.getImageTime(d);

                        if (!Utils.isValidArrayList(imgBase64s)) {
                            imgBase64s = new ArrayList<>();
                            imgFileNames = new ArrayList<>();
                        }

                        if (imagePosition == -1) {
                            imgBase64s.add(getBase64(bitmap));
                            imgFileNames.add(capBase64Filename);
                        } else {
                            imgBase64s.set(imagePosition, getBase64(bitmap));
                            imgFileNames.set(imagePosition, capBase64Filename);
                        }

                        isHideViewBtn = false;
                        imagesAdapter = new ImagesAdapter(imgBase64s);
                        imagesRv.setAdapter(imagesAdapter);
                        imagesAdapter.notifyDataSetChanged();

                        if (imagesAlert == null || !imagesAlert.isShowing())
                            showImagesDialog(imgBase64s);

                        addImage(bitmap, capBase64Filename, false, CROP_IMG);//////
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.logE("CAPTURE ERROR : " + e.toString());
                    }
                    break;

                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64))
                                captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        } else {
            switch (requestCode) {
                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64))
                                captureImageBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop, int cropRequestCode) {
        if (bitmap != null) {
            ImageLoader imageLoader = new ImageLoader(context, "POD");
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (!isCrop) {
                } else {
                    doCrop(bitmap, imageFileName, cropRequestCode);
                }
            }
        }
    }

    private void doCrop(Bitmap bitmap, String imageFileName, int cropRequestCode) {
        ImageProcessor.getInstance().setData(context, bitmap, imageFileName);
        Intent intent = new Intent(context, ADCropActivity.class);
        ((Activity) context).startActivityForResult(intent, cropRequestCode);
    }

    class ExceptionsAdapter extends RecyclerView.Adapter<ExceptionsAdapter.DataObjectHolder> {
        private ArrayList<ExceptionModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView exceptionTypeTv, noOfBoxesTv;
            Button imgBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                exceptionTypeTv = (TextView) itemView.findViewById(R.id.tv_exception_type);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_quantity);
                imgBtn = (Button) itemView.findViewById(R.id.btn_image);
                imgBtn.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    if (v.getId() == R.id.btn_image) {
                        ExceptionModel model = list.get(pos);
                        if (Utils.isValidArrayList((ArrayList<?>) model.getImgBase64s())) {
                            isHideViewBtn = true;
                            imagesAdapter = new ImagesAdapter((ArrayList<String>) model.getImgBase64s());
                            imagesRv.setAdapter(imagesAdapter);
                            imagesAdapter.notifyDataSetChanged();
                            showImagesDialog((ArrayList<String>) model.getImgBase64s());
                            captureNewBtn.setVisibility(View.GONE);
                        }
                    }
                }
            }
        }

        public ExceptionsAdapter(ArrayList<ExceptionModel> myDataset) {
            list = myDataset;
        }

        @Override
        public ExceptionsAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_inspection_exception_item, parent, false);
            return new ExceptionsAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ExceptionsAdapter.DataObjectHolder holder, final int position) {
            ExceptionModel model = list.get(position);

            if (model != null) {
                SkipBinReasonModel exception = dataSource.exceptionTypes.getException(model.getExcepType(), warehouseCode);
                if (exception != null) {
                    holder.exceptionTypeTv.setText(exception.getReason());
                    if (exception.getReasonId() == 10) {
                        holder.imgBtn.setAlpha(0.6f);
                        holder.imgBtn.setEnabled(false);
                        holder.imgBtn.setClickable(false);
                        holder.imgBtn.setFocusableInTouchMode(false);
                    }
                }
                holder.noOfBoxesTv.setText(String.valueOf(model.getExceptionCount()));
            }

            holder.exceptionTypeTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.imgBtn.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<ExceptionModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(ExceptionModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    private String getBase64Img(String imageName, String text) {
        ImageLoader imageLoader = ScanUtils.getImageLoader(context);

        String base64Img = "";
        try {
            base64Img = imageLoader.fileCache.getBase64StringToName(imageName, "text", context);
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return base64Img;
    }

    private Bitmap getBitmap(String base64) {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
