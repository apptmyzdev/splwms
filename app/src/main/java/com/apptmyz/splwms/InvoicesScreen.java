package com.apptmyz.splwms;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.BitmapScalingUtil;
import com.apptmyz.splwms.custom.CameraActivity;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.Cons;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InvoiceModel;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.data.TruckImageModel;
import com.apptmyz.splwms.data.UnloadingResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class InvoicesScreen extends Activity {

    public ArrayList<ArrayList<InvoiceModel>> childData = new ArrayList<ArrayList<InvoiceModel>>();
    public List<NewProductModel> groupData = new ArrayList<NewProductModel>();

    private ArrayList<Bitmap> imgBitmaps;
    private ArrayList<String> imgFileNames;
    private RecyclerView imagesRv;
    private ImagesAdapter imagesAdapter;
    private int imagePosition = -1;

    private Context context;
    RadioButton.OnClickListener radioClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rb_scan:
                    goNext(vehNo);
                    break;
                case R.id.rb_count:
                    Intent intent = new Intent(context, CountBoxesScreen.class);
                    startActivity(intent);
                    break;


            }
        }
    };
    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.home_iv:
                    goBack();
                    break;

                case R.id.btn_capture_image:
                    truckImageFileName = "";
                    imagePosition = -1;
                    if (Utils.isValidArrayList(imgFileNames) && imgFileNames.size() == 3) {
                        Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                    } else {
                        takePicture(context);
                    }
                    break;
                case R.id.btn_send_image:

                    if (Utils.isValidArrayList(imgFileNames)) {
                        Utils.logE("in LT image");

                        String text = "";

                        String truck = Globals.vehicleNo;
                        if (Utils.isValidString(truck)) {
                            text = text + "\n" + "Veh#  : " + truck;
                        }

                        String s = dataSource.sharedPreferences
                                .getValue(Constants.IMEI);
                        if (Utils.isValidString(s))
                            text = text + "\n" + "T-ID# : " + s;

                        String user = dataSource.sharedPreferences
                                .getValue(Constants.USERNAME_PREF);
                        if (Utils.isValidString(user))
                            text = text + "\n" + "U-ID# : " + user;

                        text = text + "\n" + "I-DT# : "
                                + Utils.getITime(Long.valueOf(truckImageFileName));

                        imageLayout.setVisibility(View.GONE);
                        TruckImageModel model = new TruckImageModel();

                        model.setVehTxnDate(Utils.getScanTimeLoading());
                        model.setLoadUnloadFlag(Constants.UNLOADING_STARTED);

                        model.setVehicleNo(Globals.vehicleNo);

                        ImageLoader loader = new ImageLoader(context, Constants.TRUCK);
                        try {
                            String base64 = loader.fileCache.getBase64StringToName(
                                    truckImageFileName, text, context);
                            if (Utils.isValidArrayList(imgBitmaps)) {
                                List<String> list = getBase64s(imgBitmaps);
                                model.setImages(list);
                                Utils.logD(model.toString());
                                new SendStackingImage().execute(model);
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }
                    } else {
                        Utils.logE("in else ");
                        Utils.showSimpleAlert(
                                context,
                                getString(R.string.alert_dialog_title),
                                "Please capture stacking image");

                    }

                    break;

                case R.id.iv_truck:
//                    showImage(truckImageFileName);
                    break;
            }
        }
    };

    private List<String> getBase64s(List<Bitmap> bitmaps) {
        List<String> list = new ArrayList<>();
        for(Bitmap bitmap: bitmaps) {
            String base64 = getBase64(bitmap);
            list.add(base64);
        }
        return list;
    }

    private TextView titleTv, vehNoTv, supplierTv;
    private RadioButton scanRb, countRb;
    private ExpandableListView invoicesELv;
    private ArrayList<InvoiceModel> invoiceList = new ArrayList<>();
    private ExpandableListAdapter expandableListAdapter;
    private ImageView home_iv;
    private DataSource dataSource;
    private String truckImageFileName;
    String vehNo, warehouseCode;
    private RelativeLayout imageLayout;
    private Button captureBtn, sendBtn;
    private TextView msgTv;
    public Gson gson = new Gson();
    public final int TAKE_PHOTO = 1;
    private ImageView truckIv;
    public final int CROP_IMAGE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invoices_screen);

        context = InvoicesScreen.this;

        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);

        titleTv = (TextView) findViewById(R.id.title_tv);
        titleTv.setText(getString(R.string.select_invoices));

        vehNoTv = (TextView) findViewById(R.id.tv_veh_no);
        supplierTv = (TextView) findViewById(R.id.tv_supplier);

        scanRb = (RadioButton) findViewById(R.id.rb_scan);
        scanRb.setOnClickListener(radioClick);
        countRb = (RadioButton) findViewById(R.id.rb_count);
        countRb.setOnClickListener(radioClick);

        home_iv = (ImageView) findViewById(R.id.home_iv);
        home_iv.setOnClickListener(onClickListener);


        invoicesELv = (ExpandableListView) findViewById(R.id.invoices_elv);

        imageLayout = (RelativeLayout) findViewById(R.id.layout_image_popup);

        captureBtn = (Button) findViewById(R.id.btn_capture_image);
        captureBtn.setOnClickListener(onClickListener);

        sendBtn = (Button) findViewById(R.id.btn_send_image);
        sendBtn.setOnClickListener(onClickListener);

        truckIv = (ImageView) findViewById(R.id.iv_truck);
        truckIv.setOnClickListener(onClickListener);

        imagesRv = (RecyclerView) findViewById(R.id.rv_images);
        imagesRv.setNestedScrollingEnabled(false);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));

        if (Utils.isValidArrayList(imgBitmaps)) {
            imagesAdapter = new ImagesAdapter(imgBitmaps);
            imagesRv.setAdapter(imagesAdapter);
        }

        msgTv = (TextView) findViewById(R.id.tv_msg);
        msgTv.setText("Before Starting Unloading, Take Picture of shipment stacked inside vehicle, Keeping back doors Open");


        Intent intent = getIntent();
        vehNo = intent.getStringExtra(Constants.VEHICLE_NUMBER);
        if (Utils.isValidString(vehNo)) {
            vehNoTv.setText(vehNo);
            Globals.vehicleNo = vehNo;
            if (dataSource.unloadingParts.getPartsData(vehNo, warehouseCode))
                resetFragmentData();
            else
                new UnloadingTask().execute(vehNo);

        }
        expandableListAdapter = new ExpandableListAdapter(context);
        invoicesELv.setAdapter(expandableListAdapter);
    }

    private void goNext(String vehNo) {
        Globals.isLoading = false;
        Intent intent = new Intent(context, PartsScanningScreen.class);
        intent.putExtra(Constants.CLASS_NAME, getClass().getName());
        intent.putExtra(Constants.VEHICLE_NUMBER, vehNo);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void goBack() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void resetData(ArrayList<NewProductModel> productList) {
        clearPPMList();
        if (Utils.isValidArrayList(productList)) {
            for (NewProductModel model : productList) {
                groupData.add(model);
                ArrayList<InvoiceModel> invoiceModels = new ArrayList<InvoiceModel>();
                invoiceModels.addAll(model.getInvoiceList());
                childData.add(invoiceModels);
            }
        }
        refreshPPM();
    }

    private void clearPPMList() {

        if (groupData != null)
            groupData.clear();
        else
            groupData = new ArrayList<NewProductModel>();

        if (childData != null)
            childData.clear();
        else
            childData = new ArrayList<ArrayList<InvoiceModel>>();
    }

    public void refreshPPM() {
        if (expandableListAdapter != null)
            expandableListAdapter.notifyDataSetChanged();

        if (Constants.isAlwaysExpandable) {
            for (int groupPosition = 0; groupPosition < groupData.size(); groupPosition++) {
                try {
                    invoicesELv.collapseGroup(groupPosition);
                    invoicesELv.expandGroup(groupPosition);
                } catch (Exception e) {
                }
            }
        }

    }

    public void resetFragmentData() {
        List<NewProductModel> productModels = null;
        dataSource.unloadingParts.getScanConsList(context, Constants.invoicesTag, true, warehouseCode);
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
        if (isLogout)
            dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

        try {
            if (!isFinishing()) {
                if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {

                    errDlg = new CustomAlertDialog(InvoicesScreen.this, Globals.lastErrMsg,
                            true, isLogout);
                    errDlg.setTitle(getString(R.string.alert_dialog_title));
                    errDlg.setCancelable(false);
                    Globals.lastErrMsg = "";

                    Utils.dismissProgressDialog();
                    isNotErr = false;
                    errDlg.show();


                }
            }
        } catch (Exception e) {
            Utils.logE(e.toString());
        }
        return isNotErr;
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;
        private View.OnClickListener groupClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int groupPosition = (Integer) v.getTag();
                if (invoicesELv.isGroupExpanded(groupPosition))
                    invoicesELv.collapseGroup(groupPosition);
                else
                    invoicesELv.expandGroup(groupPosition);

            }
        };

        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            View v = inflater.inflate(R.layout.invoice_child_item, parent, false);

            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);


            InvoiceModel invoiceModel = (InvoiceModel) getChild(groupPosition, childPosition);
            TextView invoiceNoTv = (TextView) v.findViewById(R.id.tv_invoice_no);
            TextView qtyTv = (TextView) v.findViewById(R.id.qty_tv);
            TextView boxCountTv = (TextView) v.findViewById(R.id.tv_boxes_no);


            if (Utils.isValidString(invoiceModel.getInvoiceNo()))
                invoiceNoTv.setText(invoiceModel.getInvoiceNo());


            qtyTv.setText(Integer.toString(invoiceModel.getTotalQty()));


            boxCountTv.setText(Integer.toString(invoiceModel.getNoOfBoxes()));

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int gPos = (Integer) v.getTag(R.string.group_pos);
                    int cPos = (Integer) v.getTag(R.string.child_pos);

                    NewProductModel productModel = (NewProductModel) getGroup(gPos);
                    InvoiceModel invoiceModel1 = (InvoiceModel) getChild(gPos, cPos);
                }
            });

            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.invoice_grp_item, parent, false);

            v.setTag(groupPosition);

            final NewProductModel model = (NewProductModel) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (invoicesELv.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);

            TextView partNoTv = (TextView) v.findViewById(R.id.tv_part_no);
            TextView totQtyTv = (TextView) v.findViewById(R.id.tot_qty_tv);
            TextView totBoxesTv = (TextView) v.findViewById(R.id.tv_tot_boxes_no);

            if (Utils.isValidString(model.getPartNo()))
                partNoTv.setText(model.getPartNo());

            totQtyTv.setText(Integer.toString(model.getTotalQty()));
            totBoxesTv.setText(Integer.toString(model.getTotalBoxes()));


            v.setOnClickListener(groupClickListener);


            return v;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }

    ProgressDialog unloadingDialog;

    class UnloadingTask extends AsyncTask<String, Object, Object> {
        UnloadingResponse unloadingResponse;

        @Override
        protected void onPreExecute() {
            Utils.getProgressDialog(context);

            Globals.lastErrMsg = "";
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String vehNo = params[0];

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getUnloadingUrlTag(vehNo, warehouseCode);

                unloadingResponse = (UnloadingResponse) HttpRequest
                        .getInputStreamFromUrl(url, UnloadingResponse.class, context);
                if (unloadingResponse != null) {
                    Utils.logD(unloadingResponse.toString());
                    if (unloadingResponse.getStatus()) {
                        Globals.unloadingResponse = unloadingResponse;
                    } else {
                        Globals.lastErrMsg = unloadingResponse.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.dismissProgressDialog();

            if (showErrorDialog()) {
                if (unloadingResponse.getData().isImgFlag()) {
                    imageLayout.setVisibility(View.VISIBLE);
                } else {
                    if (unloadingResponse != null && Utils.isValidArrayList((ArrayList<?>) unloadingResponse.getData().getPartList())) {
                        dataSource.unloadingParts.saveData(context, unloadingResponse, vehNo, warehouseCode);
                    }
                }
            }

            super.onPostExecute(result);
        }

    }

    class SendStackingImage extends AsyncTask<TruckImageModel, Object, Object> {
        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            Utils.getProgressDialog(context);
        }

        @Override
        protected Object doInBackground(TruckImageModel... params) {

            try {

                Globals.lastErrMsg = "";

                TruckImageModel model = params[0];
                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getImageSubmitUrlTag(warehouseCode);

                String data = gson.toJson(model);

                GeneralResponse response = (GeneralResponse) HttpRequest
                        .postData(url, data, GeneralResponse.class, context);
                if (response != null) {
                    if (response.isStatus()) {
                        Utils.logD(response.toString());


                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Object result) {
            Utils.dismissProgressDialog();
            if (showErrorDialog()) {
                if (Globals.unloadingResponse != null && Utils.isValidArrayList((ArrayList<?>) Globals.unloadingResponse.getData().getPartList())) {
                    dataSource.unloadingParts.saveData(context, Globals.unloadingResponse, vehNo, warehouseCode);
                }

            }


            super.onPostExecute(result);
        }

    }


    private void takePicture(Context context) {
        Utils.logD("*");
        Intent intent = new Intent(context, CameraActivity.class);
        startActivityForResult(intent, TAKE_PHOTO);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        switch (requestCode) {


            case TAKE_PHOTO:
                try {
                    Utils.logD("**");

                    Uri uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
                    String imageUri = uri.toString();
                    bitmap = BitmapScalingUtil.bitmapFromUri(context,
                            Uri.parse(imageUri));
                    if (bitmap != null) {
                        int w = bitmap.getWidth();
                        int h = bitmap.getHeight();
                        Matrix mat = new Matrix();
                        if (w > h)
                            mat.postRotate(90);

                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                        Globals.bitmap = bitmap;

                        String diff = dataSource.sharedPreferences
                                .getValue(Constants.TIME_DIFF);
                        long d = 0;
                        if (Utils.isValidString(diff))
                            d = Long.valueOf(diff);

                        addImage(bitmap, Utils.getImageTime(d), false);
                    }
                } catch (Exception e) {

                }

                break;
        }

    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop) {
        Utils.logD("***");

        if (truckIv != null && bitmap != null) {
            Utils.logD("*****");

            ImageLoader imageLoader = new ImageLoader(context, Constants.TRUCK);
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (truckIv.getVisibility() == View.GONE || truckIv.getVisibility() == View.INVISIBLE)
                    truckIv.setVisibility(View.VISIBLE);

                truckImageFileName = imageFileName;

                if (!Utils.isValidArrayList(imgBitmaps)) {
                    imgBitmaps = new ArrayList<>();
                    imgFileNames = new ArrayList<>();
                }
                if (imagePosition == -1) {
                    imgBitmaps.add(bitmap);
                    imgFileNames.add(imageFileName);
                } else {
                    imgBitmaps.set(imagePosition, bitmap);
                    imgFileNames.set(imagePosition, imageFileName);
                }

                imagesAdapter = new ImagesAdapter(imgBitmaps);
                imagesRv.setAdapter(imagesAdapter);
                imagesAdapter.notifyDataSetChanged();
            }

        }
    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<Bitmap> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        showImagePreview(imgBitmaps.get(pos), pos);
                        break;
                    case R.id.iv_delete:
                        imgBitmaps.remove((int) v.getTag());
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<Bitmap> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            Bitmap bitmap = bitmaps.get(position);
            if (bitmap != null) {
                holder.imageView.setImageBitmap(bitmap);

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<Bitmap> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Bitmap dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }
    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imagePosition = position;
                takePicture(context);
            }
        });
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }
}






