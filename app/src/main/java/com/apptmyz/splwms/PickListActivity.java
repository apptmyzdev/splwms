package com.apptmyz.splwms;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.splwms.custom.ADRadioGroup;
import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.CustomerMasterModel;
import com.apptmyz.splwms.data.PartMasterModel;
import com.apptmyz.splwms.data.ProductMasterModel;
import com.apptmyz.splwms.data.SlaNameComponentModel;
import com.apptmyz.splwms.data.WarehouseMasterModel;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.data.PickListResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.apptmyz.splwms.util.Utils;

public class PickListActivity extends BaseActivity {
    TextView titleTv, noProductsTv;
    ArrayList<WmsPartInvoiceModel> bins = new ArrayList<>();
    RecyclerView binsRv;
    LinearLayoutManager layoutManager;
    LinearLayout dataLayout;
    BinAdapter binAdapter;
    TextInputEditText binNumEt;
    ImageView scanBinIv;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition;
    private int timeScan;
    private TimerTask timerScanTask;
    private String binNumber, customerName, type;
    private AutoCompleteTextView customerSp, partSp, slaSp, productSp;
    private LinearLayout customerLayout, partLayout, slaLayout, productLayout;
    private Button goBtn;
    Context context;
    Gson gson = new Gson();
    ADRadioGroup radioGroup;
    boolean isBinScanned = false, isRefresh;
    int customerId, subprojectId;
    DataSource dataSource;
    ImageView refreshIv;
    String typeParam = "C", warehouseCode;
    String dataStr = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_pick_list, frameLayout);

        context = PickListActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);
        noProductsTv = (TextView) findViewById(R.id.tv_no_products);

        customerSp = (AutoCompleteTextView) findViewById(R.id.customerSp);
        partSp = (AutoCompleteTextView) findViewById(R.id.partSp);
        slaSp = (AutoCompleteTextView) findViewById(R.id.slaSp);
        productSp = (AutoCompleteTextView) findViewById(R.id.productSp);

        customerLayout = (LinearLayout) findViewById(R.id.layout_select_customer);
        partLayout = (LinearLayout) findViewById(R.id.layout_select_part);
        slaLayout = (LinearLayout) findViewById(R.id.layout_select_SLA);
        productLayout = (LinearLayout) findViewById(R.id.layout_select_product);

        goBtn = (Button) findViewById(R.id.goBtn);
        goBtn.setOnClickListener(listener);

        titleTv = (TextView) findViewById(R.id.tv_top_title);
        dataLayout = (LinearLayout) findViewById(R.id.layout_data);

        ArrayList<CustomerMasterModel> customers = dataSource.nodes.getCustomersData();
        ArrayAdapter<CustomerMasterModel> customerAdapter = new ArrayAdapter<CustomerMasterModel>(PickListActivity.this,
                android.R.layout.simple_dropdown_item_1line, customers);
        customerSp.setAdapter(customerAdapter);

        ArrayList<PartMasterModel> parts = dataSource.parts.getPartsData(warehouseCode);
        ArrayAdapter<PartMasterModel> partsAdapter = new ArrayAdapter<PartMasterModel>(PickListActivity.this,
                android.R.layout.simple_dropdown_item_1line, parts);
        partSp.setAdapter(partsAdapter);

        ArrayList<SlaNameComponentModel> slaModels = dataSource.sla.getSlaData();
        ArrayAdapter<SlaNameComponentModel> slaAdapter = new ArrayAdapter<SlaNameComponentModel>(PickListActivity.this,
                android.R.layout.simple_dropdown_item_1line, slaModels);
        slaSp.setAdapter(slaAdapter);

        ArrayList<ProductMasterModel> productMasterModels = dataSource.products.getProductsData();
        ArrayAdapter<ProductMasterModel> productAdapter = new ArrayAdapter<>(PickListActivity.this,
                android.R.layout.simple_dropdown_item_1line, productMasterModels);
        productSp.setAdapter(productAdapter);

        if (getIntent().getExtras() != null) {
            isRefresh = getIntent().getBooleanExtra("isRefresh", false);
            type = getIntent().getStringExtra("type");
            if (type.equals("Customer")) {
                typeParam = "C";
                if (Utils.isValidArrayList(customers)) {
                    dataStr = customers.get(0).getCustomerCode();
                }
                customerLayout.setVisibility(View.VISIBLE);
                partLayout.setVisibility(View.GONE);
                slaLayout.setVisibility(View.GONE);
                productLayout.setVisibility(View.GONE);
            } else if (type.equals("Part")) {
                typeParam = "P";
                if (Utils.isValidArrayList(parts)) {
                    dataStr = String.valueOf(parts.get(0).getPartId());
                }
                customerLayout.setVisibility(View.GONE);
                partLayout.setVisibility(View.VISIBLE);
                slaLayout.setVisibility(View.GONE);
                productLayout.setVisibility(View.GONE);
            } else if(type.equals("SLA")) {
                typeParam = "S";
                if (Utils.isValidArrayList(slaModels)) {
                    dataStr = String.valueOf(((SlaNameComponentModel)slaModels.get(0)).getId());
                }
                customerLayout.setVisibility(View.GONE);
                partLayout.setVisibility(View.GONE);
                slaLayout.setVisibility(View.VISIBLE);
                productLayout.setVisibility(View.GONE);
            } else {
                typeParam = "PR";
                if (Utils.isValidArrayList(productMasterModels)) {
                    dataStr = String.valueOf(((ProductMasterModel)productMasterModels.get(0)).getId());
                }
                customerLayout.setVisibility(View.GONE);
                partLayout.setVisibility(View.GONE);
                slaLayout.setVisibility(View.GONE);
                productLayout.setVisibility(View.VISIBLE);
            }
        }

        customerSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                typeParam = "C";
                dataStr = ((CustomerMasterModel)adapterView.getItemAtPosition(i)).getCustomerCode();
            }
        });

        customerSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    customerSp.showDropDown();
                }
            }
        });

        customerSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                customerSp.showDropDown();
                return false;
            }
        });

        partSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                typeParam = "P";
                dataStr = String.valueOf(((PartMasterModel)adapterView.getItemAtPosition(i)).getPartId());
            }
        });

        partSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    partSp.showDropDown();
                }
            }
        });

        partSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                partSp.showDropDown();
                return false;
            }
        });

        slaSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                typeParam = "S";
                dataStr = String.valueOf(((SlaNameComponentModel)adapterView.getItemAtPosition(i)).getId());
            }
        });

        slaSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    slaSp.showDropDown();
                }
            }
        });

        slaSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                slaSp.showDropDown();
                return false;
            }
        });

        productSp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                typeParam = "PR";
                dataStr = String.valueOf(((ProductMasterModel)adapterView.getItemAtPosition(i)).getId());
            }
        });

        productSp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    productSp.showDropDown();
                }
            }
        });

        productSp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                productSp.showDropDown();
                return false;
            }
        });

        titleTv.setText(customerName != null ? customerName : "Pick List");

        radioGroup = (ADRadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(checkListener);

        scanBinIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanBinIv.setOnClickListener(listener);
        binNumEt = (TextInputEditText) findViewById(R.id.input_bin_number);
        binNumEt.addTextChangedListener(watcher);

        binsRv = (RecyclerView) findViewById(R.id.rv_bins);
        binsRv.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager(this);
        binsRv.setLayoutManager(layoutManager);

        if (isRefresh) {
            refresh();
        } else {
            setData(1);
        }
    }

    ProgressDialog subprojectsDialog;

    class PartsTask extends AsyncTask<String, String, Object> {
        String typeParam, data;

        PartsTask(String typeParam, String data) {
            this.typeParam = typeParam;
            this.data = data;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                subprojectsDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getPickListUrl(typeParam, data, warehouseCode).replace(" ", "%20");
                bins = null;
                Utils.logD("Log 1");
                PickListResponse response = (PickListResponse) HttpRequest
                        .getInputStreamFromUrl(url, PickListResponse.class,
                                context);

                if (response != null) {
                    dataSource.pickListParts.delete(type, dataStr, warehouseCode);
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<WmsPartInvoiceModel> data = (ArrayList<WmsPartInvoiceModel>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            dataSource.pickListParts.insertPickListData(data, type, dataStr, warehouseCode);
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && subprojectsDialog != null && subprojectsDialog.isShowing()) {
                subprojectsDialog.dismiss();
            }
            if (binAdapter != null)
                binAdapter.notifyDataSetChanged();
            if (showErrorDialog()) {
                setData(radioGroup.getCheckedRadioButtonId() == R.id.rb_pending ? 0 : 1);
            }
            super.onPostExecute(result);
        }
    }

    private void setData(int status) {
        bins = new ArrayList<>();
        binAdapter = new BinAdapter(bins);
        binsRv.setAdapter(binAdapter);
        binAdapter.notifyDataSetChanged();
        bins = dataSource.pickListParts.getPickListData(type, dataStr, status, warehouseCode);
        binAdapter = new BinAdapter(bins);
        binsRv.setAdapter(binAdapter);
        binAdapter.notifyDataSetChanged();
        if (Utils.isValidArrayList(bins)) {
            dataLayout.setVisibility(View.VISIBLE);
            noProductsTv.setVisibility(View.GONE);
        } else {
            noProductsTv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setData(radioGroup.getCheckedRadioButtonId() == R.id.rb_pending ? 0 : 1);
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickListActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private RadioGroup.OnCheckedChangeListener checkListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
            switch (checkedId) {
                case R.id.rb_pending:
                    setData(0);
                    break;
                case R.id.rb_all:
                    setData(1);
                    break;
                default:
                    break;
            }
        }
    };

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            isBinScanned = false;
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (Utils.isValidString(scannedValue)) {
                                        ArrayList<WmsPartInvoiceModel> tempBins = new ArrayList<>();
                                        for (WmsPartInvoiceModel bin : bins) {
                                            if (bin.getBinNo().contains(scannedValue)
                                                    || bin.getPartNo().contains(scannedValue))
                                                tempBins.add(bin);
                                        }
                                        binAdapter = new BinAdapter(tempBins);
                                        binsRv.setAdapter(binAdapter);
                                        binAdapter.notifyDataSetChanged();
                                    } else {
                                        binAdapter = new BinAdapter(bins);
                                        binsRv.setAdapter(binAdapter);
                                        binAdapter.notifyDataSetChanged();
                                    }

                                    if (scannedValue.length() >= 2) { //change this later
                                        binNumber = scannedValue;
                                        int matchCount = dataSource.bins.matchesData(scannedValue, warehouseCode);
                                        if (matchCount == 0) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }
                                        //open next screen
                                        boolean isMatch = false;
                                        for (WmsPartInvoiceModel bin : bins) {
                                            if (binNumber.equals(bin.getBinNo())) {
                                                isMatch = true;
                                                isBinScanned = true;

                                                Intent intent = new Intent(context, PickListBinDetailActivity.class);
                                                String binStr = gson.toJson(bin);
                                                intent.putExtra("subprojectId", subprojectId);
                                                intent.putExtra("nodeId", customerId);
                                                intent.putExtra("bin", binStr);
                                                intent.putExtra("data", dataStr);
                                                intent.putExtra("type", type);
                                                startActivity(intent);
                                            }
                                        }
                                        if (!isMatch) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN Number Doesn't match any existing bins");
                                        }
                                    }
                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.goBtn:
                    validateAndGetData();
                    break;
                case R.id.iv_scan_bin:
                    binNumEt.requestFocus();
                    binNumEt.setText("");
                    break;
                case R.id.iv_refresh:
                    refresh();
                    break;
                default:
                    break;
            }
        }
    };

    private void refresh() {
        binNumEt.setText(null);
        binNumEt.clearFocus();
        if (Utils.getConnectivityStatus(context))
            new PartsTask(typeParam, dataStr).execute();
    }

    private void validateAndGetData() {
        if (Utils.getConnectivityStatus(context))
            new PartsTask(typeParam, dataStr).execute();
    }

    class BinAdapter extends RecyclerView.Adapter<BinAdapter.DataObjectHolder> {
        private ArrayList<WmsPartInvoiceModel> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView binNumTv, productNumTv, noOfBoxesTv, orderNumTv;
            LinearLayout layout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                layout = (LinearLayout) itemView.findViewById(R.id.putaway_layout);
                binNumTv = (TextView) itemView.findViewById(R.id.tv_bin_number);
                productNumTv = (TextView) itemView.findViewById(R.id.tv_product_number);
                noOfBoxesTv = (TextView) itemView.findViewById(R.id.tv_boxes_count);
                orderNumTv = (TextView) itemView.findViewById(R.id.tv_order_number);
                layout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PickListActivity.this, PickListBinDetailActivity.class);
                    String binStr = gson.toJson(list.get(pos));
                    intent.putExtra("bin", binStr);
                    intent.putExtra("subprojectId", subprojectId);
                    intent.putExtra("nodeId", customerId);
                    intent.putExtra("data", dataStr);
                    intent.putExtra("type", type);
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        }

        public BinAdapter(ArrayList<WmsPartInvoiceModel> myDataset) {
            list = myDataset;
        }

        @Override
        public BinAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                              int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_picklist_item, parent, false);
            return new BinAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final BinAdapter.DataObjectHolder holder, final int position) {
            WmsPartInvoiceModel model = list.get(position);

            if (model != null) {
                holder.binNumTv.setText(model.getBinNo());
                holder.productNumTv.setText(model.getPartNo());
                holder.noOfBoxesTv.setText(String.valueOf(model.getTotalBoxes()));
                holder.orderNumTv.setText(model.getOrderNo());
            }

            holder.binNumTv.setTag(position);
            holder.productNumTv.setTag(position);
            holder.noOfBoxesTv.setTag(position);
            holder.orderNumTv.setTag(position);
            holder.layout.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list != null ? list.size() : 0;
        }

        public void update(ArrayList<WmsPartInvoiceModel> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(WmsPartInvoiceModel dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, PickingTypeActivity.class));
    }
}
