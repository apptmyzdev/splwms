package com.apptmyz.splwms;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.data.LoginAuthenticationModel;
import com.apptmyz.splwms.data.LoginResponse;
import com.apptmyz.splwms.data.User;
import com.apptmyz.splwms.data.WarehouseMasterModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {
    private TextInputEditText textInputUsername;
    private TextInputEditText textInputPassword;
    private Context context;
    private CustomAlertDialog errDlg;
    private DataSource dataSource;
    private int roleId;
    private LoginAuthenticationModel loginAuthenticationModel = new LoginAuthenticationModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = LoginActivity.this;
        dataSource = new DataSource(context);

        textInputUsername = findViewById(R.id.et_username);
        textInputPassword = findViewById(R.id.et_password);

        String usernameStr = dataSource.sharedPreferences.getValue(Constants.USERNAME_PREF);
        String passwordStr = dataSource.sharedPreferences.getValue(Constants.PASSWORD_PREF);
        if (Utils.isValidString(usernameStr))
            textInputUsername.setText(usernameStr);
        if (Utils.isValidString(passwordStr))
            textInputPassword.setText(passwordStr);
    }

    private boolean validateUsername() {
        String usernameInput = textInputUsername.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputUsername.setError("Field can't be empty");
            return false;
        } else if (usernameInput.length() > 15) {
            textInputUsername.setError("Username too long");
            return false;
        } else {
            textInputUsername.setError(null);
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = textInputPassword.getText().toString().trim();
        if (passwordInput.isEmpty()) {
            textInputPassword.setError("Field can't be empty");
            return false;
        } else {
            textInputPassword.setError(null);
            return true;
        }
    }

    public void confirmInput(View v) {
        if (!validateUsername() | !validatePassword()) {
            return;
        }
        String userName = textInputUsername.getText().toString().trim();
        String passwordEntered = textInputPassword.getText().toString().trim();
        String encryptedPassword = Utils.stringToMd5(passwordEntered);

        loginAuthenticationModel.setUsername(userName);
        loginAuthenticationModel.setPassword(encryptedPassword); //passwordEntered
        if (Utils.getConnectivityStatus(context)) {
            new LoginTask().execute();
        }
    }

    ProgressDialog loginDialog;

    class LoginTask extends AsyncTask<String, String, Object> {
        User data = null;

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                loginDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getLoginUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(loginAuthenticationModel, LoginAuthenticationModel.class);
                LoginResponse response = (LoginResponse) HttpRequest
                        .postData(url, jsonStr, LoginResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        dataSource.sharedPreferences.delete(Constants.WAREHOUSE_CODE);
                        String token = response.getToken();
                        if (Utils.isValidString(token)) {
                            Utils.logD("TOKEN : " + token);
                            dataSource.sharedPreferences.set(Constants.TOKEN, token);
                        }
                        data = (User) response.getData();
                        if (data != null) {
                            dataSource.sharedPreferences.set(Constants.USERNAME_PREF, data.getUsername());
                            dataSource.sharedPreferences.set(Constants.USER_ID_PREF, data.getUserId() + "");
                            dataSource.sharedPreferences.set(Constants.PASSWORD_PREF, data.getPassword());
                            dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.FALSE);
                            roleId = data.getRole();
                            dataSource.sharedPreferences.set(Constants.ROLE_ID, String.valueOf(data.getRole()));
                            dataSource.sharedPreferences.set(Constants.WAREHOUSE_NAME, data.getWarehouseName());
                            dataSource.sharedPreferences.set(Constants.WAREHOUSE, data.getWarehouse());
                            dataSource.sharedPreferences.set(Constants.PERMISSIONS, gson.toJson(data.getPermissionslist()));
                            dataSource.sharedPreferences.set(Constants.WAREHOUSE_ID, data.getWarehouseId() != null ? String.valueOf(data.getWarehouseId()) : "0");
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && loginDialog != null && loginDialog.isShowing()) {
                loginDialog.dismiss();
            }
            if (showErrorDialog()) {
                    if (data != null) {
                        List<WarehouseMasterModel> warehousesList = data.getWarehouseMasterModels();
                        if (Utils.isValidArrayList((ArrayList<?>) warehousesList)) {
                            if(warehousesList.size() == 1) {
                                dataSource.sharedPreferences.set(Constants.WAREHOUSE_CODE, warehousesList.get(0).getWhCode());
                                openHomeScreen();
                            } else {
                                try {
                                    Gson gson = new Gson();
                                    Intent intent = new Intent(context, WarehouseActivity.class);
                                    dataSource.sharedPreferences.set(Constants.WAREHOUSES_JSON, gson.toJson(data.getWarehouseMasterModels()));
                                    startActivity(intent);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Utils.logD(e.toString());
                                }
                            }
                        } else {
                            Toast.makeText(context, "No Warehouses Assigned", Toast.LENGTH_SHORT).show();
                        }
                    } else
                        Toast.makeText(context, "No Warehouses Assigned", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finishAffinity();
    }

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            errDlg = new CustomAlertDialog(LoginActivity.this, Globals.lastErrMsg,
                    false, false);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    private void openHomeScreen() {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.putExtra("refreshMasters", true);
        startActivity(intent);
    }
}
