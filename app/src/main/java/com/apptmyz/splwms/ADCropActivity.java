package com.apptmyz.splwms;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.apptmyz.splwms.custom.CropCommand;
import com.apptmyz.splwms.custom.CropImageView;
import com.apptmyz.splwms.custom.EditorSaveConstants;
import com.apptmyz.splwms.custom.ImageProcessor;
import com.apptmyz.splwms.custom.ImageProcessorListener;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.Utils;

/**
 * 
 * Developed by Ashish Das
 * 
 * Email: adas@revamobile.com ,adas@revasolutions.com
 * 
 * Date: October 27, 2012
 * 
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * 
 * All rights reserved
 * 
 * 
 */

public class ADCropActivity extends Activity implements ImageProcessorListener {
	private ImageProcessor imageProcessor;

	private CropImageView imageView;
	private ImageButton okButton;
	private ImageButton cancelButton;

	private ProgressBar progressBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.crop_editor);
		Display display = getWindowManager().getDefaultDisplay();
		Globals.screenWidth = display.getWidth();
		Globals.screenHeight = display.getHeight();
		initializeComponents();
	}

	private void initializeComponents() {
		imageProcessor = ImageProcessor.getInstance();
		imageView = (CropImageView) findViewById(R.id.image_view);
		okButton = (ImageButton) findViewById(R.id.ok_button);
		okButton.setOnClickListener(okButtonListener);
		cancelButton = (ImageButton) findViewById(R.id.cancel_button);
		cancelButton.setOnClickListener(cancelButtonListener);
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		initializeValues();
	}

	private void initializeValues() {
		int roiX = 2000;//690;//70;
		int roiY = 50;//1170;///100;
		Utils.logD(Globals.screenWidth + "*" + Globals.screenHeight);

		int roiWidth = Globals.screenWidth- 140; //690;
		int roiHeight = Globals.screenHeight- 170; //1170;
		final Object data = getLastNonConfigurationInstance();
		if (data == null) {
			imageView.setImageBitmap(imageProcessor.getBitmap());
			imageView.setRegionOfInterest(new Rect(30, 90, Globals.screenWidth-120, Globals.screenHeight/2));
			imageProcessor.setProcessListener(this);
		} else {
			restoreSavedValues(data);
		}
	}

	private void restoreSavedValues(Object data) {
		Bundle savedValues = (Bundle) data;
		int bitmapToRead = savedValues.getInt("BITMAP");
		boolean isRunning = savedValues.getBoolean("IS_RUNNING");
		Rect roi = Rect.unflattenFromString(savedValues
				.getString("FLATTEN_ROI"));

		if (bitmapToRead == EditorSaveConstants.RESTORE_PREVIEW_BITMAP) {
			imageView.setImageBitmap(imageProcessor.getLastResultBitmap());
		} else {
			imageView.setImageBitmap(imageProcessor.getBitmap());
		}
		imageView.setRegionOfInterest(roi);

		if (isRunning) {
			onProcessStart();
			imageProcessor.setProcessListener(this);
		}

	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		Bundle saveObject = new Bundle();
		if (imageProcessor.getLastResultBitmap() == null) {
			saveObject.putInt("BITMAP",
					EditorSaveConstants.RESTORE_SAVED_BITMAP);
		} else {
			saveObject.putInt("BITMAP",
					EditorSaveConstants.RESTORE_PREVIEW_BITMAP);
		}

		saveObject.putString("FLATTEN_ROI", imageView.getRegionOfInterest()
				.flattenToString());
		saveObject.putBoolean("IS_RUNNING", isProgressBarVisible());
		return saveObject;
	}

	private boolean isProgressBarVisible() {
		return progressBar.getVisibility() == View.VISIBLE ? true : false;
	}

	private OnClickListener okButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			setResult(RESULT_OK);
			runImageProcessor();

		}
	};

	private void runImageProcessor() {
		CropCommand command = new CropCommand(imageView.getRegionOfInterest());
		imageProcessor.setProcessListener(this);
		imageProcessor.runCommand(command);
	}

	private OnClickListener cancelButtonListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			setResult(RESULT_CANCELED);
			finish();
		}
	};

	@Override
	public void onProcessStart() {
		// turn off buttons and show "processing" animation
		Log.i("Crop", "Start Processing");
		okButton.setEnabled(false);
		cancelButton.setEnabled(false);
		progressBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onProcessEnd(Bitmap result) {
		Log.i("Crop", "Start Processing");
		okButton.setEnabled(true);
		cancelButton.setEnabled(true);
		progressBar.setVisibility(View.INVISIBLE);
		imageView.setImageBitmap(result);
		imageView.invalidate();
		imageProcessor.save();
		finish();
	}

}
