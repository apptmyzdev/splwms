package com.apptmyz.splwms.custom;

import android.graphics.Bitmap;

/**
 * 
 * Developed by Ashish Das
 * 
 * Email: adas@revamobile.com ,adas@revasolutions.com
 * 
 * Date: October 27, 2012
 * 
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * 
 * All rights reserved
 * 
 * 
 */
public interface ImageProcessorListener {
	void onProcessStart();

	void onProcessEnd(Bitmap result);
}
