package com.apptmyz.splwms.custom;

import android.graphics.Bitmap;
import android.graphics.Rect;

import com.apptmyz.splwms.util.Utils;


/**
 * Developed by Ashish Das
 * <p>
 * Email: adas@revamobile.com ,adas@revasolutions.com
 * <p>
 * Date: October 27, 2012
 * <p>
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * <p>
 * All rights reserved
 */
public class CropCommand implements ImageProcessingCommand {

    private static final String ID = "AdCropCommand";

    private int x = 0;
    private int y = 0;
    private int width = 1;
    private int height = 1;

    public CropCommand() {
    }

    public CropCommand(int x, int y, int width, int height) {
        if (x < 0) {
            this.x = 0;
            this.width = width + x < 0 ? 1 : width + x;
        } else {
            this.x = x;
            this.width = width;
        }

        if (y < 0) {
            this.y = 0;
            this.height = height + y < 0 ? 1 : height + y;
        } else {
            this.y = y;
            this.height = height;
        }
    }

    public CropCommand(Rect r) {
        this(r.left, r.top, r.width(), r.height());
    }

    @Override
    public Bitmap process(Bitmap bitmap) {
        Utils.logI("Image Processing Command" + ID + " : " + x + " " + y + " "
                + width + " " + height);

        if (x + width > bitmap.getWidth()) {
//            if (x > bitmap.getWidth())
//                width = x - bitmap.getWidth();
//            else
                width = bitmap.getWidth() - x;
        }

        if (y + height > bitmap.getHeight()) {
//            if (y > bitmap.getHeight())
//                height = y - bitmap.getHeight();
//            else
                height = bitmap.getHeight() - y;
        }

        if (width <= 0) {
            width = 1;
        }
        if (height <= 0) {
            height = 1;
        }

        return Bitmap.createBitmap(bitmap, x, y, width, height);
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String getId() {
        return ID;
    }

}
