package com.apptmyz.splwms.custom;
/**
 * 
 * Developed by Ashish Das
 * 
 * Email: adas@revamobile.com ,adas@revasolutions.com
 * 
 * Date: October 27, 2012
 * 
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * 
 * All rights reserved
 * 
 * 
 */
public class EditorSaveConstants {
	public static final int RESTORE_SAVED_BITMAP = 0;
	public static final int RESTORE_PREVIEW_BITMAP = 1;
}
