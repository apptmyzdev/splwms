package com.apptmyz.splwms.custom;

public interface CounterListener {
    void onIncrementClick(String value);
    void onDecrementClick(String value);
    void onEditValue(String value);
}
