package com.apptmyz.splwms.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.apptmyz.splwms.R;
import com.apptmyz.splwms.util.Utils;

public class CounterView extends LinearLayout implements View.OnClickListener {

    private EditText valueEt;
    private Button incrementBtn, decrementBtn;
    private int mValue = 0, mMinLimit = 0, mMaxLimit = (int) Double.POSITIVE_INFINITY;
    private CounterListener listener;

    public CounterView(Context context) {
        super(context);
        init(context, null);
    }

    public CounterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CounterView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(final Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.counter_view, this);
        //initialize attributes here
        TypedArray ta = getContext().getResources().obtainAttributes(attrs, R.styleable.CounterView);
        Log.d("test", "init: " + ta.toString());
        mMinLimit = ta.getInteger(R.styleable.CounterView_minlimit, 1);
        if (mMinLimit != 1) {
            mValue = mMinLimit;
        }
        mMaxLimit = ta.getInteger(R.styleable.CounterView_maxlimit, (int) Double.POSITIVE_INFINITY);
        invalidate();
        ta.recycle();

        valueEt = (EditText) findViewById(R.id.et_value);
        valueEt.setText(mValue + "");
        incrementBtn = (Button) findViewById(R.id.btn_increment);
        decrementBtn = (Button) findViewById(R.id.btn_decrement);
        incrementBtn.setOnClickListener(this);
        decrementBtn.setOnClickListener(this);

        valueEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (listener != null)
                    listener.onEditValue(editable.toString());
            }
        });
    }

//        @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_increment:
                String countStr = valueEt.getText().toString().trim();
                if(Utils.isValidString(countStr)) {
                    mValue = Integer.parseInt(countStr);
                } else {
                    mValue = 0;
                }
                if (mValue < mMaxLimit) {
                    mValue++;
                    valueEt.setText(mValue + "");
                    invalidate();
                }
                if (mValue == mMaxLimit) {
                    incrementBtn.setEnabled(false);
                    incrementBtn.setClickable(false);
                    invalidate();
                }
                if (mValue > mMinLimit) {
                    decrementBtn.setEnabled(true);
                    decrementBtn.setClickable(true);
                    invalidate();
                }
                //pass value to the activity
                if (this.listener != null)
                    this.listener.onIncrementClick(this.valueEt.getText().toString());
                break;
            case R.id.btn_decrement:
                if (mValue > mMinLimit) {
                    mValue--;
                    valueEt.setText(mValue + "");
                    invalidate();
                }
                if (mValue == mMinLimit) {
                    decrementBtn.setEnabled(false);
                    decrementBtn.setClickable(false);
                    invalidate();
                }
                if (mValue < mMaxLimit) {
                    incrementBtn.setEnabled(true);
                    incrementBtn.setClickable(true);
                    invalidate();
                }
                //pass value to the activity
                if (this.listener != null)
                    this.listener.onDecrementClick(this.valueEt.getText().toString());
                break;
            default:
                break;
        }
    }

    public void setCounterListener(CounterListener listener) {
        this.listener = listener;
    }

    public void setmMaxLimit(int maxLimit) {
        mMaxLimit = maxLimit;
    }

    public void setmMinLimit(int minLimit) {
        mMinLimit = minLimit;
    }

    public int getValue() {
//        return mValue;
        String value = valueEt.getText().toString().trim();
        if (Utils.isValidString(value))
            return Integer.parseInt(value);
        else
            return 0;
    }

    public void setValue(int value) {
        mValue = value;
        valueEt.setText(value + "");
    }
}
