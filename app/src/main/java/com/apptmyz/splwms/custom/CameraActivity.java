package com.apptmyz.splwms.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.apptmyz.splwms.R;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


/**
 * Created by GayatriMudimbi on 05/11/19.
 */

public class CameraActivity extends Activity {
    private static Context context;
    public SensorManager sensorManager;
    Camera.Parameters params;
    boolean hasCameraFlash;
    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile();
            if (pictureFile == null) {
                return;
            }
            FileOutputStream fos;
            try {
                fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Uri u = Uri.fromFile(pictureFile);
                Intent intent = new Intent();
                intent.putExtra(Intent.EXTRA_STREAM, u);
                setResult(RESULT_OK, intent);
                finish();
            } catch (FileNotFoundException e) {
                Utils.logE(e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                Utils.logE(e.toString());
                e.printStackTrace();
            }

        }
    };
    private Camera mCamera;
    private CameraPreview mCameraPreview;
    private boolean flashLightStatus = false;


    SensorEventListener lightSensorEventListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub

        }

        // get sensor update and reading
        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                float currentReading = event.values[0];
                if (currentReading >= 0 && currentReading <= Integer.parseInt(Constants.LUX_SENSOR_THRESHOLD)) {
                    if (hasCameraFlash) {
                        flashLightOn();
                        Utils.logE("lux values" + String.valueOf(currentReading));

                    } else {
                        Toast.makeText(CameraActivity.this, "No flash available on your device",
                                Toast.LENGTH_SHORT).show();
                    }
                } else if (flashLightStatus) {
                    flashLightOff();
                }

//

            }
        }

    };


    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            Utils.logE(e.toString());
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }


    private static File getOutputMediaFile() {
        String fileName = "temp_photo.jpg";
        File path = new File(Environment.getExternalStorageDirectory(),
                context.getPackageName());
        if (!path.exists()) {
            path.mkdir();
        }
        return new File(path, fileName);
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera);

        context = CameraActivity.this;

        mCamera = getCameraInstance();

        mCameraPreview = new CameraPreview(this, mCamera);

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mCameraPreview);
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        final Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        // check sensor available in devise. if available then get reading
        hasCameraFlash = getPackageManager().
                hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        if (lightSensor == null) {
//            Toast.makeText(getApplicationContext(), "No Sensor",
//                    Toast.LENGTH_SHORT).show();
            // Toast.makeText(AndroidLightSensorActivity.this,
            // "No Light Sensor! quit-", Toast.LENGTH_LONG).show();
        }
        if (mCamera == null)
            mCamera = getCameraInstance();
        if (mCamera != null) {
            Parameters params = mCamera.getParameters();

            List<String> focusModes = params.getSupportedFocusModes();
            if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                // Autofocus mode is supported
                params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                mCamera.setParameters(params);
            }
        }


        Button captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                mCamera.takePicture(null, null, mPicture);
                    if (mCamera != null) {
                        if (sensorManager != null && lightSensor != null) {
                            sensorManager.registerListener(lightSensorEventListener,
                                    lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
                        }

//                        if (Build.VERSION.SDK_INT <= 19)
                        mCamera.takePicture(null, null, mPicture);
//                        else
//                            mCamera.autoFocus(new Camera.AutoFocusCallback() { //commented auto focus to fix crash from field
//                                @Override
//                                public void onAutoFocus(boolean arg0, Camera arg1) {
//                                    mCamera.takePicture(null, null, mPicture);
//                                    //mCamera.stopPreview();
//                                    try {
//                                        Thread.sleep(100);
//                                    } catch (InterruptedException e) {
//                                        // TODO Auto-generated catch block
//                                        e.printStackTrace();
//                                        Utils.logE(e.toString());
//
//                                    }
////                                    try {
////                                        if (mCamera != null)
////                                            mCamera.startPreview();
////                                    } catch (Exception e) {
////                                        e.printStackTrace();
////                                        Utils.logE(e.toString());
////
////                                    }
//                                }
//
//                            });

                    }
                } catch (Exception e) {
                    Utils.logE(e.toString());
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();              // release the camera immediately on pause event
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();        // release the camera for other applications
            mCamera = null;
            if (sensorManager != null)
                sensorManager.unregisterListener(lightSensorEventListener);

        }
    }


    private void flashLightOn() {

        try {
            if (mCamera != null) {
                params = mCamera.getParameters();
                params.setFlashMode(Parameters.FLASH_MODE_ON);
                mCamera.setParameters(params);
                Utils.logE(mCamera.getParameters().getFocusMode());
                flashLightStatus = true;
            }
        } catch (Exception e) {
            Utils.logE(e.toString());

        }


    }


    private void flashLightOff() {

        try {
            if (mCamera != null) {
                params = mCamera.getParameters();
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(params);

                flashLightStatus = false;

            }
        } catch (Exception e) {
            Utils.logE(e.toString());
        }
    }

}

