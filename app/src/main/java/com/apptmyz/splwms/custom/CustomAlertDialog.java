package com.apptmyz.splwms.custom;

/**
 * Developed by Swathi Lolla
 * <p>
 * Email: slolla@revamobile.com
 * <p>
 * Date: March 29, 2016
 * <p>
 * All code (c) 2011 Reva Tech Solutions (India) Private Limited (Reva Mobile)
 * <p>
 * All rights reserved
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.apptmyz.splwms.LoginActivity;
import com.apptmyz.splwms.R;

public class CustomAlertDialog extends Dialog {
    private TextView messageTv;
    private Button okBtn;
    private String msg;
    private boolean close;
    private boolean logout;
    Activity activity;

    public CustomAlertDialog(Activity a, String message, boolean isClose, boolean isLogout) {
        super(a, R.style.CustomAlertDialog);
        activity = a;
        msg = message;
        close = isClose;
        logout = isLogout;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_alert_dialog);

        messageTv = (TextView) findViewById(R.id.tv_error_message);
        messageTv.setText(msg);

        okBtn = (Button) findViewById(R.id.btn_ok);
        okBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();
                if (!logout) {
                    if (close)
                        activity.finish();
                } else {
                    okBtn.setEnabled(false);
                    okBtn.setClickable(false);
                    dismiss();
                    activity.finish();
                    activity.startActivity(new Intent(activity.getBaseContext(), LoginActivity.class));
                }
            }
        });

    }

}
