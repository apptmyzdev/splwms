package com.apptmyz.splwms;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.custom.ImageProcessor;
import com.apptmyz.splwms.data.GeneralResponse;
import com.apptmyz.splwms.data.InventoryData;
import com.apptmyz.splwms.data.InventoryResponse;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.data.SubmitExceptionModel;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.apptmyz.splwms.util.CameraUtils;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

public class NormalInventoryActivity extends BaseActivity {
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 3, MY_PERMISSIONS_CAMERA = 4;
    Context context;
    TextView titleTv;
    TextInputEditText binNumberEt;
    ImageView scanBinIv, scanBinExceptionIv, scanProductExceptionIv;
    ExpandableListView expandableListView;
    private Timer timerScan;
    private Handler handlerScan;
    private int repetition, exceptionPartId;
    private int timeScan, exceptionId = 1;
    private TimerTask timerScanTask;
    private InventoryData exceptionBin;
    private ImageLoader imageLoader = null;
    private String binNumber, exceptionBinNumber;
    Gson gson = new Gson();
    ExpandableListAdapter adapter;
    ArrayList<InventoryData> inventoryData = new ArrayList<>();
    private ArrayList<InventoryData> groupData = new ArrayList<>();
    private ArrayList<ArrayList<WmsPartInvoiceModel>> childData = new ArrayList<>();
    private boolean isPerpetual;
    private LinearLayout exceptionLayout;
    private TextInputEditText binExceptionEt, productExceptionEt;
    private Spinner exceptionTypeSp;
    private String imageStoragePath, resizedBase64 = "", capBase64Filename = "", capBase64 = "", warehouseCode;
    private static final int CAPTURE_IMG = 1;
    private static final int CROP_IMG = 2;
    private Button createExceptionBtn, saveExceptionBtn, cancelExceptionBtn, captureExceptionImgBtn;
    private DataSource dataSource;

    private ArrayList<Bitmap> imgBitmaps;
    private ArrayList<String> imgFileNames;
    private Button captureNewBtn, cancelImgViewBtn;
    private RecyclerView imagesRv;
    private ConstraintLayout imagesView;
    private ImagesAdapter imagesAdapter;
    private int imagePosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_normal_inventory, frameLayout);

        context = NormalInventoryActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        titleTv = (TextView) findViewById(R.id.tv_top_title);

        imagesRv = (RecyclerView) findViewById(R.id.rv_exception_images);
        imagesRv.setNestedScrollingEnabled(false);
        imagesRv.setLayoutManager(new LinearLayoutManager(context));
        imagesView = (ConstraintLayout) findViewById(R.id.layout_image_capture);
        captureNewBtn = (Button) findViewById(R.id.btn_captue_new);
        cancelImgViewBtn = (Button) findViewById(R.id.btn_cancel_imgs);
        cancelImgViewBtn.setOnClickListener(listener);
        captureNewBtn.setOnClickListener(listener);

        if (Utils.isValidArrayList(imgBitmaps)) {
            imagesAdapter = new ImagesAdapter(imgBitmaps);
            imagesRv.setAdapter(imagesAdapter);
        }

        createExceptionBtn = (Button) findViewById(R.id.btn_create_exception);
        createExceptionBtn.setOnClickListener(listener);
        exceptionLayout = (LinearLayout) findViewById(R.id.exception_layout);
        saveExceptionBtn = (Button) findViewById(R.id.btn_save_exception);
        cancelExceptionBtn = (Button) findViewById(R.id.btn_cancel_exception);
        saveExceptionBtn.setOnClickListener(listener);
        cancelExceptionBtn.setOnClickListener(listener);
        captureExceptionImgBtn = (Button) findViewById(R.id.btn_capture_image);
        captureExceptionImgBtn.setOnClickListener(listener);
        imageLoader = Utils.getImageLoader(context, "IMAGES");

        binExceptionEt = (TextInputEditText) findViewById(R.id.input_bin_number_exception);
        binExceptionEt.addTextChangedListener(exceptionBinWatcher);

        productExceptionEt = (TextInputEditText) findViewById(R.id.input_product_number_exception);
        productExceptionEt.addTextChangedListener(exceptionProductWatcher);

        exceptionTypeSp = (Spinner) findViewById(R.id.sp_exception_type);

        String[] exceptionTypesArr = getResources().getStringArray(R.array.exception_types);

        List<SkipBinReasonModel> exceptionsList = dataSource.exceptionTypes.getExceptionTypes(warehouseCode);
        ArrayAdapter<SkipBinReasonModel> exceptionTypeAdapter = new ArrayAdapter<SkipBinReasonModel>(NormalInventoryActivity.this,
                android.R.layout.simple_dropdown_item_1line, exceptionsList);
        exceptionTypeSp.setAdapter(exceptionTypeAdapter);

        exceptionTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                exceptionId = exceptionsList.get(i).getReasonId();
                if (i == 3) {
                    captureExceptionImgBtn.setVisibility(View.GONE);
                } else {
                    captureExceptionImgBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (getIntent().getExtras() != null)
            isPerpetual = getIntent().getBooleanExtra("perpetual", false);
        if (isPerpetual)
            titleTv.setText("Perpetual Inventory");
        else
            titleTv.setText("Normal Inventory");

        binNumberEt = (TextInputEditText) findViewById(R.id.input_bin_number);
        binNumberEt.addTextChangedListener(watcher);

        scanBinIv = (ImageView) findViewById(R.id.iv_scan_bin);
        scanBinIv.setOnClickListener(listener);

        scanBinExceptionIv = (ImageView) findViewById(R.id.iv_scan_bin_exception);
        scanBinExceptionIv.setOnClickListener(listener);

        scanProductExceptionIv = (ImageView) findViewById(R.id.iv_scan_product_exception);
        scanProductExceptionIv.setOnClickListener(listener);

        expandableListView = (ExpandableListView) findViewById(R.id.explv_inventory);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            expandableListView.setNestedScrollingEnabled(false);
        }
        setData();

        if (Utils.getConnectivityStatus(context))
            new InventoryDataTask().execute();
    }

    private void setData() {
        adapter = new ExpandableListAdapter(context);
        expandableListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    ProgressDialog inventoryDialog;

    class InventoryDataTask extends AsyncTask<String, String, Object> {

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                inventoryDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {

                Globals.lastErrMsg = "";

                String warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
                String url = WmsUtils.getInventoryDataUrl(warehouseCode);
                inventoryData = new ArrayList<>();
                groupData = new ArrayList<>();
                Utils.logD("Log 1");
                InventoryResponse response = (InventoryResponse) HttpRequest
                        .getInputStreamFromUrl(url, InventoryResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {
                        ArrayList<InventoryData> data = (ArrayList<InventoryData>) response.getData();
                        if (Utils.isValidArrayList((ArrayList<?>) data)) {
                            inventoryData = data;
                            groupData = inventoryData;
                            childData = new ArrayList<>();
                            for (InventoryData inventory : inventoryData) {
                                childData.add((ArrayList<WmsPartInvoiceModel>) inventory.getPartList());
                            }
                        }
                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }

            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && inventoryDialog != null && inventoryDialog.isShowing()) {
                inventoryDialog.dismiss();
            }
            adapter.notifyDataSetChanged();
            if (showErrorDialog()) {
                setData();
            }
            super.onPostExecute(result);
        }

    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(NormalInventoryActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }


    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_captue_new:
                    if (Utils.isValidArrayList(imgBitmaps) && imgBitmaps.size() == 3) {
                        Utils.showSimpleAlert(context, "Alert", "You have already captured 3 images. Can't capture more.");
                    } else {
                        imagePosition = -1;
                        captureImage(CAPTURE_IMG, capBase64Filename + "_" + imagePosition);
                    }
                    break;
                case R.id.btn_cancel_imgs:
                    imagesView.setVisibility(View.GONE);
                    break;
                case R.id.iv_scan_bin:
                    binNumberEt.setText("");
                    binNumberEt.requestFocus();
                    break;
                case R.id.iv_scan_bin_exception:
                    binExceptionEt.setText("");
                    binExceptionEt.requestFocus();
                    break;
                case R.id.iv_scan_product_exception:
                    productExceptionEt.setText("");
                    productExceptionEt.requestFocus();
                    break;
                case R.id.btn_cancel_exception:
                    binExceptionEt.setText("");
                    binExceptionEt.clearFocus();

                    productExceptionEt.setText("");
                    productExceptionEt.clearFocus();

                    exceptionTypeSp.setSelection(0);
                    captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_orange);

                    resizedBase64 = "";
                    exceptionLayout.setVisibility(View.GONE);
                    break;
                case R.id.btn_save_exception:
                    String msg = validateExceptionDetails();
                    if (!Utils.isValidString(msg)) {
                        if (Utils.getConnectivityStatus(context)) {
                            int binId = exceptionBin != null ? exceptionBin.getBinId() : 0;
                            List<String> base64s = getBase64s(imgBitmaps);
                            SubmitExceptionModel model = new SubmitExceptionModel(productNumber, exceptionPartId, binNumber, binId,
                                    Utils.getScanTime(), exceptionId, base64s);
                            new SubmitExceptionTask(model).execute();
                        }
                    } else {
                        Utils.showToast(context, msg);
                    }
                    break;
                case R.id.btn_create_exception:
                    exceptionLayout.setVisibility(View.VISIBLE);
                    break;
                case R.id.btn_capture_image:
                    capBase64Filename = "IMAGE_Exception";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                            return;
                        }
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_CAMERA);

                            return;
                        }
                    }
                    if (!Utils.isValidArrayList(imgBitmaps)) {
                        imagePosition = -1;
                        captureImage(CAPTURE_IMG, capBase64Filename + "_1"); //opens Camera Activity
                    } else {
                        imagesView.setVisibility(View.VISIBLE);
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private List<String> getBase64s(List<Bitmap> bitmaps) {
        List<String> list = new ArrayList<>();
        for(Bitmap bitmap: bitmaps) {
            String base64 = getBase64(bitmap);
            list.add(base64);
        }
        return list;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_CAMERA: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! do the
                    // calendar task you need to do.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA},
                                    MY_PERMISSIONS_CAMERA);

                            return;
                        }
                    }
                    captureImage(CAPTURE_IMG, capBase64Filename + "_1");
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                MY_PERMISSIONS_CAMERA);

                        return;
                    }
                }
                break;
            default:
                break;
        }
    }

    public void addImage(Bitmap bitmap, String imageFileName, boolean isCrop, int cropRequestCode) {
        if (bitmap != null) {
            ImageLoader imageLoader = new ImageLoader(context, "POD");
            Uri uri = imageLoader.fileCache.saveBitmapFile(bitmap,
                    imageFileName);
            if (uri != null) {
                if (!isCrop) {
                } else {
                    doCrop(bitmap, imageFileName, cropRequestCode);
                }
            }
        }
    }

    private void doCrop(Bitmap bitmap, String imageFileName, int cropRequestCode) {
        ImageProcessor.getInstance().setData(context, bitmap, imageFileName);
        Intent intent = new Intent(context, ADCropActivity.class);
        ((Activity) context).startActivityForResult(intent, cropRequestCode);
    }

    private void capturePicture(String base64, final String fileName, final int requestCode, final int cropRequestCode) {
        if (!Utils.isValidString(base64)) {
            captureImage(requestCode, fileName); //opens Camera Activity
        } else {
            final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle("Image Preview");
            alertDialog.setCancelable(true);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_pod, null);
            ImageView docketIv;
            Button cropBtn, reCaptureBtn;
            docketIv = (ImageView) view.findViewById(R.id.iv_pod);
            cropBtn = (Button) view.findViewById(R.id.btn_crop);
            reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

            alertDialog.setView(view);
            byte[] decodedString = Base64.decode(base64, Base64.NO_WRAP);
            final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
            docketIv.setImageBitmap(decodedByte);
            final AlertDialog alert = alertDialog.show();
            cropBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addImage(decodedByte, fileName, true, cropRequestCode);
                    alert.dismiss();
                }
            });
            reCaptureBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                    captureImage(requestCode, fileName);
                }
            });

        }
    }

    private void captureImage(int requestCode, String imageFileName) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE, imageFileName);
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = null;

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            fileUri = Uri.fromFile(file);
        } else {
            fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(intent, requestCode);
    }

    private String productNumber;

    private String validateExceptionDetails() {
        String message = "";
        productNumber = productExceptionEt.getText().toString().trim();
        exceptionBinNumber = binExceptionEt.getText().toString().trim();

        if (Utils.isValidString(exceptionBinNumber)) {
            if (Utils.isValidString(productNumber)) {

            } else {
                message = "Please Scan the Part";
            }
        } else {
            message = "Please Scan the BIN";
        }

        return message;
    }

    ProgressDialog submitExceptionDialog;

    class SubmitExceptionTask extends AsyncTask<String, String, Object> {
        SubmitExceptionModel input;
        GeneralResponse response;

        SubmitExceptionTask(SubmitExceptionModel input) {
            this.input = input;
        }

        @Override
        protected void onPreExecute() {
            Globals.lastErrMsg = "";
            if (!isFinishing()) {
                submitExceptionDialog = Utils.getProgressDialog(context);
            }
        }

        @Override
        protected Object doInBackground(String... params) {

            try {
                Globals.lastErrMsg = "";

                String url = WmsUtils.getSubmitExceptionUrl();
                Utils.logD("Log 1");
                Gson gson = new Gson();
                String jsonStr = gson.toJson(input, SubmitExceptionModel.class);
                response = (GeneralResponse) HttpRequest
                        .postData(url, jsonStr, GeneralResponse.class,
                                context);

                if (response != null) {
                    Utils.logD("Log 4");
                    Utils.logD(response.toString());
                    if (response.isStatus()) {

                    } else {
                        Globals.lastErrMsg = response.getMessage();
                    }
                }
            } catch (Exception e) {
                if (!Utils.isValidString(Globals.lastErrMsg))
                    Globals.lastErrMsg = e.toString();
                if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                    Globals.lastErrMsg = getString(R.string.server_not_reachable);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            if (!isFinishing() && submitExceptionDialog != null && submitExceptionDialog.isShowing()) {
                submitExceptionDialog.dismiss();
            }

            if (showErrorDialog()) {
                if (response != null)
                    Utils.showToast(context, response.getMessage());
                exceptionLayout.setVisibility(View.GONE);
            }
            super.onPostExecute(result);
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(context, HomeActivity.class));
    }

    private TextWatcher exceptionProductWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            if (timerScan == null) {
                if (exceptionBin == null) {
                    Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Please Scan the BIN first");
                    return;
                }
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = productExceptionEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (scannedValue.length() >= 2) {
                                        productNumber = scannedValue;

                                        boolean isPartExisting = false;
                                        exceptionPartId = 0;
                                        for (WmsPartInvoiceModel model : exceptionBin.getPartList()) {
                                            if (model.getPartNo() != null && model.getPartNo().toLowerCase().contains(scannedValue.toLowerCase())) {
                                                isPartExisting = true;
                                                exceptionPartId = model.getPartId();
                                                break;
                                            }
                                        }

                                        if (!isPartExisting) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "Part number doesn't belong to the Entered BIN");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }
                                    }

                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private TextWatcher exceptionBinWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            exceptionBinNumber = "";
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binExceptionEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);

                                    if (scannedValue.length() >= 2) {
                                        exceptionBinNumber = scannedValue;
                                        int matchCount = dataSource.bins.matchesData(scannedValue, warehouseCode);
                                        if (matchCount == 0) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }

                                        boolean isBinExisting = false;
                                        exceptionBin = null;
                                        for (InventoryData data : groupData) {
                                            if (data.getBinNumber() != null && data.getBinNumber().toLowerCase().contains(scannedValue.toLowerCase())) {
                                                isBinExisting = true;
                                                exceptionBin = data;
                                                break;
                                            }
                                        }

                                        if (!isBinExisting) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't belong to the Inventory");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }
                                    }

                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    private TextWatcher watcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            final String s1 = s.toString();
            binNumber = "";
            if (timerScan == null) {
                timerScan = new Timer();
                handlerScan = new Handler();
                timeScan = Constants.scanStartTime;
                repetition = (Constants.scanTime / Constants.scanStartTime);
                if (repetition < 1)
                    repetition = 1;
                Utils.logD("repetition" + repetition);
                timerScanTask = new TimerTask() {
                    @Override
                    public void run() {
                        handlerScan.post(new Runnable() {
                            @Override
                            public void run() {
                                if (timeScan == Constants.scanTime
                                        || timeScan > (Constants.scanTime * repetition)) {
                                    Utils.logD("time in loop" + timeScan);
                                    Utils.logD("Watcher Started");
                                    String scannedValue = binNumberEt
                                            .getText().toString();
                                    Utils.logD("ScannedValue: " + scannedValue);


                                    if (Utils.isValidString(scannedValue)) {
                                        ArrayList<InventoryData> tempInventoryData = new ArrayList<>();
                                        for (InventoryData inventory : inventoryData) {
                                            if (inventory.getBinNumber() != null && inventory.getBinNumber().contains(scannedValue))
                                                tempInventoryData.add(inventory);
                                        }
                                        groupData = tempInventoryData;
                                        for (InventoryData data : groupData) {
                                            childData.add((ArrayList<WmsPartInvoiceModel>) data.getPartList());
                                        }
                                        setData();
                                    } else {
                                        groupData = inventoryData;
                                        for (InventoryData data : groupData) {
                                            childData.add((ArrayList<WmsPartInvoiceModel>) data.getPartList());
                                        }
                                        setData();
                                    }

                                    if (scannedValue.length() >= 2) {
                                        binNumber = scannedValue;
                                        int matchCount = dataSource.bins.matchesData(scannedValue, warehouseCode);
                                        if (matchCount == 0) {
                                            Utils.showSimpleAlert(context, getResources().getString(R.string.alert), "BIN number doesn't exist");
                                            timeScan = 0;
                                            if (timerScan != null)
                                                timerScan.cancel();
                                            timerScan = null;
                                            return;
                                        }
                                        for (InventoryData inventory : inventoryData) {
                                            if (inventory.getBinNumber() != null && inventory.getBinNumber().equals(binNumber)) {
                                                Intent intent = new Intent(context, PerpetualInventoryDetailActivity.class);
                                                intent.putExtra("perpetual", true);
                                                String inventoryStr = gson.toJson(inventory);
                                                intent.putExtra("inventory", inventoryStr);
                                                startActivity(intent);
                                                break;
                                            }
                                        }
                                    }

                                    timeScan = 0;
                                    if (timerScan != null)
                                        timerScan.cancel();
                                    timerScan = null;
                                } else {
                                    timeScan += timeScan;
                                    Utils.logD("time " + timeScan);
                                }

                            }
                        });

                    }

                };
                timerScan.schedule(timerScanTask, Constants.scanStartTime, timeScan);
            }

        }
    };

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private LayoutInflater inflater;

        public ExpandableListAdapter(Context context) {
            super();
            this.inflater = LayoutInflater.from(context);
        }

        @Override
        public WmsPartInvoiceModel getChild(int groupPosition, int childPosition) {
            return childData.get(groupPosition).get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return (groupPosition * 1024 + childPosition);
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.child_item_inventory, parent, false);

            v.setTag(R.string.group_pos, groupPosition);
            v.setTag(R.string.child_pos, childPosition);

            final WmsPartInvoiceModel part = (WmsPartInvoiceModel) getChild(groupPosition, childPosition);
            TextView partNumberTv = (TextView) v.findViewById(R.id.tv_part_number);
            TextView qtyTv = (TextView) v.findViewById(R.id.tv_qty);
            TextView scannedBoxesTv = (TextView) v.findViewById(R.id.tv_scanned_boxes);
            TextView remainingBoxesTv = (TextView) v.findViewById(R.id.tv_remaining_boxes);
            partNumberTv.setText(part.getPartNo());
            qtyTv.setText(String.valueOf(part.getQty()));
            scannedBoxesTv.setText(String.valueOf(part.getTotalBoxes()));
            remainingBoxesTv.setText(String.valueOf(0));

            return v;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childData.get(groupPosition).size();
        }

        @Override
        public InventoryData getGroup(int groupPosition) {
            return groupData.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return groupData.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return (groupPosition * 1024);
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            View v = null;
            if (convertView != null)
                v = convertView;
            else
                v = inflater.inflate(R.layout.group_item_inventory, parent, false);

            v.setTag(groupPosition);
            v.setOnClickListener(groupClickListener);

            final InventoryData pdcData = (InventoryData) getGroup(groupPosition);

            ImageView groupIndicator = (ImageView) v
                    .findViewById(R.id.iv_group_indicator);
            if (expandableListView.isGroupExpanded(groupPosition))
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_maximized);
            else
                groupIndicator
                        .setImageResource(R.drawable.expander_ic_minimized);


            final String pdc = pdcData.getBinNumber();
            v.setOnClickListener(groupClickListener);
            TextView conTv = (TextView) v.findViewById(R.id.tv_bin_number);
            TextView noOfPartsTv = (TextView) v.findViewById(R.id.tv_num_of_bins);
            conTv.setText(pdc);
            if (pdcData.getPartList() != null)
                noOfPartsTv.setText(String.valueOf(pdcData.getPartList().size()));
            return v;
        }

        private View.OnClickListener groupClickListener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int groupPosition = (Integer) v.getTag();
                if (expandableListView.isGroupExpanded(groupPosition))
                    expandableListView.collapseGroup(groupPosition);
                else
                    expandableListView.expandGroup(groupPosition);

            }
        };

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public void onGroupCollapsed(int groupPosition) {
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = null;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case CAPTURE_IMG:
                    try {
                        bitmap = getFullBitmap(imageStoragePath);
                        if (bitmap != null) {
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] imageBytes = baos.toByteArray();
                            capBase64 = capBase64Filename;
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);

                            if (Utils.isValidString(capBase64))
                                captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            Date date = new Date();
                            long d = date.getTime();
                            Bitmap resizedBitmap = null;
                            if (bitmap != null) {
                                int w = bitmap.getWidth();
                                int h = bitmap.getHeight();
                                Matrix mat = new Matrix();
                                if (w > h)
                                    mat.postRotate(90);

                                resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mat, true);
                                ByteArrayOutputStream baosResized = new ByteArrayOutputStream();
                                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baosResized);
                                byte[] resizedImageBytes = baosResized.toByteArray();
                                resizedBase64 = Base64.encodeToString(resizedImageBytes, Base64.DEFAULT);
                            }
                        }
                        Date date = new Date();
                        long d = date.getTime();
                        capBase64Filename = Utils.getImageTime(d);

                        if (!Utils.isValidArrayList(imgBitmaps)) {
                            imgBitmaps = new ArrayList<>();
                            imgFileNames = new ArrayList<>();
                        }
                        if (imagePosition == -1) {
                            imgBitmaps.add(bitmap);
                            imgFileNames.add(capBase64Filename);
                        } else {
                            imgBitmaps.set(imagePosition, bitmap);
                            imgFileNames.set(imagePosition, capBase64Filename);
                        }

                        imagesAdapter = new ImagesAdapter(imgBitmaps);
                        imagesRv.setAdapter(imagesAdapter);
                        imagesAdapter.notifyDataSetChanged();

                        if (imagesView.getVisibility() == View.GONE)
                            imagesView.setVisibility(View.VISIBLE);

                        addImage(bitmap, capBase64Filename, false, CROP_IMG);//////
                    } catch (Exception e) {
                        e.printStackTrace();
                        Utils.logE("CAPTURE ERROR : " + e.toString());
                    }
                    break;

                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            resizedBase64 = getBase64(bitmap);
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64))
                                captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        } else {
            switch (requestCode) {
                case CROP_IMG:
                    try {
                        bitmap = ImageProcessor.getInstance().getBitmap();
                        if (bitmap != null) {
                            resizedBase64 = getBase64(bitmap);
                            capBase64 = "IMAGE_Exception";
                            if (imageLoader != null)
                                imageLoader.fileCache.saveBitmapFile(bitmap, capBase64);
                            if (Utils.isValidString(capBase64))
                                captureExceptionImgBtn.setBackgroundResource(R.drawable.btn_bg_green);

                            capBase64Filename = ImageProcessor.getInstance()
                                    .getImageFileName();
                            addImage(bitmap, capBase64Filename, false, CROP_IMG);
                        }
                    } catch (Exception e) {
                        Utils.logE(e.toString());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private String getBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70,
                byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        encoded = encoded.replace("\n", "");
        return encoded;
    }

    private Bitmap getFullBitmap(String path) {

        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            int IMAGE_MAX_SIZE = 0;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                IMAGE_MAX_SIZE = 500000; //0.5MP
            } else {
                IMAGE_MAX_SIZE = 1200000; // 1.2MP
            }
            in = getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Log.d("", "scale = " + scale + ", orig-width: " + o.outWidth + ", orig-height: " + o.outHeight);

            Bitmap b = null;
            in = getContentResolver().openInputStream(uri);

            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            Matrix mat = new Matrix();
            mat.postRotate(angle);

            b = Bitmap.createBitmap(b, 0, 0,
                    b.getWidth(), b.getHeight(), mat, true);

            in.close();

            Log.d("", "bitmap size - width: " + b.getWidth() + ", height: " +
                    b.getHeight());
            return b;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("", e.getMessage(), e);
            return null;
        }
    }

    class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.DataObjectHolder> {
        private ArrayList<Bitmap> bitmaps;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            ImageView imageView, deleteIv;
            Button viewBtn;

            public DataObjectHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.iv_image);
                deleteIv = (ImageView) itemView.findViewById(R.id.iv_delete);
                viewBtn = (Button) itemView.findViewById(R.id.btn_view);
                viewBtn.setOnClickListener(this);
                deleteIv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (v.getId()) {
                    case R.id.btn_view:
                        int pos = (int) v.getTag();
                        showImagePreview(imgBitmaps.get(pos), pos);
                        break;
                    case R.id.iv_delete:
                        imgBitmaps.remove((int) v.getTag());
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        }

        public ImagesAdapter(ArrayList<Bitmap> myDataset) {
            bitmaps = myDataset;
        }

        @Override
        public ImagesAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                 int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_image_item, parent, false);
            return new ImagesAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final ImagesAdapter.DataObjectHolder holder, final int position) {
            Bitmap bitmap = bitmaps.get(position);
            if (bitmap != null) {
                holder.imageView.setImageBitmap(bitmap);

                holder.imageView.setTag(position);
                holder.deleteIv.setTag(position);
                holder.viewBtn.setTag(position);
            }
        }

        @Override
        public int getItemCount() {
            int count = bitmaps.size();
            return bitmaps.size();
        }

        public void update(ArrayList<Bitmap> data) {
            bitmaps.clear();
            bitmaps.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(Bitmap dataObj, int index) {
            bitmaps.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            bitmaps.remove(index);
            notifyItemRemoved(index);
        }

    }

    private void showImagePreview(Bitmap bitmap, int position) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Image Preview");
        alertDialog.setCancelable(true);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_pod, null);
        ImageView docketIv;
        Button cancelBtn, reCaptureBtn;
        docketIv = (ImageView) view.findViewById(R.id.iv_pod);
        cancelBtn = (Button) view.findViewById(R.id.btn_crop);
        cancelBtn.setText("Cancel");
        reCaptureBtn = (Button) view.findViewById(R.id.btn_recapture);

        alertDialog.setView(view);
        byte[] decodedString = Base64.decode(getBase64(bitmap), Base64.NO_WRAP);
        final Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        decodedByte.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        docketIv.setImageBitmap(decodedByte);
        final AlertDialog alert = alertDialog.show();
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
        reCaptureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                imagePosition = position;
                captureImage(CAPTURE_IMG, capBase64Filename + "_" + position);
            }
        });
    }
}
