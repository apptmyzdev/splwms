package com.apptmyz.splwms.datacache;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.util.Log;

import com.apptmyz.splwms.InvoicesScreen;
import com.apptmyz.splwms.PartsScanningScreen;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.BinMasterModel;
import com.apptmyz.splwms.data.CustomerMasterModel;
import com.apptmyz.splwms.data.ExceptionModel;
import com.apptmyz.splwms.data.InboundTypeModel;
import com.apptmyz.splwms.data.InspectionPart;
import com.apptmyz.splwms.data.InventoryTypeModel;
import com.apptmyz.splwms.data.InvoiceModel;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.data.PackingModel;
import com.apptmyz.splwms.data.PackingOrderNoModel;
import com.apptmyz.splwms.data.PartMasterModel;
import com.apptmyz.splwms.data.ProductMasterModel;
import com.apptmyz.splwms.data.ProductModel;
import com.apptmyz.splwms.data.PutAwayData;
import com.apptmyz.splwms.data.SharedPreferenceItem;
import com.apptmyz.splwms.data.SkipBinReasonModel;
import com.apptmyz.splwms.data.SlaNameComponentModel;
import com.apptmyz.splwms.data.SubProjectMasterModel;
import com.apptmyz.splwms.data.UnloadingGoodPartData;
import com.apptmyz.splwms.data.UnloadingResponse;
import com.apptmyz.splwms.data.UnpackingBox;
import com.apptmyz.splwms.data.UnpackingPart;
import com.apptmyz.splwms.data.VendorMasterModel;
import com.apptmyz.splwms.data.WmsPartInvoiceModel;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataSource {
    private SQLiteDatabase database;
    private DataHelper dbHelper;
    private Context mContext;
    private Gson gson;
    public VendorMaster vendorMaster;
    public UnloadingParts unloadingParts;
    public UnloadingGoodParts unloadingGoodParts;
    public ShardPreferences sharedPreferences;
    public SkipBinReasons skipBinReasons;
    public VehicleNumbers vehicleNumbers;
    public TcNumbers tcNumbers;
    public Bins bins;
    public Parts parts;
    public Products products;
    public PutAway putAway;
    public Subprojects subprojects;
    public Nodes nodes;
    public PickListParts pickListParts;
    public ExceptionTypes exceptionTypes;
    public InventoryTypes inventoryTypes;
    public Exceptions exceptions;
    public Sla sla;
    public InboundTypes inboundTypes;
    public InboundInspection inboundInspection;
    public UnpackingParts unpackingParts;
    public UnpackingBoxes unpackingBoxes;
    public PackingParts packingParts;
    public Invoices invoices;

    public DataSource(Context context) {
        try {
            mContext = context;
            dbHelper = DataHelper.getHelper(context);
            sharedPreferences = new ShardPreferences();
            unloadingParts = new UnloadingParts();
            unloadingGoodParts = new UnloadingGoodParts();
            putAway = new PutAway();
            parts = new Parts();
            products = new Products();
            exceptionTypes = new ExceptionTypes();
            inventoryTypes = new InventoryTypes();
            vendorMaster = new VendorMaster();
            subprojects = new Subprojects();
            skipBinReasons = new SkipBinReasons();
            vehicleNumbers = new VehicleNumbers();
            tcNumbers = new TcNumbers();
            bins = new Bins();
            nodes = new Nodes();
            pickListParts = new PickListParts();
            gson = new Gson();
            exceptions = new Exceptions();
            sla = new Sla();
            inboundTypes = new InboundTypes();
            inboundInspection = new InboundInspection();
            unpackingParts = new UnpackingParts();
            unpackingBoxes = new UnpackingBoxes();
            packingParts = new PackingParts();
            invoices = new Invoices();
        } catch (SQLiteException e) {
            Utils.logE(e.toString());
        } catch (VerifyError e) {
            Utils.logE(e.toString());
        }
    }

    @SuppressLint("NewApi")
    public void open() throws SQLException {
        if ((database != null && !database.isOpen()) || database == null) {
            try {
                database = dbHelper.getWritableDatabase();
            } catch (Exception e) {
                Utils.logE(e.toString());
            }
        }
    }

    synchronized public void openRead() throws SQLException {
        if ((database != null && !database.isOpen()) || database == null) {
            database = dbHelper.getReadableDatabase();
        }
    }

    public void close() {
        if ((database != null && database.isOpen())) {
//            dbHelper.close();
        }
    }

    // ************** SHARED_PREFERENCES **************//

    public class ShardPreferences {

        public void set(String key, String value) {
            open();
            try {
                if (!Utils.isValidString(value))
                    value = "";

                if (Utils.isValidString(key)) {
                    int id = exists(key);
                    if (id == -1) {
                        create(key, value);
                    } else {
                        update(key, value);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void update(String key, String value) {
            open();
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

                if (database.update(DataHelper.SHARED_PREF_TABLE_NAME, values,
                        DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
                        null) > 0) {
                } else {
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }

        }

        public void create(String key, String value) {
            open();
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.SHARED_PREF_COLUMN_KEY, key);
                values.put(DataHelper.SHARED_PREF_COLUMN_VALUE, value);

                if (database.insert(DataHelper.SHARED_PREF_TABLE_NAME, null,
                        values) > 0) {
                } else {
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void delete(String key) {
            open();
            try {
                if (database.delete(DataHelper.SHARED_PREF_TABLE_NAME,
                        DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key + "'",
                        null) > 0) {
                } else {
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public List<SharedPreferenceItem> getAll() {
            openRead();
            List<SharedPreferenceItem> items = new ArrayList<SharedPreferenceItem>();
            Cursor cursor = null;
            try {

                cursor = database.query(DataHelper.SHARED_PREF_TABLE_NAME,
                        DataHelper.SHARED_PREF_COLUMNS, null, null, null, null,
                        null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    SharedPreferenceItem item = cursorToItem(cursor);
                    items.add(item);
                    cursor.moveToNext();
                }

            } catch (Exception e) {
                Utils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return items;
        }

        public String getValue(String key) {
            openRead();

            Cursor cursor = null;
            String value = "";

            try {
                String selectQuery = "SELECT  "
                        + DataHelper.SHARED_PREF_COLUMN_VALUE + " FROM "
                        + DataHelper.SHARED_PREF_TABLE_NAME + " WHERE "
                        + DataHelper.SHARED_PREF_COLUMN_KEY + " = '" + key
                        + "'";

                cursor = database.rawQuery(selectQuery, null);

                if (cursor.moveToFirst()) {
                    value = cursor.getString(0);
                }

            } catch (Exception e) {
                Utils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return value;

        }

        public int exists(String key) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SHARED_PREF_KEY_ID
                        + " FROM " + DataHelper.SHARED_PREF_TABLE_NAME
                        + " WHERE " + DataHelper.SHARED_PREF_COLUMN_KEY
                        + " = '" + key + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        private SharedPreferenceItem cursorToItem(Cursor cursor) {
            return new SharedPreferenceItem(cursor.getString(0),
                    cursor.getString(1));
        }
    }

    public class PutAway {
        public int matchesData(String scannedValue, String warehouseCode) {
            openRead();
            int matchingValues = 0;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  COUNT(*) FROM " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " WHERE " + DataHelper.PUT_AWAY_PART_NUMBER
                        + " LIKE '" + scannedValue + "%'";
                selectQuery += " AND " + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    matchingValues = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return matchingValues;
        }

        public PutAwayData getPutAwayPart(String partNumber, String warehouseCode) {
            PutAwayData model = null;

            openRead();
            String sql = "SELECT * FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_PART_NUMBER + " = '" + partNumber + "' AND "
                    + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "' AND "
                    + DataHelper.PUT_AWAY_STATUS + " = 0";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partIdIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_PART_ID);
                int partNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_PART_NUMBER);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVOICES);
                int vehicleNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_VEHICLE_NUMBER);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TOTAL_BOXES);
                int totalQtyIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TOTAL_QTY);
                int statusIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_STATUS);
                int binNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_BIN_NUMBER);
                int tobeScannedBoxesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES);
                int idIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_ID);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVENTORY_TYPE);
                int isProductIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_IS_PRODUCT);
                int inventoryTypeIdIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVENTORY_TYPE_ID);

                do {
                    model = new PutAwayData();
                    model.setPartNo(cursor.getString(partNumIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    Integer isProduct = cursor.getInt(isProductIdx);
                    String invoicesStr = cursor.getString(invoicesIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    model.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));
                    model.setVehicleNumber(cursor.getString(vehicleNumIdx));
                    model.setQty(cursor.getInt(totalBoxesIdx));
                    model.setTotalQty(cursor.getInt(totalQtyIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setBinNumber(cursor.getString(binNumIdx));
                    model.setNoOfBoxesToScan(cursor.getInt(tobeScannedBoxesIdx));
                    model.setId(cursor.getInt(idIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeIdx));
                    model.setIsProduct(isProduct);
                    model.setInventoryTypeId(cursor.getInt(inventoryTypeIdIdx));
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public int exists(int uniqueId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PUT_AWAY_PART_KEY_ID
                        + " FROM " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " WHERE " + DataHelper.PUT_AWAY_ID
                        + " = " + uniqueId
                        + " AND " + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public void delete(int uniqueId, String warehouseCode) {
            openRead();
            try {
                String updateSql = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " WHERE "
                        + DataHelper.PUT_AWAY_ID + " = " + uniqueId
                        + " AND " + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateAsPartialScan(int uniqueId, int noOfBoxesToScan, String warehouseCode) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " SET " + DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES + " = " + noOfBoxesToScan + " WHERE "
                        + DataHelper.PUT_AWAY_ID + " = " + uniqueId
                        + " AND " + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateAsSubmitted(int uniqueId, String warehouseCode) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PUT_AWAY_TABLE_NAME
                        + " SET " + DataHelper.PUT_AWAY_STATUS + " = " + 1
                        + " WHERE "
                        + DataHelper.PUT_AWAY_ID + " = " + uniqueId
                        + " AND " + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public long insertPutAwayData(List<PutAwayData> putAwayDataList, int status, String warehouseCode) {
            open();
            long retVal = -1;
            try {
                for (PutAwayData putAwayData : putAwayDataList) {
                    int id = exists(putAwayData.getId(), warehouseCode);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        if(putAwayData.getIsProduct() == 1) {
                            values.put(DataHelper.PUT_AWAY_PART_ID, putAwayData.getProductId());
                        } else {
                            values.put(DataHelper.PUT_AWAY_PART_ID, putAwayData.getPartId());
                        }
                        if(putAwayData.getIsProduct() == 1) {
                            values.put(DataHelper.PUT_AWAY_PART_NUMBER, putAwayData.getProductName());
                        } else {
                            values.put(DataHelper.PUT_AWAY_PART_NUMBER, putAwayData.getPartNo());
                        }
                        values.put(DataHelper.PUT_AWAY_IS_PRODUCT, putAwayData.getIsProduct());
                        values.put(DataHelper.PUT_AWAY_INVENTORY_TYPE_ID, putAwayData.getInventoryTypeId());
                        values.put(DataHelper.PUT_AWAY_STATUS, status);
                        values.put(DataHelper.PUT_AWAY_WH_CODE, warehouseCode);
                        if (status == 0)
                            values.put(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES, putAwayData.getQty());
                        else
                            values.put(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES, 0);
                        values.put(DataHelper.PUT_AWAY_VEHICLE_NUMBER, putAwayData.getVehicleNumber());
                        values.put(DataHelper.PUT_AWAY_TOTAL_BOXES, putAwayData.getQty());
                        values.put(DataHelper.PUT_AWAY_TOTAL_QTY, putAwayData.getTotalQty());
                        values.put(DataHelper.PUT_AWAY_BIN_NUMBER, putAwayData.getBinNumber());
                        values.put(DataHelper.PUT_AWAY_ID, putAwayData.getId());
                        values.put(DataHelper.PUT_AWAY_INVENTORY_TYPE, putAwayData.getInventoryTypeDesc());
//                        values.put(DataHelper.PUT_AWAY_RECEIPT_TYPE, putAwayData.getReceiptType());

                        String jsonStr = gson.toJson(putAwayData.getInvoiceList());
                        values.put(DataHelper.PUT_AWAY_INVOICES, jsonStr);

                        retVal = database.insert(DataHelper.PUT_AWAY_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<PutAwayData> getPutAwayData(int status, String warehouseCode) {
            ArrayList<PutAwayData> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_STATUS + " = " + status
                    + " AND " + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partIdIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_PART_ID);
                int partNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_PART_NUMBER);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVOICES);
                int vehicleNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_VEHICLE_NUMBER);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TOTAL_BOXES);
                int totalQtyIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TOTAL_QTY);
                int statusIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_STATUS);
                int binNumIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_BIN_NUMBER);
                int tobeScannedBoxesIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_TO_BE_SCANNED_BOXES);
                int idIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_ID);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVENTORY_TYPE);
                int isProductIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_IS_PRODUCT);
                int inventoryTypeIdIdx = cursor.getColumnIndex(DataHelper.PUT_AWAY_INVENTORY_TYPE_ID);

                do {
                    PutAwayData model = new PutAwayData();
                    model.setPartNo(cursor.getString(partNumIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    String invoicesStr = cursor.getString(invoicesIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    model.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));
                    model.setVehicleNumber(cursor.getString(vehicleNumIdx));
                    model.setQty(cursor.getInt(totalBoxesIdx));
                    model.setTotalQty(cursor.getInt(totalQtyIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setBinNumber(cursor.getString(binNumIdx));
                    model.setNoOfBoxesToScan(cursor.getInt(tobeScannedBoxesIdx));
                    model.setId(cursor.getInt(idIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeIdx));
                    model.setIsProduct(cursor.getInt(isProductIdx));
                    model.setInventoryTypeId(cursor.getInt(inventoryTypeIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME;
            database.execSQL(query);

            close();
        }

        public void deletePendingData(String warehouseCode) {
            open();
            String query = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_STATUS + " = 0 AND "
                    + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";
            database.execSQL(query);

            close();
        }

        public void deleteCompletedData(String warehouseCode) {
            open();
            String query = "DELETE FROM " + DataHelper.PUT_AWAY_TABLE_NAME + " WHERE "
                    + DataHelper.PUT_AWAY_STATUS + " = 1 AND "
                    + DataHelper.PUT_AWAY_WH_CODE + " = '" + warehouseCode + "'";
            database.execSQL(query);

            close();
        }
    }

    public class ExceptionTypes {
        public long saveExceptionTypes(List<SkipBinReasonModel> skipBinReasonModels, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SkipBinReasonModel skipBinReasonModel : skipBinReasonModels) {
                    id = exists(skipBinReasonModel.getReasonId(), warehouseCode);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.EXCEPTION_TYPE_ID, skipBinReasonModel.getReasonId());
                        values.put(DataHelper.EXCEPTION_TYPE_NAME, skipBinReasonModel.getReason());
                        values.put(DataHelper.EXCEPTION_TYPE_WH_CODE, warehouseCode);

                        retVal = database.insert(DataHelper.EXCEPTION_TYPES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int reasonId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.EXCEPTION_TYPE_KEY_ID
                        + " FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME
                        + " WHERE " + DataHelper.EXCEPTION_TYPE_ID
                        + " = " + reasonId + " AND "
                        + DataHelper.EXCEPTION_TYPE_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SkipBinReasonModel> getExceptionTypes(String warehouseCode) {
            ArrayList<SkipBinReasonModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME
                    + " WHERE " + DataHelper.EXCEPTION_TYPE_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_NAME);

                do {
                    SkipBinReasonModel model = new SkipBinReasonModel();
                    model.setReasonId(cursor.getInt(reasonIdIdx));
                    model.setReason(cursor.getString(reasonIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public SkipBinReasonModel getException(int id, String warehouseCode) {
            SkipBinReasonModel model = null;

            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_TYPE_ID + " = " + id + " AND "
                    + DataHelper.EXCEPTION_TYPE_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_NAME);
                model = new SkipBinReasonModel();
                model.setReasonId(cursor.getInt(reasonIdIdx));
                model.setReason(cursor.getString(reasonIdx));
            }
            if (cursor != null)
                cursor.close();
            close();

            return model;
        }


        public String getExceptionId(String desc, String warehouseCode) {
            String depsId = "";

            openRead();

            String sql = "SELECT " + DataHelper.EXCEPTION_TYPE_ID + " FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME
                    + " WHERE " + DataHelper.EXCEPTION_TYPE_NAME + " = '" + desc + "' AND "
                    + DataHelper.EXCEPTION_TYPE_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                depsId = cursor.getString(0);
            }

            cursor.close();
            close();

            return depsId;
        }

        public ArrayList<String> getExceptionList(String warehouseCode, String type) {
            ArrayList<String> codes = new ArrayList<String>();

            openRead();

            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME
                    + " WHERE " + DataHelper.EXCEPTION_TYPE_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int descId = cursor
                        .getColumnIndex(DataHelper.EXCEPTION_TYPE_NAME);
                do {
                    if (!(type.equals("UL") && cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID) == 14)) {
                        codes.add(cursor.getString(descId));
                    }
                } while (cursor.moveToNext());
            }

            cursor.close();
            close();

            return codes;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.EXCEPTION_TYPES_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class InventoryTypes {
        public long saveInventoryTypes(List<InventoryTypeModel> inventoryTypeModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (InventoryTypeModel inventoryTypeModel : inventoryTypeModels) {
                    id = exists(inventoryTypeModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.INVENTORY_TYPE_ID, inventoryTypeModel.getId());
                        values.put(DataHelper.INVENTORY_TYPE_CODE, inventoryTypeModel.getCode());
                        values.put(DataHelper.INVENTORY_TYPE_ACTIVE_FLAG, inventoryTypeModel.getActiveFlag());
                        values.put(DataHelper.INVENTORY_TYPE_NAME, inventoryTypeModel.getType());

                        retVal = database.insert(DataHelper.INVENTORY_TYPES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int typeId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.INVENTORY_TYPE_KEY_ID
                        + " FROM " + DataHelper.INVENTORY_TYPES_TABLE_NAME
                        + " WHERE " + DataHelper.INVENTORY_TYPE_ID
                        + " = " + typeId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public ArrayList<InventoryTypeModel> getInventoryTypes() {
            ArrayList<InventoryTypeModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INVENTORY_TYPES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int typeIdIdx = cursor.getColumnIndex(DataHelper.INVENTORY_TYPE_ID);
                int typeIdx = cursor.getColumnIndex(DataHelper.INVENTORY_TYPE_NAME);
                int typeCodeIdx = cursor.getColumnIndex(DataHelper.INVENTORY_TYPE_CODE);
                int activeFlagIdx = cursor.getColumnIndex(DataHelper.INVENTORY_TYPE_ACTIVE_FLAG);

                do {
                    InventoryTypeModel model = new InventoryTypeModel();
                    model.setId(cursor.getInt(typeIdIdx));
                    model.setType(cursor.getString(typeIdx));
                    model.setActiveFlag(cursor.getInt(activeFlagIdx));
                    model.setCode(cursor.getString(typeCodeIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.INVENTORY_TYPES_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class TcNumbers {
        public int exists(String number, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.TC_NUMBERS_KEY_ID
                        + " FROM " + DataHelper.TC_NUMBERS_TABLE_NAME
                        + " WHERE " + DataHelper.TC_NUMBERS_NUMBER
                        + " = '" + number + "' AND "
                        + DataHelper.TC_NUMBERS_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long saveTcNumbers(List<String> tcNumbers, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (String tcNumber : tcNumbers) {
                    if (exists(tcNumber, warehouseCode) == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.TC_NUMBERS_NUMBER, tcNumber);
                        values.put(DataHelper.TC_NUMBERS_WH_CODE, warehouseCode);

                        retVal = database.insert(DataHelper.TC_NUMBERS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<String> getTcNumbers(String warehouseCode) {
            ArrayList<String> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.TC_NUMBERS_TABLE_NAME
                    + " WHERE " + DataHelper.TC_NUMBERS_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int numberIdx = cursor.getColumnIndex(DataHelper.TC_NUMBERS_NUMBER);

                do {
                    models.add(cursor.getString(numberIdx));
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.TC_NUMBERS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class VehicleNumbers {

        public int exists(String number, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.VEHICLE_NUMBERS_KEY_ID
                        + " FROM " + DataHelper.VEHICLE_NUMBERS_TABLE_NAME
                        + " WHERE " + DataHelper.VEHICLE_NUMBERS_NUMBER
                        + " = '" + number + "' AND "
                        + DataHelper.VEHICLE_NUMBERS_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long saveVehicleNumbers(List<String> vehicleNumbers, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (String vehicleNum : vehicleNumbers) {
                    if (exists(vehicleNum, warehouseCode) == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.VEHICLE_NUMBERS_NUMBER, vehicleNum);
                        values.put(DataHelper.VEHICLE_NUMBERS_WH_CODE, warehouseCode);
                        retVal = database.insert(DataHelper.VEHICLE_NUMBERS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<String> getVehicleNumbers(String warehouseCode) {
            ArrayList<String> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.VEHICLE_NUMBERS_TABLE_NAME
                    + " WHERE " + DataHelper.VEHICLE_NUMBERS_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int numberIdx = cursor.getColumnIndex(DataHelper.VEHICLE_NUMBERS_NUMBER);

                do {
                    models.add(cursor.getString(numberIdx));
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.VEHICLE_NUMBERS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class SkipBinReasons {
        public long saveReasons(List<SkipBinReasonModel> skipBinReasonModels, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SkipBinReasonModel skipBinReasonModel : skipBinReasonModels) {
                    id = exists(skipBinReasonModel.getReasonId(), warehouseCode);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.SKIP_BIN_REASON_ID, skipBinReasonModel.getReasonId());
                        values.put(DataHelper.SKIP_BIN_REASON_NAME, skipBinReasonModel.getReason());
                        values.put(DataHelper.SKIP_BIN_WH_CODE, warehouseCode);

                        retVal = database.insert(DataHelper.SKIP_BIN_REASONS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int reasonId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SKIP_BIN_REASON_KEY_ID
                        + " FROM " + DataHelper.SKIP_BIN_REASONS_TABLE_NAME
                        + " WHERE " + DataHelper.SKIP_BIN_REASON_ID
                        + " = " + reasonId + " AND "
                        + DataHelper.SKIP_BIN_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SkipBinReasonModel> getSkipBinReasons(String warehouseCode) {
            ArrayList<SkipBinReasonModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.SKIP_BIN_REASONS_TABLE_NAME
                    + " WHERE " + DataHelper.SKIP_BIN_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int reasonIdIdx = cursor.getColumnIndex(DataHelper.SKIP_BIN_REASON_ID);
                int reasonIdx = cursor.getColumnIndex(DataHelper.SKIP_BIN_REASON_NAME);

                do {
                    SkipBinReasonModel model = new SkipBinReasonModel();
                    model.setReasonId(cursor.getInt(reasonIdIdx));
                    model.setReason(cursor.getString(reasonIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.SKIP_BIN_REASONS_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Parts {

        public long saveParts(List<PartMasterModel> partMasterModels, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (PartMasterModel partMasterModel : partMasterModels) {
                    id = exists(partMasterModel.getPartId(), warehouseCode);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PART_ID, partMasterModel.getPartId());
                        values.put(DataHelper.PART_NUMBER, partMasterModel.getPartNo());
                        values.put(DataHelper.PART_NAME, partMasterModel.getPartName());
                        values.put(DataHelper.PART_WH_CODE, warehouseCode);

                        retVal = database.insert(DataHelper.PART_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int partId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PART_KEY_ID
                        + " FROM " + DataHelper.PART_TABLE_NAME
                        + " WHERE " + DataHelper.PART_ID
                        + " = " + partId + " AND "
                        + DataHelper.PART_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public int exists(String partNumber, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PART_KEY_ID
                        + " FROM " + DataHelper.PART_TABLE_NAME
                        + " WHERE " + DataHelper.PART_NUMBER
                        + " = '" + partNumber + "' AND "
                        + DataHelper.PART_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public PartMasterModel getPartData(String partNum, String warehouseCode) {
            PartMasterModel model = null;
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PART_TABLE_NAME + " WHERE " + DataHelper.PART_WH_CODE + " = '"
                    + warehouseCode + "' AND " + DataHelper.PART_NUMBER + " = '" + partNum + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partIdIdx = cursor.getColumnIndex(DataHelper.PART_ID);
                int partNoIdx = cursor.getColumnIndex(DataHelper.PART_NUMBER);
                int partNameIdx = cursor.getColumnIndex(DataHelper.PART_NAME);

                model = new PartMasterModel();
                model.setPartId(cursor.getInt(partIdIdx));
                model.setPartNo(cursor.getString(partNoIdx));
                model.setPartName(cursor.getString(partNameIdx));

            }
            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public ArrayList<PartMasterModel> getPartsData(String warehouseCode) {
            ArrayList<PartMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PART_TABLE_NAME + " WHERE " + DataHelper.PART_WH_CODE + " = '"
                    + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partIdIdx = cursor.getColumnIndex(DataHelper.PART_ID);
                int partNoIdx = cursor.getColumnIndex(DataHelper.PART_NUMBER);
                int partNameIdx = cursor.getColumnIndex(DataHelper.PART_NAME);

                do {
                    PartMasterModel model = new PartMasterModel();
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setPartName(cursor.getString(partNameIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PART_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Products {
        public long saveProducts(List<ProductMasterModel> productMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (ProductMasterModel productMasterModel : productMasterModels) {
                    id = exists(productMasterModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PRODUCT_ID, productMasterModel.getId());
                        values.put(DataHelper.PRODUCT_NAME, productMasterModel.getProductName());
                        values.put(DataHelper.PRODUCT_CUSTOMER_NAME, productMasterModel.getCustomerName());
                        values.put(DataHelper.PRODUCT_CUSTOMER_ID, productMasterModel.getCustomerId());
                        values.put(DataHelper.PRODUCT_SKU, productMasterModel.getProductSku());
                        values.put(DataHelper.PRODUCT_COUNTRY_ID, productMasterModel.getCountryId());
                        values.put(DataHelper.PRODUCT_MAKE, productMasterModel.getProductMake());
                        values.put(DataHelper.PRODUCT_MODEL, productMasterModel.getProductModel());

                        retVal = database.insert(DataHelper.PRODUCT_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int productId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PRODUCT_KEY_ID
                        + " FROM " + DataHelper.PRODUCT_TABLE_NAME
                        + " WHERE " + DataHelper.PRODUCT_ID
                        + " = " + productId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ProductMasterModel getProductData(String productName) {
            ProductMasterModel model = null;
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PRODUCT_TABLE_NAME + " WHERE " + DataHelper.PRODUCT_NAME + " = '" + productName + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int productIdIdx = cursor.getColumnIndex(DataHelper.PRODUCT_ID);
                int productNameIdx = cursor.getColumnIndex(DataHelper.PRODUCT_NAME);
                int productSkuIdx = cursor.getColumnIndex(DataHelper.PRODUCT_SKU);
                int countryIdIdx = cursor.getColumnIndex(DataHelper.PRODUCT_COUNTRY_ID);
                int customerIdIdx = cursor.getColumnIndex(DataHelper.PRODUCT_CUSTOMER_ID);
                int customerNameIdx = cursor.getColumnIndex(DataHelper.PRODUCT_CUSTOMER_NAME);
                int makeIdx = cursor.getColumnIndex(DataHelper.PRODUCT_MAKE);
                int modelIdx = cursor.getColumnIndex(DataHelper.PRODUCT_MODEL);

                model = new ProductMasterModel();
                model.setId(cursor.getInt(productIdIdx));
                model.setProductName(cursor.getString(productNameIdx));
                model.setProductSku(cursor.getString(productSkuIdx));
                model.setCountryId(cursor.getInt(countryIdIdx));
                model.setCustomerId(cursor.getInt(customerIdIdx));
                model.setCustomerName(cursor.getString(customerNameIdx));
                model.setProductMake(cursor.getString(makeIdx));
                model.setProductModel(cursor.getString(modelIdx));
            }
            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public int exists(String productSKu) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PRODUCT_KEY_ID
                        + " FROM " + DataHelper.PRODUCT_TABLE_NAME
                        + " WHERE " + DataHelper.PRODUCT_SKU
                        + " = '" + productSKu + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<ProductMasterModel> getProductsData() {
            ArrayList<ProductMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PRODUCT_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int productIdIdx = cursor.getColumnIndex(DataHelper.PRODUCT_ID);
                int productNameIdx = cursor.getColumnIndex(DataHelper.PRODUCT_NAME);
                int productSkuIdx = cursor.getColumnIndex(DataHelper.PRODUCT_SKU);
                int countryIdIdx = cursor.getColumnIndex(DataHelper.PRODUCT_COUNTRY_ID);
                int customerIdIdx = cursor.getColumnIndex(DataHelper.PRODUCT_CUSTOMER_ID);
                int customerNameIdx = cursor.getColumnIndex(DataHelper.PRODUCT_CUSTOMER_NAME);
                int makeIdx = cursor.getColumnIndex(DataHelper.PRODUCT_MAKE);
                int modelIdx = cursor.getColumnIndex(DataHelper.PRODUCT_MODEL);

                do {
                    ProductMasterModel model = new ProductMasterModel();
                    model.setId(cursor.getInt(productIdIdx));
                    model.setProductName(cursor.getString(productNameIdx));
                    model.setProductSku(cursor.getString(productSkuIdx));
                    model.setCountryId(cursor.getInt(countryIdIdx));
                    model.setCustomerId(cursor.getInt(customerIdIdx));
                    model.setCustomerName(cursor.getString(customerNameIdx));
                    model.setProductMake(cursor.getString(makeIdx));
                    model.setProductModel(cursor.getString(modelIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PRODUCT_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class InboundTypes {
        public long saveInboundTypes(List<InboundTypeModel> inboundTypeModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (InboundTypeModel inboundTypeModel : inboundTypeModels) {
                    id = exists(inboundTypeModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.INBOUND_TYPE_CODE, inboundTypeModel.getInboundCode());
                        values.put(DataHelper.INBOUND_TYPE_NAME, inboundTypeModel.getInboundType());
                        values.put(DataHelper.INBOUND_ID, inboundTypeModel.getId());
                        values.put(DataHelper.INBOUND_ACTIVE_FLAG, inboundTypeModel.getActiveFlag());
                        values.put(DataHelper.INBOUND_CREATED_TIMESTAMP, inboundTypeModel.getCreatedTimestamp());
                        values.put(DataHelper.INBOUND_UPDATED_TIMESTAMP, inboundTypeModel.getUpdatedTimestamp());

                        retVal = database.insert(DataHelper.INBOUND_TYPES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int inboundId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.INBOUND_TYPES_KEY_ID
                        + " FROM " + DataHelper.INBOUND_TYPES_TABLE_NAME
                        + " WHERE " + DataHelper.INBOUND_ID
                        + " = " + inboundId + "";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<InboundTypeModel> getInboundTypesData() {
            ArrayList<InboundTypeModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INBOUND_TYPES_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int idIdx = cursor.getColumnIndex(DataHelper.INBOUND_ID);
                int codeIdx = cursor.getColumnIndex(DataHelper.INBOUND_TYPE_CODE);
                int inboundTypeNameIdx = cursor.getColumnIndex(DataHelper.INBOUND_TYPE_NAME);
                int activeFlagIdx = cursor.getColumnIndex(DataHelper.INBOUND_ACTIVE_FLAG);
                int createdTimestampIdx = cursor.getColumnIndex(DataHelper.INBOUND_CREATED_TIMESTAMP);
                int updatedTimestampIdx = cursor.getColumnIndex(DataHelper.INBOUND_UPDATED_TIMESTAMP);

                do {
                    InboundTypeModel model = new InboundTypeModel();
                    model.setActiveFlag(cursor.getInt(activeFlagIdx));
                    model.setInboundCode(cursor.getInt(codeIdx));
                    model.setInboundType(cursor.getString(inboundTypeNameIdx));
                    model.setId(cursor.getInt(idIdx));
                    model.setCreatedTimestamp(cursor.getString(createdTimestampIdx));
                    model.setUpdatedTimestamp(cursor.getString(updatedTimestampIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.INBOUND_TYPES_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Sla {
        public long saveSlaData(List<SlaNameComponentModel> slaModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SlaNameComponentModel slaModel : slaModels) {
                    id = exists(slaModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.SLA_ID, slaModel.getId());
                        values.put(DataHelper.SLA_CATEGORY_TYPE, slaModel.getCategoryType());
                        values.put(DataHelper.SLA_NAME, slaModel.getSlaName());
                        values.put(DataHelper.SLA_ORDER_CUTOFF_TIME, slaModel.getOrderCutOffTime());
                        values.put(DataHelper.SLA_ACTIVE_FLAG, slaModel.getActiveFlag());
                        values.put(DataHelper.SLA_DESCRIPTION, slaModel.getSlaDescription());
                        values.put(DataHelper.SLA_LEAD_TIME, slaModel.getLeadTime());
                        values.put(DataHelper.SLA_TIMEZONE, slaModel.getTimeZone());

                        retVal = database.insert(DataHelper.SLA_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(Integer slaId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SLA_KEY_ID
                        + " FROM " + DataHelper.SLA_TABLE_NAME
                        + " WHERE " + DataHelper.SLA_ID
                        + " = " + slaId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SlaNameComponentModel> getSlaData() {
            ArrayList<SlaNameComponentModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.SLA_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int idIdx = cursor.getColumnIndex(DataHelper.SLA_ID);
                int descriptionIdx = cursor.getColumnIndex(DataHelper.SLA_DESCRIPTION);
                int nameIdx = cursor.getColumnIndex(DataHelper.SLA_NAME);
                int categoryTypeIdx = cursor.getColumnIndex(DataHelper.SLA_CATEGORY_TYPE);
                int orderCutOffTimeIdx = cursor.getColumnIndex(DataHelper.SLA_ORDER_CUTOFF_TIME);
                int leadTimeIdx = cursor.getColumnIndex(DataHelper.SLA_LEAD_TIME);
                int timeZoneIdx = cursor.getColumnIndex(DataHelper.SLA_TIMEZONE);
                int activeFlagIdx = cursor.getColumnIndex(DataHelper.SLA_ACTIVE_FLAG);

                do {
                    SlaNameComponentModel model = new SlaNameComponentModel();
                    model.setId(cursor.getInt(idIdx));
                    model.setCategoryType(cursor.getString(categoryTypeIdx));
                    model.setSlaDescription(cursor.getString(descriptionIdx));
                    model.setSlaName(cursor.getString(nameIdx));
                    model.setOrderCutOffTime(cursor.getString(orderCutOffTimeIdx));
                    model.setLeadTime(cursor.getString(leadTimeIdx));
                    model.setTimeZone(cursor.getString(timeZoneIdx));
                    model.setActiveFlag(cursor.getInt(activeFlagIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.SLA_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Bins {
        public int matchesData(String scannedValue, String warehouseCode) {
            openRead();
            int matchingValues = 0;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  COUNT(*) FROM " + DataHelper.BIN_TABLE_NAME
                        + " WHERE " + DataHelper.BIN_NUMBER
                        + " LIKE '" + scannedValue + "%' AND "
                        + DataHelper.BIN_WH_CODE + " = '" + warehouseCode + "'";
                Utils.logD(selectQuery);
                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    matchingValues = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return matchingValues;
        }

        public long saveBins(List<BinMasterModel> binMasterModels, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (BinMasterModel binMasterModel : binMasterModels) {
                    id = exists(binMasterModel.getId(), warehouseCode);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.BIN_ID, binMasterModel.getId());
                        values.put(DataHelper.BIN_NUMBER, binMasterModel.getWhBin());
                        values.put(DataHelper.BIN_AISLE, binMasterModel.getWhAisleCode());
                        values.put(DataHelper.BIN_WH_CODE, warehouseCode);
                        values.put(DataHelper.BIN_AREA_CODE, binMasterModel.getWhAreaCode());
                        values.put(DataHelper.BIN_ZONE_CODE, binMasterModel.getWhZoneCode());
                        values.put(DataHelper.BIN_RACK_CODE, binMasterModel.getWhRackCode());
                        values.put(DataHelper.BIN_BARCODE, binMasterModel.getWhBinBarCode());
                        values.put(DataHelper.BIN_LOCATION_CODE, binMasterModel.getWhLocationCode());
                        values.put(DataHelper.BIN_LEVEL_NUMBER, binMasterModel.getLevelNumber());
                        values.put(DataHelper.BIN_ACTIVE_FLAG, binMasterModel.getActiveFlag());
                        values.put(DataHelper.BIN_POSITION, binMasterModel.getBinPosition());
                        values.put(DataHelper.BIN_RESERVE_TYPE, binMasterModel.getReserveType());
                        Gson gson = new Gson();
                        values.put(DataHelper.BIN_INVENTORY_TYPE_IDS, gson.toJson(binMasterModel.getInventoryTypeIds()));

                        retVal = database.insert(DataHelper.BIN_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int binId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.BIN_KEY_ID
                        + " FROM " + DataHelper.BIN_TABLE_NAME
                        + " WHERE " + DataHelper.BIN_ID
                        + " = " + binId + " AND "
                        + DataHelper.BIN_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public int exists(String binNumber, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.BIN_KEY_ID
                        + " FROM " + DataHelper.BIN_TABLE_NAME
                        + " WHERE " + DataHelper.BIN_NUMBER
                        + " = '" + binNumber + "' AND "
                        + DataHelper.BIN_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public BinMasterModel getBin(String binNumber, String warehouseCode) {
            BinMasterModel model = new BinMasterModel();

            openRead();
            String sql = "SELECT * FROM " + DataHelper.BIN_TABLE_NAME + " WHERE "
                    + DataHelper.BIN_NUMBER + " = '" + binNumber + "' AND "
                    + DataHelper.BIN_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int binIdIdx = cursor.getColumnIndex(DataHelper.BIN_ID);
                int binNoIdx = cursor.getColumnIndex(DataHelper.BIN_NUMBER);
                int binAisleIdx = cursor.getColumnIndex(DataHelper.BIN_AISLE);
                int areaCodeIdx = cursor.getColumnIndex(DataHelper.BIN_AREA_CODE);
                int zoneCodeIdx = cursor.getColumnIndex(DataHelper.BIN_ZONE_CODE);
                int locationCodeIdx = cursor.getColumnIndex(DataHelper.BIN_LOCATION_CODE);
                int rackCodeIdx = cursor.getColumnIndex(DataHelper.BIN_RACK_CODE);
                int barcodeIdx = cursor.getColumnIndex(DataHelper.BIN_BARCODE);
                int levelNumIdx = cursor.getColumnIndex(DataHelper.BIN_LEVEL_NUMBER);
                int binPositionIdx = cursor.getColumnIndex(DataHelper.BIN_POSITION);
                int activeFlagIdx = cursor.getColumnIndex(DataHelper.BIN_ACTIVE_FLAG);
                int reserveTypeIdx = cursor.getColumnIndex(DataHelper.BIN_RESERVE_TYPE);
                int inventoryTypeIdsIdx = cursor.getColumnIndex(DataHelper.BIN_INVENTORY_TYPE_IDS);

                do {
                    model.setId(cursor.getInt(binIdIdx));
                    model.setWhBin(cursor.getString(binNoIdx));
                    model.setWhAisleCode(cursor.getString(binAisleIdx));
                    model.setWhAreaCode(cursor.getString(areaCodeIdx));
                    model.setWhZoneCode(cursor.getString(zoneCodeIdx));
                    model.setWhBinBarCode(cursor.getString(barcodeIdx));
                    model.setWhLocationCode(cursor.getString(locationCodeIdx));
                    model.setLevelNumber(cursor.getString(levelNumIdx));
                    model.setActiveFlag(cursor.getInt(activeFlagIdx));
                    model.setBinPosition(cursor.getInt(binPositionIdx));
                    model.setReserveType(cursor.getInt(reserveTypeIdx));
                    Type token = new TypeToken<List<Integer>>() {
                    }.getType();
                    model.setInventoryTypeIds(gson.fromJson(cursor.getString(inventoryTypeIdsIdx), token));

                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return model;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.BIN_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class Subprojects {
        public long insertSubprojectData(List<SubProjectMasterModel> subProjectMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (SubProjectMasterModel subProjectMasterModel : subProjectMasterModels) {
                    id = exists(subProjectMasterModel.getSubProjectId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.SUBPROJECT_ID, subProjectMasterModel.getSubProjectId());
                        values.put(DataHelper.SUBPROJECT_NAME, subProjectMasterModel.getName());

                        values.put(DataHelper.SUBPROJECT_PROJECT_ID, subProjectMasterModel.getProjectId());
                        values.put(DataHelper.SUBPROJECT_PROJECT_NAME, subProjectMasterModel.getProjectName());
                        values.put(DataHelper.SUBPROJECT_PLANT_ID, subProjectMasterModel.getPlantId());
                        values.put(DataHelper.SUBPROJECT_PROJECT_TYPE, subProjectMasterModel.getProjectType());
                        values.put(DataHelper.SUBPROJECT_PROJECT_TYPE_ID, subProjectMasterModel.getProjectTypeId());
                        values.put(DataHelper.SUBPROJECT_LOCATION, subProjectMasterModel.getLocation());
                        values.put(DataHelper.SUBPROJECT_REVENUE_TYPE, subProjectMasterModel.getRevenueType());
                        values.put(DataHelper.SUBPROJECT_REVENUE_DETAILS, subProjectMasterModel.getRevenueDetails());
                        values.put(DataHelper.SUBPROJECT_REVENUE_TYPE_NAME, subProjectMasterModel.getRevenueTypeName());
                        values.put(DataHelper.SUBPROJECT_STATE_CODE, subProjectMasterModel.getStateCode());
                        values.put(DataHelper.SUBPROJECT_STATE, subProjectMasterModel.getState());
                        values.put(DataHelper.SUBPROJECT_CITY, subProjectMasterModel.getCity());
                        values.put(DataHelper.SUBPROJECT_USER_ID, subProjectMasterModel.getUserId());
                        values.put(DataHelper.SUBPROJECT_CREATEDBY, subProjectMasterModel.getCreatedBy());
                        values.put(DataHelper.SUBPROJECT_CREATED_TIMESTAMP, subProjectMasterModel.getCreatedTimestamp());
                        values.put(DataHelper.SUBPROJECT_UPDATEDBY, subProjectMasterModel.getUpdatedBy());
                        values.put(DataHelper.SUBPROJECT_UPDATED_TIMESTAMP, subProjectMasterModel.getUpdatedTimestamp());
                        values.put(DataHelper.SUBPROJECT_BRANCH, subProjectMasterModel.getBranch());
                        values.put(DataHelper.SUBPROJECT_BRANCH_NAME, subProjectMasterModel.getBranchName());
                        values.put(DataHelper.SUBPROJECT_SAAC_CODE, subProjectMasterModel.getSaacCode());
                        values.put(DataHelper.SUBPROJECT_DESCRIPTION, subProjectMasterModel.getDescription());
                        values.put(DataHelper.SUBPROJECT_DISCOUNT_PERCENT, subProjectMasterModel.getDiscountPercent());
                        values.put(DataHelper.SUBPROJECT_CUSTOMER_NAME, subProjectMasterModel.getCustomerName());
                        values.put(DataHelper.SUBPROJECT_CUST_ADDR, subProjectMasterModel.getCustAddr());

                        retVal = database.insert(DataHelper.SUBPROJECTS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int subprojectId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.SUBPROJECT_KEY_ID
                        + " FROM " + DataHelper.SUBPROJECTS_TABLE_NAME
                        + " WHERE " + DataHelper.SUBPROJECT_ID
                        + " = " + subprojectId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<SubProjectMasterModel> getSubprojectsData() {
            ArrayList<SubProjectMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.SUBPROJECTS_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_ID);
                int nameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_NAME);
                int projectIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_ID);
                int projectNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_NAME);
                int projectTypeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_TYPE);
                int projectTypeIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PROJECT_TYPE_ID);
                int locationIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_LOCATION);
                int revenueTypeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_TYPE);
                int revenueDetailsIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_DETAILS);
                int stateCodeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_STATE_CODE);
                int stateIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_STATE);
                int cityIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CITY);
                int userIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_USER_ID);
                int createdByIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CREATEDBY);
                int createdTimestampIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CREATED_TIMESTAMP);
                int updatedByIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_UPDATEDBY);
                int updatedTimestampIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_UPDATED_TIMESTAMP);
                int branchIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_BRANCH);
                int branchNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_BRANCH_NAME);
                int saacCodeIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_SAAC_CODE);
                int descriptionIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_DESCRIPTION);

                int discountPercentIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_DISCOUNT_PERCENT);
                int revenueIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_ID);
                int custNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_CUSTOMER_NAME);

                int revenueTypeNameIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_REVENUE_TYPE_NAME);
                int subprojectPlantIdIdx = cursor.getColumnIndex(DataHelper.SUBPROJECT_PLANT_ID);

                do {
                    SubProjectMasterModel model = new SubProjectMasterModel();
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));
                    model.setProjectName(cursor.getString(projectNameIdx));
                    model.setProjectId(cursor.getInt(projectIdIdx));
                    model.setName(cursor.getString(nameIdx));
                    model.setProjectType(cursor.getString(projectTypeIdx));
                    model.setProjectTypeId(cursor.getInt(projectTypeIdIdx));
                    model.setState(cursor.getString(stateIdx));
                    model.setLocation(cursor.getString(locationIdx));
                    model.setRevenueType(cursor.getInt(revenueTypeIdx));
                    model.setRevenueDetails(cursor.getString(revenueDetailsIdx));
                    model.setState(cursor.getString(stateIdx));
                    model.setStateCode(cursor.getString(stateCodeIdx));

                    model.setCity(cursor.getString(cityIdx));
                    model.setUserId(cursor.getInt(userIdIdx));
                    model.setCreatedBy(cursor.getString(createdByIdx));
                    model.setCreatedTimestamp(cursor.getString(createdTimestampIdx));
                    model.setUpdatedBy(cursor.getString(updatedByIdx));
                    model.setUpdatedTimestamp(cursor.getString(updatedTimestampIdx));
                    model.setBranch(cursor.getInt(branchIdx));
                    model.setBranchName(cursor.getString(branchNameIdx));
                    model.setSaacCode(cursor.getString(saacCodeIdx));

                    model.setDescription(cursor.getString(descriptionIdx));
                    model.setDiscountPercent(cursor.getDouble(discountPercentIdx));
                    model.setRevenueId(cursor.getInt(revenueIdIdx));
                    model.setCustomerName(cursor.getString(custNameIdx));
                    model.setRevenueTypeName(cursor.getString(revenueTypeNameIdx));
                    model.setPlantId(cursor.getInt(subprojectPlantIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

    public class UnloadingGoodParts {
        public void deleteGoodPartsData(String vehicleNo, String warehouseCode) {
            open();
            try {

                String deleteSql = "DELETE  FROM " + DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_GOOD_VEHICLE_NUM + " = '"
                        + vehicleNo + "'"
                        + " AND " + DataHelper.UNLOADING_GOOD_WH_CODE + " = '" + warehouseCode + "'";
                database.execSQL(deleteSql);

            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int isProduct, int productId, int partId, int inventoryTypeId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNLOADING_GOOD_KEY_ID
                        + " FROM " + DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME
                        + " WHERE "
                        + DataHelper.UNLOADING_GOOD_INVENTORY_TYPE + " = " + inventoryTypeId
                        + " AND " + DataHelper.UNLOADING_GOOD_WH_CODE + " = '" + warehouseCode + "' AND ";
                if (isProduct == 1) {
                    selectQuery += DataHelper.UNLOADING_GOOD_PRODUCT_ID + " = " + productId;
                } else {
                    selectQuery += DataHelper.UNLOADING_GOOD_PART_ID + " = " + partId;
                }

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }


        public long insertGoodParts(Context context, String dataType, String partNum, String vehicleNum, int inventoryTypeId,
                                    String inventoryType, int scannedCount, int totalBoxes, String warehouseCode,
                                    int partId, int productId, int isProduct, String productName) {
            String scanTime = Utils.getScanTime();
            open();
            long retVal = -1;
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;
            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE, inventoryTypeId);
                values.put(DataHelper.UNLOADING_GOOD_COUNT, scannedCount);
                values.put(DataHelper.UNLOADING_GOOD_NUMBER, partNum);
                values.put(DataHelper.UNLOADING_GOOD_VEHICLE_NUM, vehicleNum);
                values.put(DataHelper.UNLOADING_GOOD_SCAN_TIME, scanTime);
                values.put(DataHelper.UNLOADING_GOOD_DATATYPE, dataType);
                values.put(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE_DESC, inventoryType);
                values.put(DataHelper.UNLOADING_GOOD_TOTAL_BOXES, totalBoxes);
                values.put(DataHelper.UNLOADING_GOOD_WH_CODE, warehouseCode);
                values.put(DataHelper.UNLOADING_GOOD_PRODUCT_NAME, productName);
                values.put(DataHelper.UNLOADING_GOOD_PRODUCT_ID, productId);
                values.put(DataHelper.UNLOADING_GOOD_PART_ID, partId);
                values.put(DataHelper.UNLOADING_GOOD_IS_PRODUCT, isProduct);
                retVal = database.insert(DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME, null,
                        values);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
            return retVal;
        }

        public void updateGoodPart(int isProduct, int productId, int partId, String vehicleNo, int inventoryTypeId, int count, String warehouseCode) {
            String scanTime = Utils.getScanTime();
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME
                        + " SET " + DataHelper.UNLOADING_GOOD_COUNT + " = " + count
                        + " WHERE "
                        + DataHelper.UNLOADING_GOOD_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                        + DataHelper.UNLOADING_GOOD_INVENTORY_TYPE + " = " + inventoryTypeId
                        + " and " + DataHelper.UNLOADING_GOOD_WH_CODE + " = '" + warehouseCode + "' AND ";
                if (isProduct == 1) {
                    updateSql += DataHelper.UNLOADING_GOOD_PRODUCT_ID + " = " + productId;
                } else {
                    updateSql += DataHelper.UNLOADING_GOOD_PART_ID + " = " + partId;
                }

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public UnloadingGoodPartData getGoodPart(int isProduct, int productId, int partId, String vehicleNo, int inventoryTypeId, String warehouseCode) {
            UnloadingGoodPartData model = null;
            String sql = "SELECT * FROM "
                    + DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME
                    + " WHERE "
                    + DataHelper.UNLOADING_GOOD_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.UNLOADING_GOOD_INVENTORY_TYPE + " = " + inventoryTypeId
                    + " AND " + DataHelper.UNLOADING_GOOD_WH_CODE + " = '" + warehouseCode + "' AND ";

            if (isProduct == 1) {
                sql += DataHelper.UNLOADING_GOOD_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.UNLOADING_GOOD_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int numberIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_NUMBER);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_SCAN_TIME);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_DATATYPE);
                int scannedCountIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_COUNT);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_TOTAL_BOXES);

                model = new UnloadingGoodPartData();
                model.setNumber(cursor.getString(numberIdx));
                model.setVehicleNumber(cursor.getString(vehicleNoIdx));
                model.setScannedCount(cursor.getInt(scannedCountIdx));
                model.setScannedTime(cursor.getString(scanTimeIdx));
                model.setDataType(cursor.getString(dataTypeIdx));
                model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                model.setInventoryTypeId(cursor.getInt(inventoryTypeIdx));
                model.setTotalBoxes(cursor.getInt(totalBoxesIdx));
            }
            if (cursor != null)
                cursor.close();

            return model;
        }

        public ArrayList<UnloadingGoodPartData> getGoodParts(String vehicleNum, String warehouseCode) {
            ArrayList<UnloadingGoodPartData> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_GOOD_VEHICLE_NUM + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNLOADING_GOOD_COUNT + " != 0 AND "
                    + DataHelper.UNLOADING_GOOD_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int numberIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_NUMBER);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_SCAN_TIME);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_DATATYPE);
                int scannedCountIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_COUNT);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_TOTAL_BOXES);

                int isProductIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_IS_PRODUCT);
                int productNameIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_PRODUCT_NAME);
                int productIdIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_PRODUCT_ID);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_PART_ID);

                do {
                    UnloadingGoodPartData model = new UnloadingGoodPartData();
                    model.setNumber(cursor.getString(numberIdx));
                    model.setVehicleNumber(cursor.getString(vehicleNoIdx));
                    model.setScannedCount(cursor.getInt(scannedCountIdx));
                    model.setScannedTime(cursor.getString(scanTimeIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                    model.setInventoryTypeId(cursor.getInt(inventoryTypeIdx));
                    model.setTotalBoxes(cursor.getInt(totalBoxesIdx));

                    model.setIsProduct(cursor.getInt(isProductIdx));
                    model.setProductName(cursor.getString(productNameIdx));
                    model.setProductId(cursor.getInt(productIdIdx));
                    model.setPartId(cursor.getInt(partIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public ArrayList<UnloadingGoodPartData> getGoodParts(int isProduct, int productId, int partId, String vehicleNum, String warehouseCode) {
            ArrayList<UnloadingGoodPartData> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNLOADING_GOOD_DATA_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_GOOD_VEHICLE_NUM + " = '" + vehicleNum + "'"
                    + " and " + DataHelper.UNLOADING_GOOD_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.UNLOADING_GOOD_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.UNLOADING_GOOD_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int numberIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_NUMBER);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_SCAN_TIME);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_DATATYPE);
                int scannedCountIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_COUNT);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_INVENTORY_TYPE);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_TOTAL_BOXES);
                int isProductIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_IS_PRODUCT);
                int productNameIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_PRODUCT_NAME);
                int productIdIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_PRODUCT_ID);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNLOADING_GOOD_PART_ID);

                do {
                    UnloadingGoodPartData model = new UnloadingGoodPartData();
                    model.setNumber(cursor.getString(numberIdx));
                    model.setVehicleNumber(cursor.getString(vehicleNoIdx));
                    model.setScannedCount(cursor.getInt(scannedCountIdx));
                    model.setScannedTime(cursor.getString(scanTimeIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                    model.setInventoryTypeId(cursor.getInt(inventoryTypeIdx));
                    model.setTotalBoxes(cursor.getInt(totalBoxesIdx));

                    model.setIsProduct(cursor.getInt(isProductIdx));
                    model.setProductName(cursor.getString(productNameIdx));
                    model.setProductId(cursor.getInt(productIdIdx));
                    model.setPartId(cursor.getInt(partIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

    public class Exceptions {
        public void deleteExceptionsdata(String vehicleNo, String warehouseCode) {
            open();
            try {

                String deleteSql = "DELETE  FROM " + DataHelper.EXCEPTION_TABLE_NAME
                        + " WHERE " + DataHelper.EXCEPTION_VEHICLE_NUM + " = '"
                        + vehicleNo + "' AND " + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "'";
                database.execSQL(deleteSql);

            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public long insertException(Context context, String dataType, ExceptionModel exceptionModel, String partNum, String vehicleNum, String warehouseCode) {
            open();
            long retVal = -1;
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;

            try {
                ContentValues values = new ContentValues();
                values.put(DataHelper.EXCEPTION_TYPE_ID, exceptionModel.getExcepType());
                values.put(DataHelper.EXCEPTION_COUNT, exceptionModel.getExceptionCount());
                values.put(DataHelper.EXCEPTION_PART_NUM, partNum);
                values.put(DataHelper.EXCEPTION_VEHICLE_NUM, vehicleNum);
                values.put(DataHelper.EXCEPTION_REASON, exceptionModel.getExcepDesc());
                values.put(DataHelper.EXCEPTION_SCAN_TIME, exceptionModel.getScanTime());
                values.put(DataHelper.EXCEPTION_TRUCK_IMAGE, gson.toJson(exceptionModel.getImgBase64s()));
                values.put(DataHelper.EXCEPTION_DATATYPE, dataType);
                values.put(DataHelper.EXCEPTION_INVENTORY_TYPE_DESC, exceptionModel.getInventoryTypeDesc());
                values.put(DataHelper.EXCEPTION_INVENTORY_TYPE, exceptionModel.getInventoryType());
                values.put(DataHelper.EXCEPTION_WH_CODE, warehouseCode);
                values.put(DataHelper.EXCEPTION_IS_PRODUCT, exceptionModel.getIsProduct());
                values.put(DataHelper.EXCEPTION_PRODUCT_ID, exceptionModel.getProductId());
                values.put(DataHelper.EXCEPTION_PART_ID, exceptionModel.getPartId());
                values.put(DataHelper.EXCEPTION_PRODUCT_NAME, exceptionModel.getProductName());

                retVal = database.insert(DataHelper.EXCEPTION_TABLE_NAME, null,
                        values);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
            return retVal;
        }

        public int getProductExceptionCount(int isProduct, int productId, int partId, String dataType, String vehicleNo, String warehouseCode) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " = " + 12
                    + " AND " + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.EXCEPTION_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.EXCEPTION_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getDamagedExceptionCount(int isProduct, int productId, int partId, String dataType, String vehicleNo, String warehouseCode) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " = " + 13
                    + " AND " + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.EXCEPTION_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.EXCEPTION_PART_ID + " = " + partId;
            }

            Utils.logD("Damaged Count Query: " + sql);

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getShortageCount(int isProduct, int productId, int partId, String dataType, String vehicleNo, String warehouseCode) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " == " + 11
                    + " AND " + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.EXCEPTION_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.EXCEPTION_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getExceptionCount(int isProduct, int productId, int partId, String dataType, String vehicleNo, String warehouseCode) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "'"
                    + " AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " != " + 10
                    + " AND " + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.EXCEPTION_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.EXCEPTION_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public int getExtrasCount(int isProduct, int productId, int partId, String dataType, String vehicleNo, String warehouseCode) {
            int count = 0;
            openRead();
            String sql = "SELECT SUM(" + DataHelper.EXCEPTION_COUNT + ") FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_DATATYPE + " = '" + dataType + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNo + "' AND "
                    + DataHelper.EXCEPTION_TYPE_ID + " == " + 10
                    + " AND " + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.EXCEPTION_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.EXCEPTION_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                count = cursor.getInt(0);
            }
            if (cursor != null)
                cursor.close();
            close();
            return count;
        }

        public ArrayList<ExceptionModel> getExceptions(String vehicleNum, String warehouseCode) {
            ArrayList<ExceptionModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNum + "' AND "
                    + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_NUM);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_SCAN_TIME);
                int exceptionCountIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_COUNT);
                int exceptionTypeIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int exceptionReasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_REASON);
                int imageIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TRUCK_IMAGE);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_DATATYPE);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_INVENTORY_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_INVENTORY_TYPE);
                int isProductIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_IS_PRODUCT);
                int productIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PRODUCT_ID);
                int partIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_ID);
                int productNameIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PRODUCT_NAME);

                do {
                    ExceptionModel model = new ExceptionModel();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNum(cursor.getString(vehicleNoIdx));
                    model.setExcepDesc(cursor.getString(exceptionReasonIdx));
                    model.setExceptionCount(cursor.getInt(exceptionCountIdx));
                    String base64sStr = cursor.getString(imageIdx);
                    Type token = new TypeToken<List<String>>() {
                    }.getType();
                    model.setImgBase64s((List<String>) gson.fromJson(base64sStr, token));
                    model.setExcepType(cursor.getInt(exceptionTypeIdIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                    model.setInventoryType(cursor.getInt(inventoryTypeIdx));
                    model.setIsProduct(cursor.getInt(isProductIdx));
                    model.setProductName(cursor.getString(productNameIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setProductId(cursor.getInt(productIdIdx));
                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public ArrayList<ExceptionModel> getExceptions(int isProduct, int productId, int partId, int inventoryTypeId, String vehicleNum, String warehouseCode) {
            ArrayList<ExceptionModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.EXCEPTION_TABLE_NAME + " WHERE "
                    + DataHelper.EXCEPTION_INVENTORY_TYPE + " = '" + inventoryTypeId + "' AND "
                    + DataHelper.EXCEPTION_VEHICLE_NUM + " = '" + vehicleNum + "' AND "
                    + DataHelper.EXCEPTION_WH_CODE + " = '" + warehouseCode + "' AND ";
            if (isProduct == 1) {
                sql += DataHelper.EXCEPTION_PRODUCT_ID + " = " + productId;
            } else {
                sql += DataHelper.EXCEPTION_PART_ID + " = " + partId;
            }

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_NUM);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_VEHICLE_NUM);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_SCAN_TIME);
                int exceptionCountIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_COUNT);
                int exceptionTypeIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TYPE_ID);
                int exceptionReasonIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_REASON);
                int imageIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_TRUCK_IMAGE);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_DATATYPE);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_INVENTORY_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_INVENTORY_TYPE);
                int isProductIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_IS_PRODUCT);
                int productIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PRODUCT_ID);
                int partIdIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PART_ID);
                int productNameIdx = cursor.getColumnIndex(DataHelper.EXCEPTION_PRODUCT_NAME);

                do {
                    ExceptionModel model = new ExceptionModel();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNum(cursor.getString(vehicleNoIdx));
                    model.setExcepDesc(cursor.getString(exceptionReasonIdx));
                    model.setExceptionCount(cursor.getInt(exceptionCountIdx));
                    String base64sStr = cursor.getString(imageIdx);
                    Type token = new TypeToken<List<String>>() {
                    }.getType();
                    model.setImgBase64s((List<String>) gson.fromJson(base64sStr, token));
                    model.setExcepType(cursor.getInt(exceptionTypeIdIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                    model.setInventoryType(cursor.getInt(inventoryTypeIdx));
                    model.setIsProduct(cursor.getInt(isProductIdx));
                    model.setProductName(cursor.getString(productNameIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setProductId(cursor.getInt(productIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

    public class Nodes {
        public long insertNodeData(List<CustomerMasterModel> nodeMasterModels) {
            open();
            long retVal = -1, id = -1;
            try {
                for (CustomerMasterModel nodeMasterModel : nodeMasterModels) {
                    id = exists(nodeMasterModel.getId());
                    if (id == -1) {
                        ContentValues values = new ContentValues();

                        values.put(DataHelper.CUST_ID, nodeMasterModel.getId());
                        values.put(DataHelper.CUST_NAME, nodeMasterModel.getCustomerName());
                        values.put(DataHelper.CUST_CUST_CODE, nodeMasterModel.getCustomerCode());
                        values.put(DataHelper.CUST_ADDRESS1, nodeMasterModel.getAddress1());
                        values.put(DataHelper.CUST_ADDRESS2, nodeMasterModel.getAddress2());
                        values.put(DataHelper.CUST_ADDRESS3, nodeMasterModel.getAddress3());
                        values.put(DataHelper.CUST_CITY, nodeMasterModel.getCity());
                        values.put(DataHelper.CUST_STATE, nodeMasterModel.getState());
                        values.put(DataHelper.CUST_PINCODE, nodeMasterModel.getPinCode());

                        values.put(DataHelper.CUST_GSTIN, nodeMasterModel.getGstin());
                        values.put(DataHelper.CUST_PRIMARY_CONTACT_NAME, nodeMasterModel.getPrimaryContactName());
                        values.put(DataHelper.CUST_SECONDARY_CONTACT_NAME, nodeMasterModel.getSecondaryContactName());

                        values.put(DataHelper.CUST_PRIMARY_PHONE_NUMBER, nodeMasterModel.getPrimaryPhoneNumber());
                        values.put(DataHelper.CUST_SECONDARY_PHONE_NUMBER, nodeMasterModel.getSecondaryPhoneNumber());
                        values.put(DataHelper.CUST_PRIMARY_ALT_PHONE_NUMBER, nodeMasterModel.getPrimaryAltPhoneNumber());
                        values.put(DataHelper.CUST_SECONDARY_ALT_PHONE_NUMBER, nodeMasterModel.getSecondaryAltPhoneNumber());

                        values.put(DataHelper.CUST_PRIMARY_EMAIL_ID, nodeMasterModel.getPrimaryEmailId());
                        values.put(DataHelper.CUST_SECONDARY_EMAIL_ID, nodeMasterModel.getSecondaryEmailId());

                        values.put(DataHelper.CUST_PRIMARY_DEPT, nodeMasterModel.getPrimaryDept());
                        values.put(DataHelper.CUST_SECONDARY_DEPT, nodeMasterModel.getSecondaryDept());
                        values.put(DataHelper.CUST_PAN, nodeMasterModel.getPan());

                        values.put(DataHelper.CUST_CREATED_BY, nodeMasterModel.getCreatedBy());
                        values.put(DataHelper.CUST_UPDATED_BY, nodeMasterModel.getUpdatedBy());

                        retVal = database.insert(DataHelper.CUST_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int nodeId) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.CUST_KEY_ID
                        + " FROM " + DataHelper.CUST_TABLE_NAME
                        + " WHERE " + DataHelper.CUST_ID
                        + " = " + nodeId;

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public ArrayList<CustomerMasterModel> getCustomersData() {
            ArrayList<CustomerMasterModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.CUST_TABLE_NAME;

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int nodeIdIdx = cursor.getColumnIndex(DataHelper.CUST_ID);
                int nodeNameIdx = cursor.getColumnIndex(DataHelper.CUST_NAME);
                int custCodeIdx = cursor.getColumnIndex(DataHelper.CUST_CUST_CODE);
                int addres1Idx = cursor.getColumnIndex(DataHelper.CUST_ADDRESS1);
                int addres2Idx = cursor.getColumnIndex(DataHelper.CUST_ADDRESS2);
                int addres3Idx = cursor.getColumnIndex(DataHelper.CUST_ADDRESS3);
                int stateIdx = cursor.getColumnIndex(DataHelper.CUST_STATE);
                int cityIdx = cursor.getColumnIndex(DataHelper.CUST_CITY);
                int pincodeIdx = cursor.getColumnIndex(DataHelper.CUST_PINCODE);
                int createdByIdx = cursor.getColumnIndex(DataHelper.CUST_CREATED_BY);
                int createdTimestampIdx = cursor.getColumnIndex(DataHelper.CUST_CREATED_TIMESTAMP);
                int updatedByIdx = cursor.getColumnIndex(DataHelper.CUST_UPDATED_BY);
                int updatedTimestampIdx = cursor.getColumnIndex(DataHelper.CUST_UPDATED_TIMESTAMP);

                int gstinIdx = cursor.getColumnIndex(DataHelper.CUST_GSTIN);
                int primaryContactNameIdx = cursor.getColumnIndex(DataHelper.CUST_PRIMARY_CONTACT_NAME);
                int secondaryContactNameIdx = cursor.getColumnIndex(DataHelper.CUST_SECONDARY_CONTACT_NAME);
                int primaryPhoneIdx = cursor.getColumnIndex(DataHelper.CUST_PRIMARY_PHONE_NUMBER);
                int secondaryPhoneIdx = cursor.getColumnIndex(DataHelper.CUST_SECONDARY_PHONE_NUMBER);
                int primaryAltPhoneIdx = cursor.getColumnIndex(DataHelper.CUST_PRIMARY_ALT_PHONE_NUMBER);
                int secondaryAltPhoneIdx = cursor.getColumnIndex(DataHelper.CUST_SECONDARY_ALT_PHONE_NUMBER);
                int primaryEmailIdx = cursor.getColumnIndex(DataHelper.CUST_PRIMARY_EMAIL_ID);
                int secondaryEmailIdx = cursor.getColumnIndex(DataHelper.CUST_SECONDARY_EMAIL_ID);
                int primaryDeptIdx = cursor.getColumnIndex(DataHelper.CUST_PRIMARY_DEPT);
                int secondaryDeptIdx = cursor.getColumnIndex(DataHelper.CUST_SECONDARY_DEPT);
                int panIdx = cursor.getColumnIndex(DataHelper.CUST_PAN);

                do {
                    CustomerMasterModel model = new CustomerMasterModel();
                    model.setId(cursor.getInt(nodeIdIdx));
                    model.setCustomerName(cursor.getString(nodeNameIdx));
                    model.setAddress1(cursor.getString(addres1Idx));
                    model.setAddress2(cursor.getString(addres2Idx));
                    model.setAddress3(cursor.getString(addres3Idx));
                    model.setState(cursor.getString(stateIdx));
                    model.setPinCode(String.valueOf(cursor.getInt(pincodeIdx)));

                    model.setCity(cursor.getString(cityIdx));
                    model.setCreatedBy(cursor.getString(createdByIdx));
                    model.setUpdatedBy(cursor.getString(updatedByIdx));
                    model.setCustomerCode(cursor.getString(custCodeIdx));

                    model.setGstin(cursor.getString(gstinIdx));
                    model.setPrimaryContactName(cursor.getString(primaryContactNameIdx));
                    model.setSecondaryContactName(cursor.getString(secondaryContactNameIdx));
                    model.setPrimaryPhoneNumber(cursor.getString(primaryPhoneIdx));
                    model.setSecondaryPhoneNumber(cursor.getString(secondaryPhoneIdx));
                    model.setPrimaryAltPhoneNumber(cursor.getString(primaryAltPhoneIdx));
                    model.setSecondaryAltPhoneNumber(cursor.getString(secondaryAltPhoneIdx));
                    model.setPrimaryEmailId(cursor.getString(primaryEmailIdx));
                    model.setSecondaryEmaildId(cursor.getString(secondaryEmailIdx));
                    model.setPrimaryDept(cursor.getString(primaryDeptIdx));
                    model.setSecondaryDept(cursor.getString(secondaryDeptIdx));
                    model.setPan(cursor.getString(panIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.CUST_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class PickListParts {
        public void updateAsSubmitted(String type, String data, int partId, int binId, String warehouseCode) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                        + " SET " + DataHelper.PICKLIST_IS_PICKED + " = " + 1
                        + " WHERE " + DataHelper.PICKLIST_TYPE + " = '" + type + "' AND "
                        + DataHelper.PICKLIST_DATA + " = '" + data + "' AND " + DataHelper.PICKLIST_PART_ID
                        + " = " + partId + " AND " + DataHelper.PICKLIST_BIN_ID + " = " + binId
                        + " AND " + DataHelper.PICKLIST_WH_CODE + " = '" + warehouseCode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateBoxCount(int boxCount, String type, String data, int id, String warehouseCode) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                        + " SET " + DataHelper.PICKLIST_TOTAL_BOXES + " = " + boxCount
                        + " WHERE " + DataHelper.PICKLIST_TYPE + " = '" + type + "'"
                        + DataHelper.PICKLIST_DATA + " = '" + data + "'";
                updateSql += " AND " + DataHelper.PICKLIST_UNIQUE_ID
                        + " = " + id + " AND " + DataHelper.PICKLIST_WH_CODE + " = '" + warehouseCode + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public long insertPickListData(List<WmsPartInvoiceModel> wmsPartInvoiceModels, String type, String data, String warehouseCode) {
            open();
            long retVal = -1, id = -1;
            try {
                for (WmsPartInvoiceModel wmsPartInvoiceModel : wmsPartInvoiceModels) {
                    id = exists(wmsPartInvoiceModel.getId(), warehouseCode);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PICKLIST_UNIQUE_ID, wmsPartInvoiceModel.getId());
                        values.put(DataHelper.PICKLIST_TYPE, type);
                        values.put(DataHelper.PICKLIST_DATA, data);
                        values.put(DataHelper.PICKLIST_PART_ID, wmsPartInvoiceModel.getPartId());
                        values.put(DataHelper.PICKLIST_PART_NO, wmsPartInvoiceModel.getPartNo());
                        values.put(DataHelper.PICKLIST_IS_PICKED, wmsPartInvoiceModel.getIsPicked());
                        values.put(DataHelper.PICKLIST_TOTAL_BOXES, wmsPartInvoiceModel.getTotalBoxes());
                        values.put(DataHelper.PICKLIST_TOTAL_QTY, wmsPartInvoiceModel.getTotalQty());
                        values.put(DataHelper.PICKLIST_BIN_NO, wmsPartInvoiceModel.getBinNo());
                        values.put(DataHelper.PICKLIST_SCAN_TIME, wmsPartInvoiceModel.getScanTime());
                        values.put(DataHelper.PICKLIST_XDFLAG, wmsPartInvoiceModel.getXdFlag());
                        values.put(DataHelper.PICKLIST_NEXT_DEST, wmsPartInvoiceModel.getNextDest());
                        values.put(DataHelper.PICKLIST_EXCEP_TYPE, wmsPartInvoiceModel.getExcepType());
                        values.put(DataHelper.PICKLIST_ORDER_NO, wmsPartInvoiceModel.getOrderNo());
                        values.put(DataHelper.PICKLIST_BIN_ID, wmsPartInvoiceModel.getBinId());
                        values.put(DataHelper.PICKLIST_WH_CODE, warehouseCode);
                        values.put(DataHelper.PICKLIST_INVOICE_LIST, gson.toJson(wmsPartInvoiceModel.getInvoiceList()));

                        retVal = database.insert(DataHelper.PICKLIST_PARTS_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int exists(int uniqueId, String warehouseCode) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PICKLIST_PART_KEY_ID
                        + " FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.PICKLIST_UNIQUE_ID
                        + " = " + uniqueId + " AND " + DataHelper.PICKLIST_WH_CODE + " = '" + warehouseCode + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public ArrayList<WmsPartInvoiceModel> getPickListData(String type, String data, int status, String warehouseCode) {
            ArrayList<WmsPartInvoiceModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME + " WHERE " + DataHelper.PICKLIST_TYPE
                    + " = '" + type + "' AND " + DataHelper.PICKLIST_DATA + " = '" + data + "' AND "
                    + DataHelper.PICKLIST_WH_CODE + " = '" + warehouseCode + "'";

            if (status == 0) {
                sql += " AND " + DataHelper.PICKLIST_IS_PICKED + " = " + status;
            }

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int uniqueIdIdx = cursor.getColumnIndex(DataHelper.PICKLIST_UNIQUE_ID);
                int partIdIdx = cursor.getColumnIndex(DataHelper.PICKLIST_PART_ID);
                int partNumIdx = cursor.getColumnIndex(DataHelper.PICKLIST_PART_NO);
                int totalBoxesIdx = cursor.getColumnIndex(DataHelper.PICKLIST_TOTAL_BOXES);
                int totalQtyIdx = cursor.getColumnIndex(DataHelper.PICKLIST_TOTAL_QTY);
                int isPickedIdx = cursor.getColumnIndex(DataHelper.PICKLIST_IS_PICKED);
                int binNumIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BIN_NO);
                int scanTimeIdx = cursor.getColumnIndex(DataHelper.PICKLIST_SCAN_TIME);
                int xdFlagIdx = cursor.getColumnIndex(DataHelper.PICKLIST_XDFLAG);
                int nextDestIdx = cursor.getColumnIndex(DataHelper.PICKLIST_NEXT_DEST);
                int excepTypeIdx = cursor.getColumnIndex(DataHelper.PICKLIST_EXCEP_TYPE);
                int orderNoIdx = cursor.getColumnIndex(DataHelper.PICKLIST_ORDER_NO);
                int binIdIdx = cursor.getColumnIndex(DataHelper.PICKLIST_BIN_ID);
                int invoicesIdx = cursor.getColumnIndex(DataHelper.PICKLIST_INVOICE_LIST);

                do {
                    WmsPartInvoiceModel model = new WmsPartInvoiceModel();
                    model.setId(cursor.getInt(uniqueIdIdx));
                    model.setPartNo(cursor.getString(partNumIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setTotalBoxes(cursor.getInt(totalBoxesIdx));
                    model.setTotalQty(cursor.getInt(totalQtyIdx));
                    model.setIsPicked(cursor.getInt(isPickedIdx));
                    model.setBinNo(cursor.getString(binNumIdx));
                    model.setScanTime(cursor.getString(scanTimeIdx));
                    model.setXdFlag(cursor.getInt(xdFlagIdx));
                    model.setNextDest(cursor.getString(nextDestIdx));
                    model.setExcepType(cursor.getInt(excepTypeIdx));
                    model.setOrderNo(cursor.getString(orderNoIdx));
                    model.setBinId(cursor.getInt(binIdIdx));
                    String invoicesStr = cursor.getString(invoicesIdx);
                    Type token = new TypeToken<List<InvoiceModel>>() {
                    }.getType();
                    model.setInvoiceList((List<InvoiceModel>) gson.fromJson(invoicesStr, token));
                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void delete(String type, String data, String warehouseCode) {
            open();
            String query = "DELETE FROM " + DataHelper.PICKLIST_PARTS_TABLE_NAME
                    + " WHERE " + DataHelper.PICKLIST_TYPE + "= '" + type + "' AND "
                    + DataHelper.PICKLIST_DATA + "= '" + data + "' AND "
                    + DataHelper.PICKLIST_WH_CODE + " = '" + warehouseCode + "'";
            database.execSQL(query);

            close();
        }

    }

    /********************** Unloading parts ****************/

    public class UnloadingParts {
        private Context context;
        private PartsScanningScreen partsScanningScreen;
        private ProgressDialog pDialog;
        private boolean isProgress = false;
        private UnloadingResponse unloadingResponse;
        private String vehicleNo, whCode, receiptType;

        private void showProgressDialog() {
            if (pDialog == null) {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage(context
                        .getString(R.string.progress_dialog_title));
                pDialog.setIndeterminate(true);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        public void updateExcepType(String excepType, String barcode, String warehouseCode, String receiptType) {
            String scanTime = Utils.getScanTime();
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET "
                        + DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE + " = '" + excepType + "', "
                        + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " = '" + Constants.TRUE + "'"
                        + " WHERE " + DataHelper.UNLOADING_PARTS_INVOICE_BARCODE + " ='" + barcode + "'"
                        + " and " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "' AND "
                        + DataHelper.UNLOADING_PARTS_RECEIPT_TYPE + " = '" + receiptType + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void updateScanCount(Context context, String barcode, int inventoryTypeId,
                                    int scannedCount, String warehouseCode, String receiptType) {
            String scanTime = Utils.getScanTime();
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET "
                        + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1, "
                        + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " = '" + Constants.FALSE + "' , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_INVENTORY_TYPE_ID + " = " + inventoryTypeId + " , "
//                        + DataHelper.UNLOADING_PARTS_INVOICE_SCANNED_COUNT + " = " + scannedInvoiceQty + " , "
                        + DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED + " = " + scannedCount + ", "
                        + DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME + " = '" + scanTime + "'"
                        + " WHERE " + DataHelper.UNLOADING_PARTS_INVOICE_BARCODE + " ='" + barcode + "'"
                        + " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "' AND "
                        + DataHelper.UNLOADING_PARTS_RECEIPT_TYPE + " = '" + receiptType + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
        }

        public int exists(String unique) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNLOADING_PARTS_COLUMN_ID
                        + " FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_PARTS_UNIQUE
                        + " = '" + unique + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public void saveData(Context mContext, UnloadingResponse response, String vehicleNumber, String warehouseCode) {
            if (!isProgress) {
                isProgress = true;
                context = mContext;
                partsScanningScreen = (PartsScanningScreen) context;
                unloadingResponse = response;
                vehicleNo = vehicleNumber;
                whCode = warehouseCode;
                new Save().execute();
            }
        }

        public boolean getPartsData(String vehicleNo, String warehouseCode) {
            Boolean isExists = false;
            Cursor cursor = null;
            openRead();
            String selectQuery = "SELECT * " + "FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehicleNo
                    + "' AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";
            Utils.logD(selectQuery);
            try {
                cursor = database.rawQuery(selectQuery, null);

                if (cursor.getCount() > 0)
                    isExists = true;
                else isExists = false;

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
            }

            return isExists;
        }

        public long cacheSheetData(Context mContext,
                                   UnloadingResponse response, String vehicleNo, String whCode) {


            int numberOfRecords = 0;
            boolean isExists = false;

            try {

                open();
                database.beginTransaction();

                Cursor cursor = null;
                HashMap<String, Integer> sheetDataCached = new HashMap<String, Integer>();
                HashMap<String, Integer> sortOrder = new HashMap<String, Integer>();


                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                ContentValues values = new ContentValues();
                for (ProductModel data : response.getData().getPartList()) {
                    if (exists(data.getUniqueKey()) == -1) {
                        String dataType = response.getData().getDataType();
                        String number = Utils.getNumber(data, dataType);
                        long retVal = -1;
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO, data.getDaNo());
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE, response.getData().getDataType());

                        values.put(DataHelper.UNLOADING_PARTS_VEHICLE_NO,
                                vehicleNo);
                        values.put(DataHelper.UNLOADING_PARTS_UNIQUE, data.getUniqueKey());
                        values.put(DataHelper.UNLOADING_WH_CODE, whCode);

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO,
                                data.getPartNo());

                        values.put(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM, data.getMasterBoxNo());
                        values.put(DataHelper.UNLOADING_PARTS_RECEIPT_TYPE, data.getReceiptType());

                        int partId = data.getPartId();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID, partId);

                        int tot_boxes = data.getTotalBoxes();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES, tot_boxes);

                        int tot_qty = data.getTotalQty();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY, tot_qty);


                        String invoiceNo = data.getInvoiceNo();
                        if (Utils.isValidString(invoiceNo))
                            values.put(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO, invoiceNo);

                        values.put(DataHelper.UNLOADING_PARTS_IS_PRODUCT, data.getIsProduct());
                        values.put(DataHelper.UNLOADING_PARTS_PRODUCT_NAME, data.getProductName());
                        values.put(DataHelper.UNLOADING_PARTS_PRODUCT_ID, data.getProductId());

                        int box_count = data.getTotalBoxes();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT, box_count);

                        int invoice_box_count = data.getTotalQty();
                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE, invoice_box_count);

                        values.put(DataHelper.UNLOADING_PARTS_INVOICE_BARCODE, data.getBarcode());

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED, data.getScannedBoxes()); //for now the scanned count is 0 in future the count should come from server if any boxes are scanned

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_STATUS, 0);

                        values.put(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED, Constants.FALSE); //by default the scan completed is false

                        retVal = database.insert(
                                DataHelper.UNLOADING_PARTS_TABLE_NAME, null, values);
                        if (retVal > -1)
                            numberOfRecords++;
                    }
                }
                database.setTransactionSuccessful();
                database.endTransaction();

                Utils.logD("Records : " + numberOfRecords);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close();
            }
            return numberOfRecords;

        }

        public void deleteUnloadingdata(String vehicleNo, String warehouseCode) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '"
                        + vehicleNo + "' AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public double getScannedQty(String vehNo, String warehouseCode) {
            openRead();
            int wt = 0;
            String sql = "SELECT SUM(" + DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY
                    + ") FROM " + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode
                    + "' GROUP BY " + DataHelper.UNLOADING_PARTS_COLUMN_PART_ID;
            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                wt = cursor.getInt(0);
            }

            Utils.logD(sql);
            Utils.logD("Total Wt 2 : " + wt);
            if (cursor != null)
                cursor.close();
            close();
            return wt;
        }

        public List<NewProductModel> getScannedParts(String warehouseCode) {
            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;

            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "' AND "
                    + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";
            ArrayList<NewProductModel> scannedParts = new ArrayList<NewProductModel>();

            Cursor allCursor = database.rawQuery(allDataSql, null);
            scannedParts = getUnloadingData(allCursor);

            Utils.logD("Scan Data End");
            close();
            return scannedParts;
        }

        /* *
          filterType :
          1 -> Part
          2 -> DA Number
          3 -> Invoice
         */
        public void getFilteredScanConsList(Context context, int tag, int filterType, String filterStr,
                                            boolean isInvoice, String warehouseCode) {
            InvoicesScreen invoicesScreen = null;
            PartsScanningScreen scanScreen = null;
            if (isInvoice)
                invoicesScreen = (InvoicesScreen) context;
            else
                scanScreen = (PartsScanningScreen) context;

            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;

            Utils.logD("Scan Data Start");

            String filterSql = "";
            if (Utils.isValidString(filterStr)) {
                if (filterType == 1) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_COLUMN_PART_NO + " LIKE '%" + filterStr + "%'";
                } else if (filterType == 2) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_COLUMN_DA_NO + " LIKE '%" + filterStr + "%'";
                } else if (filterType == 3) {
                    filterSql += " AND " + DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO + " LIKE '%" + filterStr + "%'";
                }
            }

            filterSql += " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";

            String allDataSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'" + filterSql;

            String toBeScannedSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' " + filterSql;

            String exceptionSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND " + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " ='"
                    + Constants.TRUE + "'" + filterSql;


            String scanInProgressSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND "
                    + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1" + filterSql;

            ArrayList<NewProductModel> toBeScannedDockets = new ArrayList<NewProductModel>();
            ArrayList<UnloadingGoodPartData> scanInProDockets = new ArrayList<>();
            ArrayList<ExceptionModel> exceptionDockets = new ArrayList<>();

            scanInProDockets = unloadingGoodParts.getGoodParts(Globals.vehicleNo, warehouseCode);
            if (tag == -1) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
//                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.toBeScannedTag) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
            } else if (tag == Constants.scanInProgressTag) {
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
//                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.exceptionTag) {
                Utils.logD(exceptionSql);
//                Cursor exceptionCursor = database.rawQuery(exceptionSql, null);
                exceptionDockets = exceptions.getExceptions(vehNo, warehouseCode);
                scanScreen.exceptionFragment.reloadData(exceptionDockets);
            } else if (tag == Constants.invoicesTag) {
                Cursor allCursor = database.rawQuery(allDataSql, null);
//                scanInProDockets = getUnloadingData(allCursor);
//                invoicesScreen.resetData(scanInProDockets);
            }

            Utils.logD("Scan Data End");
            close();
        }

        public void getScanConsList(Context context, int tag, boolean isInvoice, String warehouseCode) {
            InvoicesScreen invoicesScreen = null;
            PartsScanningScreen scanScreen = null;
            if (isInvoice)
                invoicesScreen = (InvoicesScreen) context;
            else
                scanScreen = (PartsScanningScreen) context;

            openRead();
            String vehNo = "";
            if (Utils.isValidString(Globals.vehicleNo))
                vehNo = Globals.vehicleNo;


            Utils.logD("Scan Data Start");

            String allDataSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo + "'"
                    + " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";

            String toBeScannedSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "'" + " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";

            String exceptionSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND " + DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " ='"
                    + Constants.TRUE + "'"
                    + " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";

            String scanInProgressSql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " = '" + vehNo
                    + "' AND "
                    + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1"
                    + " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";

            ArrayList<NewProductModel> toBeScannedDockets = new ArrayList<NewProductModel>();
            ArrayList<UnloadingGoodPartData> scanInProDockets = new ArrayList<>();
            ArrayList<ExceptionModel> exceptionDockets = new ArrayList<>();

            scanInProDockets = unloadingGoodParts.getGoodParts(Globals.vehicleNo, warehouseCode);
            if (tag == -1) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
//                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.toBeScannedTag) {
                Cursor toBeScannedCursor = database.rawQuery(toBeScannedSql, null);
                toBeScannedDockets = getUnloadingData(toBeScannedCursor);
                scanScreen.toBeScanned.reloadData(toBeScannedDockets);
            } else if (tag == Constants.scanInProgressTag) {
                Cursor scanInProCursor = database.rawQuery(scanInProgressSql, null);
//                scanInProDockets = getUnloadingData(scanInProCursor);
                scanScreen.scanInProgress.reloadData(scanInProDockets);
            } else if (tag == Constants.exceptionTag) {
                Utils.logD(exceptionSql);
//                Cursor exceptionCursor = database.rawQuery(exceptionSql, null);
                exceptionDockets = exceptions.getExceptions(vehNo, warehouseCode);
                scanScreen.exceptionFragment.reloadData(exceptionDockets);
            } else if (tag == Constants.invoicesTag) {
                Cursor allCursor = database.rawQuery(allDataSql, null);
//                scanInProDockets = getUnloadingData(allCursor);
//                invoicesScreen.resetData(scanInProDockets);
            }

            Utils.logD("Scan Data End");
            close();
        }

        public NewProductModel getUnloadingData(String barcode, String warehouseCode) {
            String sql = "SELECT * FROM "
                    + DataHelper.UNLOADING_PARTS_TABLE_NAME + " WHERE "
                    + DataHelper.UNLOADING_PARTS_INVOICE_BARCODE + " = '" + barcode + "' AND "
                    + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "'";
            NewProductModel newProductModel = null;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int colId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_ID);
                int vehNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_VEHICLE_NO);
                int partNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO);
                int partId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES);
                int totQty = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY);
                int invoiceNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int boxCount = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT);
                int qtyInvoice = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE);

                int qtyScanned = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED);
                int exceptionCountIdx = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT);
                int status = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME);

                int exceptionType = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE);

                int exceptionDesc = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION);

                int isException = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION);

                int images = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES);

                int scanCompleted = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED);
                int barcodeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_INVOICE_BARCODE);
                int daNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM);
                int receiptTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_RECEIPT_TYPE);
                int isProductIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_IS_PRODUCT);
                int productNameIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_PRODUCT_NAME);
                int productIdIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_PRODUCT_ID);

                newProductModel = new NewProductModel();
                int id = cursor.getInt(colId);
                newProductModel.setId(id);
                newProductModel.setReceiptType(cursor.getString(receiptTypeIdx));

                newProductModel.setMasterBoxNo(cursor.getString(masterBoxNumIdx));

                newProductModel.setIsProduct(cursor.getInt(isProductIdx));
                newProductModel.setProductName(cursor.getString(productNameIdx));
                newProductModel.setProductId(cursor.getInt(productIdIdx));

                String vehNumber = cursor.getString(vehNo);
                if (Utils.isValidString(vehNumber))
                    newProductModel.setVehicleNo(vehNumber);

                String partNum = cursor.getString(partNo);
                if (Utils.isValidString(partNum))
                    newProductModel.setPartNo(partNum);
                else
                    newProductModel.setPartNo("");

                newProductModel.setDaNo(cursor.getString(daNoIdx));
                newProductModel.setDataType(cursor.getString(dataTypeIdx));

                int partid = cursor.getInt(partId);
                newProductModel.setPartId(partid);

                int tot_boxes = cursor.getInt(totBoxes);
                newProductModel.setTotalBoxes(tot_boxes);

                int tot_qty = cursor.getInt(totQty);
                newProductModel.setTotalQty(tot_qty);

                int qty_scanned = cursor.getInt(qtyScanned);
                newProductModel.setScannedCount(qty_scanned);

                int exceptionCount = cursor.getInt(exceptionCountIdx);
                newProductModel.setExceptionCount(exceptionCount);

                String scanned_status = cursor.getString(status);
                newProductModel.setScanned(scanned_status);

                String scan_time = cursor.getString(scanTime);
                if (Utils.isValidString(scan_time))
                    newProductModel.setScanTime(scan_time);

                InvoiceModel invoiceModel = new InvoiceModel();

                int box_count = cursor.getInt(boxCount);
                invoiceModel.setNoOfBoxes(box_count);

                int qty_invoice = cursor.getInt(qtyInvoice);
                invoiceModel.setTotalQty(qty_invoice);

                String invoice = cursor.getString(invoiceNo);
                if (Utils.isValidString(invoice)) {
                    invoiceModel.setInvoiceNo(invoice);
                    newProductModel.setInvoiceNo(invoice);
                }

                invoiceModel.setBarcode(cursor.getString(barcodeIdx));
                newProductModel.setBarcode(cursor.getString(barcodeIdx));

                String exType = cursor.getString(exceptionType);
                if (Utils.isValidString(exType))
                    newProductModel.setExceptionType(exType);

                String exDesc = cursor.getString(exceptionDesc);
                if (Utils.isValidString(exDesc))
                    newProductModel.setExceptionDesc(exDesc);

                String isEx = cursor.getString(isException);
                if (Utils.isValidString(isEx))
                    newProductModel.setIsException(isEx);

                String exImage = cursor.getString(images);
                if (Utils.isValidString(exImage))
                    newProductModel.setImages(exImage);


                String isScanCompleted = cursor.getString(scanCompleted);
                if (Utils.isValidString(isScanCompleted))
                    newProductModel.setIsScanCompleted(isScanCompleted);

            }
            if (cursor != null)
                cursor.close();

            return newProductModel;
        }

        public ArrayList<NewProductModel> getUnloadingData(Cursor cursor) {
            ArrayList<NewProductModel> productList = new ArrayList<NewProductModel>();

            if (cursor.moveToFirst()) {
                int colId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_ID);
                int vehNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_VEHICLE_NO);
                int partNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_NO);
                int partId = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_PART_ID);
                int totBoxes = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES);
                int totQty = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_QTY);
                int invoiceNo = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int boxCount = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_BOX_COUNT);
                int qtyInvoice = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_INVOICE);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_MASTER_BOX_NUM);

                int qtyScanned = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED);
                int exceptionCountIdx = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT);
                int status = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_STATUS);
                int scanTime = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME);

                int exceptionType = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE);

                int exceptionDesc = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION);

                int isException = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_EXCEPTION);

                int images = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES);

                int scanCompleted = cursor
                        .getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED);

                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_INVOICE_NO);
                int daNoIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DA_NO);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_COLUMN_DATATYPE);
                int receiptTypeIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_RECEIPT_TYPE);
                int isProductIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_IS_PRODUCT);
                int productNameIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_PRODUCT_NAME);
                int productIdIdx = cursor.getColumnIndex(DataHelper.UNLOADING_PARTS_PRODUCT_ID);

                do {
                    NewProductModel newProductModel = new NewProductModel();

                    int id = cursor.getInt(colId);
                    newProductModel.setId(id);
                    newProductModel.setReceiptType(cursor.getString(receiptTypeIdx));

                    String vehNumber = cursor.getString(vehNo);
                    if (Utils.isValidString(vehNumber))
                        newProductModel.setVehicleNo(vehNumber);

                    String partNum = cursor.getString(partNo);
                    if (Utils.isValidString(partNum))
                        newProductModel.setPartNo(partNum);
                    else
                        newProductModel.setPartNo("");

                    newProductModel.setMasterBoxNo(cursor.getString(masterBoxNumIdx));

                    int partid = cursor.getInt(partId);
                    newProductModel.setPartId(partid);

                    newProductModel.setInvoiceNo(cursor.getString(invoiceNoIdx));
                    newProductModel.setDaNo(cursor.getString(daNoIdx));
                    newProductModel.setDataType(cursor.getString(dataTypeIdx));

                    int tot_boxes = cursor.getInt(totBoxes);
                    newProductModel.setTotalBoxes(tot_boxes);

                    newProductModel.setIsProduct(cursor.getInt(isProductIdx));
                    newProductModel.setProductName(cursor.getString(productNameIdx));
                    newProductModel.setProductId(cursor.getInt(productIdIdx));

                    int tot_qty = cursor.getInt(totQty);
                    newProductModel.setTotalQty(tot_qty);

                    int qty_scanned = cursor.getInt(qtyScanned);
                    newProductModel.setScannedCount(qty_scanned);

                    int exceptionCount = cursor.getInt(exceptionCountIdx);
                    newProductModel.setExceptionCount(exceptionCount);

                    String scanned_status = cursor.getString(status);
                    newProductModel.setScanned(scanned_status);

                    String scan_time = cursor.getString(scanTime);
                    if (Utils.isValidString(scan_time))
                        newProductModel.setScanTime(scan_time);

                    InvoiceModel invoiceModel = new InvoiceModel();

                    int box_count = cursor.getInt(boxCount);
                    invoiceModel.setNoOfBoxes(box_count);

                    int qty_invoice = cursor.getInt(qtyInvoice);
                    invoiceModel.setTotalQty(qty_invoice);

                    String invoice = cursor.getString(invoiceNo);
                    if (Utils.isValidString(invoice))
                        invoiceModel.setInvoiceNo(invoice);

                    String exType = cursor.getString(exceptionType);
                    if (Utils.isValidString(exType))
                        newProductModel.setExceptionType(exType);

                    String exDesc = cursor.getString(exceptionDesc);
                    if (Utils.isValidString(exDesc))
                        newProductModel.setExceptionDesc(exDesc);

                    String isEx = cursor.getString(isException);
                    if (Utils.isValidString(isEx))
                        newProductModel.setIsException(isEx);


                    String exImage = cursor.getString(images);
                    if (Utils.isValidString(exImage))
                        newProductModel.setImages(exImage);


                    String isScanCompleted = cursor.getString(scanCompleted);
                    if (Utils.isValidString(isScanCompleted))
                        newProductModel.setIsScanCompleted(isScanCompleted);
                    productList.add(newProductModel);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();

            Utils.logD(String.valueOf(productList.size()));

            return productList;
        }

        public void updatePartScan(Context context, String vehicleNo, int inventoryTypeId,
                                   int qtyScanned, String dataType,
                                   int isProduct, int productId, int partId, String warehouseCode, String receiptType) {
            String scanTime = Utils.getScanTime();
            PartsScanningScreen partsScanningScreen = (PartsScanningScreen) context;
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " SET " + DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED + " = " + qtyScanned
                        + " , " + DataHelper.UNLOADING_PARTS_COLUMN_STATUS + " = 1 ,"
                        + DataHelper.UNLOADING_PARTS_COLUMN_SCAN_TIME + " = '" + scanTime
                        + "' , " + DataHelper.UNLOADING_PARTS_COLUMN_INVENTORY_TYPE_ID + " = " + inventoryTypeId
                        + " WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND ";

                if (isProduct == 1) {
                    updateSql += DataHelper.UNLOADING_PARTS_PRODUCT_ID + " = " + productId;
                } else {
                    updateSql += DataHelper.UNLOADING_PARTS_COLUMN_PART_ID + " = " + partId;
                }

                updateSql += " and " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "' AND "
                        + DataHelper.UNLOADING_PARTS_RECEIPT_TYPE + " = '" + receiptType + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);

                String getDataSql = "SELECT " + DataHelper.UNLOADING_PARTS_COLUMN_QTY_SCANNED + " , " +
                        DataHelper.UNLOADING_PARTS_COLUMN_TOTAL_BOXES + " FROM "
                        + DataHelper.UNLOADING_PARTS_TABLE_NAME
                        + " WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND ";
                if (isProduct == 1) {
                    getDataSql += DataHelper.UNLOADING_PARTS_PRODUCT_ID + " = " + productId;
                } else {
                    getDataSql += DataHelper.UNLOADING_PARTS_COLUMN_PART_ID + " = " + partId;
                }

                getDataSql += " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "' AND "
                        + DataHelper.UNLOADING_PARTS_RECEIPT_TYPE + " = '" + receiptType + "'";
                Utils.logD(getDataSql);

                Cursor cursor = database.rawQuery(getDataSql, null);

                int totalQty = 0, scannedQty = 0;

                if (cursor.moveToFirst()) {
                    scannedQty = cursor.getInt(0);
                    totalQty = cursor.getInt(1);
                }

                if (scannedQty > 0 && scannedQty == totalQty) {
                    String updateScanStatusSql = "UPDATE " + DataHelper.UNLOADING_PARTS_TABLE_NAME
                            + " SET " + DataHelper.UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED + " = '" + Constants.TRUE + "' "
                            + "' WHERE " + DataHelper.UNLOADING_PARTS_VEHICLE_NO + " ='" + vehicleNo + "' AND ";
                    if (isProduct == 1) {
                        updateScanStatusSql += DataHelper.UNLOADING_PARTS_PRODUCT_ID + " = " + productId;
                    } else {
                        updateScanStatusSql += DataHelper.UNLOADING_PARTS_COLUMN_PART_ID + " = " + partId;
                    }
                    updateScanStatusSql += " AND " + DataHelper.UNLOADING_WH_CODE + " = '" + warehouseCode + "' AND "
                            + DataHelper.UNLOADING_PARTS_RECEIPT_TYPE + " = '" + receiptType + "'";
                    Utils.logD(updateScanStatusSql);
                    database.execSQL(updateScanStatusSql);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            partsScanningScreen.resetAllFragmentData();
        }

        public class Save extends AsyncTask<Object, Object, Object> {
            @Override
            protected void onPreExecute() {
                showProgressDialog();
                Utils.logD("Start saving the data");
            }

            @Override
            protected List<ProductModel> doInBackground(Object... arg0) {

                try {
                    if (unloadingResponse.getData() != null)
                        sharedPreferences.set(Constants.DATA_TYPE, unloadingResponse.getData().getDataType());
                    long c = cacheSheetData(context, unloadingResponse, vehicleNo, whCode);
                    Utils.logD("Count : " + c);
                } catch (Exception e) {
                    Utils.logE(e.toString());
                }
                return null;

            }

            @Override
            protected void onPostExecute(Object result) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                    partsScanningScreen.resetAllFragmentData();
                    pDialog = null;
                    isProgress = false;
                    Utils.logD("Loaded data");
                }
                super.onPostExecute(result);
            }
        }
    }

    /******************** Vendor Master **************************/

    public class VendorMaster {


        public void truncateTable() {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.VENDOR_MASTER_TABLE_NAME;
                database.execSQL(query);
            }
            close();
        }

        public void clearAll() {
            open();
            if (database != null) {
                String query = "DELETE FROM " + DataHelper.VENDOR_MASTER_TABLE_NAME;
                database.execSQL(query);
            }
            close();
        }

        public int saveVendorMaster(List<VendorMasterModel> masterList) {
            int numberOfRecords = 0;
            clearAll();

            open();
            database.beginTransaction();

            ContentValues values = new ContentValues();
            for (VendorMasterModel b : masterList) {
                String code = b.getVendorCode();
                if (Utils.isValidString(code))
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_CODE, code);
                else
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_CODE, "");

                String type = b.getVendorType();
                if (Utils.isValidString(type))
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_TYPE, type);
                else
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_TYPE, "");

                String name = b.getVendorName();
                if (Utils.isValidString(name))
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_NAME, name);
                else
                    values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_NAME, "");

//                boolean active = b.getActiveFlag();
//                values.put(DataHelper.VENDOR_MASTER_COLUMN_ACTIVE_FLAG, active);

                int id = b.getId();
                values.put(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_ID, id);


                long retVal = database.insert(DataHelper.VENDOR_MASTER_TABLE_NAME, null, values);
                if (retVal > -1)
                    numberOfRecords++;
            }

            database.setTransactionSuccessful();
            database.endTransaction();
            close();
            Utils.logD("Vendor master records : " + numberOfRecords);
            return numberOfRecords;
        }


        public List<VendorMasterModel> getAll() {
            openRead();
            List<VendorMasterModel> items = new ArrayList<VendorMasterModel>();
            Cursor cursor = null;
            String sql;
            try {
                sql = "SELECT *" + " FROM " + DataHelper.VENDOR_MASTER_TABLE_NAME;
                cursor = database.rawQuery(sql, null);
                Log.e("query", sql);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    VendorMasterModel masterModel = new VendorMasterModel();
                    int id = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_ID);
                    int code = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_CODE);
                    int name = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_NAME);
                    int type = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_VENDOR_TYPE);
                    int active = cursor.getColumnIndex(DataHelper.VENDOR_MASTER_COLUMN_ACTIVE_FLAG);


                    int vendor_id = cursor.getInt(id);
//                    masterModel.setVendorId(vendor_id);


                    String vendor_code = cursor.getString(code);
                    if (Utils.isValidString(vendor_code))
                        masterModel.setVendorCode(vendor_code);

                    String vendor_name = cursor.getString(name);
                    if (Utils.isValidString(vendor_name))
                        masterModel.setVendorName(vendor_name);


                    String vendor_type = cursor.getString(type);
                    if (Utils.isValidString(vendor_type))
                        masterModel.setVendorType(vendor_type);


//                    String is_active = cursor.getString(active);
//                    masterModel.setActiveFlag(Boolean.parseBoolean(is_active));

                    items.add(masterModel);
                    cursor.moveToNext();
                }

            } catch (Exception e) {
                Utils.logE(e.toString());
                cursor = null;
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return items;
        }
    }

    public class InboundInspection {
        public void deleteInspectionData(String unique) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.INBOUND_DATA_TABLE_NAME
                        + " WHERE " + DataHelper.INBOUND_UNIQUE + " = '" + unique + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(String unique) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.INBOUND_KEY_ID
                        + " FROM " + DataHelper.INBOUND_DATA_TABLE_NAME
                        + " WHERE "
                        + DataHelper.INBOUND_UNIQUE + " = '"
                        + unique + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }

            return id;
        }

        public long insertInboundInspectionData(List<InspectionPart> inspectionParts, String warehouseCode) {
            open();

            long retVal = -1, id = -1;
            try {
                for (InspectionPart inspectionPart : inspectionParts) {
                    id = exists(inspectionPart.getUniqueKey());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.INBOUND_PART_ID, inspectionPart.getPartId());
                        values.put(DataHelper.INBOUND_PART_NUM, inspectionPart.getPartNo());
                        values.put(DataHelper.INBOUND_QUANTITY, inspectionPart.getQty());
                        values.put(DataHelper.INBOUND_NO_OF_MASTER_CARTONS, inspectionPart.getNoOfMasterCartons());
                        values.put(DataHelper.INBOUND_NO_OF_EXCEPTIONS, inspectionPart.getNoOfExceptions());
                        values.put(DataHelper.INBOUND_STATUS, inspectionPart.getStatus());
                        values.put(DataHelper.INBOUND_VEHICLE_NUMBER, inspectionPart.getVehicleNumber());
                        values.put(DataHelper.INBOUND_INWARD_NUMBER, inspectionPart.getInwardNumber());
                        values.put(DataHelper.INBOUND_DA_NO, inspectionPart.getDaNo());
                        values.put(DataHelper.INBOUND_INVOICE_NO, inspectionPart.getInvoiceNo());
                        values.put(DataHelper.INBOUND_MASTER_BOX_NO, inspectionPart.getMasterBoxNo());
                        values.put(DataHelper.INBOUND_RECEIPT_TYPE, inspectionPart.getReceiptType());

                        values.put(DataHelper.INBOUND_EXCEP_TYPE, inspectionPart.getExceptType());
                        values.put(DataHelper.INBOUND_EXCEPT_TYPE_DESC, inspectionPart.getExceptTypeDescription());
                        values.put(DataHelper.INBOUND_INVENTORY_TYPE, inspectionPart.getInventoryTypeId());
                        values.put(DataHelper.INBOUND_INVENTORY_TYPE_DESC, inspectionPart.getInventoryTypeDesc());
                        values.put(DataHelper.INBOUND_WH_CODE, warehouseCode);

                        values.put(DataHelper.INBOUND_UNIQUE, inspectionPart.getUniqueKey());
                        values.put(DataHelper.INBOUND_IS_PRODUCT, inspectionPart.getIsProduct());
                        values.put(DataHelper.INBOUND_PRODUCT_NAME, inspectionPart.getProductName());
                        values.put(DataHelper.INBOUND_PRODUCT_ID, inspectionPart.getProductId());

                        retVal = database.insert(DataHelper.INBOUND_DATA_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<InspectionPart> getFilteredInboundInspectionData(String filteredText, String warehouseCode) {
            ArrayList<InspectionPart> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INBOUND_DATA_TABLE_NAME + " WHERE "
                    + DataHelper.INBOUND_STATUS + " = 0 AND "
                    + DataHelper.INBOUND_WH_CODE + " = '" + warehouseCode + "'"
                    + " AND "
                    + DataHelper.INBOUND_INVENTORY_TYPE_DESC + " LIKE '%" + filteredText + "%' OR "
                    + DataHelper.INBOUND_PART_NUM + " LIKE '%" + filteredText + "%' OR "
                    + DataHelper.INBOUND_EXCEPT_TYPE_DESC + " LIKE '%" + filteredText + "%'";

            Utils.logD(sql);
            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.INBOUND_PART_ID);
                int masterCartonsIdx = cursor.getColumnIndex(DataHelper.INBOUND_NO_OF_MASTER_CARTONS);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.INBOUND_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.INBOUND_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_INWARD_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.INBOUND_QUANTITY);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_DATATYPE);
                int daNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_DA_NO);
                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVOICE_NO);
                int masterBoxNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_MASTER_BOX_NO);

                int excepTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_EXCEP_TYPE);
                int excepTypeDescIdx = cursor.getColumnIndex(DataHelper.INBOUND_EXCEPT_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVENTORY_TYPE);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVENTORY_TYPE_DESC);
                int receiptTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_RECEIPT_TYPE);

                do {
                    InspectionPart model = new InspectionPart();
                    model.setPartNo(cursor.getString(partNoIdx));
                    if (Utils.isValidString(cursor.getString(vehicleNoIdx)))
                        model.setVehicleNumber(cursor.getString(vehicleNoIdx).toUpperCase());
                    model.setInwardNumber(cursor.getString(inwardNoIdx));
                    model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setNoOfMasterCartons(cursor.getInt(masterCartonsIdx));
                    model.setQty(cursor.getInt(qtyIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setDaNo(cursor.getString(daNoIdx));
                    model.setInvoiceNo(cursor.getString(invoiceNoIdx));
                    model.setMasterBoxNo(cursor.getString(masterBoxNoIdx));
                    model.setExceptType(cursor.getInt(excepTypeIdx));
                    model.setExceptTypeDescription(cursor.getString(excepTypeDescIdx));
                    model.setInventoryTypeId(cursor.getInt(inventoryTypeIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                    model.setReceiptType(cursor.getString(receiptTypeIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public ArrayList<InspectionPart> getInboundInspectionData(String warehouseCode) {
            ArrayList<InspectionPart> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INBOUND_DATA_TABLE_NAME + " WHERE "
                    + DataHelper.INBOUND_STATUS + " = 0 AND " + DataHelper.INBOUND_WH_CODE + " ='" + warehouseCode + "'";

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.INBOUND_PART_ID);
                int masterCartonsIdx = cursor.getColumnIndex(DataHelper.INBOUND_NO_OF_MASTER_CARTONS);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.INBOUND_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.INBOUND_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_INWARD_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.INBOUND_QUANTITY);
                int dataTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_DATATYPE);
                int daNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_DA_NO);
                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVOICE_NO);
                int masterBoxNoIdx = cursor.getColumnIndex(DataHelper.INBOUND_MASTER_BOX_NO);

                int excepTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_EXCEP_TYPE);
                int excepTypeDescIdx = cursor.getColumnIndex(DataHelper.INBOUND_EXCEPT_TYPE_DESC);
                int inventoryTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVENTORY_TYPE);
                int inventoryTypeDescIdx = cursor.getColumnIndex(DataHelper.INBOUND_INVENTORY_TYPE_DESC);
                int receiptTypeIdx = cursor.getColumnIndex(DataHelper.INBOUND_RECEIPT_TYPE);

                int isProductIdx = cursor.getColumnIndex(DataHelper.INBOUND_IS_PRODUCT);
                int productNameIdx = cursor.getColumnIndex(DataHelper.INBOUND_PRODUCT_NAME);
                int productIdIdx = cursor.getColumnIndex(DataHelper.INBOUND_PRODUCT_ID);

                do {
                    InspectionPart model = new InspectionPart();
                    model.setPartNo(cursor.getString(partNoIdx));
                    if (Utils.isValidString(cursor.getString(vehicleNoIdx)))
                        model.setVehicleNumber(cursor.getString(vehicleNoIdx).toUpperCase());
                    model.setInwardNumber(cursor.getString(inwardNoIdx));
                    model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setNoOfMasterCartons(cursor.getInt(masterCartonsIdx));
                    model.setQty(cursor.getInt(qtyIdx));
                    model.setDataType(cursor.getString(dataTypeIdx));
                    model.setDaNo(cursor.getString(daNoIdx));
                    model.setInvoiceNo(cursor.getString(invoiceNoIdx));
                    model.setMasterBoxNo(cursor.getString(masterBoxNoIdx));
                    model.setExceptType(cursor.getInt(excepTypeIdx));
                    model.setExceptTypeDescription(cursor.getString(excepTypeDescIdx));
                    model.setInventoryTypeId(cursor.getInt(inventoryTypeIdx));
                    model.setInventoryTypeDesc(cursor.getString(inventoryTypeDescIdx));
                    model.setReceiptType(cursor.getString(receiptTypeIdx));
                    model.setIsProduct(cursor.getInt(isProductIdx));
                    model.setProductId(cursor.getInt(productIdIdx));
                    model.setProductName(cursor.getString(productNameIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }
            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.INBOUND_DATA_TABLE_NAME;
            database.execSQL(query);

            close();
        }
    }

    public class UnpackingBoxes {
        public UnpackingBox getBox(String vehicleNum, String inwardNum, String masterboxNum) {
            String sql = "SELECT * FROM "
                    + DataHelper.UNPACKING_BOX_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterboxNum + "'";
            UnpackingBox unpackingBox = null;

            Cursor cursor = database.rawQuery(sql, null);
            if (cursor.moveToFirst()) {
                int qtyIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_QUANTITY);

                int statusIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNumIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNumIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int boxIdIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_BOX_ID);

                int boxStatusIdx = cursor
                        .getColumnIndex(DataHelper.UNPACKING_BOX_STATUS);

                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);

                unpackingBox = new UnpackingBox();

                unpackingBox.setMasterBoxNumber(cursor.getString(masterBoxNumIdx));

                String vehNumber = cursor.getString(vehicleNumIdx);
                if (Utils.isValidString(vehNumber))
                    unpackingBox.setVehicleNumber(vehNumber);

                unpackingBox.setInwardNumber(cursor.getString(inwardNumIdx));
                unpackingBox.setBoxStatus(cursor.getInt(boxStatusIdx));
                unpackingBox.setBoxId(cursor.getInt(boxIdIdx));
                unpackingBox.setQuantity(cursor.getInt(qtyIdx));
                unpackingBox.setStatus(cursor.getInt(statusIdx));
            }
            if (cursor != null)
                cursor.close();

            return unpackingBox;
        }

        public void updateStatus(String vehicleNumber, String inwardNumber, String masterBoxNumber, int boxStatus) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNPACKING_BOX_TABLE_NAME
                        + " SET " + DataHelper.UNPACKING_BOX_STATUS + " = " + boxStatus
                        + " WHERE "
                        + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNumber
                        + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + "= '" + masterBoxNumber + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deleteUnpackingBoxData(String vehicleNo, String inwardNo) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNo + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNo + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(String masterBoxNumber, String vehicleNumber, String inwardNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNPACKING_BOX_KEY_ID
                        + " FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_MASTER_BOX_NUMBER
                        + " = '" + masterBoxNumber + "' AND " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNumber + "' AND "
                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertUnpackingBoxData(List<UnpackingBox> unpackingBoxes, String vehicleNum, String inwardNum) {
            open();

            long retVal = -1, id = -1;
            try {
                for (UnpackingBox unpackingBox : unpackingBoxes) {
                    id = exists(unpackingBox.getMasterBoxNumber(), vehicleNum, inwardNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.UNPACKING_STATUS, unpackingBox.getStatus());
                        values.put(DataHelper.UNPACKING_QUANTITY, unpackingBox.getQuantity());
                        values.put(DataHelper.UNPACKING_MASTER_BOX_NUMBER, unpackingBox.getMasterBoxNumber());
                        values.put(DataHelper.UNPACKING_VEHICLE_NUMBER, vehicleNum);
                        values.put(DataHelper.UNPACKING_INWARD_NUMBER, inwardNum);
                        values.put(DataHelper.UNPACKING_BOX_ID, unpackingBox.getBoxId());
                        values.put(DataHelper.UNPACKING_BOX_STATUS, unpackingBox.getBoxStatus());

                        retVal = database.insert(DataHelper.UNPACKING_BOX_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<UnpackingBox> getUnpackingBoxData(String vehicleNum, String inwardNum) {
            ArrayList<UnpackingBox> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_BOX_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "'";

            Cursor cursor = database.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int boxIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_BOX_ID);
                int boxStatusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_BOX_STATUS);

                do {
                    UnpackingBox model = new UnpackingBox();
                    model.setVehicleNumber(cursor.getString(vehicleNoIdx));
                    model.setInwardNumber(cursor.getString(inwardNoIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setMasterBoxNumber(cursor.getString(masterBoxNumIdx));
                    model.setQuantity(cursor.getInt(qtyIdx));
                    model.setBoxId(cursor.getInt(boxIdIdx));
                    model.setBoxStatus(cursor.getInt(boxStatusIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

    public class Invoices {
        public int exists(int subProjectId, String orderNum) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.INVOICES_KEY_ID
                        + " FROM " + DataHelper.INVOICES_TABLE_NAME
                        + " WHERE "
                        + DataHelper.INVOICE_SUBPROJECT_ID + " = " + subProjectId + " AND "
                        + DataHelper.INVOICES_ORDER_NUM + " = '" + orderNum + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertInvoicesData(List<PackingOrderNoModel> packingOrderNoModels, int subprojectId) {
            open();

            long retVal = -1, id = -1;
            try {
                for (PackingOrderNoModel orderNoModel : packingOrderNoModels) {
                    id = exists(subprojectId, orderNoModel.getOrderNo());
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.INVOICE_SUBPROJECT_ID, subprojectId);
                        values.put(DataHelper.INVOICES_ORDER_NUM, orderNoModel.getOrderNo());
                        values.put(DataHelper.INVOICES_SUPPLIER_ADDRESS, orderNoModel.getSupplierAddress());
                        values.put(DataHelper.INVOICES_SUPPLIER_NAME, orderNoModel.getSupplierName());

                        retVal = database.insert(DataHelper.INVOICES_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<PackingOrderNoModel> getInvoicesData(int subprojectId) {
            ArrayList<PackingOrderNoModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.INVOICES_TABLE_NAME + " WHERE "
                    + DataHelper.INVOICE_SUBPROJECT_ID + " = " + subprojectId;

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.INVOICE_SUBPROJECT_ID);
                int supplierNameIdx = cursor.getColumnIndex(DataHelper.INVOICES_SUPPLIER_NAME);
                int orderNumIdx = cursor.getColumnIndex(DataHelper.INVOICES_ORDER_NUM);
                int supplierAddressIdx = cursor.getColumnIndex(DataHelper.INVOICES_SUPPLIER_ADDRESS);

                do {
                    PackingOrderNoModel model = new PackingOrderNoModel();
                    model.setOrderNo(cursor.getString(orderNumIdx));
                    model.setSupplierAddress(cursor.getString(supplierAddressIdx));
                    model.setSupplierName(cursor.getString(supplierNameIdx));
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

    public class PackingParts {
        public void updatePackingCount(int partId, int subprojectId, String orderNum, int packedCartons,
                                       int remainingCartons, int noOfMasterBoxes) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.PACKING_TABLE_NAME
                        + " SET " + DataHelper.PACKING_PACKED_PRIMARY_CARTONS + " = " + packedCartons
                        + ", " + DataHelper.PACKING_REMAINING_PRIMARY_CARTONS + " = " + remainingCartons
                        + ", " + DataHelper.PACKING_NO_OF_MASTER_BOXES + " = " + noOfMasterBoxes
                        + " WHERE " + DataHelper.PACKING_PART_ID + " = '" + partId + "' AND "
                        + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum
                        + "' AND " + DataHelper.PACKING_SUBPROJECT_ID + " = '" + subprojectId + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deletePackingData(int subProjectId, String orderNum) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.PACKING_TABLE_NAME
                        + " WHERE " + DataHelper.PACKING_SUBPROJECT_ID + " = "
                        + subProjectId + " AND " + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int partId, int subProjectId, String orderNum) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.PACKING_KEY_ID
                        + " FROM " + DataHelper.PACKING_TABLE_NAME
                        + " WHERE " + DataHelper.PACKING_PART_ID
                        + " = " + partId + " AND " + DataHelper.PACKING_SUBPROJECT_ID + " = "
                        + subProjectId + " AND "
                        + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";

                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertPackingData(List<PackingModel> packingModels, int subprojectId, String orderNum) {
            open();

            long retVal = -1, id = -1;
            try {
                for (PackingModel packingModel : packingModels) {
                    id = exists(packingModel.getPartId(), subprojectId, orderNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.PACKING_PART_ID, packingModel.getPartId());
                        values.put(DataHelper.PACKING_PART_NUM, packingModel.getPartNum());
                        values.put(DataHelper.PACKING_QUANTITY, packingModel.getQuantity());
                        values.put(DataHelper.PACKING_PACKED_PRIMARY_CARTONS, packingModel.getPrimarCartonsPacked());
                        values.put(DataHelper.PACKING_REMAINING_PRIMARY_CARTONS, packingModel.getRemainingPrimaryCartons());
                        values.put(DataHelper.PACKING_PICK_STATUS, packingModel.getPickStatus());
                        values.put(DataHelper.PACKING_NO_OF_MASTER_BOXES, packingModel.getNoOfMasterBoxes());
                        values.put(DataHelper.PACKING_SHIP_ADDRESS, packingModel.getShipToAddress());
                        values.put(DataHelper.PACKING_SUBPROJECT_ID, subprojectId);
                        values.put(DataHelper.PACKING_INVOICE_NUMBER, orderNum);
                        values.put(DataHelper.PACKING_WEIGHT, packingModel.getWeight());
                        values.put(DataHelper.PACKING_SUBPROJECT_NAME, packingModel.getSubProjectName());

                        retVal = database.insert(DataHelper.PACKING_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public int getNoOfMasterBoxes() {
            int count = 0;

            openRead();
            Cursor cursor = null;
            String query = "SELECT SUM(" + DataHelper.PACKING_NO_OF_MASTER_BOXES + ") FROM " + DataHelper.PACKING_TABLE_NAME;
            Utils.logD("SQL Query " + query);
            try {
                cursor = database.rawQuery(query, null);
                if (cursor.moveToNext())
                    count = cursor.getInt(0);
                Utils.logD("Pending Count : " + count);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return count;
        }

        public ArrayList<PackingModel> getPackingData(int subprojectId, String orderNum) {
            ArrayList<PackingModel> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.PACKING_TABLE_NAME + " WHERE "
                    + DataHelper.PACKING_SUBPROJECT_ID + " = " + subprojectId + " AND "
                    + DataHelper.PACKING_INVOICE_NUMBER + " = '" + orderNum + "'";

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.PACKING_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.PACKING_PART_ID);
                int primaryCartonsPackedIdx = cursor.getColumnIndex(DataHelper.PACKING_PACKED_PRIMARY_CARTONS);
                int primaryCartonsRemainingIdx = cursor.getColumnIndex(DataHelper.PACKING_REMAINING_PRIMARY_CARTONS);
                int quantityIdx = cursor.getColumnIndex(DataHelper.PACKING_QUANTITY);
                int pickStatusIdx = cursor.getColumnIndex(DataHelper.PACKING_PICK_STATUS);
                int invoiceNoIdx = cursor.getColumnIndex(DataHelper.PACKING_INVOICE_NUMBER);
                int invoiceIdIdx = cursor.getColumnIndex(DataHelper.PACKING_INVOICE_ID);
                int subprojectIdIdx = cursor.getColumnIndex(DataHelper.PACKING_SUBPROJECT_ID);
                int subProjectNameIdx = cursor.getColumnIndex(DataHelper.PACKING_SUBPROJECT_NAME);
                int noOfMasterBoxesIdx = cursor.getColumnIndex(DataHelper.PACKING_NO_OF_MASTER_BOXES);
                int weightIdx = cursor.getColumnIndex(DataHelper.PACKING_WEIGHT);

                do {
                    PackingModel model = new PackingModel();
                    model.setPartNum(cursor.getString(partNoIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setPrimarCartonsPacked(cursor.getInt(primaryCartonsPackedIdx));
                    model.setRemainingPrimaryCartons(cursor.getInt(primaryCartonsRemainingIdx));
                    model.setPickStatus(cursor.getInt(pickStatusIdx));
                    model.setQuantity(cursor.getInt(quantityIdx));
                    model.setInvoiceNum(cursor.getString(invoiceNoIdx));
                    model.setInvoiceId(cursor.getInt(invoiceIdIdx));
                    model.setSubProjectId(cursor.getInt(subprojectIdIdx));
                    model.setSubProjectName(cursor.getString(subProjectNameIdx));
                    model.setNoOfMasterBoxes(cursor.getInt(noOfMasterBoxesIdx));
                    model.setWeight(cursor.getDouble(weightIdx));

                    models.add(model);
                }
                while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }

        public void clearAll() {
            open();
            String query = "DELETE FROM " + DataHelper.PACKING_TABLE_NAME;
            database.execSQL(query);
            close();
        }
    }

    public class UnpackingParts {
        public void updateStatus(int partId, String vehicleNumber, String inwardNumber, String masterBoxNumber, int exceptionCount,
                                 int goodBoxes) {
            openRead();
            try {
                String updateSql = "UPDATE " + DataHelper.UNPACKING_TABLE_NAME
                        + " SET " + DataHelper.UNPACKING_NO_OF_EXCEPTIONS + " = " + exceptionCount + ", "
                        + DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED + " = " + goodBoxes
                        + " WHERE "
                        + DataHelper.UNPACKING_PART_ID + " = " + partId + " AND "
                        + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNumber
                        + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + "= '" + masterBoxNumber + "'";

                Utils.logD(updateSql);
                database.execSQL(updateSql);
            } catch (Exception e) {
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public void deleteUnpackingData(String vehicleNo, String inwardNo) {
            open();
            try {
                String deleteSql = "DELETE  FROM " + DataHelper.UNPACKING_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNo + "' AND " + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNo + "'";
                database.execSQL(deleteSql);
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
        }

        public int exists(int partId, String vehicleNumber, String inwardNumber, String masterBoxNumber) {
            openRead();
            int id = -1;
            Cursor cursor = null;
            try {
                String selectQuery = "SELECT  " + DataHelper.UNPACKING_KEY_ID
                        + " FROM " + DataHelper.UNPACKING_TABLE_NAME
                        + " WHERE " + DataHelper.UNPACKING_PART_ID
                        + " = " + partId + " AND " + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '"
                        + vehicleNumber + "' AND "
                        + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNumber + "' AND "
                        + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNumber + "'";
                Utils.logD(selectQuery);
                cursor = database.rawQuery(selectQuery, null);
                if (cursor.moveToFirst()) {
                    id = cursor.getInt(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                if (cursor != null)
                    cursor.close();
                close();
            }
            return id;
        }

        public long insertUnpackingData(List<UnpackingPart> unpackingPartParts, String vehicleNum, String inwardNum, String masterBoxNum) {
            open();

            long retVal = -1, id = -1;
            try {
                for (UnpackingPart unpackingPart : unpackingPartParts) {
                    id = exists(unpackingPart.getPartId(), vehicleNum, inwardNum, masterBoxNum);
                    if (id == -1) {
                        ContentValues values = new ContentValues();
                        values.put(DataHelper.UNPACKING_PART_ID, unpackingPart.getPartId());
                        values.put(DataHelper.UNPACKING_PART_NUM, unpackingPart.getPartNo());
                        values.put(DataHelper.UNPACKING_QUANTITY, unpackingPart.getQty());
                        values.put(DataHelper.UNPACKING_PRIMARY_CARTONS, unpackingPart.getNoOfPrimaryCartons());
                        values.put(DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED, unpackingPart.getNoOfPrimaryCartonsContained());
                        values.put(DataHelper.UNPACKING_NO_OF_EXCEPTIONS, unpackingPart.getNoOfExceptions());
                        values.put(DataHelper.UNPACKING_MASTER_BOX_NUMBER, masterBoxNum);
                        values.put(DataHelper.UNPACKING_IS_ACTED_UPON, unpackingPart.getIsActedUpon());
                        values.put(DataHelper.UNPACKING_STATUS, unpackingPart.getStatus());
                        values.put(DataHelper.UNPACKING_VEHICLE_NUMBER, vehicleNum);
                        values.put(DataHelper.UNPACKING_INWARD_NUMBER, inwardNum);
                        values.put(DataHelper.UNPACKING_IS_SELECTED, unpackingPart.getIsSelected());

                        retVal = database.insert(DataHelper.UNPACKING_TABLE_NAME, null,
                                values);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.logE(e.toString());
            } finally {
                close();
            }
            return retVal;
        }

        public ArrayList<UnpackingPart> getUnpackingData(String vehicleNum, String inwardNum, String masterBoxNum) {
            ArrayList<UnpackingPart> models = new ArrayList<>();
            openRead();
            String sql = "SELECT * FROM " + DataHelper.UNPACKING_TABLE_NAME + " WHERE "
                    + DataHelper.UNPACKING_VEHICLE_NUMBER + " = '" + vehicleNum + "' AND "
                    + DataHelper.UNPACKING_INWARD_NUMBER + " = '" + inwardNum + "' AND "
                    + DataHelper.UNPACKING_MASTER_BOX_NUMBER + " = '" + masterBoxNum + "'";

            Cursor cursor = database.rawQuery(sql, null);
            Utils.logD(sql);
            if (cursor.moveToFirst()) {
                int partNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PART_NUM);
                int partIdIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PART_ID);
                int primaryCartonsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PRIMARY_CARTONS);
                int primaryCartonsContainedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_PRIMARY_CARTONS_CONTAINED);
                int exceptionsIdx = cursor.getColumnIndex(DataHelper.UNPACKING_NO_OF_EXCEPTIONS);
                int statusIdx = cursor.getColumnIndex(DataHelper.UNPACKING_STATUS);
                int vehicleNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_VEHICLE_NUMBER);
                int inwardNoIdx = cursor.getColumnIndex(DataHelper.UNPACKING_INWARD_NUMBER);
                int isActedUponIdx = cursor.getColumnIndex(DataHelper.UNPACKING_IS_ACTED_UPON);
                int masterBoxNumIdx = cursor.getColumnIndex(DataHelper.UNPACKING_MASTER_BOX_NUMBER);
                int qtyIdx = cursor.getColumnIndex(DataHelper.UNPACKING_QUANTITY);
                int isSelectedIdx = cursor.getColumnIndex(DataHelper.UNPACKING_IS_SELECTED);

                do {
                    UnpackingPart model = new UnpackingPart();
                    model.setPartNo(cursor.getString(partNoIdx));
                    model.setVehicleNumber(vehicleNum);
                    model.setInwardNumber(inwardNum);
                    model.setNoOfExceptions(cursor.getInt(exceptionsIdx));
                    model.setPartId(cursor.getInt(partIdIdx));
                    model.setStatus(cursor.getInt(statusIdx));
                    model.setNoOfPrimaryCartons(cursor.getInt(primaryCartonsIdx));
                    model.setNoOfPrimaryCartonsContained(cursor.getInt(primaryCartonsContainedIdx));
                    model.setIsActedUpon(cursor.getInt(isActedUponIdx));
                    model.setMasterBoxNum(masterBoxNum);
                    model.setQty(cursor.getInt(qtyIdx));
                    model.setIsSelected(cursor.getInt(isSelectedIdx));

                    models.add(model);
                } while (cursor.moveToNext());
            }

            if (cursor != null)
                cursor.close();
            close();
            return models;
        }
    }

}
