package com.apptmyz.splwms.datacache;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.apptmyz.splwms.util.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataHelper extends SQLiteOpenHelper {
    public final static int DATABASE_VERSION = 1;
    public final static String DATABASE_NAME = "splwms";

    private static DataHelper instance;

    public DataHelper(Context context) {
        super(context, DataHelper.DATABASE_NAME, null,
                DataHelper.DATABASE_VERSION);
    }

    public static synchronized DataHelper getHelper(Context context) {
        if (instance == null)
            instance = new DataHelper(context);

        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DataHelper.SHARED_PREF_DATABASE_CREATE);
        database.execSQL(DataHelper.PUT_AWAY_DATABASE_CREATE_IF_NOT_EXISTS);
        database.execSQL(DataHelper.UNLOADING_PARTS_DATABASE_CREATE);
        Utils.logD(DataHelper.UNLOADING_PARTS_DATABASE_CREATE);
        database.execSQL(DataHelper.VENDOR_MASTER_DATABASE_CREATE);
        database.execSQL(DataHelper.LOADING_PARTS_DATABASE_CREATE);
        database.execSQL(DataHelper.SUBPROJECT_DATABASE_CREATE);
        database.execSQL(DataHelper.CUST_DATABASE_CREATE);
        database.execSQL(DataHelper.PICKLIST_PARTS_CREATE_DATABASE);
        database.execSQL(DataHelper.EXCEPTION_CREATE_DATABASE);
        database.execSQL(DataHelper.UNLOADING_GOOD_CREATE_DATABASE);
        database.execSQL(DataHelper.BIN_CREATE_DATABASE);
        database.execSQL(DataHelper.PART_CREATE_DATABASE);
        database.execSQL(DataHelper.PRODUCT_CREATE_DATABASE);
        database.execSQL(DataHelper.SKIP_BIN_REASON_DATABASE_CREATE);
        database.execSQL(DataHelper.EXCEPTION_TYPE_DATABASE_CREATE);
        database.execSQL(DataHelper.INVENTORY_TYPES_DATABASE_CREATE);
        database.execSQL(DataHelper.SLA_CREATE_DATABASE);
        database.execSQL(DataHelper.INBOUND_TYPE_CREATE_DATABASE);
        database.execSQL(DataHelper.INBOUND_INSPECTION_CREATE_DATABASE);
        database.execSQL(DataHelper.UNPACKING_CREATE_DATABASE);
        database.execSQL(DataHelper.PACKING_CREATE_DATABASE);
        database.execSQL(DataHelper.INVOICES_CREATE_DATABASE);
        database.execSQL(DataHelper.VEHICLE_NUMBERS_CREATE_DATABASE);
        database.execSQL(DataHelper.TC_NUMBERS_CREATE_DATABASE);
        database.execSQL(DataHelper.UNPACKING_BOX_CREATE_DATABASE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
     * ************* SHARED_PREFERENCES **************
     **/
    public static final String SHARED_PREF_TABLE_NAME = "sharedpreferences";
    public final static String SHARED_PREF_KEY_ID = "SHARED_PREF_KEY_ID";
    public static final String SHARED_PREF_COLUMN_KEY = "SHARED_KEY";
    public static final String SHARED_PREF_COLUMN_VALUE = "SHARED_VALUE";

    public static final String SHARED_PREF_DATABASE_CREATE = "create table if not exists "
            + SHARED_PREF_TABLE_NAME + "(" + SHARED_PREF_KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + SHARED_PREF_COLUMN_KEY
            + " text not null, " + SHARED_PREF_COLUMN_VALUE
            + " text not null);";

    public static final String SHARED_PREF_DATABASE_CREATE_IF_NOT_EXISTS = "create table if not exists "
            + SHARED_PREF_TABLE_NAME + "(" + SHARED_PREF_KEY_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT," + SHARED_PREF_COLUMN_KEY
            + " text not null, " + SHARED_PREF_COLUMN_VALUE
            + " text not null);";

    public static final String[] SHARED_PREF_COLUMNS = {
            SHARED_PREF_COLUMN_KEY, SHARED_PREF_COLUMN_VALUE};

    /**
     * *************** PUT AWAY DATA ************
     **/
    public static final String PUT_AWAY_TABLE_NAME = "putAwayData";

    public static final String PUT_AWAY_PART_KEY_ID = "PUT_AWAY_PART_KEY_ID";
    public static final String PUT_AWAY_PART_ID = "PUT_AWAY_PART_ID";
    public static final String PUT_AWAY_PART_NUMBER = "PUT_AWAY_PART_NUMBER";
    public static final String PUT_AWAY_TOTAL_BOXES = "PUT_AWAY_TOTAL_BOXES";
    public static final String PUT_AWAY_TOTAL_QTY = "PUT_AWAY_TOTAL_QTY";
    public static final String PUT_AWAY_VEHICLE_NUMBER = "PUT_AWAY_VEHICLE_NUMBER";
    public static final String PUT_AWAY_INVOICES = "PUT_AWAY_INVOICES";
    public static final String PUT_AWAY_STATUS = "PUT_AWAY_STATUS";
    public static final String PUT_AWAY_ID = "PUT_AWAY_ID";
    public static final String PUT_AWAY_BIN_NUMBER = "PUT_AWAY_BIN_NUMBER";
    public static final String PUT_AWAY_TO_BE_SCANNED_BOXES = "PUT_AWAY_TO_BE_SCANNED_BOXES";
    public static final String PUT_AWAY_INVENTORY_TYPE = "PUT_AWAY_INVENTORY_TYPE";
    public static final String PUT_AWAY_WH_CODE = "PUT_AWAY_WH_CODE";
    public static final String PUT_AWAY_IS_PRODUCT = "PUT_AWAY_IS_PRODUCT";
    public static final String PUT_AWAY_INVENTORY_TYPE_ID = "PUT_AWAY_INVENTORY_TYPE_ID";

    public static final String PUT_AWAY_DATABASE_CREATE_IF_NOT_EXISTS = "create table if not exists "
            + PUT_AWAY_TABLE_NAME + "(" + PUT_AWAY_PART_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PUT_AWAY_ID + " INTEGER, "
            + PUT_AWAY_WH_CODE + " TEXT, "
            + PUT_AWAY_PART_ID + " INTEGER, " + PUT_AWAY_PART_NUMBER + " TEXT, "
            + PUT_AWAY_TOTAL_BOXES + " INTEGER, " + PUT_AWAY_TOTAL_QTY + " INTEGER, "
            + PUT_AWAY_VEHICLE_NUMBER + " TEXT, " + PUT_AWAY_INVOICES + " TEXT, "
            + PUT_AWAY_STATUS + " INTEGER, "
            + PUT_AWAY_INVENTORY_TYPE + " TEXT, "
            + PUT_AWAY_IS_PRODUCT + " INTEGER, "
            + PUT_AWAY_INVENTORY_TYPE_ID + " INTEGER, "
            + PUT_AWAY_BIN_NUMBER + " TEXT, " + PUT_AWAY_TO_BE_SCANNED_BOXES + " INTEGER);";

    /********************** Unloading Table ***************/

    public static final String UNLOADING_PARTS_TABLE_NAME = "UNLOADING_PARTS_TABLE_NAME";
    public static final String UNLOADING_PARTS_COLUMN_ID = "UNLOADING_PARTS_COLUMN_ID";
    public static final String UNLOADING_PARTS_VEHICLE_NO = "UNLOADING_PARTS_VEHICLE_NO";
    public static final String UNLOADING_PARTS_COLUMN_PART_NO = "UNLOADING_PARTS_COLUMN_PART_NO";
    public static final String UNLOADING_PARTS_COLUMN_DA_NO = "DA_NO";
    public static final String UNLOADING_PARTS_COLUMN_DATATYPE = "DATA_TYPE";
    public static final String UNLOADING_PARTS_COLUMN_PART_ID = "UNLOADING_PARTS_COLUMN_PART_ID";
    public static final String UNLOADING_PARTS_COLUMN_TOTAL_BOXES = "UNLOADING_PARTS_COLUMN_TOTAL_BOXES";
    public static final String UNLOADING_PARTS_COLUMN_TOTAL_QTY = "UNLOADING_PARTS_COLUMN_TOTAL_QTY";
    public static final String UNLOADING_PARTS_COLUMN_INVOICE_NO = "UNLOADING_PARTS_COLUMN_INVOICE_NO";
    public static final String UNLOADING_PARTS_COLUMN_BOX_COUNT = "UNLOADING_PARTS_COLUMN_BOX_COUNT";
    public static final String UNLOADING_PARTS_COLUMN_QTY_INVOICE = "UNLOADING_PARTS_COLUMN_QTY_INVOICE";
    public static final String UNLOADING_PARTS_COLUMN_QTY_SCANNED = "UNLOADING_PARTS_COLUMN_QTY_SCANNED";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT = "UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT";
    public static final String UNLOADING_PARTS_COLUMN_STATUS = "UNLOADING_PARTS_COLUMN_STATUS";
    public static final String UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED = "UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED";
    public static final String UNLOADING_PARTS_COLUMN_SCAN_TIME = "UNLOADING_PARTS_COLUMN_SCAN_TIME";
    public static final String UNLOADING_PARTS_COLUMN_IS_EXCEPTION = "UNLOADING_PARTS_COLUMN_IS_EXCEPTION";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE = "UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION = "UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION";
    public static final String UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES = "UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES";
    public static final String UNLOADING_PARTS_INVOICE_BARCODE = "UNLOADING_PARTS_INVOICE_BARCODE";
    public static final String UNLOADING_PARTS_INVOICE_SCANNED_COUNT = "UNLOADING_PARTS_INVOICE_SCANNED_COUNT";
    public static final String UNLOADING_PARTS_MASTER_BOX_NUM = "UNLOADING_PARTS_MASTER_BOX_NUM";
    public static final String UNLOADING_PARTS_COLUMN_INVENTORY_TYPE_ID = "UNLOADING_PARTS_COLUMN_INVENTORY_TYPE_ID";
    public static final String UNLOADING_WH_CODE = "UNLOADING_WH_CODE";
    public static final String UNLOADING_PARTS_RECEIPT_TYPE = "UNLOADING_PARTS_RECEIPT_TYPE";

    public static final String UNLOADING_PARTS_PRODUCT_ID = "UNLOADING_PARTS_PRODUCT_ID";
    public static final String UNLOADING_PARTS_PRODUCT_NAME = "UNLOADING_PARTS_PRODUCT_NAME";
    public static final String UNLOADING_PARTS_IS_PRODUCT = "UNLOADING_PARTS_IS_PRODUCT";
    public static final String UNLOADING_PARTS_UNIQUE = "UNLOADING_PARTS_UNIQUE";

    public static final String UNLOADING_PARTS_DATABASE_CREATE = "create table "
            + UNLOADING_PARTS_TABLE_NAME + "(" + UNLOADING_PARTS_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + UNLOADING_PARTS_VEHICLE_NO + " TEXT, "
            + UNLOADING_WH_CODE + " TEXT, "
            + UNLOADING_PARTS_COLUMN_DA_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_DATATYPE + " TEXT, "
            + UNLOADING_PARTS_COLUMN_INVOICE_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_PART_NO + " TEXT, "
            + UNLOADING_PARTS_COLUMN_PART_ID + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_TOTAL_BOXES + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_TOTAL_QTY + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_BOX_COUNT + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_QTY_INVOICE + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_QTY_SCANNED + " INTEGER, "
            + UNLOADING_PARTS_COLUMN_STATUS + " TEXT, "
            + UNLOADING_PARTS_COLUMN_IS_SCANCOMPLETED + " TEXT, "
            + UNLOADING_PARTS_COLUMN_SCAN_TIME + " TEXT, "
            + UNLOADING_PARTS_COLUMN_IS_EXCEPTION + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_TYPE + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_DESCRIPTION + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_IMAGES + " TEXT, "
            + UNLOADING_PARTS_COLUMN_EXCEPTION_COUNT + " INTEGER, "
            + UNLOADING_PARTS_INVOICE_BARCODE + " TEXT, "
            + UNLOADING_PARTS_MASTER_BOX_NUM + " TEXT, "
            + UNLOADING_PARTS_COLUMN_INVENTORY_TYPE_ID + " INTEGER, "
            + UNLOADING_PARTS_INVOICE_SCANNED_COUNT + " INTEGER, "
            + UNLOADING_PARTS_PRODUCT_ID + " INTEGER, "
            + UNLOADING_PARTS_PRODUCT_NAME + " TEXT, "
            + UNLOADING_PARTS_IS_PRODUCT + " INTEGER, "
            + UNLOADING_PARTS_UNIQUE + " TEXT, "
            + UNLOADING_PARTS_RECEIPT_TYPE + " TEXT)";

    /********************Vendor Master ***********************/


    public static final String VENDOR_MASTER_TABLE_NAME = "VENDOR_MASTER_TABLE_NAME";
    public static final String VENDOR_MASTER_COLUMN_ID = "VENDOR_MASTER_COLUMN_ID";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_ID = "VENDOR_MASTER_COLUMN_VENDOR_ID";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_CODE = "VENDOR_MASTER_COLUMN_CODE";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_NAME = "VENDOR_MASTER_COLUMN_NAME";
    public static final String VENDOR_MASTER_COLUMN_VENDOR_TYPE = "VENDOR_MASTER_COLUMN_TYPE";
    public static final String VENDOR_MASTER_COLUMN_ACTIVE_FLAG = "VENDOR_MASTER_COLUMN_ACTIVE_FLAG";


    public static final String VENDOR_MASTER_DATABASE_CREATE = "create table "
            + VENDOR_MASTER_TABLE_NAME + "(" + VENDOR_MASTER_COLUMN_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + VENDOR_MASTER_COLUMN_VENDOR_ID + " INTEGER, "
            + VENDOR_MASTER_COLUMN_VENDOR_CODE + " TEXT, "
            + VENDOR_MASTER_COLUMN_VENDOR_NAME + " TEXT, "
            + VENDOR_MASTER_COLUMN_VENDOR_TYPE + " TEXT, "
            + VENDOR_MASTER_COLUMN_ACTIVE_FLAG + " TEXT) ";


    /***************************** Loading sheet***************************/

    public static final String LOADING_PARTS_TABLE_NAME = "LOADING_PARTS_TABLE_NAME";
    public static final String LOADING_PARTS_COLUMN_ID = "LOADING_PARTS_COLUMN_ID";
    public static final String LOADING_PARTS_VEHICLE_NO = "LOADING_PARTS_VEHICLE_NO";
    public static final String LOADING_PARTS_COLUMN_PART_NO = "LOADING_PARTS_COLUMN_PART_NO";
    public static final String LOADING_PARTS_COLUMN_PART_ID = "LOADING_PARTS_COLUMN_PART_ID";
    public static final String LOADING_PARTS_COLUMN_TOTAL_BOXES = "LOADING_PARTS_COLUMN_TOTAL_BOXES";
    public static final String LOADING_PARTS_COLUMN_TOTAL_QTY = "LOADING_PARTS_COLUMN_TOTAL_QTY";
    public static final String LOADING_PARTS_COLUMN_BIN_NO = "LOADING_PARTS_COLUMN_BIN_NO";
    public static final String LOADING_PARTS_COLUMN_BIN_ID = "LOADING_PARTS_COLUMN_BIN_ID";
    public static final String LOADING_PARTS_COLUMN_SCAN_TIME = "LOADING_PARTS_COLUMN_SCAN_TIME"; // not using these columns for now
    public static final String LOADING_PARTS_COLUMN_XD_FLAG = "LOADING_PARTS_COLUMN_XD_FLAG";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_NEXT_DEST = "LOADING_PARTS_COLUMN_NEXT_DEST";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_EXCEP_TYPE = "LOADING_PARTS_COLUMN_EXCEP_TYPE";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_EXCEP_DESC = "LOADING_PARTS_COLUMN_EXCEP_DESC";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_EXCEP_IMAGE = "LOADING_PARTS_COLUMN_EXCEP_IMAGE";// not using these columns for now
    public static final String LOADING_PARTS_COLUMN_QTY_SCANNED = "LOADING_PARTS_COLUMN_QTY_SCANNED";
    public static final String LOADING_PARTS_COLUMN_STATUS = "LOADING_PARTS_COLUMN_STATUS";


    public static final String LOADING_PARTS_DATABASE_CREATE = "create table "
            + LOADING_PARTS_TABLE_NAME + "(" + LOADING_PARTS_COLUMN_ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + LOADING_PARTS_VEHICLE_NO + " TEXT, "
            + LOADING_PARTS_COLUMN_PART_NO + " TEXT, "
            + LOADING_PARTS_COLUMN_PART_ID + " INTEGER, "
            + LOADING_PARTS_COLUMN_TOTAL_BOXES + " INTEGER, "
            + LOADING_PARTS_COLUMN_TOTAL_QTY + " INTEGER, "
            + LOADING_PARTS_COLUMN_BIN_NO + " TEXT, "
            + LOADING_PARTS_COLUMN_BIN_ID + " INTEGER, "
            + LOADING_PARTS_COLUMN_SCAN_TIME + " TEXT, "
            + LOADING_PARTS_COLUMN_XD_FLAG + " INTEGER, "
            + LOADING_PARTS_COLUMN_NEXT_DEST + " TEXT, "
            + LOADING_PARTS_COLUMN_EXCEP_TYPE + " TEXT, "
            + LOADING_PARTS_COLUMN_EXCEP_DESC + " TEXT, "
            + LOADING_PARTS_COLUMN_EXCEP_IMAGE + " TEXT, "
            + LOADING_PARTS_COLUMN_QTY_SCANNED + " INTEGER, "
            + LOADING_PARTS_COLUMN_STATUS + " TEXT) ";

    /*
     * Subprojects Table
     * */

    public static final String SUBPROJECTS_TABLE_NAME = "subprojects";

    public static final String SUBPROJECT_KEY_ID = "SUBPROJECT_KEY_ID";
    public static final String SUBPROJECT_ID = "SUBPROJECT_ID";
    public static final String SUBPROJECT_NAME = "SUBPROJECT_NAME";
    public static final String SUBPROJECT_PROJECT_ID = "SUBPROJECT_PROJECT_ID";
    public static final String SUBPROJECT_PROJECT_NAME = "SUBPROJECT_PROJECT_NAME";
    public static final String SUBPROJECT_PROJECT_TYPE_ID = "SUBPROJECT_PROJECT_TYPE_ID";
    public static final String SUBPROJECT_PROJECT_TYPE = "SUBPROJECT_PROJECT_TYPE";
    public static final String SUBPROJECT_LOCATION = "SUBPROJECT_LOCATION";
    public static final String SUBPROJECT_REVENUE_TYPE = "SUBPROJECT_REVENUE_TYPE";
    public static final String SUBPROJECT_REVENUE_DETAILS = "SUBPROJECT_REVENUE_DETAILS";
    public static final String SUBPROJECT_STATE_CODE = "SUBPROJECT_STATE_CODE";
    public static final String SUBPROJECT_STATE = "SUBPROJECT_STATE";
    public static final String SUBPROJECT_CITY = "SUBPROJECT_CITY";
    public static final String SUBPROJECT_USER_ID = "SUBPROJECT_USER_ID";
    public static final String SUBPROJECT_CREATEDBY = "SUBPROJECT_CREATEDBY";
    public static final String SUBPROJECT_CREATED_TIMESTAMP = "SUBPROJECT_CREATED_TIMESTAMP";
    public static final String SUBPROJECT_UPDATEDBY = "SUBPROJECT_UPDATEDBY";
    public static final String SUBPROJECT_UPDATED_TIMESTAMP = "SUBPROJECT_UPDATED_TIMESTAMP";
    public static final String SUBPROJECT_BRANCH = "SUBPROJECT_BRANCH";
    public static final String SUBPROJECT_BRANCH_NAME = "SUBPROJECT_BRANCH_NAME";
    public static final String SUBPROJECT_SAAC_CODE = "SUBPROJECT_SAAC_CODE";
    public static final String SUBPROJECT_DESCRIPTION = "SUBPROJECT_DESCRIPTION";
    public static final String SUBPROJECT_DISCOUNT_PERCENT = "SUBPROJECT_DISCOUNT_PERCENT";
    public static final String SUBPROJECT_REVENUE_ID = "SUBPROJECT_REVENUE_ID";
    public static final String SUBPROJECT_CUSTOMER_NAME = "SUBPROJECT_CUSTOMER_NAME";
    public static final String SUBPROJECT_REVENUE_TYPE_NAME = "SUBPROJECT_REVENUE_TYPE_NAME";
    public static final String SUBPROJECT_CUST_BILL_ADDR = "SUBPROJECT_CUST_BILL_ADDR";
    public static final String SUBPROJECT_TAX_DETAILS = "SUBPROJECT_TAX_DETAILS";
    public static final String SUBPROJECT_COLLECTION_MODEL = "SUBPROJECT_COLLECTION_MODEL";
    public static final String SUBPROJECT_FUEL_CHARGE_DETAILS = "SUBPROJECT_FUEL_CHARGE_DETAILS";
    public static final String SUBPROJECT_PLANT_ID = "SUBPROJECT_PLANT_ID";
    public static final String SUBPROJECT_CUST_ADDR = "SUBPROJECT_CUST_ADDR";
    public static final String SUBPROJECT_ROUTE_LIST = "SUBPROJECT_ROUTE_LIST";

    public static final String SUBPROJECT_DATABASE_CREATE = "CREATE TABLE "
            + SUBPROJECTS_TABLE_NAME + "(" + SUBPROJECT_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SUBPROJECT_ID + " INTEGER, " + SUBPROJECT_NAME + " TEXT, " + SUBPROJECT_PROJECT_ID + " INTEGER, "
            + SUBPROJECT_PROJECT_NAME + " TEXT, " + SUBPROJECT_PROJECT_TYPE_ID + " INTEGER, " + SUBPROJECT_PROJECT_TYPE + " TEXT, "
            + SUBPROJECT_LOCATION + " TEXT, " + SUBPROJECT_REVENUE_TYPE + " TEXT, " + SUBPROJECT_REVENUE_DETAILS + " TEXT, "
            + SUBPROJECT_STATE_CODE + " TEXT, " + SUBPROJECT_STATE + " TEXT, " + SUBPROJECT_CITY + " TEXT, "
            + SUBPROJECT_USER_ID + " INTEGER, " + SUBPROJECT_CREATEDBY + " TEXT, " + SUBPROJECT_CREATED_TIMESTAMP + " TEXT,"
            + SUBPROJECT_UPDATEDBY + " TEXT, " + SUBPROJECT_UPDATED_TIMESTAMP + " TEXT, " + SUBPROJECT_BRANCH + " INTEGER, "
            + SUBPROJECT_BRANCH_NAME + " TEXT, " + SUBPROJECT_SAAC_CODE + " TEXT, " + SUBPROJECT_DESCRIPTION + " TEXT, "
            + SUBPROJECT_DISCOUNT_PERCENT + " REAL, " + SUBPROJECT_REVENUE_ID + " INTEGER, " + SUBPROJECT_CUSTOMER_NAME + " TEXT, "
            + SUBPROJECT_REVENUE_TYPE_NAME + " TEXT, " + SUBPROJECT_CUST_BILL_ADDR + " TEXT, " + SUBPROJECT_TAX_DETAILS + " TEXT, "
            + SUBPROJECT_COLLECTION_MODEL + " TEXT, " + SUBPROJECT_FUEL_CHARGE_DETAILS + " TEXT, " + SUBPROJECT_PLANT_ID + " INTEGER, "
            + SUBPROJECT_CUST_ADDR + " TEXT, " + SUBPROJECT_ROUTE_LIST + " TEXT);";

    /*
     *  CUSTOMER TABLE
     * */

    public static final String CUST_TABLE_NAME = "customers";

    public static final String CUST_KEY_ID = "CUST_KEY_ID";
    public static final String CUST_ID = "CUST_ID";
    public static final String CUST_NAME = "CUST_NAME";
    public static final String CUST_CUST_CODE = "CUST_CUST_CODE";
    public static final String CUST_LEGAL_ENTITY_NAME = "CUST_LEGAL_ENTITY_NAME";
    public static final String CUST_ADDRESS1 = "CUST_ADDRESS1";
    public static final String CUST_ADDRESS2 = "CUST_ADDRESS2";
    public static final String CUST_ADDRESS3 = "CUST_ADDRESS3";
    public static final String CUST_STATE = "CUST_STATE";
    public static final String CUST_CITY = "CUST_CITY";
    public static final String CUST_PINCODE = "CUST_PINCODE";
    public static final String CUST_CREATED_BY = "CUST_CREATED_BY";
    public static final String CUST_CREATED_TIMESTAMP = "CUST_CREATED_TIMESTAMP";
    public static final String CUST_UPDATED_BY = "CUST_UPDATED_BY";
    public static final String CUST_UPDATED_TIMESTAMP = "CUST_UPDATED_TIMESTAMP";
    public static final String CUST_GSTIN = "CUST_GSTIN";
    public static final String CUST_PRIMARY_CONTACT_NAME = "CUST_PRIMARY_CONTACT_NAME";
    public static final String CUST_SECONDARY_CONTACT_NAME = "CUST_SECONDARY_CONTACT_NAME";
    public static final String CUST_PRIMARY_PHONE_NUMBER = "CUST_PRIMARY_PHONE_NUMBER";
    public static final String CUST_SECONDARY_PHONE_NUMBER = "CUST_SECONDARY_PHONE_NUMBER";
    public static final String CUST_PRIMARY_ALT_PHONE_NUMBER = "CUST_PRIMARY_ALT_PHONE_NUMBER";
    public static final String CUST_SECONDARY_ALT_PHONE_NUMBER = "CUST_SECONDARY_ALT_PHONE_NUMBER";
    public static final String CUST_PRIMARY_EMAIL_ID = "CUST_PRIMARY_EMAIL_ID";
    public static final String CUST_SECONDARY_EMAIL_ID = "CUST_SECONDARY_EMAIL_ID";
    public static final String CUST_PRIMARY_DEPT = "CUST_PRIMARY_DEPT";
    public static final String CUST_SECONDARY_DEPT = "CUST_SECONDARY_DEPT";
    public static final String CUST_PAN = "CUST_PAN";

    public static final String CUST_DATABASE_CREATE = "CREATE TABLE " + CUST_TABLE_NAME
            + "(" + CUST_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + CUST_ID + " INTEGER, "
            + CUST_NAME + " TEXT, " + CUST_CUST_CODE + " TEXT, "
            + CUST_ADDRESS1 + " TEXT, " + CUST_ADDRESS2 + " TEXT, " + CUST_ADDRESS3 + " TEXT, " + CUST_STATE + " TEXT, "
            + CUST_CITY + " TEXT, " + CUST_PINCODE + " INTEGER, "
            + CUST_CREATED_BY + " TEXT, " + CUST_CREATED_TIMESTAMP + " TEXT, "
            + CUST_UPDATED_BY + " TEXT, " + CUST_UPDATED_TIMESTAMP + " TEXT, "
            + CUST_LEGAL_ENTITY_NAME + " TEXT, " + CUST_GSTIN + " TEXT, "
            + CUST_PRIMARY_CONTACT_NAME + " TEXT, " + CUST_SECONDARY_CONTACT_NAME + " TEXT, "
            + CUST_PRIMARY_PHONE_NUMBER + " TEXT, " + CUST_SECONDARY_PHONE_NUMBER + " TEXT, "
            + CUST_PRIMARY_ALT_PHONE_NUMBER + " TEXT, " + CUST_SECONDARY_ALT_PHONE_NUMBER + " TEXT, "
            + CUST_PRIMARY_EMAIL_ID + " TEXT, " + CUST_SECONDARY_EMAIL_ID + " TEXT, "
            + CUST_PRIMARY_DEPT + " TEXT, " + CUST_SECONDARY_DEPT + " TEXT, " + CUST_PAN + " TEXT);";

    /*
     * BINS table */
    public static final String BIN_TABLE_NAME = "bins";

    public static final String BIN_KEY_ID = "BIN_KEY_ID";
    public static final String BIN_ID = "BIN_ID";
    public static final String BIN_NUMBER = "BIN_NUMBER";
    public static final String BIN_AISLE = "BIN_AISLE";
    public static final String BIN_WH_CODE = "BIN_WH_CODE";
    public static final String BIN_AREA_CODE = "BIN_AREA_CODE";
    public static final String BIN_ZONE_CODE = "BIN_ZONE_CODE";
    public static final String BIN_RACK_CODE = "BIN_RACK_CODE";
    public static final String BIN_LOCATION_CODE = "BIN_LOCATION_CODE";
    public static final String BIN_BARCODE = "BIN_BARCODE";
    public static final String BIN_POSITION = "BIN_POSITION";
    public static final String BIN_LEVEL_NUMBER = "BIN_LEVEL_NUMBER";
    public static final String BIN_ACTIVE_FLAG = "BIN_ACTIVE_FLAG";
    public static final String BIN_INVENTORY_TYPE_IDS = "BIN_INVENTORY_TYPE_IDS";
    public static final String BIN_RESERVE_TYPE = "BIN_RESERVE_TYPE";

    public static final String BIN_CREATE_DATABASE = "CREATE TABLE " + BIN_TABLE_NAME + "("
            + BIN_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + BIN_ID + " INTEGER, "
            + BIN_NUMBER + " TEXT, " + BIN_WH_CODE + " TEXT, " + BIN_AISLE + " TEXT, "
            + BIN_AREA_CODE + " TEXT, " + BIN_ZONE_CODE + " TEXT, " + BIN_RACK_CODE + " TEXT, "
            + BIN_LOCATION_CODE + " TEXT, " + BIN_BARCODE + " TEXT, " + BIN_POSITION + " INTEGER, " + BIN_LEVEL_NUMBER
            + " TEXT, " + BIN_ACTIVE_FLAG + " INTEGER, " + BIN_RESERVE_TYPE + " INTEGER, " + BIN_INVENTORY_TYPE_IDS + " TEXT);";

    /*
     * PARTS table */
    public static final String PART_TABLE_NAME = "parts";

    public static final String PART_KEY_ID = "PART_KEY_ID";
    public static final String PART_ID = "PART_ID";
    public static final String PART_NUMBER = "PART_NUMBER";
    public static final String PART_NAME = "PART_NAME";
    public static final String PART_WH_CODE = "PART_WH_CODE";

    public static final String PART_CREATE_DATABASE = "CREATE TABLE " + PART_TABLE_NAME + "("
            + PART_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PART_ID + " INTEGER, "
            + PART_NAME + " TEXT, "
            + PART_WH_CODE + " TEXT, "
            + PART_NUMBER + " TEXT);";

    /*
     * Products table *
     * */

    public static final String PRODUCT_TABLE_NAME = "products";

    public static final String PRODUCT_KEY_ID = "PRODUCT_KEY_ID";
    public static final String PRODUCT_ID = "PRODUCT_ID";
    public static final String PRODUCT_NAME = "PRODUCT_NAME";
    public static final String PRODUCT_SKU = "PRODUCT_SKU";
    public static final String PRODUCT_CUSTOMER_NAME = "PRODUCT_CUSTOMER_NAME";
    public static final String PRODUCT_CUSTOMER_ID = "PRODUCT_CUSTOMER_ID";
    public static final String PRODUCT_MAKE = "PRODUCT_MAKE";
    public static final String PRODUCT_MODEL = "PRODUCT_MODEL";
    public static final String PRODUCT_COUNTRY_ID = "PRODUCT_COUNTRY_ID";

    public static final String PRODUCT_CREATE_DATABASE = "CREATE TABLE " + PRODUCT_TABLE_NAME + "("
            + PRODUCT_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PRODUCT_ID + " INTEGER, "
            + PRODUCT_NAME + " TEXT, " + PRODUCT_SKU + " TEXT, " + PRODUCT_CUSTOMER_NAME + " TEXT, "
            + PRODUCT_CUSTOMER_ID + " INTEGER, " + PRODUCT_COUNTRY_ID + " INTEGER, " + PRODUCT_MAKE + " TEXT, "
            + PRODUCT_MODEL + " TEXT)";

    /*
     * SLA TYPES TABLE
     * */
    public static final String SLA_TABLE_NAME = "sla";

    public static final String SLA_KEY_ID = "SLA_KEY_ID";
    public static final String SLA_ID = "SLA_ID";
    public static final String SLA_CATEGORY_TYPE = "SLA_CATEGORY_TYPE";
    public static final String SLA_NAME = "SLA_NAME";
    public static final String SLA_DESCRIPTION = "SLA_DESCRIPTION";
    public static final String SLA_ORDER_CUTOFF_TIME = "SLA_ORDER_CUTOFF_TIME";
    public static final String SLA_LEAD_TIME = "SLA_LEAD_TIME";
    public static final String SLA_TIMEZONE = "SLA_TIMEZONE";
    public static final String SLA_ACTIVE_FLAG = "SLA_ACTIVE_FLAG";

    public static final String SLA_CREATE_DATABASE = "CREATE TABLE " + SLA_TABLE_NAME + " ("
            + SLA_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + SLA_ID + " INTEGER, "
            + SLA_CATEGORY_TYPE + " TEXT, " + SLA_NAME + " TEXT, " + SLA_ORDER_CUTOFF_TIME + " TEXT, "
            + SLA_DESCRIPTION + " TEXT, "
            + SLA_LEAD_TIME + " TEXT, " + SLA_TIMEZONE + " TEXT, " + SLA_ACTIVE_FLAG + " INTEGER)";

    /*
     * INBOUND TYPES TABLE
     * */
    public static final String INBOUND_TYPES_TABLE_NAME = "inboundTypes";

    public static final String INBOUND_TYPES_KEY_ID = "INBOUND_TYPES_KEY_ID";
    public static final String INBOUND_ID = "INBOUND_ID";
    public static final String INBOUND_TYPE_NAME = "INBOUND_TYPE_NAME";
    public static final String INBOUND_TYPE_CODE = "INBOUND_TYPE_CODE";
    public static final String INBOUND_ACTIVE_FLAG = "INBOUND_ACTIVE_FLAG";
    public static final String INBOUND_CREATED_TIMESTAMP = "INBOUND_CREATED_TIMESTAMP";
    public static final String INBOUND_UPDATED_TIMESTAMP = "INBOUND_UPDATED_TIMESTAMP";

    public static final String INBOUND_TYPE_CREATE_DATABASE = "CREATE TABLE " + INBOUND_TYPES_TABLE_NAME + " ("
            + INBOUND_TYPES_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + INBOUND_ID + " INTEGER, "
            + INBOUND_TYPE_NAME + " TEXT, " + INBOUND_TYPE_CODE + " INTEGER, " + INBOUND_ACTIVE_FLAG + " INTEGER, "
            + INBOUND_CREATED_TIMESTAMP + " TEXT, " + INBOUND_UPDATED_TIMESTAMP + " TEXT);";

    /*
     * SKIP BIN REASONS TABLE*/

    public static final String SKIP_BIN_REASONS_TABLE_NAME = "skipBinReasons";
    public static final String SKIP_BIN_REASON_KEY_ID = "SKIP_BIN_REASON_KEY_ID";
    public static final String SKIP_BIN_REASON_ID = "SKIP_BIN_REASON_ID";
    public static final String SKIP_BIN_REASON_NAME = "SKIP_BIN_REASON_NAME";
    public static final String SKIP_BIN_WH_CODE = "SKIP_BIN_WH_CODE";

    public static final String SKIP_BIN_REASON_DATABASE_CREATE = "CREATE TABLE " + SKIP_BIN_REASONS_TABLE_NAME + "("
            + SKIP_BIN_REASON_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + SKIP_BIN_WH_CODE + " TEXT, "
            + SKIP_BIN_REASON_ID + " INTEGER, "
            + SKIP_BIN_REASON_NAME + " TEXT);";

    /*
     * EXCEPTION TYPES TABLE*/

    public static final String EXCEPTION_TYPES_TABLE_NAME = "exceptionTypes";
    public static final String EXCEPTION_TYPE_KEY_ID = "EXCEPTION_TYPE_KEY_ID";
    public static final String EXCEPTION_TYPE_ID = "EXCEPTION_TYPE_ID";
    public static final String EXCEPTION_TYPE_NAME = "EXCEPTION_TYPE_NAME";
    public static final String EXCEPTION_TYPE_WH_CODE = "EXCEPTION_TYPE_WH_CODE";

    public static final String EXCEPTION_TYPE_DATABASE_CREATE = "CREATE TABLE " + EXCEPTION_TYPES_TABLE_NAME + "("
            + EXCEPTION_TYPE_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EXCEPTION_TYPE_WH_CODE + " TEXT, "
            + EXCEPTION_TYPE_ID + " INTEGER, "
            + EXCEPTION_TYPE_NAME + " TEXT);";

    /*
     * INVENTORY TYPES TABLE*/

    public static final String INVENTORY_TYPES_TABLE_NAME = "inventoryTypes";
    public static final String INVENTORY_TYPE_KEY_ID = "INVENTORY_TYPE_KEY_ID";
    public static final String INVENTORY_TYPE_ID = "INVENTORY_TYPE_ID";
    public static final String INVENTORY_TYPE_NAME = "INVENTORY_TYPE_NAME";
    public static final String INVENTORY_TYPE_CODE = "INVENTORY_TYPE_CODE";
    public static final String INVENTORY_TYPE_ACTIVE_FLAG = "INVENTORY_TYPE_ACTIVE_FLAG";

    public static final String INVENTORY_TYPES_DATABASE_CREATE = "CREATE TABLE " + INVENTORY_TYPES_TABLE_NAME + "("
            + INVENTORY_TYPE_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + INVENTORY_TYPE_ID + " INTEGER, "
            + INVENTORY_TYPE_NAME + " TEXT, "
            + INVENTORY_TYPE_CODE + " TEXT, " + INVENTORY_TYPE_ACTIVE_FLAG + " INTEGER"
            + ");";

    /*
     * PICK LIST PARTS TABLE
     * */

    public static final String PICKLIST_PARTS_TABLE_NAME = "picklistparts";

    public static final String PICKLIST_PART_KEY_ID = "PICKLIST_PART_KEY_ID";
    public static final String PICKLIST_UNIQUE_ID = "PICKLIST_UNIQUE_ID";
    public static final String PICKLIST_PART_SUBPROJECT_ID = "PICKLIST_PART_SUBPROJECT_ID";
    public static final String PICKLIST_PART_CUST_ID = "PICKLIST_PART_CUST_ID";
    public static final String PICKLIST_PART_NO = "PICKLIST_PART_NO";
    public static final String PICKLIST_PART_ID = "PICKLIST_PART_ID";
    public static final String PICKLIST_TOTAL_BOXES = "PICKLIST_TOTAL_BOXES";
    public static final String PICKLIST_TOTAL_QTY = "PICKLIST_TOTAL_QTY";
    public static final String PICKLIST_BIN_NO = "PICKLIST_BIN_NO";
    public static final String PICKLIST_SCAN_TIME = "PICKLIST_SCAN_TIME";
    public static final String PICKLIST_XDFLAG = "PICKLIST_XDFLAG";
    public static final String PICKLIST_NEXT_DEST = "PICKLIST_NEXT_DEST";
    public static final String PICKLIST_EXCEP_TYPE = "PICKLIST_EXCEP_TYPE";
    public static final String PICKLIST_EXCEPT_DESC = "PICKLIST_EXCEPT_DESC";
    public static final String PICKLIST_EXCEP_IMAGE = "PICKLIST_EXCEP_IMAGE";
    public static final String PICKLIST_INVOICE_LIST = "PICKLIST_INVOICE_LIST";
    public static final String PICKLIST_IS_PICKED = "PICKLIST_IS_PICKED";
    public static final String PICKLIST_ORDER_NO = "PICKLIST_ORDER_NO";
    public static final String PICKLIST_BIN_ID = "PICKLIST_BIN_ID";
    public static final String PICKLIST_TYPE = "PICKLIST_TYPE";
    public static final String PICKLIST_DATA = "PICKLIST_DATA";
    public static final String PICKLIST_WH_CODE = "PICKLIST_WH_CODE";

    public static final String PICKLIST_PARTS_CREATE_DATABASE = "CREATE TABLE " + PICKLIST_PARTS_TABLE_NAME + "("
            + PICKLIST_PART_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PICKLIST_UNIQUE_ID + " INTEGER, "
            + PICKLIST_WH_CODE + " TEXT, "
            + PICKLIST_PART_SUBPROJECT_ID + " INTEGER, " + PICKLIST_PART_CUST_ID + " INTEGER, "
            + PICKLIST_PART_NO + " TEXT, "
            + PICKLIST_PART_ID + " INTEGER, " + PICKLIST_TOTAL_BOXES + " INTEGER, " + PICKLIST_BIN_NO + " TEXT, "
            + PICKLIST_TOTAL_QTY + " INTEGER, " + PICKLIST_SCAN_TIME + " TEXT, " + PICKLIST_XDFLAG + " INTEGER, "
            + PICKLIST_NEXT_DEST + " TEXT, " + PICKLIST_EXCEP_TYPE + " INTEGER, " + PICKLIST_EXCEPT_DESC + " TEXT, "
            + PICKLIST_EXCEP_IMAGE + " TEXT, " + PICKLIST_INVOICE_LIST + " TEXT, " + PICKLIST_IS_PICKED + " INTEGER, "
            + PICKLIST_ORDER_NO + " TEXT, " + PICKLIST_BIN_ID + " INTEGER, " + PICKLIST_TYPE + " TEXT, "
            + PICKLIST_DATA + " TEXT);";

    /********* EXCEPTIONS DATA **************/

    public static final String EXCEPTION_TABLE_NAME = "exceptions";

    public static final String EXCEPTION_KEY_ID = "EXCEPTION_KEY_ID";
    public static final String EXCEPTION_REASON = "EXCEPTION_REASON";
    public static final String EXCEPTION_TRUCK_IMAGE = "EXCEPTION_TRUCK_IMAGE";
    public static final String EXCEPTION_VEHICLE_NUM = "EXCEPTION_VEHICLE_NUM";
    public static final String EXCEPTION_WH_CODE = "EXCEPTION_WH_CODE";
    public static final String EXCEPTION_PART_NUM = "EXCEPTION_PART_NUM";
    public static final String EXCEPTION_COUNT = "EXCEPTION_COUNT";
    public static final String EXCEPTION_SCAN_TIME = "EXCEPTION_SCAN_TIME";
    public static final String EXCEPTION_DATATYPE = "EXCEPTION_DATATYPE";
    public static final String EXCEPTION_INVENTORY_TYPE_DESC = "EXCEPTION_INVENTORY_TYPE_DESC";
    public static final String EXCEPTION_INVENTORY_TYPE = "EXCEPTION_INVENTORY_TYPE";
    public static final String EXCEPTION_IS_PRODUCT = "EXCEPTION_IS_PRODUCT";
    public static final String EXCEPTION_PRODUCT_ID = "EXCEPTION_PRODUCT_ID";
    public static final String EXCEPTION_PART_ID = "EXCEPTION_PART_ID";
    public static final String EXCEPTION_PRODUCT_NAME = "EXCEPTION_PRODUCT_NAME";

    public static final String EXCEPTION_CREATE_DATABASE = "CREATE TABLE " + EXCEPTION_TABLE_NAME + "("
            + EXCEPTION_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + EXCEPTION_PART_NUM + " TEXT, " + EXCEPTION_VEHICLE_NUM + " TEXT, "
            + EXCEPTION_WH_CODE + " TEXT, "
            + EXCEPTION_TYPE_ID + " INTEGER, " + EXCEPTION_COUNT + " INTEGER, " + EXCEPTION_REASON + " TEXT, "
            + EXCEPTION_TRUCK_IMAGE + " TEXT, "
            + EXCEPTION_DATATYPE + " TEXT, "
            + EXCEPTION_INVENTORY_TYPE_DESC + " TEXT, "
            + EXCEPTION_INVENTORY_TYPE + " INTEGER, "
            + EXCEPTION_IS_PRODUCT + " INTEGER, "
            + EXCEPTION_PRODUCT_ID + " INTEGER, "
            + EXCEPTION_PART_ID + " INTEGER, "
            + EXCEPTION_PRODUCT_NAME + " TEXT, "
            + EXCEPTION_SCAN_TIME + " TEXT)";

    /********* GOOD SCANNED UNLOADING DATA **************/

    public static final String UNLOADING_GOOD_DATA_TABLE_NAME = "unloadingGood";

    public static final String UNLOADING_GOOD_KEY_ID = "UNLOADING_GOOD_KEY_ID";
    public static final String UNLOADING_GOOD_VEHICLE_NUM = "UNLOADING_GOOD_VEHICLE_NUM";
    public static final String UNLOADING_GOOD_NUMBER = "UNLOADING_GOOD_NUMBER";
    public static final String UNLOADING_GOOD_COUNT = "UNLOADING_GOOD_COUNT";
    public static final String UNLOADING_GOOD_SCAN_TIME = "UNLOADING_GOOD_SCAN_TIME";
    public static final String UNLOADING_GOOD_DATATYPE = "UNLOADING_GOOD_DATATYPE";
    public static final String UNLOADING_GOOD_INVENTORY_TYPE_DESC = "UNLOADING_GOOD_INVENTORY_TYPE_DESC";
    public static final String UNLOADING_GOOD_INVENTORY_TYPE = "UNLOADING_GOOD_INVENTORY_TYPE";
    public static final String UNLOADING_GOOD_TOTAL_BOXES = "UNLOADING_GOOD_TOTAL_BOXES";
    public static final String UNLOADING_GOOD_WH_CODE = "UNLOADING_GOOD_WH_CODE";

    public static final String UNLOADING_GOOD_IS_PRODUCT = "UNLOADING_GOOD_IS_PRODUCT";
    public static final String UNLOADING_GOOD_PRODUCT_NAME = "UNLOADING_GOOD_PRODUCT_NAME";
    public static final String UNLOADING_GOOD_PRODUCT_ID = "UNLOADING_GOOD_PRODUCT_ID";
    public static final String UNLOADING_GOOD_PART_ID = "UNLOADING_GOOD_PART_ID";

    public static final String UNLOADING_GOOD_CREATE_DATABASE = "CREATE TABLE " + UNLOADING_GOOD_DATA_TABLE_NAME + "("
            + UNLOADING_GOOD_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UNLOADING_GOOD_NUMBER + " TEXT, " + UNLOADING_GOOD_VEHICLE_NUM + " TEXT, "
            + UNLOADING_GOOD_COUNT + " INTEGER, "
            + UNLOADING_GOOD_WH_CODE + " TEXT, "
            + UNLOADING_GOOD_DATATYPE + " TEXT, "
            + UNLOADING_GOOD_TOTAL_BOXES + " INTEGER, "
            + UNLOADING_GOOD_INVENTORY_TYPE_DESC + " TEXT, "
            + UNLOADING_GOOD_INVENTORY_TYPE + " INTEGER, "
            + UNLOADING_GOOD_IS_PRODUCT + " INTEGER, "
            + UNLOADING_GOOD_PRODUCT_NAME + " TEXT, "
            + UNLOADING_GOOD_PRODUCT_ID + " INTEGER, "
            + UNLOADING_GOOD_PART_ID + " INTEGER, "
            + UNLOADING_GOOD_SCAN_TIME + " TEXT)";

    /***********  INBOUND INSPECTION DATA TABLE  *************/

    public static final String INBOUND_DATA_TABLE_NAME = "inboundInspection";

    public static final String INBOUND_KEY_ID = "INBOUND_KEY_ID";
    public static final String INBOUND_PART_ID = "INBOUND_PART_ID";
    public static final String INBOUND_PART_NUM = "INBOUND_PART_NUM";
    public static final String INBOUND_NO_OF_MASTER_CARTONS = "INBOUND_NO_OF_MASTER_CARTONS";
    public static final String INBOUND_NO_OF_EXCEPTIONS = "INBOUND_NO_OF_EXCEPTIONS";
    public static final String INBOUND_STATUS = "INBOUND_STATUS";
    public static final String INBOUND_VEHICLE_NUMBER = "INBOUND_VEHICLE_NUMBER";
    public static final String INBOUND_INWARD_NUMBER = "INBOUND_INWARD_NUMBER";
    public static final String INBOUND_QUANTITY = "INBOUND_QUANTITY";
    public static final String INBOUND_DATATYPE = "INBOUND_DATATYPE";
    public static final String INBOUND_DA_NO = "INBOUND_DA_NO";
    public static final String INBOUND_INVOICE_NO = "INBOUND_INVOICE_NO";
    public static final String INBOUND_MASTER_BOX_NO = "INBOUND_MASTER_BOX_NO";
    public static final String INBOUND_NO_INVOICE = "INBOUND_NO_INVOICE";
    public static final String INBOUND_NO_MASTER_BOX = "INBOUND_NO_MASTER_BOX";
    public static final String INBOUND_EXCEP_TYPE = "INBOUND_EXCEP_TYPE";
    public static final String INBOUND_EXCEPT_TYPE_DESC = "INBOUND_EXCEPT_TYPE_DESC";
    public static final String INBOUND_INVENTORY_TYPE = "INBOUND_INVENTORY_TYPE";
    public static final String INBOUND_INVENTORY_TYPE_DESC = "INBOUND_INVENTORY_TYPE_DESC";
    public static final String INBOUND_WH_CODE = "INBOUND_WH_CODE";
    public static final String INBOUND_RECEIPT_TYPE = "INBOUND_RECEIPT_TYPE";

    public static final String INBOUND_UNIQUE = "INBOUND_UNIQUE";
    public static final String INBOUND_IS_PRODUCT = "INBOUND_IS_PRODUCT";
    public static final String INBOUND_PRODUCT_NAME = "INBOUND_PRODUCT_NAME";
    public static final String INBOUND_PRODUCT_ID = "INBOUND_PRODUCT_ID";

    public static final String INBOUND_INSPECTION_CREATE_DATABASE = "CREATE TABLE " + INBOUND_DATA_TABLE_NAME + "("
            + INBOUND_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + INBOUND_PART_ID + " INTEGER, "
            + INBOUND_PART_NUM + " TEXT, " + INBOUND_NO_OF_MASTER_CARTONS + " TEXT, " + INBOUND_NO_OF_EXCEPTIONS + " INTEGER, "
            + INBOUND_STATUS + " INTEGER, " + INBOUND_VEHICLE_NUMBER + " TEXT, "
            + INBOUND_DATATYPE + " TEXT, "
            + INBOUND_WH_CODE + " TEXT, "
            + INBOUND_DA_NO + " TEXT, " + INBOUND_INVOICE_NO + " TEXT, " + INBOUND_MASTER_BOX_NO + " TEXT, "
            + INBOUND_NO_INVOICE + " INTEGER, " + INBOUND_NO_MASTER_BOX + " INTEGER, "
            + INBOUND_INWARD_NUMBER + " TEXT, "
            + INBOUND_EXCEP_TYPE + " INTEGER, " + INBOUND_EXCEPT_TYPE_DESC + " TEXT, "
            + INBOUND_INVENTORY_TYPE + " INTEGER, " + INBOUND_INVENTORY_TYPE_DESC + " TEXT, "
            + INBOUND_QUANTITY + " INTEGER, "
            + INBOUND_UNIQUE + " TEXT, "
            + INBOUND_IS_PRODUCT + " INTEGER, "
            + INBOUND_PRODUCT_ID + " INTEGER, "
            + INBOUND_PRODUCT_NAME + " TEXT, "
            + INBOUND_RECEIPT_TYPE + " TEXT);";

    /*********** UNPACKING Box TABLE  *************/
    public static final String UNPACKING_BOX_TABLE_NAME = "unpackingBoxes";

    public static final String UNPACKING_BOX_KEY_ID = "UNPACKING_BOX_KEY_ID";
    public static final String UNPACKING_QUANTITY = "UNPACKING_QUANTITY";
    public static final String UNPACKING_STATUS = "UNPACKING_STATUS";
    public static final String UNPACKING_VEHICLE_NUMBER = "UNPACKING_VEHICLE_NUMBER";
    public static final String UNPACKING_INWARD_NUMBER = "UNPACKING_INWARD_NUMBER";
    public static final String UNPACKING_MASTER_BOX_NUMBER = "UNPACKING_MASTER_BOX_NUMBER";
    public static final String UNPACKING_BOX_ID = "UNPACKING_BOX_ID";
    public static final String UNPACKING_BOX_STATUS = "UNPACKING_BOX_STATUS";

    public static final String UNPACKING_BOX_CREATE_DATABASE = "CREATE TABLE " + UNPACKING_BOX_TABLE_NAME + "("
            + UNPACKING_BOX_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + UNPACKING_QUANTITY + " INTEGER, "
            + UNPACKING_BOX_ID + " INTEGER, "
            + UNPACKING_BOX_STATUS + " INTEGER, "
            + UNPACKING_STATUS + " INTEGER, " + UNPACKING_VEHICLE_NUMBER + " TEXT, "
            + UNPACKING_INWARD_NUMBER + " TEXT, " + UNPACKING_MASTER_BOX_NUMBER + " TEXT);";

    /*********** UNPACKING DATA TABLE  *************/
    public static final String UNPACKING_TABLE_NAME = "unpackingParts";

    public static final String UNPACKING_KEY_ID = "UNPACKING_KEY_ID";
    public static final String UNPACKING_PART_ID = "UNPACKING_PART_ID";
    public static final String UNPACKING_PART_NUM = "UNPACKING_PART_NUM";
    public static final String UNPACKING_IS_SELECTED = "UNPACKING_IS_SELECTED";
    public static final String UNPACKING_PRIMARY_CARTONS = "UNPACKING_PRIMARY_CARTONS";
    public static final String UNPACKING_PRIMARY_CARTONS_CONTAINED = "UNPACKING_PRIMARY_CARTONS_CONTAINED";
    public static final String UNPACKING_NO_OF_EXCEPTIONS = "UNPACKING_NO_OF_EXCEPTIONS";
    public static final String UNPACKING_IS_ACTED_UPON = "UNPACKING_IS_ACTED_UPON";

    public static final String UNPACKING_CREATE_DATABASE = "CREATE TABLE " + UNPACKING_TABLE_NAME + "("
            + UNPACKING_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + UNPACKING_PART_ID + " INTEGER, "
            + UNPACKING_PART_NUM + " TEXT, " + UNPACKING_QUANTITY + " INTEGER, "
            + UNPACKING_IS_SELECTED + " INTEGER, "
            + UNPACKING_PRIMARY_CARTONS + " INTEGER, "
            + UNPACKING_PRIMARY_CARTONS_CONTAINED + " INTEGER, " + UNPACKING_NO_OF_EXCEPTIONS + " INTEGER, "
            + UNPACKING_STATUS + " INTEGER, " + UNPACKING_IS_ACTED_UPON + " INTEGER, " + UNPACKING_VEHICLE_NUMBER + " TEXT, "
            + UNPACKING_INWARD_NUMBER + " TEXT, " + UNPACKING_MASTER_BOX_NUMBER + " TEXT);";

    /************************ Packing data table ****************/
    public static final String PACKING_TABLE_NAME = "packing";

    public static final String PACKING_KEY_ID = "PACKING_KEY_ID";
    public static final String PACKING_PART_ID = "PACKING_PART_ID";
    public static final String PACKING_PART_NUM = "PACKING_PART_NUM";
    public static final String PACKING_QUANTITY = "PACKING_QUANTITY";
    public static final String PACKING_PACKED_PRIMARY_CARTONS = "PACKING_PACKED_PRIMARY_CARTONS";
    public static final String PACKING_REMAINING_PRIMARY_CARTONS = "PACKING_REMAINING_PRIMARY_CARTONS";
    public static final String PACKING_INVOICE_NUMBER = "PACKING_INVOICE_NUMBER";
    public static final String PACKING_INVOICE_ID = "PACKING_INVOICE_ID";
    public static final String PACKING_SUBPROJECT_ID = "PACKING_SUBPROJECT_ID";
    public static final String PACKING_SUBPROJECT_NAME = "PACKING_SUBPROJECT_NAME";
    public static final String PACKING_SHIP_ADDRESS = "PACKING_SHIP_ADDRESS";
    public static final String PACKING_PICK_STATUS = "PACKING_PICK_STATUS";
    public static final String PACKING_WEIGHT = "PACKING_WEIGHT";
    public static final String PACKING_NO_OF_MASTER_BOXES = "PACKING_NO_OF_MASTER_BOXES";

    public static final String PACKING_CREATE_DATABASE = "CREATE TABLE " + PACKING_TABLE_NAME + "("
            + PACKING_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PACKING_PART_ID + " INTEGER, "
            + PACKING_PART_NUM + " TEXT, " + PACKING_QUANTITY + " INTEGER, " + PACKING_PACKED_PRIMARY_CARTONS + " INTEGER, "
            + PACKING_REMAINING_PRIMARY_CARTONS + " INTEGER, " + PACKING_INVOICE_NUMBER + " TEXT, " + PACKING_INVOICE_ID +
            " INTEGER, " + PACKING_SUBPROJECT_ID + " INTEGER, " + PACKING_SUBPROJECT_NAME + " TEXT, " + PACKING_SHIP_ADDRESS
            + " TEXT, " + PACKING_NO_OF_MASTER_BOXES + " INTEGER, "
            + PACKING_WEIGHT + " REAL, "
            + PACKING_PICK_STATUS + " INTEGER);";

    /*********** invoices data table ***************/
    public static final String INVOICES_TABLE_NAME = "invoices";
    public static final String INVOICES_KEY_ID = "INVOICES_KEY_ID";
    public static final String INVOICE_SUBPROJECT_ID = "INVOICE_SUBPROJECT_ID";
    public static final String INVOICES_ORDER_NUM = "INVOICES_ORDER_NUM";
    public static final String INVOICES_SUPPLIER_NAME = "INVOICES_SUPPLIER_NAME";
    public static final String INVOICES_SUPPLIER_ADDRESS = "INVOICES_SUPPLIER_ADDRESS";

    public static final String INVOICES_CREATE_DATABASE = "CREATE TABLE " + INVOICES_TABLE_NAME +
            "(" + INVOICES_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + INVOICE_SUBPROJECT_ID + " INTEGER, "
            + INVOICES_ORDER_NUM + " TEXT, "
            + INVOICES_SUPPLIER_NAME + " TEXT, " + INVOICES_SUPPLIER_ADDRESS + " TEXT);";

    /******* vehicle numbers table **************/
    public static final String VEHICLE_NUMBERS_TABLE_NAME = "vehicleNumbers";

    public static final String VEHICLE_NUMBERS_KEY_ID = "VEHICLE_NUMBERS_KEY_ID";
    public static final String VEHICLE_NUMBERS_NUMBER = "VEHICLE_NUMBERS_NUMBER";
    public static final String VEHICLE_NUMBERS_WH_CODE = "VEHICLE_NUMBERS_WH_CODE";

    public static final String VEHICLE_NUMBERS_CREATE_DATABASE = "CREATE TABLE " + VEHICLE_NUMBERS_TABLE_NAME + "("
            + VEHICLE_NUMBERS_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + VEHICLE_NUMBERS_NUMBER + " TEXT, " + VEHICLE_NUMBERS_WH_CODE + " TEXT);";

    /******* TC numbers table **************/
    public static final String TC_NUMBERS_TABLE_NAME = "tcNumbers";

    public static final String TC_NUMBERS_KEY_ID = "TC_NUMBERS_KEY_ID";
    public static final String TC_NUMBERS_NUMBER = "TC_NUMBERS_NUMBER";
    public static final String TC_NUMBERS_WH_CODE = "TC_NUMBERS_WH_CODE";

    public static final String TC_NUMBERS_CREATE_DATABASE = "CREATE TABLE " + TC_NUMBERS_TABLE_NAME + "("
            + TC_NUMBERS_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TC_NUMBERS_WH_CODE + " TEXT, "
            + TC_NUMBERS_NUMBER + " TEXT);";

    public ArrayList<Cursor> getData(String Query) {
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[]{"message"};
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2 = new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);

        try {
            String maxQuery = Query;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);

            //add value to cursor2
            Cursor2.addRow(new Object[]{"Success"});

            alc.set(1, Cursor2);
            if (null != c && c.getCount() > 0) {

                alc.set(0, c);
                c.moveToFirst();

                return alc;
            }
            return alc;
        } catch (SQLException sqlEx) {
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + sqlEx.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        } catch (Exception ex) {
            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[]{"" + ex.getMessage()});
            alc.set(1, Cursor2);
            return alc;
        }
    }

}
