package com.apptmyz.splwms.util;

import android.graphics.Bitmap;

import com.apptmyz.splwms.data.Bin;
import com.apptmyz.splwms.data.DestinationDataModel;
import com.apptmyz.splwms.data.LoadUnload;
import com.apptmyz.splwms.data.Part;
import com.apptmyz.splwms.data.RouteData;
import com.apptmyz.splwms.data.SheetDetailsResponse;
import com.apptmyz.splwms.data.UnloadingResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Globals {
    public static String lastErrMsg;
    public static int screenWidth;
    public static int screenHeight;
    public static String username;
    public static String empId;
    public static String scCode;
    public static String baseUrl;

    public static LoadUnload selectedSheet;

    public static String sheetNo;

    public static SheetDetailsResponse sheetDetailsResponse;

    public static String serverTime;
    public static long timeDiff;

    public static boolean isDataSubmitted;

    public static String conNumber;

    public static boolean xdEnabled;
    public static RouteData selectedRoute;
    public static String conNum;

    public static Bitmap bitmap;

    public static String groupId;
    public static String bayNo;
    public static HashMap<String, DestinationDataModel> destMap;
    public static Boolean isLoading = false;
    public static String vehicleNo;
    public static String partNo;
    public static String vendorId;
    public static int partID;
    public static ArrayList<Bin> bins = new ArrayList<>();
    public static UnloadingResponse unloadingResponse;

    public static boolean isPrint = false;
    public static boolean isPause = false;
    public static boolean selectedPrint = false;
    public static String BLUETOOTH_STATE;
}
