package com.apptmyz.splwms.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.LoginActivity;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.Cons;
import com.apptmyz.splwms.data.LoadUnload;
import com.apptmyz.splwms.data.NewCon;
import com.apptmyz.splwms.data.Packet;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.apptmyz.splwms.util.Globals.conNumber;

public class ScanUtils {

    // ***************************************************************************************

    public static View.OnClickListener logoutListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Context context = view.getContext();
            showAlert(context);
        }

    };
    // ***************************************************************************************
    private static Context context;
    //    private static ScanningScreen scanningScreen;
    //    private static ALScanningScreen ALScanningScreen;
    private static ProgressDialog pDialog;
    private static boolean isProgress = false;
    private static View binToastView;
    private static RelativeLayout binLayout;
    private static TextView binText;
    private static ImageView flightIv;
    private static View inputConView;
    private static EditText conNum;
//    private static ScanningScreen scanScreen;

    // ****************************************************************************************************
    private static TextWatcher conWatcher = new TextWatcher() {
        boolean isVaildPattern = true;

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String scannedValue = s.toString();
            if (scannedValue != null && scannedValue.length() > 0) {
                if (scannedValue.charAt(0) == '0') {
                    conNum.setText("");
                }

                Pattern ps = Pattern.compile(Constants.SCANNING_CHAR_PATTERN);
                Matcher ms = ps.matcher(scannedValue.toString());
                isVaildPattern = ms.matches();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String scannedValue = conNum.getText().toString();
            if (scannedValue != null) {
                scannedValue = scannedValue.trim();
                if (scannedValue.length() != Constants.scanStringLength)
                    conNum.setError(context.getString(R.string.pkt_error));
                else if (!isVaildPattern) {
                    conNum.setError(context.getString(R.string.format_error));
                }

            }
        }
    };
    private static View extraConView;
    private static EditText searchConEt;
    private static ListView searchLv;
    private static List<String> itemsList = new ArrayList<String>();
    private static List<String> filteredList = new ArrayList<String>();
    private static ImageButton clearIb;
    private static Context extraContext;

    //    public static boolean isUnloadSheet(LoadUnload sheet) {
//        if (sheet.getSheetType().equalsIgnoreCase(Constants.unloadTag))
//            return true;
//        return false;
//    }
    private static LinearLayout prevLayout, searchLayout;
    private static TextView conNumTv;
    private static RadioGroup conRadioGroup;


    // public static boolean isConPriorityVaild(Con con) {
    // boolean isVaild = true;
    // if (Constants.isDktScanPriority) {
    // String priority = con.getPriority();
    // if (priority != null) {
    // if (priority.equalsIgnoreCase(Constants.firstPriority))
    // isVaild = true;
    // else if (priority.equalsIgnoreCase(Constants.secondPriority)) {
    // if (!isPriorityDktScanCompleted(Constants.firstPriority))
    // isVaild = false;
    // } else if (priority.equalsIgnoreCase(Constants.thirdPriority)) {
    // if (!isPriorityDktScanCompleted(Constants.firstPriority)
    // || !isPriorityDktScanCompleted(Constants.secondPriority))
    // isVaild = false;
    // } else if (!isPriorityDktScanCompleted(Constants.firstPriority)
    // || !isPriorityDktScanCompleted(Constants.secondPriority)
    // || !isPriorityDktScanCompleted(Constants.thirdPriority)) {
    // isVaild = false;
    // }
    // }
    // }
    // return isVaild;
    // }

    // public static boolean isAllPriorityConScanned() {
    // boolean isVaild = true;
    // if (Constants.isDktScanPriority) {
    // if (!isPriorityDktScanCompleted(Constants.firstPriority)
    // || !isPriorityDktScanCompleted(Constants.secondPriority)
    // || !isPriorityDktScanCompleted(Constants.thirdPriority)) {
    // isVaild = false;
    // }
    // return isVaild;
    // }
    // return true;
    // }
    //
    // private static boolean isPriorityDktScanCompleted(String
    // checkForPriority) {
    // boolean isVaild = true;
    // for (Docket docket : Globals.submitDockets) {
    // String priority = docket.getPriority();
    // boolean isScanCompleted = docket.isScanCompleted();
    // if (priority != null && priority.equalsIgnoreCase(checkForPriority)
    // && !isScanCompleted) {
    // isVaild = false;
    // break;
    // }
    // }
    // return isVaild;
    // }

    // public static void playErrForDktPriority(Context context) {
    // if (Constants.isDktScanPriority) {
    //
    // if (!isPriorityDktScanCompleted(Constants.firstPriority))
    // Utils.playAudioList(context, R.raw.red_scan_first);
    // else if (!isPriorityDktScanCompleted(Constants.secondPriority))
    // Utils.playAudioList(context, R.raw.yellow_scan_first);
    // else if (!isPriorityDktScanCompleted(Constants.thirdPriority))
    // Utils.playAudioList(context, R.raw.green_scan_first);
    // else
    // Utils.playAudioList(context, R.raw.high_priority_scan);
    // }
    // }
    private static AlertDialog extraAlertDialog;
    private static TextWatcher extraConWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {
            String str = searchConEt.getText().toString();
//            if (str.length() > 0)
//                clearIb.setVisibility(View.VISIBLE);
//            else
//                clearIb.setVisibility(View.INVISIBLE);

            filteredList.clear();

            if (Utils.isValidString(str)) {
                filteredList = filterData(itemsList, searchConEt.getText()
                        .toString().trim());
                searchLv.setAdapter(new MyAdapter(context, R.layout.packet_view, filteredList));
            } else {
                searchLv.setAdapter(new MyAdapter(context, R.layout.packet_view, itemsList));
            }

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    private static View packetView;
    //    private static ScanningScreen scnScreen;
    //    private static ALScanningScreen alScanningScreen;
//    private static LoadingProcessScreen loadingProcessScreen;
    private static EditText packetNumEt;

    // ****************************************************************************************************

    /* Show Manual Keyboard for extra packet */
    private static RadioGroup radioGroup;
    private static RadioButton pktRb, mbRb;
    private static boolean isPkt = true;
//    private static TextWatcher watcher = new TextWatcher() {
//        boolean isVaildPattern = true;
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before,
//                                  int count) {
//            String scannedValue = s.toString();
//            if (scannedValue != null && scannedValue.length() > 0) {
//                if (scannedValue.charAt(0) == '0') {
//                    packetNumEt.setText("");
//                }
//                if (scannedValue.length() < Constants.scanStringLength)
//                    scnScreen.scannedPktEt.setText(scannedValue.substring(0,
//                            scannedValue.length()));
//
//                Pattern ps = Pattern.compile(Constants.SCANNING_CHAR_PATTERN);
//                Matcher ms = ps.matcher(scannedValue.toString());
//                isVaildPattern = ms.matches();
//            }
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count,
//                                      int after) {
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//            String scannedValue = packetNumEt.getText().toString();
//            if (scannedValue != null) {
//                scannedValue = scannedValue.trim();
//                if (scannedValue.length() != Constants.scanStringLength)
//                    packetNumEt.setError(context.getString(R.string.pkt_error));
//                else if (!isVaildPattern) {
//                    packetNumEt.setError(context
//                            .getString(R.string.format_error));
//                }
//
//            }
//        }
//    };
//    private static TextWatcher alwatcher = new TextWatcher() {
//        boolean isVaildPattern = true;
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before,
//                                  int count) {
//            String scannedValue = s.toString();
//            if (scannedValue != null && scannedValue.length() > 0) {
//                if (scannedValue.charAt(0) == '0') {
//                    packetNumEt.setText("");
//                }
//                if (scannedValue.length() < Constants.scanStringLength)
//                    alScanningScreen.scannedPktEt.setText(scannedValue.substring(0,
//                            scannedValue.length()));
//
//                Pattern ps = Pattern.compile(Constants.SCANNING_CHAR_PATTERN);
//                Matcher ms = ps.matcher(scannedValue.toString());
//                isVaildPattern = ms.matches();
//            }
//        }
//
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count,
//                                      int after) {
//
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//            String scannedValue = packetNumEt.getText().toString();
//            if (scannedValue != null) {
//                scannedValue = scannedValue.trim();
//                if (scannedValue.length() != Constants.scanStringLength)
//                    packetNumEt.setError(context.getString(R.string.pkt_error));
//                else if (!isVaildPattern) {
//                    packetNumEt.setError(context
//                            .getString(R.string.format_error));
//                }
//
//            }
//        }
//    };

    public static void showAlert(final Context context) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(context.getString(R.string.alert_dialog_title));
        alert.setMessage(context.getString(R.string.logout_alert));
        alert.setPositiveButton(context.getString(R.string.ok_btn),
                new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        logout(context);
                    }
                });
        alert.setNegativeButton(context.getString(R.string.cancel_btn),
                new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alert.show();
    }

    private static void logout(Context context) {
        Activity activity = ((Activity) context);

        DataSource dataSource = new DataSource(context);

        dataSource.sharedPreferences.delete(Constants.USERNAME_PREF);
        dataSource.sharedPreferences.delete(Constants.PASSWORD_PREF);
        dataSource.sharedPreferences.delete(Constants.SCCODE_PREF);

        Intent intent = new Intent(context, LoginActivity.class);
        ComponentName cn = intent.getComponent();
//        Intent mainIntent = IntentCompat.makeRestartActivityTask(cn);
//        activity.startActivity(mainIntent);
//        activity.finish();
    }

    private static void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(context);
            // pDialog.setTitle(Constants.LOADING);
            pDialog.setMessage(context
                    .getString(R.string.progress_dialog_title));
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

//    public static void showExtraCon(final Context context, final int scanPkt,
//                                    final String scanTime, final HashMap<String, String> collisionCons,
//                                    final boolean isMb, final boolean isManuallyScanned) {
//
//        final ScanningScreen scScreen = (ScanningScreen) context;
//
//        final String[] items = new String[collisionCons.size()];
//        int i = 0;
//        for (Map.Entry<String, String> entry : collisionCons.entrySet()) {
//            items[i] = entry.getKey();
//            i++;
//        }
//
//        Arrays.sort(items);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(context.getString(R.string.which_con));
//        builder.setCancelable(false);
//
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                String conNumber = items[which]; // dockets.get(which);
//                showConfirmationDialog(context, scanPkt, scanTime,
//                        collisionCons, conNumber, isMb, isManuallyScanned);
//            }
//
//        });
//
//        builder.setPositiveButton(context.getString(R.string.new_con),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        enterConNumber(context, null, scanPkt, scanTime, isMb,
//                                isManuallyScanned);
//                    }
//                });
//
//        if (!isUnloadSheet(Globals.selectedSheet)) {
//            builder.setNegativeButton(context.getString(R.string.cancel_btn),
//                    new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // if (scanDkts != null) {
//                            // scanDkts.addExtra(Constants.INVALID_DKT_NUMBER,
//                            // scanPkt, scanTime, isMb);
//                            // }
//                            showConfirmCancelDialog(context, scanPkt, scanTime,
//                                    collisionCons, isMb, isManuallyScanned);
//                            if (dialog != null)
//                                dialog.dismiss();
//                        }
//                    });
//        }
//
//        builder.show();
//
//    }

//    public static void AutoLoadingloadAllData(Context mContext) {
//        if (!isProgress) {
//            isProgress = true;
//            context = mContext;
//            ALScanningScreen = (ALScanningScreen) context;
//            new AutoLoadingLoad().execute();
//        }
//    }

//    public static void loadAllData(Context mContext) {
//        if (!isProgress) {
//            isProgress = true;
//            context = mContext;
//            scanningScreen = (ScanningScreen) context;
//            new Load().execute();
//        }
//    }

    public static String getGroupCounterText(int noOfScannedPkts, int childCount) {
        return noOfScannedPkts + Constants.SLASH + childCount;
    }

    public static ImageLoader getImageLoader(Context context) {
        return new ImageLoader(context, ScanUtils.getSheetFolderName());
    }

    public static ImageLoader getImageLoaderCon(Context context) {
        return new ImageLoader(context, conNumber);
    }

    public static ImageLoader getImageLoaderQc(Context context) {
        return new ImageLoader(context, "QC" + ScanUtils.getSheetFolderName());
    }

    public static ImageLoader getImageLoaderBox(Context context) {
        return new ImageLoader(context, "Box");
    }

    public static String getSheetFolderName() {
//        String sheetNo = Globals.selectedSheet.getSheetNo();
//        String carrier = Globals.selectedSheet.getCarrier();
        String partNo = Globals.partNo;
        String vehicleNo = Globals.vehicleNo;
//
//        String fileName = sheetNo + "__" + carrier;
        String fileName = "";
        if (Utils.isValidString(Globals.vehicleNo)) {
            fileName = vehicleNo +"__"+ partNo;
        }

        return fileName;
    }

    public static boolean isLoadSheet(LoadUnload sheet) {
        if (sheet.getSheetType().equalsIgnoreCase(Constants.loadTag))
            return true;

        return false;
    }

    public static boolean isUnloadSheet(String sheet) {
        if (sheet.equalsIgnoreCase(Constants.unloadTag))
            return true;
        return false;
    }

    public static int conBackgroundRes(Cons con) {
        int bgId = R.drawable.docket_bg;
        String priority = con.getPriority();
        if (priority != null) {
            if (priority.equalsIgnoreCase(Constants.firstPriority))
                bgId = R.drawable.red_docket_bg;
            else if (priority.equalsIgnoreCase(Constants.secondPriority))
                bgId = R.drawable.yellow_docket_bg;
            else if (priority.equalsIgnoreCase(Constants.thirdPriority))
                bgId = R.drawable.green_docket_bg;
        }

        return bgId;
    }

    public static int conBackgroundRes(NewCon con) {
        int bgId = R.drawable.new_green_docket_bg; //R.drawable.docket_bg;
        String priority = con.getPriority();
        if (priority != null) {
            if (priority.equalsIgnoreCase(Constants.firstPriority))
                bgId = R.drawable.red_docket_bg;
            else if (priority.equalsIgnoreCase(Constants.secondPriority))
                bgId = R.drawable.brown_docket_bg;  // using brown instead of yellow as yellow is already used when short reason is recorded at con level
//            else if (priority.equalsIgnoreCase(Constants.thirdPriority))
//                bgId = R.drawable.green_docket_bg;
        }

        return bgId;
    }

    public static void playBin(Context context, String bin, String color,
                               boolean isProd) {

        if (Utils.isValidString(bin) && !bin.equalsIgnoreCase("-"))
            showBinToast(context, bin, color, isProd);
        else {
            if (isProd)
                showBinToast(context, "", color, isProd);
            Utils.playAudioList(context, R.raw.scanned);
        }
    }

    public static void showBinToast(Context context, String bin, String color,
                                    boolean isProd) {
        LayoutInflater factory = LayoutInflater.from(context);

        binToastView = factory.inflate(R.layout.toast_view, null);
        binLayout = (RelativeLayout) binToastView
                .findViewById(R.id.layout_toast);
        binLayout.setBackgroundColor(Color.parseColor(color));
        binText = (TextView) binToastView.findViewById(R.id.tv_bin_toast);
        flightIv = (ImageView) binToastView.findViewById(R.id.iv_flight);
        if (isProd) {
            flightIv.setVisibility(View.VISIBLE);
        } else
            flightIv.setVisibility(View.GONE);

        Toast toast = new Toast(context);
        toast.setView(binToastView);

        binText.setText(bin);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
//        toast.setDuration(500);
        toast.setDuration(250); //changed by gayatri as per aloks req
        toast.show();
        Utils.dissmissKeyboard(binToastView);
    }

//    public static void enterConNumber(final Context context,
//                                      final String conNumber, final int pktNo, final String scanTime,
//                                      final boolean isMb, final boolean isManuallyScanned) {
//        LayoutInflater factory = LayoutInflater.from(context);
//
//        inputConView = factory.inflate(R.layout.input_extra_docket, null);
//        conNum = (EditText) inputConView.findViewById(R.id.et_input_docket);
//        conNum.addTextChangedListener(conWatcher);
//
//        scanScreen = (ScanningScreen) context;
//
//        if (conNumber != null && conNumber.length() > 0) {
//            conNum.setText(conNumber.toString().trim());
//        }
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setView(inputConView);
//
//        builder.setTitle(context.getString(R.string.con_number));
//        builder.setPositiveButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//
//                });
//        builder.setNegativeButton(context.getString(R.string.ok_btn),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        boolean isVaild = false;
//
//                        String con = conNum.getText().toString();
//                        if (con != null) {
//                            con = con.trim();
//                            if (con.length() == Constants.scanStringLength) {
//                                showConfirmationPopUp(context, con, pktNo,
//                                        scanTime, isMb, isManuallyScanned);
//
//                                isVaild = true;
//
////                            } else {
////                                Utils.showToast(context, "Please enter 9 digit con number");
//                            }
////                        } else {
////                            Utils.showToast(context, "Please enter 9 digit con number");
//                        }
//                        if (!isVaild)
//                            scanScreen.startTimer();
//                    }
//
//                });
//        AlertDialog alertDialog = builder.create();
//        alertDialog.setCancelable(false);
//        alertDialog.show();
//
//    }

//    @SuppressWarnings("deprecation")
//    private static void showConfirmationPopUp(final Context context,
//                                              final String conNumber, final int pktNumber, final String scanTime,
//                                              final boolean isMb, final boolean isManuallyScanned) {
//        AlertDialog dlg = new AlertDialog.Builder(context).create();
//        dlg.setTitle(context.getString(R.string.confirmation));
//        dlg.setMessage("Are you sure the con number " + conNumber
//                + " is correct ?");
//        dlg.setButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        enterConNumber(context, conNumber, pktNumber, scanTime,
//                                isMb, isManuallyScanned);
//                        dialog.dismiss();
//                    }
//                });
//        dlg.setButton2(context.getString(R.string.ok_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        // scanDockets.addExtraPacket(docketNumber, pktNumber,
//                        // scanTime,
//                        // false);
//                        DataSource source = new DataSource(context);
//
//                        int id = source.consData.existsId(
//                                Globals.selectedSheet.getSheetNo(), conNumber,
//                                pktNumber);
//                        if (id == -1)
//                            scanScreen.addExtra(conNumber, pktNumber, scanTime,
//                                    isMb, isManuallyScanned);
//                        else
//                            scanScreen.resolveCollision(conNumber, pktNumber,
//                                    isManuallyScanned);
//
//                        dialog.dismiss();
//                    }
//                });
//
//        dlg.show();
//
//    }

//    public static void showCommonCon(final Context context, final int scanPkt,
//                                     final String scanTime, final HashMap<String, String> collisionCons,
//                                     final boolean isMb, final boolean isManuallyScanned) {
//
//        final String[] items = new String[collisionCons.size()];
//
//        int i = 0;
//        for (Map.Entry<String, String> entry : collisionCons.entrySet()) {
//            items[i] = entry.getKey();
//            i++;
//        }
//
//        Arrays.sort(items);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setTitle(context.getString(R.string.which_con));
//        builder.setCancelable(false);
//
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                String dktNumber = items[which];
//                if (Utils.isValidString(dktNumber))
//                    showConConfirmationPopUp(context, scanPkt, scanTime,
//                            collisionCons, isMb, dktNumber, isManuallyScanned);
//            }
//        });
//
//        builder.setPositiveButton(context.getString(R.string.new_con),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        enterConNumber(context, null, scanPkt, scanTime, isMb,
//                                isManuallyScanned);
//                    }
//                });
//        builder.setNegativeButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        if (dialog != null)
//                            dialog.dismiss();
//                    }
//                });
//
//        builder.show();
//
//    }

//    @SuppressWarnings("deprecation")
//    private static void showConConfirmationPopUp(final Context context,
//                                                 final int scanPkt, final String scanTime,
//                                                 final HashMap<String, String> collisionDkts, final boolean isMb,
//                                                 final String conNumber, final boolean isManuallyScanned) {
//        final ScanningScreen scScreen = (ScanningScreen) context;
//
//        AlertDialog dlg = new AlertDialog.Builder(context).create();
//        dlg.setTitle(context.getString(R.string.confirmation));
//        dlg.setMessage("Are you sure the packet number " + scanPkt
//                + " belongs to the con " + conNumber + " ?");
//
//        dlg.setButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        showCommonCon(context, scanPkt, scanTime,
//                                collisionDkts, isMb, isManuallyScanned);
//                        dialog.dismiss();
//                    }
//                });
//
//        dlg.setButton2(context.getString(R.string.ok_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        if (Utils.isValidString(conNumber)) {
//                            if (collisionDkts.containsKey(conNumber)) {
//                                scScreen.resolveCollision(conNumber, scanPkt,
//                                        isManuallyScanned);
//                            } else
//                                scScreen.addExtra(conNumber, scanPkt, scanTime,
//                                        isMb, isManuallyScanned);
//                        }
//                        dialog.dismiss();
//                    }
//                });
//
//        dlg.show();
//
//    }

//    public static void showExtraCon(final Context context, final int scanPkt,
//                                    final String scanTime, final HashMap<String, String> collisionCons,
//                                    final boolean isMb, final boolean isManuallyScanned) {
//
//        final ScanningScreen scScreen = (ScanningScreen) context;
//
//        extraContext = context;
//
////        final String[] items = new String[collisionCons.size()];
//        itemsList = new ArrayList<String>();
//        int i = 0;
//        for (Map.Entry<String, String> entry : collisionCons.entrySet()) {
////            items[i] = entry.getKey();
//            itemsList.add(entry.getKey());
//            i++;
//        }
//
//        Collections.sort(itemsList);
//
//        LayoutInflater factory = LayoutInflater.from(context);
//
//
//        extraConView = factory.inflate(R.layout.extra_popup, null);
//        prevLayout = (LinearLayout) extraConView.findViewById(R.id.layout_prev_extra);
//        searchLayout = (LinearLayout) extraConView.findViewById(R.id.layout_con_search);
//        conNumTv = (TextView) extraConView.findViewById(R.id.tv_con);
//        conRadioGroup = (RadioGroup) extraConView.findViewById(R.id.rg_con);
//
//
//        searchConEt = (EditText) extraConView.findViewById(R.id.et_search);
////        clearIb = (ImageButton) extraConView.findViewById(R.id.ib_clear);
//        searchConEt.addTextChangedListener(extraConWatcher);
//        searchLv = (ListView) extraConView.findViewById(R.id.lv_search);
//        searchLv.setAdapter(new MyAdapter(context, R.layout.packet_view, itemsList));
//
//        final String con = ScanningScreen.extraCon;
//        if (Utils.isValidString(con)) {
//            prevLayout.setVisibility(View.VISIBLE);
//            String sourceString = "Is the Con Number " + "<b>" + con + "</b>";
//            conNumTv.setText(Html.fromHtml(sourceString));
////            conNumTv.setText("Is the Con Number " + con);
//        } else {
//            prevLayout.setVisibility(View.GONE);
//            searchLayout.setVisibility(View.VISIBLE);
//        }
//
//
////        clearIb.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                if(Utils.isValidArrayList(itemsList))
////                    searchLv.setAdapter(new MyAdapter(context, R.layout.packet_view, itemsList));
////            }
////        });
//
////        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        extraAlertDialog = new AlertDialog.Builder(context).create();
//        extraAlertDialog.setView(extraConView);
//        extraAlertDialog.setTitle(context.getString(R.string.which_con));
//        extraAlertDialog.setCancelable(false);
//
////        builder.setItems(items, new DialogInterface.OnClickListener() {
////            @Override
////            public void onClick(DialogInterface dialog, int which) {
////                String conNumber = items[which]; // dockets.get(which);
////                showConfirmationDialog(context, scanPkt, scanTime,
////                        collisionCons, conNumber, isMb, isManuallyScanned);
////            }
////
////        });
//
//        if (Utils.isValidString(con)) {
//            extraAlertDialog.setButton3(context.getString(R.string.ok_btn),
//                    new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            if (prevLayout.getVisibility() == View.VISIBLE) {
//                                if (conRadioGroup.getCheckedRadioButtonId() == R.id.rb_no)
//                                    enterConNumber(context, null, scanPkt, scanTime, isMb,
//                                            isManuallyScanned);
//                                else {
//                                    ScanningScreen screen = (ScanningScreen) context;
//                                    screen.addExtra(con, scanPkt, scanTime, false,
//                                            isManuallyScanned);
//                                    dialog.dismiss();
//                                }
//                            } else {
//                                enterConNumber(context, null, scanPkt, scanTime, isMb,
//                                        isManuallyScanned);
//                            }
//                        }
//
//
//                    });
//        } else {
//            extraAlertDialog.setButton3(context.getString(R.string.new_con),
//                    new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            if (prevLayout.getVisibility() == View.VISIBLE) {
//                                if (conRadioGroup.getCheckedRadioButtonId() == R.id.rb_no)
//                                    enterConNumber(context, null, scanPkt, scanTime, isMb,
//                                            isManuallyScanned);
//                                else {
//                                    ScanningScreen screen = (ScanningScreen) context;
//                                    screen.addExtra(con, scanPkt, scanTime, false,
//                                            isManuallyScanned);
//                                    dialog.dismiss();
//                                }
//                            } else {
//                                enterConNumber(context, null, scanPkt, scanTime, isMb,
//                                        isManuallyScanned);
//                            }
//                        }
//
//
//                    });
//        }
//
//        conRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                if (checkedId == R.id.rb_yes) {
//                    searchLayout.setVisibility(View.GONE);
//                    Button b = extraAlertDialog.getButton(AlertDialog.BUTTON3);
//                    if (b != null) {
//                        b.setText("OK");
//                    }
//                } else if (checkedId == R.id.rb_no) {
//                    searchLayout.setVisibility(View.VISIBLE);
//                    Button b = extraAlertDialog.getButton(AlertDialog.BUTTON3);
//                    if (b != null) {
//                        b.setText("New Con");
//                    }
//                }
//            }
//        });
//
//        if (!isUnloadSheet(Globals.selectedSheet.getSheetType())) {
//            extraAlertDialog.setButton2(context.getString(R.string.cancel_btn),
//                    new DialogInterface.OnClickListener() {
//
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            // if (scanDkts != null) {
//                            // scanDkts.addExtra(Constants.INVALID_DKT_NUMBER,
//                            // scanPkt, scanTime, isMb);
//                            // }
//                            showConfirmCancelDialog(context, scanPkt, scanTime,
//                                    collisionCons, isMb, isManuallyScanned);
//                            if (dialog != null)
//                                dialog.dismiss();
//                        }
//                    });
//        }
//
//
//        searchLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String conNumber = (String) parent.getAdapter().getItem(position);
//                if (Utils.isValidString(conNumber)) {
//                    showConfirmationDialog(context, scanPkt, scanTime,
//                            collisionCons, conNumber, isMb, isManuallyScanned);
//                    extraAlertDialog.dismiss();
//                }
//            }
//        });
//
//        extraAlertDialog.show();
//
//    }

    private static List<String> filterData(List<String> orgList, String s) {
        List<String> l = new ArrayList<String>();
        if (Utils.isValidArrayList((ArrayList<?>) orgList)) {
            for (String st : itemsList) {
                if (st.contains(s))
                    l.add(st);
            }
        }

        return l;
    }


    // ****************************************************************************************************

    /* Show Manual Keyboard */

//    @SuppressWarnings("deprecation")
//    private static void showConfirmCancelDialog(final Context context,
//                                                final int scanPkt, final String scanTime,
//                                                final HashMap<String, String> collisionDkts, final boolean isMb,
//                                                final boolean isManuallyScanned) {
//
//        AlertDialog dlg = new AlertDialog.Builder(context).create();
//        dlg.setTitle(context.getString(R.string.confirmation));
//        dlg.setMessage(context.getString(R.string.cancel_conf));
//        dlg.setButton(context.getString(R.string.no),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        showExtraCon(context, scanPkt, scanTime, collisionDkts,
//                                isMb, isManuallyScanned);
//                        dialog.dismiss();
//                    }
//                });
//        dlg.setButton2(context.getString(R.string.yes),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////						ScanningScreen screen = (ScanningScreen) context;
////						if (screen != null) {
////							screen.addExtra(Constants.INVALID_DKT_NUMBER,
////									scanPkt, scanTime, isMb, isManuallyScanned);
////						}
//                        dialog.dismiss();
//                    }
//                });
//
//        dlg.show();
//
//    }
//
//    @SuppressWarnings("deprecation")
//    private static void showConfirmationDialog(final Context context,
//                                               final int scanPkt, final String scanTime,
//                                               final HashMap<String, String> collisionCons,
//                                               final String conNumber, final boolean isMb,
//                                               final boolean isManuallyScanned) {
//
//        AlertDialog dlg = new AlertDialog.Builder(context).create();
//        dlg.setTitle(context.getString(R.string.confirmation));
//        dlg.setMessage("Are you sure the con number " + conNumber
//                + " is correct ?");
//        dlg.setButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        showExtraCon(context, scanPkt, scanTime, collisionCons,
//                                isMb, isManuallyScanned);
//                        dialog.dismiss();
//                    }
//                });
//        dlg.setButton2(context.getString(R.string.ok_btn),
//                new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        ScanningScreen screen = (ScanningScreen) context;
//                        screen.addExtra(conNumber, scanPkt, scanTime, false,
//                                isManuallyScanned);
//                        dialog.dismiss();
//                    }
//                });
//
//        dlg.show();
//
//    }

    public static boolean isSameCon(Cons c1, Cons c2) {
        boolean isSameDocket = false;

        if (c1.getConNo().equalsIgnoreCase(c2.getConNo())) {
            ArrayList<Packet> pkts1 = (ArrayList<Packet>) c1.getPackets();
            ArrayList<Packet> pkts2 = (ArrayList<Packet>) c2.getPackets();

            if (Utils.isValidArrayList(pkts1) && Utils.isValidArrayList(pkts2)
                    && pkts1.size() == pkts2.size()) {
                for (Packet pkt1 : pkts1) {
                    isSameDocket = false;
                    for (Packet pkt2 : pkts2) {
                        if (pkt1.getPktNo() == pkt2.getPktNo()) {
                            isSameDocket = true;
                            break;
                        }
                    }
                    if (!isSameDocket) {
                        break;
                    }
                }
            }

        }

        return isSameDocket;
    }

    public static boolean isSamePacket(Packet pkt1, Packet pkt2) {
        boolean isSamePacket = false;

        if (pkt1.getPktNo() == pkt1.getPktNo()) {
            if (pkt1.equals(pkt2))
                isSamePacket = true;
        }

        return isSamePacket;
    }

//    public static void enterPacketNumberNew(Context context) {
//        LayoutInflater factory = LayoutInflater.from(context);
//
//        scnScreen = (ScanningScreen) context;
//
//        packetView = factory.inflate(R.layout.input_packet, null);
//        packetNumEt = (EditText) packetView.findViewById(R.id.et_input_packet);
//
//        packetNumEt.addTextChangedListener(watcher);
//        packetNumEt.setInputType(InputType.TYPE_CLASS_NUMBER);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
////        builder.setTitle(context.getString(R.string.pkt_number));
//        builder.setView(packetView);
//
//        builder.setPositiveButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        scnScreen.isManual = false;
//                        scnScreen.startTimer();
//                    }
//
//                });
//        builder.setNegativeButton(context.getString(R.string.ok_btn),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        boolean isValid = false;
//
//                        String scannedValue = packetNumEt.getText().toString();
//                        if (scannedValue != null) {
//                            scannedValue = scannedValue.trim();
//
//                            if (scannedValue.length() == Constants.scanStringLength) {
//                                scnScreen.checkScanPacket(scannedValue, false,
//                                        true);
//                                isValid = true;
//                                scnScreen.isManual = false;
//                            } else {
//                                scnScreen.isManual = false;
//                                scnScreen.scannedPktEt.setText("");
//                                scnScreen.setScanTextChangedListener(false);
//                            }
//                        }
//                    }
//
//                });
//        AlertDialog alertDialog = builder.create();
//        alertDialog.setCancelable(false);
//        alertDialog.show();
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f);
//        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f);
//    }

//    public static void enteralPacketNumberNew(Context context) {
//        LayoutInflater factory = LayoutInflater.from(context);
//
//        alScanningScreen = (ALScanningScreen) context;
//
//        packetView = factory.inflate(R.layout.input_packet, null);
//        packetNumEt = (EditText) packetView.findViewById(R.id.et_input_packet);
//
//        packetNumEt.addTextChangedListener(alwatcher);
//        packetNumEt.setInputType(InputType.TYPE_CLASS_NUMBER);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
////        builder.setTitle(context.getString(R.string.pkt_number));
//        builder.setView(packetView);
//
//        builder.setPositiveButton(context.getString(R.string.cancel_btn),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                        alScanningScreen.isManual = false;
//                        alScanningScreen.startTimer();
//                    }
//
//                });
//        builder.setNegativeButton(context.getString(R.string.ok_btn),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                        boolean isValid = false;
//
//                        String scannedValue = packetNumEt.getText().toString();
//                        if (scannedValue != null) {
//                            scannedValue = scannedValue.trim();
//
//                            if (scannedValue.length() == Constants.scanStringLength) {
//                                alScanningScreen.checkScanPacket(scannedValue, false,
//                                        true);
//                                isValid = true;
//                                alScanningScreen.isManual = false;
//                            } else {
//                                alScanningScreen.isManual = false;
//                                alScanningScreen.scannedPktEt.setText("");
//                                alScanningScreen.setScanTextChangedListener(false);
//                            }
//                        }
//                    }
//
//                });
//        AlertDialog alertDialog = builder.create();
//        alertDialog.setCancelable(false);
//        alertDialog.show();
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f);
//        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.0f);
//    }


    public boolean onEvaluateInputViewShown(Context context) {
        Configuration config = context.getResources().getConfiguration();
        return config.keyboard == Configuration.KEYBOARD_NOKEYS
                || config.hardKeyboardHidden == Configuration.KEYBOARDHIDDEN_YES;
    }

//    public static class AutoLoadingLoad extends AsyncTask<Object, Object, List<NewCon>> {
//        @Override
//        protected void onPreExecute() {
//            showProgressDialog();
//            Utils.logD("Start loading the data");
//        }
//
//        @Override
//        protected List<NewCon> doInBackground(Object... arg0) {
//
//            List<NewCon> consList = null;
//            try {
//
//                DataSource dataSource = new DataSource(context);
//
//                String sheetNum = Globals.sheetNo;
//
////                if (Globals.selectedSheet != null)
////                    sheetNum = Globals.selectedSheet.getSheetNo();
//
//                if (Utils.isValidString(sheetNum)) {
//                    consList = dataSource.consData.getConsData(sheetNum,
//                            ALScanningScreen.gson, false);
//                }
//
//            } catch (Exception e) {
//            }
//            return consList;
//
//        }
//
//        @Override
//        protected void onPostExecute(List<NewCon> result) {
//
//            List<NewCon> consList = (List<NewCon>) result;
//            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//
//            if (pDialog != null && pDialog.isShowing()) {
//                pDialog.dismiss();
//                try {
//                    if (Utils.isValidArrayList((ArrayList<?>) consList)) {
//                        for (NewCon c : consList) {
////                            Utils.logE(c.toString());
////
//                        }
//
//                        ALScanningScreen.resetAllFragmentsData();
//                        // scanningScreen.setFc();
//                    } else {
//                        ALScanningScreen.getDockets();
//                    }
//
//                } catch (Exception e) {
//                }
//
//                pDialog = null;
//                isProgress = false;
//                Utils.logD("Loaded data");
//            }
//
//            super.onPostExecute(result);
//        }
//    }

//    public static class Load extends AsyncTask<Object, Object, List<NewCon>> {
//        @Override
//        protected void onPreExecute() {
//            showProgressDialog();
//            Utils.logD("Start loading the data");
//        }
//
//        @Override
//        protected List<NewCon> doInBackground(Object... arg0) {
//
//            List<NewCon> consList = null;
//            try {
//
//                DataSource dataSource = new DataSource(context);
//
//                String sheetNum = "";
//
//                if (Globals.selectedSheet != null)
//                    sheetNum = Globals.selectedSheet.getSheetNo();
//
//                if (Utils.isValidString(sheetNum)) {
//                    consList = dataSource.consData.getConsData(sheetNum,
//                            scanningScreen.gson, false);
//                }
//
//            } catch (Exception e) {
//            }
//            return consList;
//
//        }
//
//        @Override
//        protected void onPostExecute(List<NewCon> result) {
//
//            List<NewCon> consList = (List<NewCon>) result;
//
//
//            if (pDialog != null && pDialog.isShowing()) {
//                pDialog.dismiss();
//                try {
//                    if (Utils.isValidArrayList((ArrayList<?>) consList)) {
////                        for (NewCon c : consList) {
////                        }
//
//                        scanningScreen.resetAllFragmentsData();
//                        // scanningScreen.setFc();
//                    } else {
//                        scanningScreen.getDockets();
//                    }
//
//                } catch (Exception e) {
//                }
//
//                pDialog = null;
//                isProgress = false;
//                Utils.logD("Loaded data");
//            }
//
//            super.onPostExecute(result);
//        }
//    }

    public static class ViewHolder {
        public TextView conTv;
    }

    public static class MyAdapter extends ArrayAdapter<String> {
        private LayoutInflater inflater;
        private int layoutId;

        public MyAdapter(Context context, int layoutId, List<String> objects) {
            super(context, 0, objects);
            this.layoutId = layoutId;
            this.inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
//            if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.conTv = (TextView) convertView
                    .findViewById(R.id.tv_pkt_no);
            convertView.setTag(holder);
//            } else {
//                holder = (ViewHolder) convertView.getTag();
//            }

            String pkt = getItem(position);
            holder.conTv.setText(pkt.trim());

            return convertView;
        }

    }


}
