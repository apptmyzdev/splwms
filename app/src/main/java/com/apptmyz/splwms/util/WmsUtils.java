package com.apptmyz.splwms.util;

public class WmsUtils {
    public static final String BASE_URL = "https://espl.spoton.co.in/spl/api/";//"https://espl.spoton.co.in/erts/api/wms/";//"http://104.237.4.37:8080/erts/api/wms/";//"http://172.0.1.124:8080/spl/api/";//
    public static final String loginTag = "wms/login";
    public static final String putAwayTag = "wms/putaway";
    public static final String dataTag = "data";
    public static final String slashTag = "/";
    public static final String vehicleNumTag = "vehicleNo";
    public static final String questionMarkTag = "?";
    public static final String equalsTag = "=";
    public static final String ampersandTag = "&";
    public static final String completeTag = "complete";
    public static final String generateTag = "wms/generate";
    public static final String grnTag = "grn";
    public static final String vendorMasterTag = "vendor";
    public static final String submitTag = "submit";
    public static final String tokenTag = "wms/token";

    public static final String subprojectsTag = "subproject";
    public static final String customerTag = "customer";
    public static final String nodeTag = "node";
    public static final String picklistTag = "wms/picklist";
    public static final String binTag = "wms/bin";
    public static final String bin = "bin";
    public static final String allTag = "all";
    public static final String skipBinTag = "wms/skipbin";
    public static final String skipBin = "skipbin";
    public static final String reasonTag = "reason";
    public static final String pickingTag = "wms/picking";
    public static final String inventoryTag = "wms/inventory";

    public static String unloadingUrlTag = "unloading/get/";
    public static String unloadingSubmitUrlTag = "unloading/submit";
    public static String routeMasterUrlTag = "vendor/route?vendor=";
    public static String vehicleMasterUrlTag = "route/vehicle?route=";
    public static String loadingDataUrlTag = "loading/data?vendor=";
    public static String loadingSubmitDataUrlTag = "loading/submit?vendor=";
    public static String imageSubmitUrlTag = "unloading/save/vehicle/img";

    public static final String searchTag = "wms/search";
    public static final String partTag = "wms/part";
    public static final String exceptionTag = "exception";
    public static final String typeTag = "type";
    public static final String transferTag = "transfer";

    public static final String masterTag = "master";
    public static final String slaCategoriesTag = "slacategories";
    public static final String slaTypesTag = "slaType";
    public static final String inboundTag = "inbound";

    public static final String gateIn = "security/get/asnNumbers/gatein";
    public static final String gateInSave = "security/save";
    public static final String gateOut = "security/get/asnNumbers/gateout";

    public static final String vendorMaster = "vendor/vendormaster/get/all";
    public static final String orderDetails = "order/all/READY%20FOR%20DISPATCH";
    public static final String generateTc = "order/assign/vendor/Manual";
    public static final String generateTcVendor = "order/assign/vendor";

    public static final String vehicleNumbersTag = "unloading/get/vehicleNumbers/all";
    public static final String tcNumbersTag = "order/get/tcNumbers/all";

    public static final String inventoryTypesTag = "master/receipt/all";

    public static String getLoginUrl() {
        return BASE_URL + loginTag;
    }

    public static String getPutAwayUrl(String warehouseCode) {
        return BASE_URL + putAwayTag + slashTag + dataTag + slashTag + warehouseCode;
    }

    public static String getPutAwayCompletedUrl(String warehouseCode) {
        return BASE_URL + putAwayTag + slashTag + completeTag + slashTag + dataTag + slashTag + warehouseCode;
    }

    public static String getCreateGrnUrl(String warehouseCode) {
        return BASE_URL + generateTag + slashTag + grnTag + slashTag + warehouseCode;
    }

    public static String getUnloadingUrlTag(String vehicleNo, String warehouseCode) {
        return BASE_URL + unloadingUrlTag + vehicleNo + slashTag + warehouseCode;
    }

    public static String getUnloadingSubmitUrlTag(String warehouseCode) {
        return BASE_URL + unloadingSubmitUrlTag + slashTag + warehouseCode;
    }

    public static String getSubmitPutAwayUrl(String warehouseCode) {
        return BASE_URL + putAwayTag + slashTag + submitTag + slashTag + warehouseCode;
    }

    public static String getForwardOrderDetails() {
        return BASE_URL + "order/all/READY%20FOR%20DISPATCH";
    }

    public static String getTransportOrderDetails() {
        return BASE_URL + "transport/order/all/CREATED";
    }

    public static String getRmaOrderDetails() {
        return BASE_URL + "order/rma/all/CREATED";
    }

    public static String getTokenUrl() {
        return BASE_URL + tokenTag;
    }


    public static String getPickListSubprojectsUrl() {
        return BASE_URL + subprojectsTag;
    }

    public static String getPickListCustomersUrl() {
        return BASE_URL + "customer/contract/assigned/data"; //customerTag + slashTag + allTag;
    }

    public static String getPickListUrl(String type, String data, String warehouseCode) {
        return BASE_URL + picklistTag + slashTag + dataTag + slashTag + warehouseCode + questionMarkTag + typeTag + equalsTag + type
                + ampersandTag + dataTag + equalsTag + data;
    }

    public static String getSubmitPickListUrl(String warehouseCode) {
        return BASE_URL + picklistTag + slashTag + submitTag + slashTag + warehouseCode;
    }

    public static String getBinMastersUrl(String warehouseCode) {
        return BASE_URL + "warehouse/whbin/get/" + warehouseCode;
    }

    public static String getSkipBinReasonsUrl(String warehouseCode) {
        return BASE_URL + skipBinTag + slashTag + reasonTag + slashTag + warehouseCode;
    }

    public static String getSkipBinUrl() {
        return BASE_URL + pickingTag + slashTag + skipBin;
    }

    public static String getInventoryDataUrl(String warehouseCode) {
        return BASE_URL + inventoryTag + slashTag + dataTag + slashTag + warehouseCode;
    }

    public static String getVendorMasterTag() {
        return BASE_URL + vendorMasterTag;
    }

    public static String getRouteMasterUrlTag(String vendor) {
        return BASE_URL + routeMasterUrlTag + vendor;
    }

    public static String getVehicleMasterUrlTag(String route) {
        return BASE_URL + vehicleMasterUrlTag + route;
    }

    public static String getLoadingDataUrlTag(String vendorCode, String vehno) {
        return BASE_URL + loadingDataUrlTag + vendorCode + "&vehicleNo=" + vehno;
    }

    public static String getLoadingSubmitDataUrlTag(String vendorId, String vehNo) {
        return BASE_URL + loadingSubmitDataUrlTag + vendorId + "&vehicleNo=" + vehNo;
    }

    public static String getExceptionTypesUrl(String warehouseCode) {
        return BASE_URL + inventoryTag + slashTag + exceptionTag + slashTag + reasonTag + slashTag + warehouseCode;
    }

    public static String getSubmitExceptionUrl() {
        return BASE_URL + inventoryTag + slashTag + exceptionTag;
    }

    public static String getImageSubmitUrlTag(String warehouseCode) {
        return BASE_URL + imageSubmitUrlTag + slashTag + warehouseCode;
    }

    public static String getSearchBinOrPartUrl(String data, String type, String warehouseCode) {
        return BASE_URL + searchTag + slashTag + bin + slashTag + "part" + slashTag + warehouseCode + questionMarkTag + typeTag + equalsTag + type
                + ampersandTag + dataTag + equalsTag + data;
    }

    public static String getPartMastersUrl(String warehouseCode) {
        return BASE_URL + partTag + slashTag + allTag + slashTag + warehouseCode;
    }

    public static String getProductMastersUrl() {
        return BASE_URL + "installbase/get/products/data";
    }

    public static String getBinTransferUrl() {
        return BASE_URL + binTag + slashTag + transferTag;
    }

    public static String getSubmitInventoryUrl() {
        return BASE_URL + inventoryTag + slashTag + submitTag;
    }

    public static String getSlaUrl() {
        return BASE_URL + masterTag + slashTag + "slaName" + slashTag + allTag;

    }

    public static String getInboundTypesUrl() {
        return BASE_URL + "inbound/type/all";
    }

    public static String getInspectionUrl(String warehouseCode) {
        return BASE_URL + "inspection/get/all" + slashTag + warehouseCode;
    }

    public static String getSubmitInspectionUrl(String warehouseCode) {
        return BASE_URL + "inspection/submit" + slashTag + warehouseCode;
    }

    public static String getUnpackingDataUrl(String vehicleNum, String inwardNum) {
        return BASE_URL + "unpacking/data?vehicleNo=" + vehicleNum + "&inwardNo=" + inwardNum;
    }

    public static String getSubmitUnpackingUrl() {
        return BASE_URL + "unpacking/submit";
    }

    public static String getInvoicesUrl(int subprojectId) {
        return BASE_URL + "packing/orderlist?subproject=" + subprojectId;
    }

    public static String getPackingDataUrl(int subprojectId, String orderNum) {
        return BASE_URL + "packing/data?subproject=" + subprojectId + "&orderNo=" + orderNum;
    }

    public static String getPackingSubmitUrl() {
        return BASE_URL + "packing/submit";
    }

    public static String getInwardNumsUrl(String vehicleNum, int flag) {
        return BASE_URL + "vehicle/inwardNo?vehicleNo=" + vehicleNum + "&flag=" + flag;
    }

    public static String getDocumentHandoverUrl(String tcNumber) {
        return BASE_URL + "order/doumentHandover/tc/invoice?tcNo=" + tcNumber;
    }

    public static String getSubmitHandoverUrl() {
        return BASE_URL + "order/documentHandover/submit";
    }

    public static String getGateIn(String warehouseCode) {
        return BASE_URL + gateIn + slashTag + warehouseCode;
    }

    public static String getGateInSave(String warehouseCode) {
        return BASE_URL + gateInSave + slashTag + warehouseCode;
    }

    public static String getGateOut(String warehouseCode) {
        return BASE_URL + gateOut + slashTag + warehouseCode;
    }

    public static String getVendorMaster() {
        return BASE_URL + "vendor/orders/get/vendors";
    }

    public static String getOrderDetails() {
        return BASE_URL + orderDetails;
    }

    public static String getForwardGenerateTc() {
        return BASE_URL + generateTc;
    }

    public static String getTransportGenerateTc() {
        return BASE_URL + "transport/order/assign/vendor/Manual";
    }

    public static String getRmaGenerateTc() {
        return BASE_URL + "order/rma/assign/vendor/Manual";
    }

    public static String getForwardGenerateTcVendor() {
        return BASE_URL + generateTcVendor;
    }

    public static String getTransportGenerateTcVendor() {
        return BASE_URL + "transport/order/assign/vendor";
    }

    public static String getRmaGenerateTcVendor() {
        return BASE_URL + "order/rma/assign/vendor";
    }

    public static String getInventoryTypesUrl() {
        return BASE_URL + inventoryTypesTag;
    }

    public static String getVehicleNumbersUrl(String warehouseCode) {
        return BASE_URL + vehicleNumbersTag + slashTag + warehouseCode;
    }

    public static String getTcNumbersUrl(String warehouseCode) {
        return BASE_URL + tcNumbersTag + slashTag + warehouseCode;
    }

    public static String getWarehousesUrl(String username) {
        return BASE_URL + "wms/get/" + username;
    }

    public static String getEditVehicleUrl(int id, String vehicleNo) {
        return BASE_URL + "/inbound/change/vehicleNo/" + id + "/" + vehicleNo;
    }

}
