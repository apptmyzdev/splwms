package com.apptmyz.splwms.util;

public class Constants {
    public static final String LOG_TAG = "WMS";
    public static final boolean isUsingFileEncryption = false;
    public static final String SESSION_ID_PREF = "sessionId";

    public static final String TOKEN = "token";
    public static final String USERNAME_PREF = "username";
    public static final String USER_ID_PREF = "userid";
    public static final String PASSWORD_PREF = "password";
    public static final String LOGOUT_PREF = "logout";
    public static final String ROLE_ID = "roleId";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String IMEI = "imei";
    public static final String DATA_TYPE = "DATA_TYPE";

    public static final String DATABASE = "Database";
    public static final String LOGOUT = "Logout";
    public static final String SYNC_MASTERS = "Sync Masters";
    public static final String CHANGE_WAREHOUSE = "Change Warehouse";
    public static final String SEARCH_BIN_PART = "Search BIN/Part";
    public static final String BIN_TO_BIN_TRANSFER = "BIN to BIN Transfer";
    public static final String GATE_OUT_STR = "Gate Out";
    public static final String GATE_IN_STR = "Gate In";
    public static final String PERPETUAL_INVENTORY = "Perpetual Inventory";
    public static final String NORMAL_INVENTORY = "Normal Inventory";
    public static final String DOCUMENT_HANDOVER = "Document Handover";
    public static final String PICKLIST = "Pick List";
    public static final String PUTAWAY = "Put Away";
    public static final String INBOUND_INSPECTION = "Inbound Inspection";
    public static final String GENERATE_TC = "Generate TC";
    public static final String UNLOADING = "Unloading";

    public static final String WAREHOUSE_NAME = "WAREHOUSE_NAME";
    public static final String WAREHOUSE_ID = "WAREHOUSE_ID";
    public static final String WAREHOUSE = "WAREHOUSE";
    public static final String WAREHOUSE_CODE = "WAREHOUSE_CODE";
    public static final String WAREHOUSES_JSON = "WAREHOUSES_JSON";

    public static final String TOKEN_EXPIRED = "Token Expired. Login Again";
    public static final String PROB_WITH_SERVER = "Problem with Server";
    public static final String SERVER_NOT_REACHABLE = "Server is not Reachable";
    public static final String ERROR_PARSING = "Error Parsing the Data";
    public static final String DATA_INVALID = "Data is Invalid";
    public static final String DEVICE_CONNECTIVITY = "Device Connectivity Issue";

    public static final String CHECK_MASTERS_PREF = "CHECK_MASTERS_PREF";
    public static final boolean isUseingFileEncryption = false;
    public static final boolean isAlwaysExpandable = false;

    public static final int scanStringLength = 9;
    public static final int scanTime = 1000; //890; //1000;
    public static final int scanStartTime = 50;

    public static final String DEVICE_TOKEN = "deviceToken";
    public static final int maxProgressRange = 100;

    public static final String SCCODE_PREF = "SCCODE";
    public static final String EMP_ID_PREF = "EMP_ID";
    public static final String firstPriority = "R";
    public static final String secondPriority = "Y";
    public static final String thirdPriority = "G";
    public static final String loadTag = "LT";
    public static final String unloadTag = "UT";
    public static final String qualityCheckTag = "QC";
    public static final String docketIndexTag = "DocketIndex";
    public static final String packetIndexTag = "PacketIndex";
    public static final int maxImageSize = 5;
    public static final int truckMaxImageSize = 3;
    public static final String CHECK_TOKEN_PREF = "CHECK_TOKEN_PREF";
    public static final String BLANK_SPACE = " ";
    public static final String Scanned = "Scanned";
    public static final String SLASH = "/";
    public static final String HIFEN = "-";
    public static final int toBeScannedTag = 1;
    public static final int scanInProgressTag = 2;
    public static final int scanCompletedTag = 3;
    public static final int exceptionTag = 4;
    public static final int invoicesTag = 5;
    public static final String GREEN = "#008000";
    public static final String RED = "#FF0000";
    public static final String BLUE = "#0000FF";
    public static final String CLASS_NAME = "CLASS_NAME";
    public static final String cacheDateFormat = "yyyy-MM-dd";
    public static final int SCAN_STATUS_MATCH = 1;
    public static final int SCAN_STATUS_COLLISION = 2;
    public static final int SCAN_STATUS_SCANNED_EXTRA = 3;
    public static final int SCAN_STATUS_MB_NOT_SCANNED = 4;
    public static final int SCAN_STATUS_SHORT_REASON = 5;
    public static final String FILTERED_PREF = "FILTERED_PREF";
    public static final String INVALID_DKT_NUMBER = "000000000";
    public static final String LT_UT_FLAG = "LT_UT_FLAG";
    public static final String SERVER_TIME = "SERVER_TIME";
    public static final String TIME_DIFF = "TIME_DIFF";
    public static final String OP = "OP";
    public static final String LT = "LT";
    public static final String UT = "UT";
    public static final String PT = "PT";
    public static final String DT = "DT";
    public static final String WEIGHT_FORMAT_DISPLAY = "#,##0.00";
    public static final String WEIGHT_FORMAT = "###0.00";
    public static final String NOT_FITTING_IN_VEHICLE = "PPV";
    public static final String PIECES_RECEIVED_SHORT = "PPS";
    public static final String STACKING_IMAGE_VALIDATION = "STACKING_IMAGE_VALIDATION";
    public static final int REMARKS_LENGTH = 32;
    public static final String VEHICLE = "VEHICLE";
    public static final String VEHICLE_NUMBER = "VEHICLE_NUMBER";
    public static final String VENDOR_ID = "VENDOR_ID";
    //image processing
    public static final String PIXEL_DIFF_THRESHOLD = "30";
    public static final Double FALSE_PIXEL_UPPER_LIMIT = 90.0;
    public static final String LUX_SENSOR_THRESHOLD = "30";
    public static final String UROVO = "QUALCOMM";
    public static final String CIPHER = "CIPHER";
    public static final String CHECK_UPGRADE_DATE_PREF = "UPGRADE_DATE";
    public static final String NO_DATA_AVAILABLE = "NO_DATA_AVAILABLE";
    public static final int UNLOADING_STARTED = 1;
    public static final int UNLOADING_COMPLETED = 10;
    public static final int LOADING_STARTED = 11;
    public static final int LOADING_COMPLETED = 12;
    public static final String PERMISSIONS = "PERMISSIONS";
    public static String EXTRA_CODE = "PER";
    public static String SHORT_CODE = "PSR";
    public static String PKT_MISMATCH = "PPM";
    public static String dateFormat = "dd-MMM-yyyy HH:mm:ss";
    public static String dateFormat2 = "yyyy-MM-dd HH:mm:ss";
    public static String dateFormat3 = "dd-MM-yyyy HH:mm:ss";
    public static String SCANNING_CHAR_PATTERN = "^[0-9]+";
    public static String SERVER_UNAVAILABLE;
    public static String PROB_WITH_NETWORK;
    public static String TRUCK = "TRUCK";
    public static String DAMAGE_CODE = "12";
    public static String SHORT = "11";
    public static String EXTRA = "10";

    public static final String isPrinterAvailble = "IS_PRINTER_AVAILABLE";
    public static final int connAttemptsTym = 1000;
    public static final int maxConnAttemptsTym = 2000; //16000

    public static final int PRINT_SLEEP_TIME = 800;

    public static final String DEVICE = "DEVICE";
    public static final int REQUEST_ENABLE_BT = 31;
    public static final int REQUEST_PAIRED_DEVICE = 32;
    public static final int REQUEST_DISCOVERABLE_BT = 33;

    public static final String SIGNATURE = "SIGNATURE";
    public static final int SIGNATURE_CODE = 2;

    public static final Integer GATE_IN = 1;
    public static final Integer GATE_OUT = 0;
}
