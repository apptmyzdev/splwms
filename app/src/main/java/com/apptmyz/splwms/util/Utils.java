package com.apptmyz.splwms.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.apptmyz.splwms.BuildConfig;
import com.apptmyz.splwms.R;
import com.apptmyz.splwms.data.InspectionPart;
import com.apptmyz.splwms.data.LoadUnload;
import com.apptmyz.splwms.data.NewProductModel;
import com.apptmyz.splwms.data.ProductModel;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.fileutils.ImageLoader;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

public class Utils {
    private static final Gson gson = new Gson();
    public static ArrayList<Integer> audioList = new ArrayList<Integer>();
    private static MediaPlayer mPlayer = null;
    private static int lastPlayAudio = 0;
    private static ProgressDialog dialog;

    public static void enableView(View view) {
        view.setAlpha(1.0f);
        view.setClickable(true);
        view.setEnabled(true);
    }

    private void disableView(View view) {
        view.setAlpha(0.5f);
        view.setClickable(false);
        view.setEnabled(false);
    }

    public static String getNumber(NewProductModel model) {
        String number = "";
        String dataType = model.getDataType();
        if (dataType.equals("P")) {
            number = model.getPartNo();
        } else if (dataType.equals("DA")) {
            number = model.getDaNo();
        } else if (dataType.equals("IN")) {
            number = model.getInvoiceNo();
        } else if (dataType.equals("MB")) {
            number = model.getMasterBoxNo();
        }

        return number;
    }

    public static String getNumber(InspectionPart model) {
        String number = "";
        String dataType = model.getDataType();
        if (dataType.equals("P")) {
            number = model.getPartNo();
        } else if (dataType.equals("DA")) {
            number = model.getDaNo();
        } else if (dataType.equals("IN")) {
            number = model.getInvoiceNo();
        } else if (dataType.equals("MB")) {
            number = model.getMasterBoxNo();
        }
        return number;
    }

    public static String getNumber(ProductModel model, String dataType) {
        String number = "";
        if (dataType.equals("P")) {
            number = model.getPartNo();
        } else if (dataType.equals("DA")) {
            number = model.getDaNo();
        } else if (dataType.equals("IN")) {
            number = model.getInvoiceNo();
        } else if (dataType.equals("MB")) {
            number = model.getMasterBoxNo();
        }
        return number;
    }

    public static void logE(String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(Constants.LOG_TAG, msg);
        }
    }

    public static void logI(String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(Constants.LOG_TAG, msg);
        }
    }

    public static void logD(String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(Constants.LOG_TAG, msg);
        }
    }

    public static boolean isValidString(String str) {
        if (str != null) {
            str = str.trim();
            if (str.length() > 0)
                return true;
        }
        return false;
    }

    public static boolean isValidArrayList(ArrayList<?> list) {
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static boolean isValidVehicleNum(String vehicleNum) {
        if (Utils.isValidString(vehicleNum)) {
            String vehicleNumPattern1 = "^[A-Z]{2}[0-9]{6}";
            String vehicleNumPattern2 = "^[A-Z]{2}[0-9]{2}[A-Z]{1}[0-9]{4}";
            String vehicleNumPattern3 = "^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}";
            String vehicleNumPattern4 = "^[A-Z]{3}[0-9]{4}";
            String vehicleNumPattern5 = "^[A-Z]{2}[0-9]{3}[A-Z]{1}[0-9]{4}";
            String vehicleNumPattern6 = "^[A-Z]{2}[0-9]{2}[A-Z]{3}[0-9]{4}"; //AB12ABC1234
            String vehicleNumPattern = vehicleNumPattern1 + "|" + vehicleNumPattern2 + "|" + vehicleNumPattern3 + "|" +
                    vehicleNumPattern4 + "|" + vehicleNumPattern5 + "|" + vehicleNumPattern6;

            Pattern vehiclePattern = Pattern.compile(vehicleNumPattern);
            Matcher vehicleMatcher = vehiclePattern.matcher(vehicleNum);
            return vehicleMatcher.matches();
        } else
            return false;
    }

    public static ImageLoader getImageLoader(Context context, String text) {
        String name = text;
        return new ImageLoader(context, name);
    }

    public static String stringToMd5(String base) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(base.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
// Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static boolean isValidVehicleNo(String vehicleNo) {
        String vehicleNumPattern1 = "^[A-Z]{2}[0-9]{6}";
        String vehicleNumPattern2 = "^[A-Z]{2}[0-9]{2}[A-Z]{1}[0-9]{4}";
        String vehicleNumPattern3 = "^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}";
        String vehicleNumPattern4 = "^[A-Z]{3}[0-9]{4}";
        String vehicleNumPattern5 = "^[A-Z]{2}[0-9]{3}[A-Z]{1}[0-9]{4}";
        String vehicleNumPattern6 = "^[A-Z]{2}[0-9]{2}[A-Z]{3}[0-9]{4}"; //AB12ABC1234
        String vehicleNumPattern = vehicleNumPattern1 + "|" + vehicleNumPattern2 + "|" + vehicleNumPattern3 + "|" +
                vehicleNumPattern4 + "|" + vehicleNumPattern5 + "|" + vehicleNumPattern6;

        Pattern vehiclePattern = Pattern.compile(vehicleNumPattern);
        Matcher vehicleMatcher = vehiclePattern.matcher(vehicleNo);
        return vehicleMatcher.matches();
    }

    public static void dissmissKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) v.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static ProgressDialog getProgressDialog(Context context) {
        try {
            if (dialog != null && dialog.isShowing())
                dismissProgressDialog();

            dialog = new ProgressDialog(context, R.style.StyledDialog);

            SpannableString msg = new SpannableString("Loading. Please wait...");
            msg.setSpan(new ForegroundColorSpan(Color.BLACK), 0, msg.length(), 0);

            dialog.setMessage(msg);
            dialog.setIndeterminateDrawable(context.getResources().getDrawable(
                    R.drawable.progress_animation));
            dialog.setCancelable(false);
            dialog.show();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return dialog;
    }

    public static Object parse(InputStream is, Class<?> classOfT, boolean isGzip)
            throws Exception {
        try {
            if (isGzip) {
                InputStream ist = new BufferedInputStream(new GZIPInputStream(
                        is));

                logD("Gzipped");
                String s = IOUtils.toString(ist, "UTF-8");

                s = s.replaceAll("[\\x00-\\x1F\\x80-\\xFF]", "");
                is.close();
                return gson.fromJson(s, classOfT);
            } else {
                Reader readr = new InputStreamReader(is);
                return gson.fromJson(readr, classOfT);
            }
        } catch(JsonSyntaxException jse) {
            jse.printStackTrace();
            Utils.logE(jse.toString());
            Globals.lastErrMsg = Constants.ERROR_PARSING;
            throw new WmsException(Constants.ERROR_PARSING,
                    Constants.DATA_INVALID);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.logE(e.toString());
            Globals.lastErrMsg = Constants.ERROR_PARSING;
            throw new WmsException(Constants.ERROR_PARSING,
                    Constants.DATA_INVALID);
        }
    }

    public static String getStringFromList(List<NameValuePair> data) {
        String value = "";
        boolean first = true;
        for (NameValuePair item : data) {
            if (!first) {
                value += "&";

            }
            first = false;
            value += item.getName() + "=" + item.getValue();
        }
        return value;

    }

    private static void audioManager(Context context) {
        AudioManager audioManager = (AudioManager) ((Activity) context)
                .getSystemService(Context.AUDIO_SERVICE);

        int vol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (!audioManager.isSpeakerphoneOn())
            audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        if (vol != 100) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 100,
                    AudioManager.FLAG_ALLOW_RINGER_MODES);
        }
    }

    public static void releasePlayer() {
        try {
            if (mPlayer != null) {
                mPlayer.stop();
                mPlayer.release();
                mPlayer = null;
            }
        } catch (Exception e) {
            Utils.logD("Audio Release exception : " + e.toString());
        }
    }

    private static void playAudio(final Context context, int audioId) {
        try {
            lastPlayAudio = audioId;
            mPlayer = MediaPlayer.create(context, audioId);
            mPlayer.setVolume(100, 100);
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    releasePlayer();

                    int audio = nextAudio();

                    if (audio != -1 && lastPlayAudio != audio)
                        playAudio(context, audio);
                    else {
                        audioList.clear();
                    }
                }
            });
            mPlayer.start();

        } catch (Exception e) {
            Utils.logD("Audio exception : " + e.toString());
        }
    }

    public static void playAudioList(final Context context, int audioId) {
        if (audioList == null)
            audioList = new ArrayList<Integer>();
        if (audioList.contains(audioId))
            audioList = new ArrayList<Integer>();
        audioList.add(audioId);
        if (mPlayer == null) {
            audioManager(context);
            playAudio(context, audioId);

        }
    }

    public static int nextAudio() {
        int audio = -1;
        int index = 0;
        for (Integer audioId : audioList) {
            if (lastPlayAudio == audioId) {
                audio = index;
                audio = audio + 1;
                break;
            }
            index++;
        }

        try {
            audio = audioList.get(audio);
            if (lastPlayAudio == audio)
                audio = nextAudio();
        } catch (Exception e) {
            audio = -1;
        }

        return audio;
    }

    public static String getlabel(LoadUnload sheet) {
        String s = sheet.getSheetNo();
        if (!Utils.isValidString(s))
            s = " ";

        String s1 = sheet.getCarrier();
        if (!Utils.isValidString(s1))
            s1 = "";

        String toBranchCode = sheet.getToBranchCode();
        if (!Utils.isValidString(toBranchCode))
            toBranchCode = "";
        s = s + Constants.BLANK_SPACE + Constants.HIFEN + Constants.BLANK_SPACE
                + s1 + Constants.BLANK_SPACE + Constants.HIFEN + Constants.BLANK_SPACE + toBranchCode;
        return s;
    }

    public static String getScanTime() {
        String scanTime = "";
        Format formatter = new SimpleDateFormat(Constants.dateFormat2);
        scanTime = formatter.format(new Date());
        return scanTime;
    }

    public static String getScanTimeLoading() {
        String scanTime = "";
        Format formatter = new SimpleDateFormat(Constants.dateFormat3);
        scanTime = formatter.format(new Date());
        return scanTime;
    }

    public static String getScanTime(long diff) {
        String scanTime = "";
        Date d = new Date();
        long t = d.getTime();
        long t1 = t + diff;
        Date date = new Date(t1);
        Format formatter = new SimpleDateFormat(Constants.dateFormat);
        scanTime = formatter.format(date);
        logD("ScanTime original :" + d.toString());
        logD("ScanTime diff :" + diff);
        logD("ScanTime :" + scanTime);
        return scanTime;
    }

    public static String getImageTime(long diff) {
        String scanTime = "";
        Date d = new Date();
        long t = d.getTime();
        long t1 = t + diff;
        Date date = new Date(t1);
        scanTime = date.getTime() + "";
        return scanTime;
    }

    public static String getImageTime() {
        String scanTime = "";
        Date d = new Date();
        long t = d.getTime();
        long t1 = t;
        Date date = new Date(t1);
        scanTime = date.getTime() + "";
        return scanTime;
    }


    public static String getITime(long time) {
        String scanTime = "";
        Date d = new Date(time);
        Format formatter = new SimpleDateFormat(Constants.dateFormat);
        scanTime = formatter.format(d);
        logD("ITime :" + scanTime);
        return scanTime;
    }

    public static String getTime(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }

    public static void updateTimeStamp(Context context, String name) {
        SimpleDateFormat ISO8601UTC = new SimpleDateFormat(
                Constants.cacheDateFormat);
        String now = ISO8601UTC.format(new Date());

        DataSource dataSource = new DataSource(context);
        dataSource.sharedPreferences.set(name, now);
    }

    public static boolean checkIfCalledToday(Context context, String name) {
        DataSource dataSource = new DataSource(context);
        String date = dataSource.sharedPreferences.getValue(name);
        SimpleDateFormat ISO8601UTC = new SimpleDateFormat(
                Constants.cacheDateFormat);
        String now = ISO8601UTC.format(new Date());
        if (date.equals(now))
            return true;
        else
            return false;
    }

    public static Date getDate(String time) {
        Date date = null;
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.dateFormat);
        try {
            date = (Date) formatter.parseObject(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean isExtra(String excepType) {
        if (Utils.isValidString(excepType)) {
            if (excepType.equalsIgnoreCase(Constants.EXTRA_CODE))
                return true;
        } else {
            return false;
        }

        return false;
    }

    public static boolean isShort(String excepType) {
        if (Utils.isValidString(excepType)) {
            if (excepType.equalsIgnoreCase(Constants.SHORT_CODE))
                return true;
        } else {
            return false;
        }

        return false;
    }

    public static boolean isInValid(String dkt) {
        if (Utils.isValidString(dkt)) {
            if (dkt.equalsIgnoreCase(Constants.INVALID_DKT_NUMBER))
                return true;
        } else {
            return false;
        }

        return false;
    }

    public static void showSimpleAlert(Context context, String title, String msg) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(title);
        alert.setMessage(msg);
        alert.setPositiveButton(context.getString(R.string.ok_btn),
                new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alert.show();
    }

    public static String getHindiText(String s) {
        String hindistr = s.split(" ")[0];
        hindistr = hindistr.replace("\\", "");
        String[] arr = hindistr.split("u");
        String hinditext = "";
        for (int i = 1; i < arr.length; i++) {
            int hexVal = Integer.parseInt(arr[i], 16);
            hinditext += (char) hexVal;
        }
        return hinditext;
    }

    public static boolean getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null
                && activeNetwork.isConnected();
        return isConnected;
    }

    public static String getFormattedWtDisplay(double wt) {
        String p = "";
        DecimalFormat decimalFormat = new DecimalFormat(Constants.WEIGHT_FORMAT_DISPLAY);
        p = decimalFormat.format(wt);
        return p;
    }

    public static String getFormattedWt(double wt) {
        String p = "";
        DecimalFormat decimalFormat = new DecimalFormat(Constants.WEIGHT_FORMAT);
        p = decimalFormat.format(wt);
        return p;
    }

    public static String getMDHash(String value) {
        MessageDigest m;
        try {
            m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(value.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            // // Now we need to zero pad it if you actually want the full 32
            // // chars.
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }

            return hashtext.toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;

    }


    public static ProgressDialog getUpdateProgressDialog(Context context) {
        dismissProgressDialog();
        dialog = new ProgressDialog(context, R.style.StyledDialog);

        try {
            SpannableString msg = new SpannableString("Downloading the new version...");
            msg.setSpan(new ForegroundColorSpan(Color.BLACK), 0, msg.length(), 0);
            dialog.setIndeterminateDrawable(context.getResources().getDrawable(
                    R.drawable.progress_animation));
            dialog.setMessage(msg);
            dialog.setCancelable(false);
            dialog.show();
        } catch (Exception e) {
            Utils.logE(e.toString());
        }
        return dialog;
    }

    public static void dismissProgressDialog() {
        try {
            if (dialog != null && dialog.isShowing() == true)
                dialog.dismiss();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }


    public static String getFormattedDate(Date date, String format) {
        String d = "";
        SimpleDateFormat sf = new SimpleDateFormat(format);
        d = sf.format(date);
        return d;
    }

    public static boolean isUpdatesCalledToday(Context context, String name) {
        DataSource dataSource = new DataSource(context);
        String date = dataSource.sharedPreferences
                .getValue(name);
        SimpleDateFormat ISO8601UTC = new SimpleDateFormat(
                Constants.cacheDateFormat);
        String now = ISO8601UTC.format(new Date());
        if (Utils.isValidString(date)) {
            if (date.equals(now))
                return true;
            else
                return false;
        } else
            return false;
    }

    public static void updateDate(Context context, String name) {
        DataSource dataSource = new DataSource(context);
        SimpleDateFormat ISO8601UTC = new SimpleDateFormat(
                Constants.cacheDateFormat);
        String now = ISO8601UTC.format(new Date());
        dataSource.sharedPreferences.set(
                name, now);
    }

    public static List<String> splitToNChar(String text, int size) {
        List<String> parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts;
    }

    public static boolean isTab(Context context) {
        return isTablet(context);
    }


    public static boolean isTablet(Context context) {
        if ((context.getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE) {

            return true;
        }
        return false;
    }

    public static boolean ishhd() {
        boolean isHhhd = false;
        if (Build.MANUFACTURER.equalsIgnoreCase(Constants.UROVO) || Build.MANUFACTURER.contains(Constants.CIPHER))
            isHhhd = true;
        else isHhhd = false;

        return isHhhd;

    }

    public static boolean isValidImage(Bitmap bmp, Context context, Boolean isStackingImage) {
        boolean validImage = false;

        DataSource dataSource = new DataSource(context);
        Boolean stackingImageValidation = Boolean.parseBoolean(dataSource.sharedPreferences
                .getValue(Constants.STACKING_IMAGE_VALIDATION));
        if (stackingImageValidation && isStackingImage) {


            boolean sameColor = true, firstTime = true;
            int i = 0, oldR = 0, oldG = 0, oldB = 0, trueCount = 0, falseCount = 0;


            for (int x = 0; x < bmp.getWidth(); x++) {
                for (int y = 0; y < bmp.getHeight(); y++) {
                    int color = bmp.getPixel(x, y);
                    int r = Color.red(color);
                    int g = Color.green(color);
                    int b = Color.blue(color);

//                Log.e("rgb", String.valueOf(Math.abs(r))+" " + String.valueOf(Math.abs(g)+" " + String.valueOf(Math.abs( b))));

                    if (firstTime) {
                        oldR = r;
                        oldG = g;
                        oldB = b;
                        firstTime = false;
                    } else {
//                    Log.e("diff", String.valueOf(Math.abs(oldR - r) + Math.abs(oldG - g) + Math.abs(oldB - b)));

                        if ((Math.abs(oldR - r) + Math.abs(oldG - g) + Math.abs(oldB - b)) > Integer.parseInt(Constants.PIXEL_DIFF_THRESHOLD)) {
                            sameColor = false;
                            falseCount++;

                        } else {
                            sameColor = true;
                            trueCount++;
                        }
//                        oldR = r;
//                        oldG = g;
//                        oldB = b;
                    }

                }


            }

            int allPixelCount = bmp.getWidth() * bmp.getHeight();

            double falsePixelPert = (falseCount * 100) / allPixelCount;
            double truePixelPert = (trueCount * 100) / allPixelCount;

            Utils.logE("all pixel count" + Integer.toString(allPixelCount));
            Utils.logE("false pixel count" + Integer.toString(falseCount));
            Utils.logE("true pixel count" + Integer.toString(trueCount));
            Utils.logE(" false pert " + Double.toString(falsePixelPert));
            Utils.logE(" true pert " + Double.toString(truePixelPert));

            if (truePixelPert <= Constants.FALSE_PIXEL_UPPER_LIMIT) {
                if (bmp.getHeight() >= 100 && bmp.getWidth() >= 100) {

                    validImage = true;
                } else {
                    validImage = false;
                }

            } else {
                validImage = false;
            }
        } else {
            return true;
        }

        Utils.logE("valid image:" + validImage);

        return validImage;
    }

    public static boolean isValidVehNo(String vehNo) {
        String vehicleNumPattern1 = "^[A-Z]{2}[0-9]{6}";
        String vehicleNumPattern2 = "^[A-Z]{2}[0-9]{2}[A-Z]{1}[0-9]{4}";
        String vehicleNumPattern3 = "^[A-Z]{2}[0-9]{2}[A-Z]{2}[0-9]{4}";
        String vehicleNumPattern4 = "^[A-Z]{3}[0-9]{4}";
        String vehicleNumPattern5 = "^[A-Z]{2}[0-9]{3}[A-Z]{1}[0-9]{4}";
        String vehicleNumPattern6 = "^[A-Z]{2}[0-9]{2}[A-Z]{3}[0-9]{4}"; //AB12ABC1234
        String vehicleNumPattern = vehicleNumPattern1 + "||" + vehicleNumPattern2 + "||" + vehicleNumPattern3 + "||" +
                vehicleNumPattern4 + "||" + vehicleNumPattern5 + "||" + vehicleNumPattern6;

        Pattern vehiclePattern = Pattern.compile(vehicleNumPattern);
        Matcher vehicleMatcher = vehiclePattern.matcher(vehNo);

        if (vehNo.length() != 0 && vehicleMatcher.matches()) {
            return true;
        } else return false;
    }

    public static boolean isValidBayNumber(String bayNo) {
        String bayNoPattern = "^[a-zA-Z0-9]+$";
        Pattern vehiclePattern = Pattern.compile(bayNoPattern);
        Matcher vehicleMatcher = vehiclePattern.matcher(bayNo);

        if ((bayNo.length() > 0 && bayNo.length() <= 3) && vehicleMatcher.matches()) {
            return true;
        } else return false;
    }

    public static String getTime(Context context) {
        DataSource dataSource = new DataSource(context);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");

        String time = "";
        try {
            Long diff = Long.valueOf(dataSource.sharedPreferences.getValue(
                    Constants.TIME_DIFF));
            if (diff != 0) {
                Utils.logD("diff" + diff);
                Date date = new Date();
                date.setTime(date.getTime() + diff);
                time = simpleDateFormat.format(date);
            } else {
                Date date = new Date();
                time = simpleDateFormat.format(date);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    public static boolean isValidHandler(String code) {

        String pattern = ".*[a-zA-Z]+.*";
        boolean atleastOneAlpha = code.matches(pattern);
        return atleastOneAlpha;
    }
}
