package com.apptmyz.splwms.util;

import android.content.Context;
import android.content.pm.PackageManager;

import com.apptmyz.splwms.datacache.DataSource;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.net.UnknownHostException;

public class HttpRequest {
    private static final String CONTENT_TYPE = "application/json";

    public static Object postData(String restAPIPath, String data, Class classOfT, Context context)
            throws WmsException {

        Utils.logD("URL : " + restAPIPath);
        Utils.logD("Data : " + data);

        try {
            DataSource dataSource = new DataSource(context);

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(restAPIPath);

            try {
                String versionVal = context.getPackageManager()
                        .getPackageInfo(context.getPackageName(), 0).versionCode + "";
                if (Utils.isValidString(versionVal))
                    httpPost.addHeader("apkVersion", versionVal);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            String token = dataSource.sharedPreferences.getValue(Constants.TOKEN);
            if (Utils.isValidString(token))
                httpPost.addHeader("token", token);

            String imei = dataSource.sharedPreferences.getValue(Constants.IMEI);
            if (Utils.isValidString(imei))
                httpPost.addHeader("imei", imei);

            Utils.logD(imei);
            Utils.logD(token);

            httpPost.setEntity(new ByteArrayEntity(data.getBytes("UTF8")));

            httpPost.addHeader("Content-type", CONTENT_TYPE);
            httpPost.addHeader("Content-Encoding", "gzip");

            HttpResponse response = httpclient.execute(httpPost);

            int statusCode = response.getStatusLine().getStatusCode();

            Utils.logD("StatusCode : " + statusCode);
            if (response == null && statusCode != 200) {
                if (statusCode == 401 || statusCode == 403) {
                    Globals.lastErrMsg = Constants.TOKEN_EXPIRED;
                } else if (statusCode > 400 && statusCode < 500) {
                    Globals.lastErrMsg = Constants.SERVER_NOT_REACHABLE;
                } else if (statusCode >= 500) {
                    Globals.lastErrMsg = Constants.PROB_WITH_SERVER;
                    throw new WmsException("", Constants.SERVER_NOT_REACHABLE);
                }
                return null;
            } else {
                Header contentEncoding = response
                        .getFirstHeader("Content-Encoding");
                boolean isGzip = false;
                if (contentEncoding != null
                        && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    isGzip = true;
                }
                return Utils.parse(response.getEntity().getContent(), classOfT, isGzip);//,classOfT, false);
            }

        } catch (Exception e) {
            Globals.lastErrMsg = "Server Unavailable while sending data";
            throw new WmsException(Globals.lastErrMsg, "");
        }

    }

    public static Object getInputStreamFromUrl(String url, Class classOfT,
                                               Context context) throws WmsException {
        Utils.logD("URL : " + url);
        try {
            DataSource dataSource = new DataSource(context);
            HttpGet httpGet = new HttpGet(url);
            HttpClient httpclient = new DefaultHttpClient();
            httpGet.addHeader("Content-type", CONTENT_TYPE);
            httpGet.addHeader("Accept-Encoding", "gzip");

            String token = dataSource.sharedPreferences.getValue(Constants.TOKEN);
            if (Utils.isValidString(token)) {
                Utils.logD("TOKEN : " + token);
                httpGet.addHeader("token", token);
            }

            String imei = dataSource.sharedPreferences.getValue(Constants.IMEI);
            if (Utils.isValidString(imei))
                httpGet.addHeader("imei", imei);

            try {
                String versionVal = context.getPackageManager()
                        .getPackageInfo(context.getPackageName(), 0).versionCode + "";
                if (Utils.isValidString(versionVal))
                    httpGet.addHeader("apkVersion", versionVal);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

//            String userId = "";
//            userId = dataSource.sharedPreferences
//                    .getValue(Constants.USER_ID_PREF);
//            if (Utils.isValidString(userId))
//                httpGet.addHeader("userId", userId);
            Utils.logD("Log 2");
            HttpResponse response = httpclient.execute(httpGet);

            Utils.logD("Log 3");
            int statusCode = response.getStatusLine().getStatusCode();

            Utils.logD("StatusCode : " + statusCode);
            if (response == null && statusCode != 200) {
                if (statusCode == 401 || statusCode == 403) {
                    Globals.lastErrMsg = Constants.TOKEN_EXPIRED;
                } else if (statusCode > 400 && statusCode < 500) {
                    Globals.lastErrMsg = Constants.SERVER_NOT_REACHABLE;
                } else if (statusCode >= 500) {
                    Globals.lastErrMsg = Constants.PROB_WITH_SERVER;
                    throw new WmsException("", Constants.SERVER_NOT_REACHABLE);
                }
                return null;

            } else {
                Header contentEncoding = response
                        .getFirstHeader("Content-Encoding");
                boolean isGzip = false;
                if (contentEncoding != null
                        && contentEncoding.getValue().equalsIgnoreCase("gzip")) {
                    isGzip = true;
                }
                return Utils.parse(response.getEntity().getContent(), classOfT,
                        isGzip);
            }

        } catch (UnknownHostException e) {
            Globals.lastErrMsg = Constants.SERVER_NOT_REACHABLE;
            throw new WmsException("", Constants.PROB_WITH_SERVER);
        } catch (Exception e) {
            Utils.logD(e.getMessage());
            Globals.lastErrMsg = Constants.DEVICE_CONNECTIVITY;
            throw new WmsException(Constants.DEVICE_CONNECTIVITY, "");
        }
    }


}
