package com.apptmyz.splwms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PickingTypeActivity extends BaseActivity {

    TextView titleTv;
    RecyclerView customersRv;
    LinearLayoutManager layoutManager;
    Context context;
    ArrayList<String> pickingTypes = new ArrayList<>();
    CustomerAdapter customerAdapter;
    DataSource dataSource;
    EditText searchView;
    ImageView refreshIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_picking_type, frameLayout);

        context = PickingTypeActivity.this;
        dataSource = new DataSource(context);
        titleTv = (TextView) findViewById(R.id.tv_top_title);
        titleTv.setText("Pick List");

        searchView = (EditText) findViewById(R.id.searchView);
        refreshIv = (ImageView) findViewById(R.id.iv_refresh);
        refreshIv.setOnClickListener(listener);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                setFilteredData(editable.toString());
            }
        });

        customersRv = (RecyclerView) findViewById(R.id.rv_subprojects);
        layoutManager = new LinearLayoutManager(context);
        customersRv.setLayoutManager(layoutManager);

        pickingTypes = new ArrayList<>();
        pickingTypes.add("Customer");
        pickingTypes.add("Part");
        pickingTypes.add("SLA");
        pickingTypes.add("Product");

        customerAdapter = new CustomerAdapter(pickingTypes);
        customersRv.setAdapter(customerAdapter);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                default:
                    break;
            }
        }
    };

    private void setFilteredData(String query) {
        ArrayList<String> tempList = new ArrayList<>();

        for (String model : pickingTypes) {
            if (model.toLowerCase().contains(query.toLowerCase())) {
                tempList.add(model);
            }
        }
        customerAdapter = new CustomerAdapter(tempList);
        customersRv.setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    private void setData() {
        customerAdapter = new CustomerAdapter(pickingTypes);
        customersRv.setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(PickingTypeActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.DataObjectHolder> {
        private ArrayList<String> list;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {
            TextView customerTv;
            RelativeLayout layout;

            public DataObjectHolder(View itemView) {
                super(itemView);

                layout = (RelativeLayout) itemView.findViewById(R.id.customer_layout);
                customerTv = (TextView) itemView.findViewById(R.id.tv_customer);
                layout.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (v.getTag() != null) {
                    int pos = (int) v.getTag();
                    intent = new Intent(PickingTypeActivity.this, PickListActivity.class);
                    intent.putExtra("type", list.get(pos));
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            }
        }

        public CustomerAdapter(ArrayList<String> myDataset) {
            list = myDataset;
        }

        @Override
        public CustomerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_customer_item, parent, false);
            return new CustomerAdapter.DataObjectHolder(view);
        }

        @Override
        public void onBindViewHolder(final CustomerAdapter.DataObjectHolder holder, final int position) {
            String model = list.get(position);

            if (model != null) {
                holder.customerTv.setText(model);
            }

            holder.customerTv.setTag(position);
            holder.layout.setTag(position);
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public void update(ArrayList<String> data) {
            list.clear();
            list.addAll(data);
            notifyDataSetChanged();
        }

        public void addItem(String dataObj, int index) {
            list.add(dataObj);
            notifyItemInserted(index);
        }

        public void deleteItem(int index) {
            list.remove(index);
            notifyItemRemoved(index);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(context, HomeActivity.class));
    }
}
