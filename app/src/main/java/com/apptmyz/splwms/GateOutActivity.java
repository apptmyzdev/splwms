package com.apptmyz.splwms;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.apptmyz.splwms.custom.CustomAlertDialog;
import com.apptmyz.splwms.custom.NonScrollList;
import com.apptmyz.splwms.data.GateInResponseModel;
import com.apptmyz.splwms.data.GateInSaveReqModel;
import com.apptmyz.splwms.data.GateInSaveResponse;
import com.apptmyz.splwms.data.GateOutResponse;
import com.apptmyz.splwms.datacache.DataSource;
import com.apptmyz.splwms.util.Constants;
import com.apptmyz.splwms.util.Globals;
import com.apptmyz.splwms.util.HttpRequest;
import com.apptmyz.splwms.util.Utils;
import com.apptmyz.splwms.util.WmsException;
import com.apptmyz.splwms.util.WmsUtils;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRadioButton;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GateOutActivity extends BaseActivity {
    Context context;
    LinearLayout orderLl, orderListLl;
    EditText tarnsporterNoEt, transporterRepNameEt, noOfBoxEt, remarksEt;
    Button addOrderBtn, saveBtn, saveInputBtn, cancelBtn;
    ListView orderLv;
    TextView orderTv;
    String transporterNo, transporterRepName, noOfBoxes, remarks, orderNo;

    GateOutResponse gateOutResponse;
    List<String> gateOutResponseModelList;
    List<String> addedOrderList = new ArrayList<>();

    GateInSaveReqModel gateInSaveReqModel;
    GateInSaveResponse gateInSaveResponse;
    List<String> addInvoiceList = new ArrayList<>();
    private DataSource dataSource;
    private String warehouseCode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_gate_out, frameLayout);

        context = GateOutActivity.this;
        dataSource = new DataSource(context);
        warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
        titleTv.setText("Gate Out");
        orderListLl = (LinearLayout) findViewById(R.id.orderList_ll);
        orderLl = (LinearLayout) findViewById(R.id.orderNo_ll);
        orderLl.setOnClickListener(listener);
        tarnsporterNoEt = (EditText) findViewById(R.id.transporterNo_et);
        transporterRepNameEt = (EditText) findViewById(R.id.transporterRepName_et);
        noOfBoxEt = (EditText) findViewById(R.id.noOfBox_et);
        remarksEt = (EditText) findViewById(R.id.remarks_et);
        orderTv = (TextView) findViewById(R.id.orderNo_tv);
        addOrderBtn = (Button) findViewById(R.id.addOrder_btn);
        addOrderBtn.setOnClickListener(listener);
        saveBtn = (Button) findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(listener);
        orderLv = (ListView) findViewById(R.id.order_lv);

        new GateOutTask().execute();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.addOrder_btn:
                    addOrder();
                    break;
                case R.id.orderNo_ll:
                    if (Utils.isValidArrayList((ArrayList<?>) gateOutResponseModelList))
                        showOrderDialog();
                    else
                        Toast.makeText(context, "No Data Available", Toast.LENGTH_LONG).show();
                    break;
                case R.id.save_btn:
                    orderNo = orderTv.getText().toString();
                    if (Utils.isValidString(orderNo)) {
                        showInputDialog();
                    } else
                        Toast.makeText(context, "Please Select PO/ASN No.", Toast.LENGTH_LONG).show();

                    break;

            }
        }
    };

    private void addOrder() {
        if (Utils.isValidArrayList((ArrayList<?>) gateOutResponseModelList)) {
            String orderNo = orderTv.getText().toString();
            Log.d("list", gateOutResponseModelList.toString());
            for (String model : gateOutResponseModelList) {
                if (model.equalsIgnoreCase(orderNo)) {
                    orderListLl.setVisibility(View.VISIBLE);
                    addedOrderList.add(model);
                    if (!containsOrderNo(addedOrderList, orderNo)) {
                        OrderAdapter adapter = new OrderAdapter(context, R.layout.order_list_items, addedOrderList);
                        orderLv.setAdapter(adapter);

                    } else if (addedOrderList.size() == 1) {
                        OrderAdapter adapter = new OrderAdapter(context, R.layout.order_list_items, addedOrderList);
                        orderLv.setAdapter(adapter);
                    } else
                        Toast.makeText(context, "Order No. Already Exists, Please Choose Another Order No.", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(context, "Please Select Order No. To Add", Toast.LENGTH_LONG).show();

        }
    }

    public static boolean containsOrderNo(Collection<String> c, String orderNo) {
        for (String g : c) {
            if (g != null && g.equals(orderNo)) {
                return true;
            }
        }
        return false;
    }

    @SuppressLint("RestrictedApi")
    public void showOrderDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.gateout_type_radiolist);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        TextView ok = (TextView) dialog.findViewById(R.id.ok_tv);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel_tv);
        final RadioGroup aorderRg = (RadioGroup) dialog.findViewById(R.id.order_rg);
        final ColorStateList colorStateList = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_enabled}},
                new int[]{getResources().getColor(R.color.green)}
        );
        if (Utils.isValidArrayList((ArrayList<?>) gateOutResponseModelList)) {
            for (String model : gateOutResponseModelList) {
                AppCompatRadioButton orderRbtn = new AppCompatRadioButton(context);
                orderRbtn.setHighlightColor(context.getResources().getColor(R.color.green));
                orderRbtn.setId(View.generateViewId());
                Log.e("id", String.valueOf(View.generateViewId()));
                orderRbtn.setText(model);
                orderRbtn.setTextSize(14);
                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(context, null);
                params.setMargins(0, 15, 0, 0);
                orderRbtn.setLayoutParams(params);
                orderRbtn.setTextColor(getResources().getColor(R.color.green));
                orderRbtn.setSupportButtonTintList(colorStateList);
                aorderRg.addView(orderRbtn);
            }
        }
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer orderId = aorderRg.getCheckedRadioButtonId();
                int childCount = aorderRg.getChildCount();
                Log.e("child id", String.valueOf(childCount));
                for (int i = 0; i < childCount; i++) {
                    if (aorderRg.getChildAt(i) instanceof RadioButton) {
                        RadioButton r_btn = (RadioButton) aorderRg.getChildAt(i);
                        if (r_btn.getId() == orderId) {
                            Log.e("btn id", String.valueOf(r_btn.getId()));
                            Log.e("sla id", String.valueOf(orderId));
                            orderNo = r_btn.getText().toString();
                            orderTv.setText(orderNo);
                            addOrder();
                            break;
                        }
                    }
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }


    public class GateOutTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
            String url = WmsUtils.getGateOut(warehouseCode);
            if (Utils.isValidString(url)) {
                InputStream is = null;
                try {
                    gateOutResponse = (GateOutResponse) HttpRequest.postData(url, "", GateOutResponse.class, context);
                    if (gateOutResponse != null && gateOutResponse.isStatus()) {
                        gateOutResponseModelList = gateOutResponse.getData();
                        Log.e("response", String.valueOf(gateOutResponseModelList));
                    } else {
                        Globals.lastErrMsg = gateOutResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();

        }
    }

    public class GateOutSaveTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.getProgressDialog(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            warehouseCode = dataSource.sharedPreferences.getValue(Constants.WAREHOUSE_CODE);
            String url = WmsUtils.getGateInSave(warehouseCode);
            if (Utils.isValidString(url)) {
                try {
                    Gson gson = new Gson();
                    String data = gson.toJson(gateInSaveReqModel);
                    gateInSaveResponse = (GateInSaveResponse) HttpRequest.postData(url, data, GateInSaveResponse.class, context);
                    if (gateInSaveResponse != null && gateInSaveResponse.isStatus()) {

                    } else {
                        Globals.lastErrMsg = gateInSaveResponse.getMessage();
                    }
                } catch (WmsException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    if (!Utils.isValidString(Globals.lastErrMsg))
                        Globals.lastErrMsg = e.toString();
                    if (Globals.lastErrMsg.equalsIgnoreCase("null"))
                        Globals.lastErrMsg = getString(R.string.server_not_reachable);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Utils.dismissProgressDialog();
            if (showErrorDialog()) {
                if (showErrorDialog()) {
                    startActivity(new Intent(context, HomeActivity.class));
                    Toast.makeText(context, gateInSaveResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    CustomAlertDialog errDlg;

    private boolean showErrorDialog() {
        boolean isNotErr = true;
        if (Globals.lastErrMsg != null && Globals.lastErrMsg.length() > 0) {
            boolean isLogout = Globals.lastErrMsg.equals(Constants.TOKEN_EXPIRED);
            if (isLogout)
                dataSource.sharedPreferences.set(Constants.LOGOUT_PREF, Constants.TRUE);

            errDlg = new CustomAlertDialog(GateOutActivity.this, Globals.lastErrMsg,
                    false, isLogout);
            errDlg.setTitle(getString(R.string.alert_dialog_title));
            errDlg.setCancelable(false);
            Globals.lastErrMsg = "";
            isNotErr = false;
            if (!isFinishing()) {
                errDlg.show();
            }
        }
        return isNotErr;
    }

    public class OrderAdapter extends ArrayAdapter<String> {
        Context context;
        int resource;
        List<String> gateOutResponseModelList;

        public OrderAdapter(@NonNull Context context, int resource, List<String> gateOutResponseModelList) {
            super(context, 0, gateOutResponseModelList);
            this.context = context;
            this.resource = resource;
            this.gateOutResponseModelList = gateOutResponseModelList;
        }

        @Override
        public int getCount() {
            return gateOutResponseModelList.size();
        }

        @SuppressLint("ResourceAsColor")
        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(resource, parent, false);
            TextView orderTv = (TextView) convertView.findViewById(R.id.order_tv);
            ImageView deleteIv = (ImageView) convertView.findViewById(R.id.delete_iv);
            String model = gateOutResponseModelList.get(position);
            if (Utils.isValidString(model))
                orderTv.setText(model);

            deleteIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gateOutResponseModelList.remove(position);
                    notifyDataSetChanged();

                }
            });

            return convertView;

        }
    }

    private AlertDialog alert;

    private void showInputDialog() {
        final android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setCancelable(false);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_gate_in_input, null, false);
        view.setVisibility(View.VISIBLE);
        tarnsporterNoEt = (EditText) view.findViewById(R.id.transporterNo_et);
        transporterRepNameEt = (EditText) view.findViewById(R.id.transporterRepName_et);
        noOfBoxEt = (EditText) view.findViewById(R.id.noOfBox_et);
        remarksEt = (EditText) view.findViewById(R.id.remarks_et);

        saveInputBtn = (Button) view.findViewById(R.id.save_input_btn);
        cancelBtn = (Button) view.findViewById(R.id.btn_cancel);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });

        saveInputBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transporterNo = tarnsporterNoEt.getText().toString();
                transporterRepName = transporterRepNameEt.getText().toString();
                noOfBoxes = noOfBoxEt.getText().toString();
                remarks = remarksEt.getText().toString();
                if (Utils.isValidString(transporterNo)) {
                    if (Utils.isValidString(transporterRepName)) {
                        if (Utils.isValidString(noOfBoxes)) {
                            if (Utils.isValidArrayList((ArrayList<?>) addedOrderList)) {
                                for (String model : addedOrderList) {
                                    addInvoiceList.add(model);
                                }
                                Log.d("list", String.valueOf(addInvoiceList));
                                gateInSaveReqModel = new GateInSaveReqModel(Constants.GATE_OUT, transporterNo, transporterRepName, noOfBoxes, remarks, addInvoiceList);
                                Log.d("model", gateInSaveReqModel.toString());
                                new GateOutSaveTask().execute();
                            } else
                                Toast.makeText(context, "Please Add Order No.", Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(context, "Please Enter No. Of Boxes", Toast.LENGTH_LONG).show();
                    } else
                        Toast.makeText(context, "Please Enter Transporter Representative Name", Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(context, "Please Enter Transporter Phone Number", Toast.LENGTH_LONG).show();
            }
        });
        alertDialog.setView(view);
        alert = alertDialog.show();
    }
}
